#ifndef _RENDERER_TERRAIN_H
#define _RENDERER_TERRAIN_H

void renderer_terrain_init(void);
void renderer_terrain_update(void);
void renderer_terrain_draw(void);

#endif
