#include "game_loader.h"

#include <assert.h>
#include <stdio.h>

#include "entity.h"
#include "game.h"
#include "parson.h"

void game_loader_save(const char *filename) {
    JSON_Value *json_root_value;
    JSON_Object *json_root_object;
    char *serialized_string;

    json_root_value = json_value_init_object();
    json_root_object = json_value_get_object(json_root_value);

    json_object_set_number(json_root_object, "team_supply_0", game_state.team_supply[0]);
    json_object_set_number(json_root_object, "team_supply_1", game_state.team_supply[1]);
    json_object_set_number(json_root_object, "team_supply_max_0", game_state.team_supply_max[0]);
    json_object_set_number(json_root_object, "team_supply_max_1", game_state.team_supply_max[1]);
    json_object_set_number(json_root_object, "team_money_0", game_state.team_money[0]);
    json_object_set_number(json_root_object, "team_money_1", game_state.team_money[1]);

    {
        int key_index;
        int i;
        struct entity_id entity_id;
        array_entity_id_t entities;
        int num_types;
        enum entity_type types[] = {
            ENTITY_TYPE_BARRACKS,
            ENTITY_TYPE_FACTORY,
            ENTITY_TYPE_HELIPORT,
            ENTITY_TYPE_TURRET,
            ENTITY_TYPE_SUPPLY_DEPOT,
            ENTITY_TYPE_WORKER,
            ENTITY_TYPE_SOLDIER,
            ENTITY_TYPE_MEDIC,
            ENTITY_TYPE_TANK,
            ENTITY_TYPE_VEHICLE,
            ENTITY_TYPE_HELICOPTER,
        };

        num_types = sizeof(types) / sizeof(*types);

        array_init(&entities);
        get_all_active_entities_with_type(&entities, num_types, types);

        key_index = 0;
        array_foreach(&entities, entity_id, i) {
            char json_key_name[256];
            struct entity *entity;

            entity = get_entity_from_id(entity_id);
            if (!entity) {
                continue;
            }

            sprintf_s(json_key_name, 256, "entity_%d.type", key_index);
            json_object_dotset_string(json_root_object, json_key_name, entity_type_name[entity->type]);

            if (entity->type == ENTITY_TYPE_BARRACKS ||
                entity->type == ENTITY_TYPE_FACTORY ||
                entity->type == ENTITY_TYPE_HELIPORT ||
                entity->type == ENTITY_TYPE_TURRET ||
                entity->type == ENTITY_TYPE_SUPPLY_DEPOT) {
                int row, col;

                col = (int)(entity->position.x / GAME_TILE_WIDTH);
                row = (int)(entity->position.y / GAME_TILE_WIDTH);

                sprintf_s(json_key_name, 256, "entity_%d.col", key_index);
                json_object_dotset_number(json_root_object, json_key_name, col);

                sprintf_s(json_key_name, 256, "entity_%d.row", key_index);
                json_object_dotset_number(json_root_object, json_key_name, row);

                sprintf_s(json_key_name, 256, "entity_%d.team_num", key_index);
                json_object_dotset_number(json_root_object, json_key_name, entity->team_num);
            }

            if (entity->type == ENTITY_TYPE_WORKER ||
                entity->type == ENTITY_TYPE_SOLDIER ||
                entity->type == ENTITY_TYPE_MEDIC ||
                entity->type == ENTITY_TYPE_TANK ||
                entity->type == ENTITY_TYPE_VEHICLE ||
                entity->type == ENTITY_TYPE_HELICOPTER) {
                float row, col;

                col = entity->position.x / GAME_TILE_WIDTH;
                row = entity->position.y / GAME_TILE_WIDTH;

                sprintf_s(json_key_name, 256, "entity_%d.col", key_index);
                json_object_dotset_number(json_root_object, json_key_name, col);

                sprintf_s(json_key_name, 256, "entity_%d.row", key_index);
                json_object_dotset_number(json_root_object, json_key_name, row);

                sprintf_s(json_key_name, 256, "entity_%d.team_num", key_index);
                json_object_dotset_number(json_root_object, json_key_name, entity->team_num);
            }

            key_index++;
        }

        json_object_dotset_number(json_root_object, "num_entities", key_index);

        array_deinit(&entities);
    }

    serialized_string = json_serialize_to_string_pretty(json_root_value);

    {
        FILE *f;

        fopen_s(&f, filename, "wb");
        fputs(serialized_string, f);
        fclose(f);

        puts(serialized_string);
    }

    json_free_serialized_string(serialized_string);
    json_value_free(json_root_value);
}

void game_loader_load(const char *filename) {
    JSON_Value *json_root_value;
    JSON_Object *json_root_object;

    json_root_value = json_parse_file(filename);
    json_root_object = json_value_get_object(json_root_value);

    game_deinit();
    game_init();

    {
        int i;
        int num_entities;
        num_entities = (int)json_object_dotget_number(json_root_object, "num_entities");

        for (i = 0; i < num_entities; i++) {
            enum entity_type type;
            const char *type_name;
            char json_key_name[256];

            sprintf_s(json_key_name, 256, "entity_%d.type", i);
            type_name = json_object_dotget_string(json_root_object, json_key_name);

            if (strcmp(type_name, entity_type_name[ENTITY_TYPE_BARRACKS]) == 0) {
                type = ENTITY_TYPE_BARRACKS;
            } else if (strcmp(type_name, entity_type_name[ENTITY_TYPE_FACTORY]) == 0) {
                type = ENTITY_TYPE_FACTORY;
            } else if (strcmp(type_name, entity_type_name[ENTITY_TYPE_HELIPORT]) == 0) {
                type = ENTITY_TYPE_HELIPORT;
            } else if (strcmp(type_name, entity_type_name[ENTITY_TYPE_TURRET]) == 0) {
                type = ENTITY_TYPE_TURRET;
            } else if (strcmp(type_name, entity_type_name[ENTITY_TYPE_SUPPLY_DEPOT]) == 0) {
                type = ENTITY_TYPE_SUPPLY_DEPOT;
            } else if (strcmp(type_name, entity_type_name[ENTITY_TYPE_WORKER]) == 0) {
                type = ENTITY_TYPE_WORKER;
            } else if (strcmp(type_name, entity_type_name[ENTITY_TYPE_SOLDIER]) == 0) {
                type = ENTITY_TYPE_SOLDIER;
            } else if (strcmp(type_name, entity_type_name[ENTITY_TYPE_MEDIC]) == 0) {
                type = ENTITY_TYPE_MEDIC;
            } else if (strcmp(type_name, entity_type_name[ENTITY_TYPE_TANK]) == 0) {
                type = ENTITY_TYPE_TANK;
            } else if (strcmp(type_name, entity_type_name[ENTITY_TYPE_VEHICLE]) == 0) {
                type = ENTITY_TYPE_VEHICLE;
            } else if (strcmp(type_name, entity_type_name[ENTITY_TYPE_HELICOPTER]) == 0) {
                type = ENTITY_TYPE_HELICOPTER;
            } else {
                assert("Invalid entity type in save file" && false);
            }

            if (type == ENTITY_TYPE_BARRACKS ||
                type == ENTITY_TYPE_FACTORY ||
                type == ENTITY_TYPE_HELIPORT ||
                type == ENTITY_TYPE_TURRET ||
                type == ENTITY_TYPE_SUPPLY_DEPOT) {
                int row, col, team_num;

                sprintf_s(json_key_name, 256, "entity_%d.row", i);
                row = (int)json_object_dotget_number(json_root_object, json_key_name);

                sprintf_s(json_key_name, 256, "entity_%d.col", i);
                col = (int)json_object_dotget_number(json_root_object, json_key_name);

                sprintf_s(json_key_name, 256, "entity_%d.team_num", i);
                team_num = (int)json_object_dotget_number(json_root_object, json_key_name);

                switch (type) {
                    case ENTITY_TYPE_BARRACKS:
                        create_barracks_entity(row, col, team_num);
                        break;
                    case ENTITY_TYPE_FACTORY:
                        create_factory_entity(row, col, team_num);
                        break;
                    case ENTITY_TYPE_HELIPORT:
                        create_heliport_entity(row, col);
                        break;
                    case ENTITY_TYPE_TURRET:
                        create_turret_entity(row, col, team_num);
                        break;
                    case ENTITY_TYPE_SUPPLY_DEPOT:
                        create_supply_depot_entity(row, col);
                        break;
                    default:
                        assert(false);
                }
            }

            if (type == ENTITY_TYPE_WORKER ||
                type == ENTITY_TYPE_SOLDIER ||
                type == ENTITY_TYPE_MEDIC ||
                type == ENTITY_TYPE_TANK ||
                type == ENTITY_TYPE_VEHICLE ||
                type == ENTITY_TYPE_HELICOPTER) {
                float row, col;
                int team_num;

                sprintf_s(json_key_name, 256, "entity_%d.row", i);
                row = (float)json_object_dotget_number(json_root_object, json_key_name);

                sprintf_s(json_key_name, 256, "entity_%d.col", i);
                col = (float)json_object_dotget_number(json_root_object, json_key_name);

                sprintf_s(json_key_name, 256, "entity_%d.team_num", i);
                team_num = (int)json_object_dotget_number(json_root_object, json_key_name);

                switch (type) {
                    case ENTITY_TYPE_WORKER:
                        create_worker_entity(row, col, team_num);
                        break;
                    case ENTITY_TYPE_SOLDIER:
                        create_soldier_entity(row, col, team_num);
                        break;
                    case ENTITY_TYPE_MEDIC:
                        create_medic_entity(row, col, team_num);
                        break;
                    case ENTITY_TYPE_TANK:
                        create_tank_entity(row, col, team_num);
                        break;
                    case ENTITY_TYPE_VEHICLE:
                        create_vehicle_entity(row, col, team_num);
                        break;
                    case ENTITY_TYPE_HELICOPTER:
                        create_helicopter_entity(row, col, team_num);
                        break;
                    default:
                        assert(false);
                }
            }
        }
    }

    json_value_free(json_root_value);
}
