#include "collider.h"

#include <assert.h>

#include "physics.h"

static void init_collider(struct collider *collider) {
    array_init(&collider->collision_info_array);
}

static void deinit_collider(struct collider *collider) {
    struct entity_id *user_data = cpBodyGetUserData(collider->cp_body);
    free(user_data);

    cpSpaceRemoveShape(physics_state.cp_space, collider->cp_shape);
    cpSpaceRemoveBody(physics_state.cp_space, collider->cp_body);
    cpShapeFree(collider->cp_shape);
    cpBodyFree(collider->cp_body);
    array_deinit(&collider->collision_info_array);
}

static struct {
    array_collider_t collider_array;
    array_uint32_t gen_array;
    array_uint32_t free_array;
    array_bool_t active_array;
} _collider_manager;

void collider_manager_init(void) {
    array_init(&_collider_manager.collider_array);
    array_init(&_collider_manager.gen_array);
    array_init(&_collider_manager.free_array);
    array_init(&_collider_manager.active_array);

    // idx 0 represents the NULL collider
    struct collider blank;
    memset(&blank, 0, sizeof(blank));
    array_push(&_collider_manager.collider_array, blank);
    array_push(&_collider_manager.gen_array, 0);
    array_push(&_collider_manager.active_array, false);
}

struct collider_id allocate_collider(void) {
    struct collider_id id;

    if (_collider_manager.free_array.length == 0) {
        struct collider collider;
        memset(&collider, 0, sizeof(collider));
        array_push(&_collider_manager.collider_array, collider);
        array_push(&_collider_manager.gen_array, 0);
        array_push(&_collider_manager.active_array, true);
        id.gen = 0;
        id.idx = _collider_manager.collider_array.length - 1;
    } else {
        uint32_t idx = array_pop(&_collider_manager.free_array);
        id.gen = _collider_manager.gen_array.data[idx];
        id.idx = idx;
        _collider_manager.active_array.data[id.idx] = true;
    }

    // printf("allocate collider: idx %d, gen: %d\n", id.idx, id.gen);

    struct collider *collider = get_collider_from_id(id);
    init_collider(collider);

    return id;
}

void delete_collider(struct collider_id id) {
    uint32_t cur_gen = _collider_manager.gen_array.data[id.idx];
    if (id.idx == 0 || id.gen != cur_gen) {
        return;
    }

    // printf("delete collider: idx %d, gen: %d\n", id.idx, id.gen);

    struct collider *collider = get_collider_from_id(id);
    deinit_collider(collider);

    _collider_manager.gen_array.data[id.idx]++;
    _collider_manager.active_array.data[id.idx] = false;
    array_push(&_collider_manager.free_array, id.idx);
}

struct collider *get_collider_from_id(struct collider_id id) {
    uint32_t cur_gen = _collider_manager.gen_array.data[id.idx];
    if (id.idx == 0 || cur_gen != id.gen) {
        return NULL;
    }
    return &_collider_manager.collider_array.data[id.idx];
}

void get_all_active_colliders(array_collider_id_t *colliders) {
    int i;

    array_clear(colliders);
    for (i = 0; i < _collider_manager.collider_array.length; i++) {
        struct collider_id collider_id;

        if (!_collider_manager.active_array.data[i]) {
            continue;
        }

        collider_id.idx = i;
        collider_id.gen = _collider_manager.gen_array.data[i];
        array_push(colliders, collider_id);
    }
}

void set_collider_position(struct collider *collider, vec2 position) {
    cpBodySetPosition(collider->cp_body, cpv(position.x, position.y));
    collider->position = position;
}

void apply_force_to_collider(struct collider *collider, vec2 force) {
    cpBody *cp_body = collider->cp_body;
    cpVect cp_force = cpv(force.x, force.y);
    cpVect cp_pos = cpv(collider->position.x, collider->position.y);

    cpBodyApplyForceAtWorldPoint(cp_body, cp_force, cp_pos);
}

struct collider_id create_circle_collider(bool is_dynamic,
                                          bool does_collide,
                                          float radius,
                                          float mass,
                                          enum collider_category category) {
    struct collider_id id;
    struct collider *circle;
    struct collider_id *user_data = malloc(sizeof(struct collider_id));

    id = allocate_collider();
    *user_data = id;
    circle = get_collider_from_id(id);

    circle->type = COLLIDER_TYPE_CIRCLE;
    circle->does_collide = does_collide;
    circle->position = V2_ZERO;
    circle->velocity = V2_ZERO;
    circle->is_dynamic = is_dynamic;
    circle->mass = mass;
    circle->circle.radius = radius;

    if (is_dynamic) {
        float moment = (float)cpMomentForCircle(mass, 0.0f, radius, cpv(0.0f, 0.0f));
        circle->cp_body = cpBodyNew(mass, moment);
    } else {
        circle->cp_body = cpBodyNewKinematic();
    }
    cpSpaceAddBody(physics_state.cp_space, circle->cp_body);
    cpBodySetUserData(circle->cp_body, user_data);

    circle->cp_shape = cpCircleShapeNew(circle->cp_body, radius, cpv(0.0f, 0.0f));
    cpShapeSetElasticity(circle->cp_shape, 0.0f);
    cpSpaceAddShape(physics_state.cp_space, circle->cp_shape);

    switch (category) {
        case COLLIDER_CATEGORY_BUILDING:
        case COLLIDER_CATEGORY_GROUND_UNIT: {
            cpBitmask mask = COLLIDER_CATEGORY_BUILDING |
                             COLLIDER_CATEGORY_GROUND_UNIT;
            cpShapeSetFilter(circle->cp_shape,
                             cpShapeFilterNew(CP_NO_GROUP, category, mask));
            break;
        }
        case COLLIDER_CATEGORY_AIR_UNIT: {
            cpBitmask mask = COLLIDER_CATEGORY_AIR_UNIT;
            cpShapeSetFilter(circle->cp_shape,
                             cpShapeFilterNew(CP_NO_GROUP, category, mask));
            break;
        }
    }

    return id;
}

struct collider_id create_box_collider(bool is_dynamic,
                                       bool does_collide,
                                       float width,
                                       float height,
                                       float mass,
                                       enum collider_category category) {
    struct collider_id id;
    struct collider *box;
    struct collider_id *user_data = malloc(sizeof(struct collider_id));

    id = allocate_collider();
    *user_data = id;
    box = get_collider_from_id(id);

    box->type = COLLIDER_TYPE_BOX;
    box->does_collide = does_collide;
    box->position = V2_ZERO;
    box->velocity = V2_ZERO;
    box->is_dynamic = is_dynamic;
    box->mass = mass;
    box->box.width = width;
    box->box.height = height;

    if (is_dynamic) {
        float moment = (float)cpMomentForBox(mass, width, height);
        box->cp_body = cpBodyNew(mass, moment);
    } else {
        box->cp_body = cpBodyNewKinematic();
    }
    cpSpaceAddBody(physics_state.cp_space, box->cp_body);
    cpBodySetUserData(box->cp_body, user_data);

    box->cp_shape = cpBoxShapeNew(box->cp_body, width, height, 0.0f);
    cpShapeSetElasticity(box->cp_shape, 0.0f);
    cpSpaceAddShape(physics_state.cp_space, box->cp_shape);

    cpShapeSetFilter(box->cp_shape,
                     cpShapeFilterNew(CP_NO_GROUP,
                                      category,
                                      CP_ALL_CATEGORIES));

    return id;
}

struct collider_id create_line_segment_collider(bool is_dynamic,
                                                bool does_collide,
                                                float mass,
                                                float thickness,
                                                vec2 p0,
                                                vec2 p1,
                                                enum collider_category category) {
    struct collider_id id;
    struct collider *line_segment;
    struct collider_id *user_data = malloc(sizeof(struct collider_id));
    cpVect cp_p0, cp_p1;

    cp_p0 = cpv(p0.x, p0.y);
    cp_p1 = cpv(p1.x, p1.y);

    id = allocate_collider();
    *user_data = id;
    line_segment = get_collider_from_id(id);

    line_segment->type = COLLIDER_TYPE_LINE_SEGMENT;
    line_segment->does_collide = does_collide;
    line_segment->position = V2_ZERO;
    line_segment->velocity = V2_ZERO;
    line_segment->is_dynamic = is_dynamic;
    line_segment->mass = mass;
    line_segment->line_segment.thickness = thickness;
    line_segment->line_segment.p0 = p0;
    line_segment->line_segment.p1 = p1;

    if (is_dynamic) {
        float moment;

        moment = (float)cpMomentForSegment(mass, cp_p0, cp_p1, 0.01f);
        line_segment->cp_body = cpBodyNew(mass, moment);
    } else {
        line_segment->cp_body = cpBodyNewKinematic();
    }
    cpSpaceAddBody(physics_state.cp_space, line_segment->cp_body);
    cpBodySetUserData(line_segment->cp_body, user_data);

    line_segment->cp_shape = cpSegmentShapeNew(line_segment->cp_body, cp_p0, cp_p1, 0.01f);
    cpShapeSetElasticity(line_segment->cp_shape, 0.0f);
    cpSpaceAddShape(physics_state.cp_space, line_segment->cp_shape);

    cpShapeSetFilter(line_segment->cp_shape, cpShapeFilterNew(CP_NO_GROUP, category, CP_ALL_CATEGORIES));

    return id;
}
