CC := gcc

BIN := prog
SOURCES := $(shell find src -iname '*.c')
OBJS := $(SOURCES:.c=.o)
DEPS = $(SOURCES:%.c=%.d)
CFLAGS := -Isrc -Ichipmunk/include -Wall -isystemsingle_file_libs -DLINUX_BUILD -g
LIBS := -lGL -lGLU -lGLEW -lglfw -lm -Lchipmunk -lchipmunk

CHIPMUNK_SOURCES := $(shell find chipmunk/src -iname '*.c')
CHIPMUNK_OBJS := $(CHIPMUNK_SOURCES:%.c=%.o)
CHIPMUNK_CFLAGS := -Ichipmunk/include -O2 -Wall -g -DNDEBUG
CHIPMUNK_LIBS := -lm -lpthread

all: chipmunk/libchipmunk.a $(BIN)

chipmunk/libchipmunk.a: $(CHIPMUNK_OBJS)
	ar rcs chipmunk/libchipmunk.a $(CHIPMUNK_OBJS)

$(CHIPMUNK_OBJS): %.o: %.c
	$(CC) $(CHIPMUNK_CFLAGS) $(CHIPMUNK_LIBS) -c -o $@ $<

chipmunk_clean:
	rm -f chipmunk/libchipmunk.a $(CHIPMUNK_OBJS)

$(BIN): $(OBJS)
	$(CC) -o $@ $(OBJS) $(LIBS) 

$(OBJS): %.o: %.c
	$(CC) $(CFLAGS) -c -MMD -o $@ $<

-include $(DEPS)

clean: chipmunk_clean
	rm -f $(BIN) $(OBJS) $(DEPS)
