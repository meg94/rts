#include "editor.h"

#include <float.h>
#include <stdarg.h>

#include "array.h"
#include "controls.h"
#include "entity.h"
#include "game.h"
#include "game_loader.h"
#include "renderer.h"
#include "ui.h"

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#include "nuklear.h"
#include "nuklear_glfw_gl3.h"

static struct nk_context *ctx;
static struct GLFWwindow *win;

void game_editor_init(struct GLFWwindow *window) {
    ctx = nk_glfw3_init(window, NK_GLFW3_INSTALL_CALLBACKS);
    win = window;

    struct nk_font_atlas *atlas;
    nk_glfw3_font_stash_begin(&atlas);
    nk_glfw3_font_stash_end();

    game_editor_state.entity_editor.draw_collider = false;
    game_editor_state.entity_editor.draw_selection_polygon = false;
    game_editor_state.entity_editor.draw_attack_radius = false;
    game_editor_state.entity_editor.draw_sight_radius = false;
    game_editor_state.map_editor.draw_tile_borders = false;
    game_editor_state.map_editor.draw_tile_sight_visibility = false;
    game_editor_state.map_editor.draw_tile_bfs_dist = false;
}

void game_editor_update(float dt) {
    if (nk_item_is_any_active(ctx)) {
        return;
    }
}

static void entity_selected_pane_editor(const char *name, struct ui_selected_entity_pane *pane) {
    nk_layout_row_dynamic(ctx, 25, 1);
    nk_label(ctx, name, NK_TEXT_LEFT);

    /*
	for (int i = 0; i < pane->num_buttons; i++) {
		nk_layout_row_dynamic(ctx, 25, 1);
		nk_labelf(ctx, NK_TEXT_LEFT, "button %s:", pane->buttons[i].action);

		nk_layout_row_dynamic(ctx, 25, 2);
		nk_property_float(ctx, "#px", -FLT_MAX, &pane->buttons[i].position.x, FLT_MAX, 0.01f, 0.01f);
		nk_property_float(ctx, "#py", -FLT_MAX, &pane->buttons[i].position.y, FLT_MAX, 0.01f, 0.01f);
	}
	*/

    for (int i = 0; i < pane->num_button_panels; i++) {
        nk_layout_row_dynamic(ctx, 25, 1);
        nk_labelf(ctx, NK_TEXT_LEFT, "button_panel: %s", pane->button_panels[i].name);

        for (int j = 0; j < pane->button_panels[i].num_strings; j++) {
            nk_layout_row_dynamic(ctx, 25, 1);
            nk_labelf(ctx, NK_TEXT_LEFT, "string: %s", pane->button_panels[i].strings[j]);

            nk_layout_row_dynamic(ctx, 25, 2);
            nk_property_float(ctx, "#px", -FLT_MAX, &pane->button_panels[i].string_positions[j].x, FLT_MAX, 0.01f, 0.01f);
            nk_property_float(ctx, "#py", -FLT_MAX, &pane->button_panels[i].string_positions[j].y, FLT_MAX, 0.01f, 0.01f);
        }
    }
}

void game_editor_draw(void) {
    nk_glfw3_new_frame();

    bool is_editor_hovered = false;
    array_entity_id_t entity_array;
    array_init(&entity_array);
    get_all_active_entities_with_type(&entity_array, 0, NULL);

    static bool first = true;
    nk_flags minimized_flag = 0;
    if (first) {
        minimized_flag = NK_WINDOW_MINIMIZED;
        first = false;
    }

    if (nk_begin(ctx, "UI Selected Pane Editor", nk_rect(5, 5, 400, 400),
                 NK_WINDOW_BORDER | NK_WINDOW_MOVABLE | NK_WINDOW_SCALABLE |
                     NK_WINDOW_MINIMIZABLE | NK_WINDOW_TITLE | minimized_flag)) {
        entity_selected_pane_editor("Soldier", &ui_state.selected_soldier_ui_pane);
        entity_selected_pane_editor("Soldier", &ui_state.selected_worker_ui_pane);

        if (nk_window_is_hovered(ctx)) {
            is_editor_hovered = true;
        }
    }
    nk_end(ctx);

    if (nk_begin(ctx, "UI Editor", nk_rect(5, 45, 400, 400),
                 NK_WINDOW_BORDER | NK_WINDOW_MOVABLE | NK_WINDOW_SCALABLE |
                     NK_WINDOW_MINIMIZABLE | NK_WINDOW_TITLE | minimized_flag)) {
        nk_layout_row_dynamic(ctx, 25, 2);

        struct rect_2D *rect;

        rect = &ui_state.ui_pane_rects[UI_PANE_RECT_TYPE_MAIN_0];
        nk_property_float(ctx, "p0.x", -10.0f, &rect->top_left.x, 10.0f, 0.01f, 0.01f);
        nk_property_float(ctx, "p0.y", -10.0f, &rect->top_left.y, 10.0f, 0.01f, 0.01f);
        nk_property_float(ctx, "p0.w", -10.0f, &rect->size.x, 10.0f, 0.01f, 0.01f);

        nk_property_float(ctx, "p0.h", -10.0f, &rect->size.y, 10.0f, 0.01f, 0.01f);

        rect = &ui_state.ui_pane_rects[UI_PANE_RECT_TYPE_MAIN_1];
        nk_property_float(ctx, "p1.x", -10.0f, &rect->top_left.x, 10.0f, 0.01f, 0.01f);
        nk_property_float(ctx, "p1.y", -10.0f, &rect->top_left.y, 10.0f, 0.01f, 0.01f);
        nk_property_float(ctx, "p1.w", -10.0f, &rect->size.x, 10.0f, 0.01f, 0.01f);
        nk_property_float(ctx, "p1.h", -10.0f, &rect->size.y, 10.0f, 0.01f, 0.01f);

        rect = &ui_state.ui_pane_rects[UI_PANE_RECT_TYPE_MAIN_2];
        nk_property_float(ctx, "p2.x", -10.0f, &rect->top_left.x, 10.0f, 0.01f, 0.01f);
        nk_property_float(ctx, "p2.y", -10.0f, &rect->top_left.y, 10.0f, 0.01f, 0.01f);
        nk_property_float(ctx, "p2.w", -10.0f, &rect->size.x, 10.0f, 0.01f, 0.01f);
        nk_property_float(ctx, "p2.h", -10.0f, &rect->size.y, 10.0f, 0.01f, 0.01f);

        rect = &ui_state.ui_pane_rects[UI_PANE_RECT_TYPE_MINIMAP];
        nk_property_float(ctx, "mm.x", -10.0f, &rect->top_left.x, 10.0f, 0.01f, 0.01f);
        nk_property_float(ctx, "mm.y", -10.0f, &rect->top_left.y, 10.0f, 0.01f, 0.01f);
        nk_property_float(ctx, "mm.w", -10.0f, &rect->size.x, 10.0f, 0.01f, 0.01f);
        nk_property_float(ctx, "mm.h", -10.0f, &rect->size.y, 10.0f, 0.01f, 0.01f);

        if (nk_button_label(ctx, "Save Game")) {
            game_loader_save("testing.dat");
        }

        if (nk_button_label(ctx, "Load Game")) {
            game_loader_load("testing.dat");
        }

        if (nk_window_is_hovered(ctx)) {
            is_editor_hovered = true;
        }
    }
    nk_end(ctx);

    if (nk_begin(ctx, "Entity Editor", nk_rect(410, 5, 400, 400),
                 NK_WINDOW_BORDER | NK_WINDOW_MOVABLE | NK_WINDOW_SCALABLE |
                     NK_WINDOW_MINIMIZABLE | NK_WINDOW_TITLE | minimized_flag)) {
        {
            nk_layout_row_dynamic(ctx, 25, 1);
            nk_checkbox_label(ctx, "Draw Selection Polygons", &game_editor_state.entity_editor.draw_selection_polygon);
            nk_checkbox_label(ctx, "Draw Colliders", &game_editor_state.entity_editor.draw_collider);
            nk_checkbox_label(ctx, "Draw Attack Radius", &game_editor_state.entity_editor.draw_attack_radius);
            nk_checkbox_label(ctx, "Draw Sight Radius", &game_editor_state.entity_editor.draw_sight_radius);
        }

        struct entity_id entity_id;
        int i;
        array_foreach((&entity_array), entity_id, i) {
            struct entity *entity = get_entity_from_id(entity_id);
            if (!entity || entity->type == ENTITY_TYPE_TERRAIN_BORDER) {
                continue;
            }

            if (entity->is_hovered || entity->is_selected) {
                nk_style_push_color(ctx, &ctx->style.tab.text, nk_rgb(255, 0, 0));
            }

            char title[256];
            sprintf_s(title, 256, "E%d - %s", entity_id.idx, entity_type_name[entity->type]);
            if (nk_tree_push_id(ctx, NK_TREE_NODE, title, NK_MINIMIZED, i)) {
                nk_layout_row_dynamic(ctx, 25, 1);
                nk_label(ctx, "Allocation Info:", NK_TEXT_LEFT);
                nk_layout_row_dynamic(ctx, 25, 2);
                {
                    nk_labelf(ctx, NK_TEXT_LEFT, "IDX: %d", entity->id.idx);
                    nk_labelf(ctx, NK_TEXT_LEFT, "GEN: %d", entity->id.gen);
                }

                nk_layout_row_dynamic(ctx, 25, 1);
                nk_label(ctx, "Position:", NK_TEXT_LEFT);

                nk_layout_row_dynamic(ctx, 25, 2);
                {
                    nk_property_float(ctx, "#px", -FLT_MAX, &entity->position.x, FLT_MAX, 1, 1);
                    nk_property_float(ctx, "#py", -FLT_MAX, &entity->position.y, FLT_MAX, 1, 1);
                }

                nk_layout_row_dynamic(ctx, 25, 1);
                nk_label(ctx, "Selection Box:", NK_TEXT_LEFT);

                nk_layout_row_dynamic(ctx, 25, 2);
                {
                    vec2 *s = &ui_state.entity_selection_box[4 * entity->type];
                    nk_property_float(ctx, "#p0x", -FLT_MAX, &s[0].x, FLT_MAX, 0.01f, 0.01f);
                    nk_property_float(ctx, "#p0y", -FLT_MAX, &s[0].y, FLT_MAX, 0.01f, 0.01f);
                    nk_property_float(ctx, "#p1x", -FLT_MAX, &s[1].x, FLT_MAX, 0.01f, 0.01f);
                    nk_property_float(ctx, "#p1y", -FLT_MAX, &s[1].y, FLT_MAX, 0.01f, 0.01f);
                    nk_property_float(ctx, "#p2x", -FLT_MAX, &s[2].x, FLT_MAX, 0.01f, 0.01f);
                    nk_property_float(ctx, "#p2y", -FLT_MAX, &s[2].y, FLT_MAX, 0.01f, 0.01f);
                    nk_property_float(ctx, "#p3x", -FLT_MAX, &s[3].x, FLT_MAX, 0.01f, 0.01f);
                    nk_property_float(ctx, "#p3y", -FLT_MAX, &s[3].y, FLT_MAX, 0.01f, 0.01f);
                }

                {
                    struct mesh_instance *parent = get_mesh_instance_from_id(entity->mesh_instance_id);
                    struct mesh_instance_child *child = get_mesh_instance_child("body2", parent);
                    if (child) {
                        nk_layout_row_dynamic(ctx, 25, 1);
                        nk_label(ctx, "Body Mesh:", NK_TEXT_LEFT);

                        nk_layout_row_dynamic(ctx, 25, 2);
                        nk_property_float(ctx, "#px", -FLT_MAX, &child->position.x, FLT_MAX, 0.01f, 0.01f);
                        nk_property_float(ctx, "#py", -FLT_MAX, &child->position.y, FLT_MAX, 0.01f, 0.01f);
                        nk_property_float(ctx, "#sx", -FLT_MAX, &child->scale.x, FLT_MAX, 0.01f, 0.01f);
                        nk_property_float(ctx, "#sy", -FLT_MAX, &child->scale.y, FLT_MAX, 0.01f, 0.01f);
                    }
                }

                {
                    struct mesh_instance *parent = get_mesh_instance_from_id(entity->mesh_instance_id);
                    struct mesh_instance_child *child = get_mesh_instance_child("hovered", parent);
                    if (child) {
                        nk_layout_row_dynamic(ctx, 25, 1);
                        nk_label(ctx, "Hovered Indicator Mesh:", NK_TEXT_LEFT);

                        nk_layout_row_dynamic(ctx, 25, 2);
                        nk_property_float(ctx, "#px", -FLT_MAX, &child->position.x, FLT_MAX, 0.01f, 0.01f);
                        nk_property_float(ctx, "#py", -FLT_MAX, &child->position.y, FLT_MAX, 0.01f, 0.01f);
                        nk_property_float(ctx, "#sx", -FLT_MAX, &child->scale.x, FLT_MAX, 0.01f, 0.01f);
                        nk_property_float(ctx, "#sy", -FLT_MAX, &child->scale.y, FLT_MAX, 0.01f, 0.01f);
                    }
                }

                {
                    struct mesh_instance *parent = get_mesh_instance_from_id(entity->mesh_instance_id);
                    struct mesh_instance_child *child = get_mesh_instance_child("selected2", parent);
                    if (child) {
                        nk_layout_row_dynamic(ctx, 25, 1);
                        nk_label(ctx, "Selected Indicator Mesh:", NK_TEXT_LEFT);

                        nk_layout_row_dynamic(ctx, 25, 2);
                        nk_property_float(ctx, "#px", -FLT_MAX, &child->position.x, FLT_MAX, 0.01f, 0.01f);
                        nk_property_float(ctx, "#py", -FLT_MAX, &child->position.y, FLT_MAX, 0.01f, 0.01f);
                        nk_property_float(ctx, "#sx", -FLT_MAX, &child->scale.x, FLT_MAX, 0.01f, 0.01f);
                        nk_property_float(ctx, "#sy", -FLT_MAX, &child->scale.y, FLT_MAX, 0.01f, 0.01f);
                    }
                }

                {
                    nk_layout_row_dynamic(ctx, 25, 2);
                    nk_property_float(ctx, "#Sight Radius", -FLT_MAX, &entity->sight_radius, FLT_MAX, 0.01f, 0.01f);
                }

                if (entity->type == ENTITY_TYPE_SOLDIER) {
                    switch (entity->soldier.state_type) {
                        case SOLDIER_STATE_IDLE:
                            nk_labelf(ctx, NK_TEXT_LEFT, "state_type: idle");
                            break;
                        case SOLDIER_STATE_ATTACKING_ENEMY:
                            nk_labelf(ctx, NK_TEXT_LEFT, "state_type: attacking_enemy");
                            break;
                        case SOLDIER_STATE_MOVING:
                            nk_labelf(ctx, NK_TEXT_LEFT, "state_type: moving");
                            break;
                    }
                }

                nk_tree_pop(ctx);
            }

            if (entity->is_hovered || entity->is_selected) {
                nk_style_pop_color(ctx);
            }
        }

        if (nk_window_is_hovered(ctx)) {
            is_editor_hovered = true;
        }
    }
    nk_end(ctx);

    if (nk_begin(ctx, "Map Editor", nk_rect(410, 45, 400, 400),
                 NK_WINDOW_BORDER | NK_WINDOW_MOVABLE | NK_WINDOW_SCALABLE |
                     NK_WINDOW_MINIMIZABLE | NK_WINDOW_TITLE | minimized_flag)) {
        {
            nk_layout_row_dynamic(ctx, 25, 1);
            nk_checkbox_label(ctx, "Draw Tile Borders", &game_editor_state.map_editor.draw_tile_borders);
            nk_checkbox_label(ctx, "Draw Tile Sight Visibility", &game_editor_state.map_editor.draw_tile_sight_visibility);
            nk_checkbox_label(ctx, "Draw Tile BFS Dist", &game_editor_state.map_editor.draw_tile_bfs_dist);
        }

        if (nk_window_is_hovered(ctx)) {
            is_editor_hovered = true;
        }
    }
    nk_end(ctx);

    game_editor_state.is_editor_hovered = is_editor_hovered;

    struct nk_style *s = &ctx->style;
    nk_style_push_color(ctx, &s->window.background, nk_rgba(0, 0, 0, 0));
    nk_style_push_style_item(ctx, &s->window.fixed_background, nk_style_item_color(nk_rgba(0, 0, 0, 0)));
    if (nk_begin(ctx, "UI Background Window", nk_rect(0.0f, 0.0f, (float)renderer_state.window_width, (float)renderer_state.window_height),
                 NK_WINDOW_NO_INPUT | NK_WINDOW_NO_SCROLLBAR | NK_WINDOW_BACKGROUND)) {
        struct nk_command_buffer *b = nk_window_get_canvas(ctx);
        for (int i = 0; i < NUM_UI_PANE_RECT_TYPES; i++) {
            struct rect_2D rect = ui_state.ui_pane_rects[i];

            float x = rect.top_left.x;
            float y = rect.top_left.y;
            float w = rect.size.x;
            float h = rect.size.y;

            x = (8.0f + x) / 16.0f * renderer_state.window_width;
            y = renderer_state.window_height - (4.5f + y) / 9.0f * renderer_state.window_height;
            w = w / 16.0f * renderer_state.window_width;
            h = h / 9.0f * renderer_state.window_height;

            nk_stroke_rect(b, nk_rect(x, y, w, h), 0.0f, 1.0f, nk_rgb(255, 255, 0));
        }

        if (game_editor_state.map_editor.draw_tile_borders) {
            for (int row = 0; row < GAME_MAP_SIZE; row++) {
                vec2 game_pos[2], ui_pos[2];

                game_pos[0].x = 0.0f;
                game_pos[0].y = row * GAME_TILE_WIDTH;
                ui_pos[0] = game_to_ui_position(game_pos[0]);
                ui_pos[0].x = (8.0f + ui_pos[0].x) / 16.0f * renderer_state.window_width;
                ui_pos[0].y = renderer_state.window_height - (4.5f + ui_pos[0].y) / 9.0f * renderer_state.window_height;

                game_pos[1].x = GAME_MAP_SIZE * GAME_TILE_WIDTH;
                game_pos[1].y = row * GAME_TILE_WIDTH;
                ui_pos[1] = game_to_ui_position(game_pos[1]);
                ui_pos[1].x = (8.0f + ui_pos[1].x) / 16.0f * renderer_state.window_width;
                ui_pos[1].y = renderer_state.window_height - (4.5f + ui_pos[1].y) / 9.0f * renderer_state.window_height;

                nk_stroke_line(b, ui_pos[0].x, ui_pos[0].y, ui_pos[1].x, ui_pos[1].y, 1.0f, nk_rgb(100, 100, 100));
            }

            for (int col = 0; col < GAME_MAP_SIZE; col++) {
                vec2 game_pos[2], ui_pos[2];

                game_pos[0].x = col * GAME_TILE_WIDTH;
                game_pos[0].y = 0.0f;
                ui_pos[0] = game_to_ui_position(game_pos[0]);
                ui_pos[0].x = (8.0f + ui_pos[0].x) / 16.0f * renderer_state.window_width;
                ui_pos[0].y = renderer_state.window_height - (4.5f + ui_pos[0].y) / 9.0f * renderer_state.window_height;

                game_pos[1].x = col * GAME_TILE_WIDTH;
                game_pos[1].y = GAME_MAP_SIZE * GAME_TILE_WIDTH;
                ui_pos[1] = game_to_ui_position(game_pos[1]);
                ui_pos[1].x = (8.0f + ui_pos[1].x) / 16.0f * renderer_state.window_width;
                ui_pos[1].y = renderer_state.window_height - (4.5f + ui_pos[1].y) / 9.0f * renderer_state.window_height;

                nk_stroke_line(b, ui_pos[0].x, ui_pos[0].y, ui_pos[1].x, ui_pos[1].y, 1.0f, nk_rgb(100, 100, 100));
            }
        }

        if (game_editor_state.map_editor.draw_tile_sight_visibility) {
            for (int row = 0; row < GAME_MAP_SIZE; row++) {
                for (int col = 0; col < GAME_MAP_SIZE; col++) {
                    if (!game_state.map_tile_visible[GAME_MAP_SIZE * col + row]) {
                        continue;
                    }

                    float x, y;
                    vec2 points[4];

                    x = (col + 0.5f) * GAME_TILE_WIDTH;
                    y = (row + 0.5f) * GAME_TILE_WIDTH;

                    points[0].x = x - 0.5f * GAME_TILE_WIDTH;
                    points[0].y = y - 0.5f * GAME_TILE_WIDTH;

                    points[1].x = x - 0.5f * GAME_TILE_WIDTH;
                    points[1].y = y + 0.5f * GAME_TILE_WIDTH;

                    points[2].x = x + 0.5f * GAME_TILE_WIDTH;
                    points[2].y = y + 0.5f * GAME_TILE_WIDTH;

                    points[3].x = x + 0.5f * GAME_TILE_WIDTH;
                    points[3].y = y - 0.5f * GAME_TILE_WIDTH;

                    for (int i = 0; i < 4; i++) {
                        points[i] = game_to_ui_position(points[i]);
                        points[i].x = (8.0f + points[i].x) / 16.0f * renderer_state.window_width;
                        points[i].y = renderer_state.window_height - (4.5f + points[i].y) / 9.0f * renderer_state.window_height;
                    }

                    nk_fill_polygon(b, (float *)points, 4, nk_rgba(0, 255, 0, 50));
                }
            }
        }

        if (game_editor_state.map_editor.draw_tile_bfs_dist) {
            for (int row = 0; row < GAME_MAP_SIZE; row++) {
                for (int col = 0; col < GAME_MAP_SIZE; col++) {
                    vec2 pos;
                    int idx, len, dist;
                    char str[16];

                    pos.x = (col + 0.5f) * GAME_TILE_WIDTH;
                    pos.y = (row + 0.5f) * GAME_TILE_WIDTH;
                    pos = game_to_ui_position(pos);
                    pos.x = (8.0f + pos.x) / 16.0f * renderer_state.window_width;
                    pos.y = renderer_state.window_height - (4.5f + pos.y) / 9.0f * renderer_state.window_height;

                    idx = GAME_MAP_SIZE * col + row;
                    dist = game_state.game_bfs_dist[idx];

					if (dist == INT_MAX) {
                        dist = -1;    
					}

                    sprintf_s(str, 16, "%d", dist);
                    len = strlen(str);

                    nk_draw_text(b, nk_rect(pos.x, pos.y, 10.0f, 10.0f), str, len, ctx->style.font,
                                 nk_rgba(0, 0, 0, 0), nk_rgba(255, 255, 255, 255));
                }
            }
        }

        struct entity_id entity_id;
        int i;
        array_foreach((&entity_array), entity_id, i) {
            struct entity *entity = get_entity_from_id(entity_id);
            if (!entity) {
                continue;
            }

            /*
            if (entity->type == ENTITY_TYPE_SOLDIER) {
                for (int j = 0; j < entity->soldier.moving_state.path.length - 1; j++) {
                    vec2 p0 = game_to_ui_position(entity->soldier.moving_state.path.data[j]);
                    vec2 p1 = game_to_ui_position(entity->soldier.moving_state.path.data[j + 1]);

                    p0.x = (8.0f + p0.x) / 16.0f * renderer_state.window_width;
                    p0.y = renderer_state.window_height - (4.5f + p0.y) / 9.0f * renderer_state.window_height;

                    p1.x = (8.0f + p1.x) / 16.0f * renderer_state.window_width;
                    p1.y = renderer_state.window_height - (4.5f + p1.y) / 9.0f * renderer_state.window_height;

                    nk_stroke_line(b, p0.x, p0.y, p1.x, p1.y, 1.0f, nk_rgb(255, 0, 0));
                }
            }
			*/

            if (game_editor_state.entity_editor.draw_selection_polygon) {
                vec2 points[4];
                vec2 ui_position = game_to_ui_position(entity->position);
                get_ui_entity_selection_points(entity->type, ui_position, points);

                points[0].x = (8.0f + points[0].x) / 16.0f * renderer_state.window_width;
                points[0].y = renderer_state.window_height - (4.5f + points[0].y) / 9.0f * renderer_state.window_height;
                points[1].x = (8.0f + points[1].x) / 16.0f * renderer_state.window_width;
                points[1].y = renderer_state.window_height - (4.5f + points[1].y) / 9.0f * renderer_state.window_height;
                points[2].x = (8.0f + points[2].x) / 16.0f * renderer_state.window_width;
                points[2].y = renderer_state.window_height - (4.5f + points[2].y) / 9.0f * renderer_state.window_height;
                points[3].x = (8.0f + points[3].x) / 16.0f * renderer_state.window_width;
                points[3].y = renderer_state.window_height - (4.5f + points[3].y) / 9.0f * renderer_state.window_height;

                nk_stroke_line(b, points[0].x, points[0].y, points[1].x, points[1].y, 1.0f, nk_rgb(255, 255, 255));
                nk_stroke_line(b, points[1].x, points[1].y, points[2].x, points[2].y, 1.0f, nk_rgb(255, 255, 255));
                nk_stroke_line(b, points[2].x, points[2].y, points[3].x, points[3].y, 1.0f, nk_rgb(255, 255, 255));
                nk_stroke_line(b, points[3].x, points[3].y, points[0].x, points[0].y, 1.0f, nk_rgb(255, 255, 255));
            }

            if (game_editor_state.entity_editor.draw_sight_radius && entity->sight_radius > 0.0f) {
                for (int j = 0; j < 30; j++) {
                    vec2 p0, p1;
                    float theta0, theta1;

                    theta0 = 2.0f * (float)M_PI * (j / 30.0f);
                    theta1 = 2.0f * (float)M_PI * ((j + 1) / 30.0f);

                    p0.x = entity->position.x + cosf(theta0) * entity->sight_radius;
                    p0.y = entity->position.y + sinf(theta0) * entity->sight_radius;
                    p1.x = entity->position.x + cosf(theta1) * entity->sight_radius;
                    p1.y = entity->position.y + sinf(theta1) * entity->sight_radius;

                    p0 = game_to_ui_position(p0);
                    p0.x = (8.0f + p0.x) / 16.0f * renderer_state.window_width;
                    p0.y = renderer_state.window_height - (4.5f + p0.y) / 9.0f * renderer_state.window_height;

                    p1 = game_to_ui_position(p1);
                    p1.x = (8.0f + p1.x) / 16.0f * renderer_state.window_width;
                    p1.y = renderer_state.window_height - (4.5f + p1.y) / 9.0f * renderer_state.window_height;

                    nk_stroke_line(b, p0.x, p0.y, p1.x, p1.y, 1.0f, nk_rgb(255, 0, 0));
                }
            }

            if (game_editor_state.entity_editor.draw_collider) {
                struct collider *collider = get_collider_from_id(entity->collider_id);
                if (!collider) {
                    continue;
                }

                switch (collider->type) {
                    case COLLIDER_TYPE_CIRCLE: {
                        vec2 points[32];
                        for (int j = 0; j < 32; j++) {
                            float theta = 2.0f * (float)M_PI * (j / 31.0f);
                            points[j].x = collider->position.x + cosf(theta) * collider->circle.radius;
                            points[j].y = collider->position.y + sinf(theta) * collider->circle.radius;
                            points[j] = game_to_ui_position(points[j]);
                            points[j].x = (8.0f + points[j].x) / 16.0f * renderer_state.window_width;
                            points[j].y = renderer_state.window_height - (4.5f + points[j].y) / 9.0f * renderer_state.window_height;
                        }
                        nk_fill_polygon(b, (float *)points, 32, nk_rgb(0, 255, 0));
                        break;
                    }
                    case COLLIDER_TYPE_BOX: {
                        vec2 points[4];

                        points[0].x = collider->position.x - 0.5f * collider->box.width;
                        points[0].y = collider->position.y + 0.5f * collider->box.height;

                        points[1].x = collider->position.x + 0.5f * collider->box.width;
                        points[1].y = collider->position.y + 0.5f * collider->box.height;

                        points[2].x = collider->position.x + 0.5f * collider->box.width;
                        points[2].y = collider->position.y - 0.5f * collider->box.height;

                        points[3].x = collider->position.x - 0.5f * collider->box.width;
                        points[3].y = collider->position.y - 0.5f * collider->box.height;

                        for (int j = 0; j < 4; j++) {
                            points[j] = game_to_ui_position(points[j]);
                            points[j].x = (8.0f + points[j].x) / 16.0f * renderer_state.window_width;
                            points[j].y = renderer_state.window_height - (4.5f + points[j].y) / 9.0f * renderer_state.window_height;
                        }

                        nk_fill_polygon(b, (float *)points, 4, nk_rgb(0, 255, 0));
                        break;
                    }
                    case COLLIDER_TYPE_LINE_SEGMENT: {
                        vec2 points[2];

                        points[0] = collider->line_segment.p0;
                        points[1] = collider->line_segment.p1;

                        for (int j = 0; j < 2; j++) {
                            points[j] = game_to_ui_position(points[j]);
                            points[j].x = (8.0f + points[j].x) / 16.0f * renderer_state.window_width;
                            points[j].y = renderer_state.window_height - (4.5f + points[j].y) / 9.0f * renderer_state.window_height;
                        }

                        nk_stroke_line(b, points[0].x, points[0].y, points[1].x, points[1].y, 1.0f, nk_rgb(0, 255, 0));
                        break;
                    }
                }
            }
        }
    }
    nk_end(ctx);
    nk_style_pop_color(ctx);
    nk_style_pop_style_item(ctx);

    array_deinit(&entity_array);

    nk_glfw3_render(NK_ANTI_ALIASING_ON, 512 * 1024, 128 * 1024);
}
