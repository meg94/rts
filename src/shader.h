#ifndef _RENDERER_SHADER_H
#define _RENDERER_SHADER_H

#include "IGL.h"

struct shader_store {
    struct mesh_shader {
        GLuint program;
        GLuint mvp_mat_location;
        GLuint tex_pct_location;
        GLuint color_pct_location;
        GLuint color_location;
        GLuint texture_location;
        GLuint opacity_location;
        GLuint position_location;
        GLuint texture_coord_location;
    } mesh_shader;

    struct mesh_texture_shader {
        GLuint program;
        GLuint mvp_mat_location;
        GLuint texture_location;
        GLuint opacity_location;
        GLuint position_location;
        GLuint texture_coord_location;
    } mesh_texture_shader;

    struct mesh_texture_atlas_shader {
        GLuint program;
        GLuint mvp_mat_location;
        GLuint texture_location;
        GLuint opacity_location;
		GLuint color_location;
		GLuint color_pct_location;
        GLuint position_location;
        GLuint texture_coord_location;
        GLuint vtc_p;
        GLuint vtc_s;
    } mesh_texture_atlas_shader;

    struct map_tile_shader {
        GLuint program;
        GLuint mvp_mat_location;
        GLuint texture_location;
        GLuint tex_coord_top_left_location;
        GLuint tex_coord_width_location;
        GLuint position_location;
        GLuint texture_coord_location;
    } map_tile_shader;

    struct fog_of_war_shader {
        GLuint program;
        GLuint mvp_mat_location;
        GLuint color_location;
        GLuint texture_location;
        GLuint position_location;
        GLuint texture_coord_location;
    } fog_of_war_shader;

    struct minimap_shader {
        GLuint program;
        GLuint mvp_mat_location;
        GLuint position_location;
        GLuint color_location;
    } minimap_shader;

    struct unit_body_shader {
        GLuint program;
        GLuint mvp_mat_location;
        GLuint idx_location;
        GLuint gen_location;
        GLuint texture_location;
        GLuint position_location;
        GLuint texture_coord_location;
    } unit_body_shader;

    struct debug_shader {
        GLuint program;
        GLuint mvp_mat_location;
        GLuint color_location;
        GLuint position_location;
    } debug_shader;

    struct gaussian_blur_shader {
        GLuint program;
        GLuint mvp_mat_location;
        GLuint texture_location;
        GLuint horizontal_location;
        GLuint position_location;
        GLuint texture_coord_location;
    } gaussian_blur_shader;
} shader_store;

void shader_store_init(void);
GLint shader_create_program(const char *vertex_shader_code, const char *fragment_shader_code);

#endif
