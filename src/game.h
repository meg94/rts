#ifndef _GAME_GAME_H
#define _GAME_GAME_H

#include "entity.h"
#include "renderer.h"

#define GAME_TILE_WIDTH (0.2f)
#define GAME_MAP_SIZE (100)
#define GAME_MAP_WIDTH (GAME_MAP_SIZE * GAME_TILE_WIDTH)

enum terrain_type {
    TERRAIN_TYPE_GRASS,
    TERRAIN_TYPE_WATER,
    TERRAIN_TYPE_NOTHING,
};

struct game_state {
    int team_supply[2];
    int team_supply_max[2];
    int team_money[2];
    vec2 cam_pos;
    float cam_size;
    enum terrain_type terrain[GAME_MAP_SIZE * GAME_MAP_SIZE];
    bool map_tile_occupied[GAME_MAP_SIZE * GAME_MAP_SIZE];
    bool map_tile_visible[GAME_MAP_SIZE * GAME_MAP_SIZE];
    array_entity_id_t selected_entity_array;
    int game_bfs_dist[GAME_MAP_SIZE * GAME_MAP_SIZE];
    array_entity_id_t team_0_entities, team_1_entities;
} game_state;

void game_init(void);
void game_deinit(void);
void game_update(float dt);
vec2 game_get_pos(vec2 mouse_pos);
void game_set_cam_pos(vec2 cam_pos);
void game_do_pathfinding(vec2 start, vec2 target, array_vec2_t *path);
void game_set_cam_size(float cam_size);

#endif
