#ifndef TDF_UTIL_H
#define TDF_UTIL_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <errno.h>

#if defined(LINUX_BUILD) || defined(JS_BUILD) || defined(WINDOWS_BUILD)
#include <stdio.h>
#define LOG(args...) printf(args)
#endif

#if defined(ANDROID_BUILD)
#include <android/asset_manager.h>
#include <android/log.h>
AAssetManager *asset_manager;
const char *files_dir_path;
int files_dir_path_len;
#define LOG(args...) __android_log_print(ANDROID_LOG_INFO, "tdf", args)
#endif

char *util_read_file(const char *filename, int *file_length, const char *mode);
FILE *util_fopen(const char *filename, const char *args);
#endif
