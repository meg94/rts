CC := emcc

BIN := prog.html
SOURCES := $(shell find src -iname '*.c')
OBJS := $(SOURCES:.c=.bc)
DEPS = $(SOURCES:%.c=%.d)
CFLAGS := -Isrc -Ichipmunk/include -Wall -isystemsingle_file_libs -DJS_BUILD -g -s ALLOW_MEMORY_GROWTH=1 -s USE_WEBGL2=1 -s FULL_ES3=1 -s USE_GLFW=3 --preload-file assets/
LIBS := -lGL -lGLU -lGLEW -lglfw -lm -Lchipmunk

CHIPMUNK_SOURCES := $(shell find chipmunk/src -iname '*.c')
CHIPMUNK_OBJS := $(CHIPMUNK_SOURCES:%.c=%.bc)
CHIPMUNK_CFLAGS := -Ichipmunk/include -O2 -Wall -g -DNDEBUG -s ALLOW_MEMORY_GROWTH=1
CHIPMUNK_LIBS := -lm -lpthread

all: chipmunk/libchipmunk.bc $(BIN)

chipmunk/libchipmunk.bc: $(CHIPMUNK_OBJS)
	emcc -o chipmunk/libchipmunk.bc $(CHIPMUNK_OBJS)

$(CHIPMUNK_OBJS): %.bc: %.c
	$(CC) $(CHIPMUNK_CFLAGS) $(CHIPMUNK_LIBS) -c -o $@ $<

chipmunk_clean:
	rm -f chipmunk/libchipmunk.bc $(CHIPMUNK_OBJS)

$(BIN): $(OBJS)
	$(CC) chipmunk/libchipmunk.bc -o $@ $(OBJS) $(LIBS) $(CFLAGS)

$(OBJS): %.bc: %.c
	$(CC) $(CFLAGS) -c -MMD -o $@ $<

-include $(DEPS)

clean: chipmunk_clean
	rm -f $(BIN) $(OBJS) $(DEPS)
