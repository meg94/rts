#ifndef _GAME_UI_H
#define _GAME_UI_H

#include "entity.h"
#include "font.h"
#include "maths.h"

struct ui_panel {
    vec2 center;
    vec2 size;
};

enum UI_PANE_RECT_TYPE {
    UI_PANE_RECT_TYPE_MAIN_0 = 0,
    UI_PANE_RECT_TYPE_MAIN_1,
    UI_PANE_RECT_TYPE_MAIN_2,
    UI_PANE_RECT_TYPE_MINIMAP,
    NUM_UI_PANE_RECT_TYPES,
};

enum unit_action_type {
    UNIT_ACTION_TYPE_MOVE,
    UNIT_ACTION_TYPE_ATTACK_MOVE,
    UNIT_ACTION_TYPE_PATROL_MOVE,
};

#define MAX_UI_SELECTED_ENTITY_PANE_NUM_BUTTONS 10
#define MAX_UI_SELECTED_ENTITY_PANE_BUTTON_ACTION_LENGTH 256
#define MAX_UI_SELECTED_ENTITY_PANE_BUTTON_SCREEN_LENGTH 256
#define MAX_UI_SELECTED_ENTITY_PANE_BUTTON_TEXTURE_NAME_LENGTH 256
#define MAX_UI_SELECTED_ENTITY_PANE_BUTTON_TOOLTIP_TEXT_LENGTH 256

struct ui_selected_entity_pane_button {
    char action[MAX_UI_SELECTED_ENTITY_PANE_BUTTON_ACTION_LENGTH];
    char texture_name[MAX_UI_SELECTED_ENTITY_PANE_BUTTON_TEXTURE_NAME_LENGTH];
    vec2 position;
    vec2 scale;
    bool has_tooltip;
    struct {
        char text[MAX_UI_SELECTED_ENTITY_PANE_BUTTON_TOOLTIP_TEXT_LENGTH];
        vec2 position;
    } tooltip;
};

#define MAX_UI_SELECTED_ENTITY_PANE_NUM_IMAGES 10
#define MAX_UI_SELECTED_ENTITY_PANE_IMAGE_TEXTURE_NAME_LENGTH 256

struct ui_selected_entity_pane_image {
    char texture_name[MAX_UI_SELECTED_ENTITY_PANE_BUTTON_TEXTURE_NAME_LENGTH];
    vec2 position;
    vec2 scale;
};

#define MAX_UI_SELECTED_ENTITY_PANE_NUM_STRINGS 10
#define MAX_UI_SELECTED_ENTITY_PANE_STRING_TEXT_LENGTH 256
#define MAX_UI_SELECTED_ENTITY_PANE_STRING_SCREEN_LENGTH 256

struct ui_selected_entity_pane_string {
    char text[MAX_UI_SELECTED_ENTITY_PANE_STRING_TEXT_LENGTH];
    vec3 color;
    vec2 position;
    float size;
};

#define MAX_UI_SELECTED_ENTITY_PANE_BUTTON_LIST_NUM_BUTTONS 10
#define MAX_UI_SELECTED_ENTITY_PANE_BUTTON_LIST_ACTION_LENGTH 256

struct ui_selected_entity_pane_button_list {
    char action[MAX_UI_SELECTED_ENTITY_PANE_BUTTON_LIST_ACTION_LENGTH];
    vec2 position;
    vec2 scale;
    vec2 padding;

    int num_buttons;
    struct texture textures[MAX_UI_SELECTED_ENTITY_PANE_BUTTON_LIST_NUM_BUTTONS];
};

#define MAX_NUM_BUTTON_PANELS 10
#define MAX_BUTTON_PANEL_NAME_LENGTH 256
#define MAX_BUTTON_PANEL_TEXTURE_LENGTH 256
#define MAX_BUTTON_PANEL_ACTION_LENGTH 256
#define MAX_BUTTON_PANEL_STRING_LENGTH 256
#define MAX_BUTTON_PANEL_NUM_BUTTONS 6
#define MAX_BUTTON_PANEL_NUM_STRINGS 6

struct ui_selected_entity_pane_button_panel {
    vec2 position, padding;
    float size;

    char name[MAX_BUTTON_PANEL_NAME_LENGTH];
    char textures[MAX_BUTTON_PANEL_NUM_BUTTONS][MAX_BUTTON_PANEL_TEXTURE_LENGTH];
    char actions[MAX_BUTTON_PANEL_NUM_BUTTONS][MAX_BUTTON_PANEL_ACTION_LENGTH];
    char transitions[MAX_BUTTON_PANEL_NUM_BUTTONS][MAX_BUTTON_PANEL_NAME_LENGTH];
    bool active[MAX_BUTTON_PANEL_NUM_BUTTONS];

    int num_strings;
    char strings[MAX_BUTTON_PANEL_NUM_STRINGS][MAX_BUTTON_PANEL_STRING_LENGTH];
    vec2 string_positions[MAX_BUTTON_PANEL_NUM_STRINGS];
    vec3 string_colors[MAX_BUTTON_PANEL_NUM_STRINGS];
    float string_sizes[MAX_BUTTON_PANEL_NUM_STRINGS];
};

struct ui_selected_entity_pane {
    char cur_button_panel_name[MAX_BUTTON_PANEL_NAME_LENGTH];
    int num_button_panels;
    struct ui_selected_entity_pane_button_panel button_panels[MAX_NUM_BUTTON_PANELS];

    int num_buttons;
    struct ui_selected_entity_pane_button buttons[MAX_UI_SELECTED_ENTITY_PANE_NUM_BUTTONS];

    int num_images;
    struct ui_selected_entity_pane_image images[MAX_UI_SELECTED_ENTITY_PANE_NUM_IMAGES];

    int num_strings;
    struct ui_selected_entity_pane_string strings[MAX_UI_SELECTED_ENTITY_PANE_NUM_STRINGS];

    bool has_button_list;
    struct ui_selected_entity_pane_button_list button_list;
};

struct ui_state {
    vec2 entity_selection_box[4 * NUM_OF_ENTITY_TYPES];
    struct rect_2D ui_pane_rects[NUM_UI_PANE_RECT_TYPES];

    struct font_mesh gold_text;

    bool selection_box_open;
    vec2 selection_box_corner_1;
    vec2 selection_box_corner_2;

    bool is_placing_entity;
    struct entity_id placeholder_id, worker_id;

    bool is_doing_unit_action;
    enum unit_action_type unit_action_type;

    bool is_moving_minimap_camera;

    mat4 proj_mat;
    mat4 game_to_minimap_mat;

    struct {
        vec2 supply_pos;
        float supply_size;

        vec2 money_pos;
        float money_size;
    } resources_info_ui;

    struct ui_selected_entity_pane selected_soldier_ui_pane;
    struct ui_selected_entity_pane selected_worker_ui_pane;
    struct ui_selected_entity_pane selected_refinery_ui_pane;
    struct ui_selected_entity_pane selected_building_construction_ui_pane;
    struct ui_selected_entity_pane selected_barracks_ui_pane;
    struct ui_selected_entity_pane selected_factory_ui_pane;
    struct ui_selected_entity_pane selected_heliport_ui_pane;
    struct ui_selected_entity_pane selected_command_center_ui_pane;
    struct ui_selected_entity_pane selected_medic_ui_pane;
    struct ui_selected_entity_pane selected_tank_ui_pane;
    struct ui_selected_entity_pane selected_vehicle_ui_pane;
    struct ui_selected_entity_pane selected_helicopter_ui_pane;
    struct ui_selected_entity_pane selected_turret_ui_pane;
    struct ui_selected_entity_pane selected_supply_depot_ui_pane;
} ui_state;

void ui_init(void);
void ui_update(float dt);
void ui_draw(void);

void get_ui_entity_selection_points(enum entity_type entity_type, vec2 ui_position, vec2 *points);
vec2 game_to_ui_position(vec2 game_pos);

#endif
