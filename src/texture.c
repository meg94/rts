#include "texture.h"

#include "util.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

GLuint texture_load(const char *file_name, int filter) {
    int x, y, n;
    int force_channels = 4;

    int buffer_len = 0;
    unsigned char *buffer = (unsigned char *)util_read_file(file_name, &buffer_len, "rb");
    stbi_set_flip_vertically_on_load(0);
    unsigned char *image_data = stbi_load_from_memory(buffer, buffer_len, &x, &y, &n, force_channels);

    if (!image_data) {
        printf("Could not load texture file %s\n", file_name);
        return 0;
    }

    GLuint location;
    glGenTextures(1, &location);
    glBindTexture(GL_TEXTURE_2D, location);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, x, y, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
    glBindTexture(GL_TEXTURE_2D, 0);

    free(buffer);
    stbi_image_free(image_data);

    return location;
}

struct texture create_image_texture(GLuint image) {
    struct texture texture;
    texture.type = TEXTURE_TYPE_IMAGE;
    texture.image = image;
    return texture;
}

struct texture create_color_texture(vec3 color) {
    struct texture texture;
    texture.type = TEXTURE_TYPE_COLOR;
    texture.color = color;
    return texture;
}

struct texture create_atlas_image_texture(const char *texture_name) {
    struct texture texture;
    texture.type = TEXTURE_TYPE_ATLAS_IMAGE;
    struct texture_atlas_data *data = map_get(&texture_store.atlas_data_map, texture_name);
    assert(data);
    texture.atlas_image = *data;
    return texture;
}

struct texture create_solid_color_atlas_image_texture(const char *texture_name, vec3 color, float color_pct) {
    struct texture texture;
    struct texture_atlas_data *data;

    data = map_get(&texture_store.atlas_data_map, texture_name);
    assert(data);

    texture.type = TEXTURE_TYPE_SOLID_COLOR_ATLAS_IMAGE;
    texture.solid_color_atlas_image.color = color;
    texture.solid_color_atlas_image.color_pct = color_pct;
    texture.solid_color_atlas_image.atlas_data = *data;

    return texture;
}

static void load_unit_textures(const char *name, int n,
                               struct texture *down, struct texture *down_d,
                               struct texture *side, struct texture *up,
                               struct texture *up_d) {
    assert(strlen(name) < 200 && "Unit texture name too long");
    char file_name[256];

    for (int i = 0; i < n; i++) {
        GLuint img;

        sprintf_s(file_name, 256, "%s/down/%d.png", name, i + 1);
        img = texture_load(file_name, GL_NEAREST);
        down[i] = create_image_texture(img);

        sprintf_s(file_name, 256, "%s/down-d/%d.png", name, i + 1);
        img = texture_load(file_name, GL_NEAREST);
        down_d[i] = create_image_texture(img);

        sprintf_s(file_name, 256, "%s/side/%d.png", name, i + 1);
        img = texture_load(file_name, GL_NEAREST);
        side[i] = create_image_texture(img);

        sprintf_s(file_name, 256, "%s/up/%d.png", name, i + 1);
        img = texture_load(file_name, GL_NEAREST);
        up[i] = create_image_texture(img);

        sprintf_s(file_name, 256, "%s/up-d/%d.png", name, i + 1);
        img = texture_load(file_name, GL_NEAREST);
        up_d[i] = create_image_texture(img);
    }
}

static void load_textures(const char *folder_name, int n, struct texture *texs) {
    assert(strlen(folder_name) < 200 && "Texture folder name too long");
    char file_name[256];

    for (int i = 0; i < n; i++) {
        GLuint img;

        sprintf_s(file_name, 256, "%s/%d.png", folder_name, i + 1);
        img = texture_load(file_name, GL_NEAREST);
        texs[i] = create_image_texture(img);
        ;
    }
}

void texture_store_init(void) {
    {
        map_init(&texture_store.atlas_data_map);
        char name[256];
        struct texture_atlas_data data;

        FILE *atlas_data_file = util_fopen("texture_atlas.data", "r");

        while (fscanf_s(atlas_data_file, "%s %d %d %d %d",
                        name, 256, &data.x, &data.y, &data.w, &data.h) != EOF) {
            map_set(&texture_store.atlas_data_map, name, data);
        }

        fclose(atlas_data_file);
    }
    texture_store.texture_atlas = create_image_texture(texture_load("texture_atlas.png", GL_NEAREST));

    texture_store.circle_texture = create_image_texture(texture_load("circle.jpg", GL_LINEAR));
    texture_store.bricks_texture = create_image_texture(texture_load("bricks.jpg", GL_LINEAR));

    texture_store.red = create_image_texture(texture_load("red.jpg", GL_LINEAR));
    texture_store.green = create_image_texture(texture_load("green.jpg", GL_LINEAR));
    texture_store.blue = create_image_texture(texture_load("blue.jpg", GL_LINEAR));
    texture_store.pink = create_image_texture(texture_load("pink.jpg", GL_LINEAR));
    texture_store.teal = create_image_texture(texture_load("teal.jpg", GL_LINEAR));
    texture_store.yellow = create_image_texture(texture_load("yellow.jpg", GL_LINEAR));
    texture_store.white = create_image_texture(texture_load("white.jpg", GL_LINEAR));

    {
        GLuint img = texture_load("map.png", GL_NEAREST);
        texture_store.map = create_image_texture(img);
    }

    {
        GLuint img = texture_load("tile.png", GL_NEAREST);
        texture_store.tile = create_image_texture(img);
    }

    load_unit_textures("worker", 4,
                       texture_store.worker_down, texture_store.worker_down_d,
                       texture_store.worker_side, texture_store.worker_up,
                       texture_store.worker_up_d);

    load_unit_textures("infantry", 4,
                       texture_store.infantry_down, texture_store.infantry_down_d,
                       texture_store.infantry_side, texture_store.infantry_up,
                       texture_store.infantry_up_d);

    load_unit_textures("elite-soldier", 4,
                       texture_store.elite_infantry_down, texture_store.elite_infantry_down_d,
                       texture_store.elite_infantry_side, texture_store.elite_infantry_up,
                       texture_store.elite_infantry_up_d);

    load_unit_textures("tank", 2,
                       texture_store.tank_down, texture_store.tank_down_d,
                       texture_store.tank_side, texture_store.tank_up,
                       texture_store.tank_up_d);

    load_unit_textures("helicopter", 2,
                       texture_store.helicopter_down, texture_store.helicopter_down_d,
                       texture_store.helicopter_side, texture_store.helicopter_up,
                       texture_store.helicopter_up_d);

    {
        GLuint img1 = texture_load("barracks/1.png", GL_NEAREST);
        texture_store.barracks[0] = create_image_texture(img1);

        GLuint img2 = texture_load("barracks/2.png", GL_NEAREST);
        texture_store.barracks[1] = create_image_texture(img2);

        GLuint img3 = texture_load("barracks/3.png", GL_NEAREST);
        texture_store.barracks[2] = create_image_texture(img3);

        GLuint img4 = texture_load("barracks/4.png", GL_NEAREST);
        texture_store.barracks[3] = create_image_texture(img4);

        GLuint img5 = texture_load("barracks/5.png", GL_NEAREST);
        texture_store.barracks[4] = create_image_texture(img5);

        GLuint img6 = texture_load("barracks/6.png", GL_NEAREST);
        texture_store.barracks[5] = create_image_texture(img6);
    }

    {
        GLuint img1 = texture_load("factory/1.png", GL_NEAREST);
        texture_store.factory[0] = create_image_texture(img1);

        GLuint img2 = texture_load("factory/2.png", GL_NEAREST);
        texture_store.factory[1] = create_image_texture(img2);

        GLuint img3 = texture_load("factory/3.png", GL_NEAREST);
        texture_store.factory[2] = create_image_texture(img3);

        GLuint img4 = texture_load("factory/4.png", GL_NEAREST);
        texture_store.factory[3] = create_image_texture(img4);

        GLuint img5 = texture_load("factory/5.png", GL_NEAREST);
        texture_store.factory[4] = create_image_texture(img5);

        GLuint img6 = texture_load("factory/6.png", GL_NEAREST);
        texture_store.factory[5] = create_image_texture(img6);
    }

    {
        GLuint img1 = texture_load("heliport/1.png", GL_NEAREST);
        texture_store.heliport[0] = create_image_texture(img1);

        GLuint img2 = texture_load("heliport/2.png", GL_NEAREST);
        texture_store.heliport[1] = create_image_texture(img2);

        GLuint img3 = texture_load("heliport/3.png", GL_NEAREST);
        texture_store.heliport[2] = create_image_texture(img3);

        GLuint img4 = texture_load("heliport/4.png", GL_NEAREST);
        texture_store.heliport[3] = create_image_texture(img4);

        GLuint img5 = texture_load("heliport/5.png", GL_NEAREST);
        texture_store.heliport[4] = create_image_texture(img5);

        GLuint img6 = texture_load("heliport/6.png", GL_NEAREST);
        texture_store.heliport[5] = create_image_texture(img6);
    }

    {
        GLuint img1 = texture_load("command-center/1.png", GL_NEAREST);
        texture_store.command_center[0] = create_image_texture(img1);

        GLuint img2 = texture_load("command-center/2.png", GL_NEAREST);
        texture_store.command_center[1] = create_image_texture(img2);

        GLuint img3 = texture_load("command-center/3.png", GL_NEAREST);
        texture_store.command_center[2] = create_image_texture(img3);

        GLuint img4 = texture_load("command-center/4.png", GL_NEAREST);
        texture_store.command_center[3] = create_image_texture(img4);

        GLuint img5 = texture_load("command-center/5.png", GL_NEAREST);
        texture_store.command_center[4] = create_image_texture(img5);

        GLuint img6 = texture_load("command-center/6.png", GL_NEAREST);
        texture_store.command_center[5] = create_image_texture(img6);
    }

    {
        GLuint img1 = texture_load("turret/1.png", GL_NEAREST);
        texture_store.turret[0] = create_image_texture(img1);

        GLuint img2 = texture_load("turret/2.png", GL_NEAREST);
        texture_store.turret[1] = create_image_texture(img2);

        GLuint img3 = texture_load("turret/3.png", GL_NEAREST);
        texture_store.turret[2] = create_image_texture(img3);

        GLuint img4 = texture_load("turret/4.png", GL_NEAREST);
        texture_store.turret[3] = create_image_texture(img4);

        GLuint img5 = texture_load("turret/5.png", GL_NEAREST);
        texture_store.turret[4] = create_image_texture(img5);

        GLuint img6 = texture_load("turret/6.png", GL_NEAREST);
        texture_store.turret[5] = create_image_texture(img6);

        GLuint img7 = texture_load("turret/7.png", GL_NEAREST);
        texture_store.turret[6] = create_image_texture(img7);

        GLuint img8 = texture_load("turret/8.png", GL_NEAREST);
        texture_store.turret[7] = create_image_texture(img8);
    }

    {
        GLuint img = texture_load("barrier.png", GL_NEAREST);
        texture_store.barrier = create_image_texture(img);
    }

    {
        GLuint img1 = texture_load("unit-selection/1.png", GL_NEAREST);
        texture_store.unit_selection[0] = create_image_texture(img1);

        GLuint img2 = texture_load("unit-selection/2.png", GL_NEAREST);
        texture_store.unit_selection[1] = create_image_texture(img2);

        GLuint img3 = texture_load("unit-selection/3.png", GL_NEAREST);
        texture_store.unit_selection[2] = create_image_texture(img3);

        GLuint img4 = texture_load("unit-selection/4.png", GL_NEAREST);
        texture_store.unit_selection[3] = create_image_texture(img4);
    }

    {
        GLuint img1 = texture_load("building-selection/1.png", GL_NEAREST);
        texture_store.building_selection[0] = create_image_texture(img1);

        GLuint img2 = texture_load("building-selection/2.png", GL_NEAREST);
        texture_store.building_selection[1] = create_image_texture(img2);

        GLuint img3 = texture_load("building-selection/3.png", GL_NEAREST);
        texture_store.building_selection[2] = create_image_texture(img3);

        GLuint img4 = texture_load("building-selection/4.png", GL_NEAREST);
        texture_store.building_selection[3] = create_image_texture(img4);
    }

    {
        GLuint img1 = texture_load("vehicle-selection/1.png", GL_NEAREST);
        texture_store.vehicle_selection[0] = create_image_texture(img1);

        GLuint img2 = texture_load("vehicle-selection/2.png", GL_NEAREST);
        texture_store.vehicle_selection[1] = create_image_texture(img2);

        GLuint img3 = texture_load("vehicle-selection/3.png", GL_NEAREST);
        texture_store.vehicle_selection[2] = create_image_texture(img3);

        GLuint img4 = texture_load("vehicle-selection/4.png", GL_NEAREST);
        texture_store.vehicle_selection[3] = create_image_texture(img4);

        GLuint img5 = texture_load("vehicle-selection/5.png", GL_NEAREST);
        texture_store.vehicle_selection[4] = create_image_texture(img5);
    }

    {
        GLuint img1 = texture_load("refinery/1.png", GL_NEAREST);
        texture_store.refinery[0] = create_image_texture(img1);

        GLuint img2 = texture_load("refinery/2.png", GL_NEAREST);
        texture_store.refinery[1] = create_image_texture(img2);

        GLuint img3 = texture_load("refinery/3.png", GL_NEAREST);
        texture_store.refinery[2] = create_image_texture(img3);
    }

    {
        GLuint img1 = texture_load("supply-depot/1.png", GL_NEAREST);
        texture_store.supply_depot[0] = create_image_texture(img1);

        GLuint img2 = texture_load("supply-depot/2.png", GL_NEAREST);
        texture_store.supply_depot[1] = create_image_texture(img2);
    }

    {
        GLuint img1 = texture_load("explosion/1.png", GL_NEAREST);
        texture_store.explosion[0] = create_image_texture(img1);

        GLuint img2 = texture_load("explosion/2.png", GL_NEAREST);
        texture_store.explosion[1] = create_image_texture(img2);

        GLuint img3 = texture_load("explosion/3.png", GL_NEAREST);
        texture_store.explosion[2] = create_image_texture(img3);

        GLuint img4 = texture_load("explosion/4.png", GL_NEAREST);
        texture_store.explosion[3] = create_image_texture(img4);

        GLuint img5 = texture_load("explosion/5.png", GL_NEAREST);
        texture_store.explosion[4] = create_image_texture(img5);

        GLuint img6 = texture_load("explosion/6.png", GL_NEAREST);
        texture_store.explosion[5] = create_image_texture(img6);

        GLuint img7 = texture_load("explosion/7.png", GL_NEAREST);
        texture_store.explosion[6] = create_image_texture(img7);

        GLuint img8 = texture_load("explosion/8.png", GL_NEAREST);
        texture_store.explosion[7] = create_image_texture(img8);

        GLuint img9 = texture_load("explosion/9.png", GL_NEAREST);
        texture_store.explosion[8] = create_image_texture(img9);
    }

    {
        GLuint img1 = texture_load("bullet/1.png", GL_NEAREST);
        texture_store.bullet[0] = create_image_texture(img1);

        GLuint img2 = texture_load("bullet/2.png", GL_NEAREST);
        texture_store.bullet[1] = create_image_texture(img2);

        GLuint img3 = texture_load("bullet/3.png", GL_NEAREST);
        texture_store.bullet[2] = create_image_texture(img3);

        GLuint img4 = texture_load("bullet/4.png", GL_NEAREST);
        texture_store.bullet[3] = create_image_texture(img4);

        GLuint img5 = texture_load("bullet/5.png", GL_NEAREST);
        texture_store.bullet[4] = create_image_texture(img5);
    }

    {
        GLuint img1 = texture_load("buttons/1-square.png", GL_NEAREST);
        texture_store.buttons_square[0] = create_image_texture(img1);

        GLuint img2 = texture_load("buttons/2-square.png", GL_NEAREST);
        texture_store.buttons_square[1] = create_image_texture(img2);
    }

    {
        GLuint img1 = texture_load("health-bar/1.png", GL_NEAREST);
        texture_store.health_bar[0] = create_image_texture(img1);

        GLuint img2 = texture_load("health-bar/2.png", GL_NEAREST);
        texture_store.health_bar[1] = create_image_texture(img2);

        GLuint img3 = texture_load("health-bar/3.png", GL_NEAREST);
        texture_store.health_bar[2] = create_image_texture(img3);

        GLuint img4 = texture_load("health-bar/4.png", GL_NEAREST);
        texture_store.health_bar[3] = create_image_texture(img4);

        GLuint img5 = texture_load("health-bar/5.png", GL_NEAREST);
        texture_store.health_bar[4] = create_image_texture(img5);
    }

    load_textures("under-construction", 19, texture_store.under_construction);

    texture_store.ui_hud_panel = create_image_texture(texture_load("hud-panel.png", GL_NEAREST));
}
