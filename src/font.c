#include "font.h"

#include <string.h>
#include "shader.h"
#include "texture.h"
#include "array.h"

struct font_tex_coord {
    float x0, y0, x1, y1;
};

struct font_char {
    int x;
    int y;
    int width;
    int height;
    int x_offset;
    int y_offset;
    int x_advance;
};

struct font {
    struct font_char chars[256];
};

static struct font f = {
    .chars[32] = {.x = 1, .y = 28, .width = 0, .height = 0, .x_offset = 0, .y_offset = 43, .x_advance = 21},
    .chars[33] = {.x = 2, .y = 1, .width = 6, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 11},
    .chars[34] = {.x = 9, .y = 1, .width = 16, .height = 11, .x_offset = 1, .y_offset = 16, .x_advance = 21},
    .chars[35] = {.x = 26, .y = 1, .width = 27, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 32},
    .chars[36] = {.x = 54, .y = 1, .width = 22, .height = 32, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[37] = {.x = 77, .y = 1, .width = 27, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 32},
    .chars[38] = {.x = 105, .y = 1, .width = 27, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 32},
    .chars[39] = {.x = 133, .y = 1, .width = 6, .height = 11, .x_offset = 1, .y_offset = 16, .x_advance = 11},
    .chars[40] = {.x = 140, .y = 1, .width = 11, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 16},
    .chars[41] = {.x = 152, .y = 1, .width = 11, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 16},
    .chars[42] = {.x = 164, .y = 1, .width = 16, .height = 16, .x_offset = 1, .y_offset = 16, .x_advance = 21},
    .chars[43] = {.x = 181, .y = 7, .width = 16, .height = 16, .x_offset = 1, .y_offset = 22, .x_advance = 21},
    .chars[44] = {.x = 198, .y = 23, .width = 11, .height = 10, .x_offset = 1, .y_offset = 38, .x_advance = 16},
    .chars[45] = {.x = 210, .y = 12, .width = 16, .height = 5, .x_offset = 1, .y_offset = 27, .x_advance = 21},
    .chars[46] = {.x = 227, .y = 23, .width = 6, .height = 5, .x_offset = 1, .y_offset = 38, .x_advance = 11},
    .chars[47] = {.x = 234, .y = 1, .width = 27, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 32},
    .chars[48] = {.x = 262, .y = 1, .width = 21, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[49] = {.x = 284, .y = 1, .width = 11, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 16},
    .chars[50] = {.x = 296, .y = 1, .width = 22, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[51] = {.x = 319, .y = 1, .width = 22, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[52] = {.x = 342, .y = 1, .width = 22, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[53] = {.x = 365, .y = 1, .width = 22, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[54] = {.x = 388, .y = 1, .width = 21, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[55] = {.x = 410, .y = 1, .width = 22, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[56] = {.x = 433, .y = 1, .width = 21, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[57] = {.x = 455, .y = 1, .width = 21, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[58] = {.x = 477, .y = 7, .width = 6, .height = 16, .x_offset = 1, .y_offset = 22, .x_advance = 11},
    .chars[59] = {.x = 484, .y = 7, .width = 6, .height = 21, .x_offset = 1, .y_offset = 22, .x_advance = 11},
    .chars[60] = {.x = 491, .y = 1, .width = 16, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 21},
    .chars[61] = {.x = 1, .y = 40, .width = 16, .height = 16, .x_offset = 1, .y_offset = 22, .x_advance = 21},
    .chars[62] = {.x = 18, .y = 34, .width = 16, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 21},
    .chars[63] = {.x = 35, .y = 34, .width = 22, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[64] = {.x = 58, .y = 34, .width = 27, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 32},
    .chars[65] = {.x = 86, .y = 34, .width = 21, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[66] = {.x = 108, .y = 34, .width = 21, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[67] = {.x = 130, .y = 34, .width = 16, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 21},
    .chars[68] = {.x = 147, .y = 34, .width = 21, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[69] = {.x = 169, .y = 34, .width = 16, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 21},
    .chars[70] = {.x = 186, .y = 34, .width = 16, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 21},
    .chars[71] = {.x = 203, .y = 34, .width = 21, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[72] = {.x = 225, .y = 34, .width = 22, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[73] = {.x = 248, .y = 34, .width = 16, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 21},
    .chars[74] = {.x = 265, .y = 34, .width = 22, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[75] = {.x = 288, .y = 34, .width = 22, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[76] = {.x = 311, .y = 34, .width = 16, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 21},
    .chars[77] = {.x = 328, .y = 34, .width = 27, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 32},
    .chars[78] = {.x = 356, .y = 34, .width = 22, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[79] = {.x = 379, .y = 34, .width = 21, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[80] = {.x = 401, .y = 34, .width = 21, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[81] = {.x = 423, .y = 34, .width = 21, .height = 32, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[82] = {.x = 445, .y = 34, .width = 21, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[83] = {.x = 467, .y = 34, .width = 22, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[84] = {.x = 490, .y = 34, .width = 16, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 21},
    .chars[85] = {.x = 1, .y = 67, .width = 22, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[86] = {.x = 24, .y = 67, .width = 22, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[87] = {.x = 47, .y = 67, .width = 27, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 32},
    .chars[88] = {.x = 75, .y = 67, .width = 22, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[89] = {.x = 98, .y = 67, .width = 22, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[90] = {.x = 121, .y = 67, .width = 16, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 21},
    .chars[91] = {.x = 138, .y = 67, .width = 11, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 16},
    .chars[92] = {.x = 150, .y = 67, .width = 27, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 32},
    .chars[93] = {.x = 178, .y = 67, .width = 11, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 16},
    .chars[94] = {.x = 190, .y = 67, .width = 16, .height = 11, .x_offset = 1, .y_offset = 16, .x_advance = 21},
    .chars[95] = {.x = 207, .y = 89, .width = 22, .height = 5, .x_offset = 1, .y_offset = 38, .x_advance = 27},
    .chars[96] = {.x = 230, .y = 67, .width = 11, .height = 11, .x_offset = 1, .y_offset = 16, .x_advance = 16},
    .chars[97] = {.x = 242, .y = 73, .width = 21, .height = 21, .x_offset = 1, .y_offset = 22, .x_advance = 27},
    .chars[98] = {.x = 264, .y = 67, .width = 21, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[99] = {.x = 286, .y = 73, .width = 16, .height = 21, .x_offset = 1, .y_offset = 22, .x_advance = 21},
    .chars[100] = {.x = 303, .y = 67, .width = 21, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[101] = {.x = 325, .y = 73, .width = 20, .height = 21, .x_offset = 1, .y_offset = 22, .x_advance = 27},
    .chars[102] = {.x = 346, .y = 67, .width = 16, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 21},
    .chars[103] = {.x = 363, .y = 73, .width = 21, .height = 32, .x_offset = 1, .y_offset = 22, .x_advance = 27},
    .chars[104] = {.x = 385, .y = 67, .width = 22, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[105] = {.x = 408, .y = 67, .width = 6, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 11},
    .chars[106] = {.x = 415, .y = 67, .width = 11, .height = 38, .x_offset = 1, .y_offset = 16, .x_advance = 16},
    .chars[107] = {.x = 427, .y = 67, .width = 22, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 27},
    .chars[108] = {.x = 450, .y = 67, .width = 6, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 11},
    .chars[109] = {.x = 457, .y = 73, .width = 27, .height = 21, .x_offset = 1, .y_offset = 22, .x_advance = 32},
    .chars[110] = {.x = 485, .y = 73, .width = 22, .height = 21, .x_offset = 1, .y_offset = 22, .x_advance = 27},
    .chars[111] = {.x = 1, .y = 112, .width = 21, .height = 21, .x_offset = 1, .y_offset = 22, .x_advance = 27},
    .chars[112] = {.x = 23, .y = 112, .width = 21, .height = 32, .x_offset = 1, .y_offset = 22, .x_advance = 27},
    .chars[113] = {.x = 45, .y = 112, .width = 21, .height = 32, .x_offset = 1, .y_offset = 22, .x_advance = 27},
    .chars[114] = {.x = 67, .y = 112, .width = 16, .height = 21, .x_offset = 1, .y_offset = 22, .x_advance = 21},
    .chars[115] = {.x = 84, .y = 112, .width = 22, .height = 21, .x_offset = 1, .y_offset = 22, .x_advance = 27},
    .chars[116] = {.x = 107, .y = 106, .width = 16, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 21},
    .chars[117] = {.x = 124, .y = 112, .width = 22, .height = 21, .x_offset = 1, .y_offset = 22, .x_advance = 27},
    .chars[118] = {.x = 147, .y = 112, .width = 22, .height = 21, .x_offset = 1, .y_offset = 22, .x_advance = 27},
    .chars[119] = {.x = 170, .y = 112, .width = 27, .height = 21, .x_offset = 1, .y_offset = 22, .x_advance = 32},
    .chars[120] = {.x = 198, .y = 112, .width = 16, .height = 21, .x_offset = 1, .y_offset = 22, .x_advance = 21},
    .chars[121] = {.x = 215, .y = 112, .width = 22, .height = 32, .x_offset = 1, .y_offset = 22, .x_advance = 27},
    .chars[122] = {.x = 238, .y = 112, .width = 22, .height = 21, .x_offset = 1, .y_offset = 22, .x_advance = 27},
    .chars[123] = {.x = 261, .y = 106, .width = 16, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 21},
    .chars[124] = {.x = 278, .y = 106, .width = 6, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 11},
    .chars[125] = {.x = 285, .y = 106, .width = 16, .height = 27, .x_offset = 1, .y_offset = 16, .x_advance = 21},
    .chars[126] = {.x = 302, .y = 106, .width = 22, .height = 11, .x_offset = 1, .y_offset = 16, .x_advance = 27},
};

static struct font_shader {
    GLuint program;
    GLuint mvp_mat_location;
    GLuint color_location;
    GLuint font_texture_location;
    GLuint position_location;
    GLuint texture_coord_location;
} font_shader;

#define FONT_TEX_COORD(x, y) {.x0 = x * 32.0f / 1024.0f, .y0 = y * 64.0f / 1024.0f, .x1 = (x + 1) * 32.0f / 1024.0f, .y1 = (y + 1) * 64.0f / 1024.0f}

static struct font_tex_coord font_tex_coords[256] = {
        ['a'] = FONT_TEX_COORD(0, 1),
        ['b'] = FONT_TEX_COORD(1, 1),
        ['c'] = FONT_TEX_COORD(2, 1),
        ['d'] = FONT_TEX_COORD(3, 1),
        ['e'] = FONT_TEX_COORD(4, 1),
        ['f'] = FONT_TEX_COORD(5, 1),
        ['g'] = FONT_TEX_COORD(6, 1),
        ['h'] = FONT_TEX_COORD(7, 1),
        ['i'] = FONT_TEX_COORD(8, 1),
        ['j'] = FONT_TEX_COORD(9, 1),
        ['k'] = FONT_TEX_COORD(10, 1),
        ['l'] = FONT_TEX_COORD(11, 1),
        ['m'] = FONT_TEX_COORD(12, 1),
        ['n'] = FONT_TEX_COORD(13, 1),
        ['o'] = FONT_TEX_COORD(14, 1),
        ['p'] = FONT_TEX_COORD(15, 1),
        ['q'] = FONT_TEX_COORD(16, 1),
        ['r'] = FONT_TEX_COORD(17, 1),
        ['s'] = FONT_TEX_COORD(18, 1),
        ['t'] = FONT_TEX_COORD(19, 1),
        ['u'] = FONT_TEX_COORD(20, 1),
        ['v'] = FONT_TEX_COORD(21, 1),
        ['w'] = FONT_TEX_COORD(22, 1),
        ['x'] = FONT_TEX_COORD(23, 1),
        ['y'] = FONT_TEX_COORD(24, 1),
        ['z'] = FONT_TEX_COORD(25, 1),

        ['A'] = FONT_TEX_COORD(0, 2),
        ['B'] = FONT_TEX_COORD(1, 2),
        ['C'] = FONT_TEX_COORD(2, 2),
        ['D'] = FONT_TEX_COORD(3, 2),
        ['E'] = FONT_TEX_COORD(4, 2),
        ['F'] = FONT_TEX_COORD(5, 2),
        ['G'] = FONT_TEX_COORD(6, 2),
        ['H'] = FONT_TEX_COORD(7, 2),
        ['I'] = FONT_TEX_COORD(8, 2),
        ['J'] = FONT_TEX_COORD(9, 2),
        ['K'] = FONT_TEX_COORD(10, 2),
        ['L'] = FONT_TEX_COORD(11, 2),
        ['M'] = FONT_TEX_COORD(12, 2),
        ['N'] = FONT_TEX_COORD(13, 2),
        ['O'] = FONT_TEX_COORD(14, 2),
        ['P'] = FONT_TEX_COORD(15, 2),
        ['Q'] = FONT_TEX_COORD(16, 2),
        ['R'] = FONT_TEX_COORD(17, 2),
        ['S'] = FONT_TEX_COORD(18, 2),
        ['T'] = FONT_TEX_COORD(19, 2),
        ['U'] = FONT_TEX_COORD(20, 2),
        ['V'] = FONT_TEX_COORD(21, 2),
        ['W'] = FONT_TEX_COORD(22, 2),
        ['X'] = FONT_TEX_COORD(23, 2),
        ['Y'] = FONT_TEX_COORD(24, 2),
        ['Z'] = FONT_TEX_COORD(25, 2),

        ['1'] = FONT_TEX_COORD(0, 0),
        ['2'] = FONT_TEX_COORD(1, 0),
        ['3'] = FONT_TEX_COORD(2, 0),
        ['4'] = FONT_TEX_COORD(3, 0),
        ['5'] = FONT_TEX_COORD(4, 0),
        ['6'] = FONT_TEX_COORD(5, 0),
        ['7'] = FONT_TEX_COORD(6, 0),
        ['8'] = FONT_TEX_COORD(7, 0),
        ['9'] = FONT_TEX_COORD(8, 0),
        ['0'] = FONT_TEX_COORD(9, 0),
        ['.'] = FONT_TEX_COORD(10, 0),
        ['-'] = FONT_TEX_COORD(11, 0),
        [','] = FONT_TEX_COORD(12, 0),
        ['('] = FONT_TEX_COORD(13, 0),
        [')'] = FONT_TEX_COORD(14, 0),
        ['<'] = FONT_TEX_COORD(15, 0),
        ['>'] = FONT_TEX_COORD(16, 0),
        ['_'] = FONT_TEX_COORD(17, 0),
        [':'] = FONT_TEX_COORD(18, 0),
        ['/'] = FONT_TEX_COORD(19, 0),
        ['+'] = FONT_TEX_COORD(20, 0),
        [' '] = FONT_TEX_COORD(0, 20),
};

static GLuint font_texture;

/*
static struct font_tex_coord create_font_tex_coord(int x, int y) {
    return (struct font_tex_coord){
        .x0 = x * 32.0f / 1024.0f,
        .y0 = y * 64.0f / 1024.0f,
        .x1 = (x + 1) * 32.0f / 1024.0f,
        .y1 = (y + 1) * 64.0f / 1024.0f};
}
*/

void font_mesh_init(struct font_mesh *mesh) {
    glGenBuffers(1, &mesh->position_vbo);
    glGenBuffers(1, &mesh->texture_coord_vbo);
}

void font_mesh_change_string(struct font_mesh *font_mesh, const char *string) {
    array_vec2_t position_coords_array;
    array_init(&position_coords_array);

    array_vec2_t texture_coords_array;
    array_init(&texture_coords_array);

    int cur_x = 0;
    int cur_y = 0;
    int max_height = 0;

    int char_index = 0;
    while (string[char_index]) {
        int font_char_index = string[char_index];
        struct font_char c = f.chars[font_char_index];

        float px0 = (float)(cur_x + c.x_offset);
        float py0 = (float) (cur_y - c.y_offset);
        float px1 = (float) (cur_x + c.x_offset + c.width);
        float py1 = (float) (cur_y - c.y_offset - c.height);

        vec2 position_coords[6];
        position_coords[0] = V2(px0, py0);
        position_coords[1] = V2(px0, py1);
        position_coords[2] = V2(px1, py1);
        position_coords[3] = V2(px0, py0);
        position_coords[4] = V2(px1, py1);
        position_coords[5] = V2(px1, py0);

        float tx0 = c.x / 512.0f;
        float tx1 = (c.x + c.width) / 512.0f;
        float ty0 = c.y / 256.0f;
        float ty1 = (c.y + c.height) / 256.0f;

        vec2 texture_coords[6];
        texture_coords[0] = V2(tx0, ty0);
        texture_coords[1] = V2(tx0, ty1);
        texture_coords[2] = V2(tx1, ty1);
        texture_coords[3] = V2(tx0, ty0);
        texture_coords[4] = V2(tx1, ty1);
        texture_coords[5] = V2(tx1, ty0);

        if (c.height > max_height) {
            max_height = c.height;
        }

        for (int i = 0; i < 6; i++) {
            array_push(&position_coords_array, position_coords[i]);
            array_push(&texture_coords_array, texture_coords[i]);
        }

        cur_x += c.x_advance;
        char_index++;
    }

    font_mesh->width = (float) cur_x;
    font_mesh->height = (float) max_height;
    font_mesh->num_characters = char_index;

    glBindBuffer(GL_ARRAY_BUFFER, font_mesh->position_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * position_coords_array.length, position_coords_array.data, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, font_mesh->texture_coord_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * texture_coords_array.length, texture_coords_array.data, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(1);

    array_deinit(&position_coords_array);
    array_deinit(&texture_coords_array);
}

void font_draw_string(const char *string, mat4 mvp_mat, vec4 color,
                      enum font_justify_horizontal horizontal_justify,
                      enum font_justify_vertical vertical_justify) {
    static bool initialized = false;
    static struct font_mesh font_mesh;
    if (!initialized) {
        initialized = true;
        font_mesh_init(&font_mesh);

        {
            static const char *font_vertex_shader =
                "in vec2 vertex_position;"
                "in vec2 vertex_texture_coord;"
                "uniform mat4 mvp_mat;"
                "out vec2 fragment_texture_coord;"

                "void main() {"
                "   fragment_texture_coord = vertex_texture_coord;"
                "   gl_Position = mvp_mat * vec4(vertex_position, 0.0, 1.0);"
                "}";

            static const char *font_fragment_shader =
                "precision highp float;"
                "uniform vec4 color;"
                "uniform sampler2D font_texture;"
                "in vec2 fragment_texture_coord;"
                "out vec4 g_fragment_color;"

                "void main() {"
                "   float a = texture(font_texture, fragment_texture_coord).a;"
                "   g_fragment_color = vec4(color.xyz, color.w  * a);"
                "}";

            GLint program = shader_create_program(font_vertex_shader, font_fragment_shader);
            GLint mvp_mat_loc = glGetUniformLocation(program, "mvp_mat");
            GLint color_loc = glGetUniformLocation(program, "color");
            GLint font_texture_loc = glGetUniformLocation(program, "font_texture");
            GLint position_loc = glGetAttribLocation(program, "vertex_position");
            GLint texture_coord_loc = glGetAttribLocation(program, "vertex_texture_coord");

            font_shader.program = (GLuint)program;
            font_shader.mvp_mat_location = (GLuint)mvp_mat_loc;
            font_shader.color_location = (GLuint)color_loc;
            font_shader.font_texture_location = (GLuint)font_texture_loc;
            font_shader.position_location = (GLuint)position_loc;
            font_shader.texture_coord_location = (GLuint)texture_coord_loc;
        }

        font_texture = texture_load("04b03_font_atlas.png", GL_LINEAR);
    }

    if (!*string) {
        return;
    }
    font_mesh_change_string(&font_mesh, string);

    float xt0 = 0.0f, yt0 = 0.0f, zt0 = 0.0f;
    switch (horizontal_justify) {
        case FONT_JUSTIFY_HORIZONTAL_CENTER: {
            xt0 = -0.5f * font_mesh.width;
            break;
        }

        case FONT_JUSTIFY_HORIZONTAL_RIGHT: {
            xt0 = -font_mesh.width;
            break;
        }

        case FONT_JUSTIFY_HORIZONTAL_LEFT: {
            xt0 = 0.0f;
            break;
        }
    }

    switch (vertical_justify) {
        case FONT_JUSTIFY_VERTICAL_CENTER: {
            yt0 = -0.5f * font_mesh.height;
            break;
        }

        case FONT_JUSTIFY_VERTICAL_BOTTOM: {
            yt0 = -(10.0f / 64.0f) * font_mesh.height;
            break;
        }

        case FONT_JUSTIFY_VERTICAL_TOP: {
            yt0 = -(1.0f - 14.0f / 64.0f) * font_mesh.height;
            break;
        }
    }
    zt0 = 0.0f;

    mat4 mvp = mat4_multiply_n(2, mvp_mat, mat4_translation(V3(xt0, yt0, zt0)));

    glUseProgram(font_shader.program);
    glUniformMatrix4fv(font_shader.mvp_mat_location, 1, GL_TRUE, mvp.m);
    glUniform4f(font_shader.color_location, color.x, color.y, color.z, color.w);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, font_texture);
    glBindBuffer(GL_ARRAY_BUFFER, font_mesh.position_vbo);
    glVertexAttribPointer(font_shader.position_location, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(font_shader.position_location);
    glBindBuffer(GL_ARRAY_BUFFER, font_mesh.texture_coord_vbo);
    glVertexAttribPointer(font_shader.texture_coord_location, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(font_shader.texture_coord_location);
    glDrawArrays(GL_TRIANGLES, 0, 6 * font_mesh.num_characters);
}
