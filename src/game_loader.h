#ifndef _GAME_LOADER_H
#define _GAME_LOADER_H

void game_loader_save(const char *filename);
void game_loader_load(const char *filename);

#endif
