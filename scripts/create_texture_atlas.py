from PIL import Image, ImageDraw
import glob, os


width = 2048
height = 2048
left = 0
top = 0
row_height = 0
texture_atlas = Image.new("RGBA", (width, height))
texture_atlas_data = open("../assets/texture_atlas.data", "w")
asset_list_file = open("../assets/asset_list","r")

for asset in asset_list_file.readlines():
    asset_filename = asset[0:-1] # Trim the new line at the end
    asset_image = Image.open("../assets/" + asset_filename)
    w = asset_image.size[0]
    h = asset_image.size[1]

    if (left + w > width):
        left = 0
        top += row_height
        row_height = 0

    if (h > row_height):
        row_height = h

    texture_atlas_data.write(asset_filename + " " + str(left) + " " + str(top) + " " + str(w) + " " + str(h) + "\n")
    texture_atlas.paste(asset_image, (left, top, left + w, top + h))

    left += w

asset_list_file.close()
texture_atlas_data.close()
texture_atlas.save("../assets/texture_atlas.png")
