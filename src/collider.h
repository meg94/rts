#ifndef _PHYSICS_COLLIDER_H
#define _PHYSICS_COLLIDER_H

#include <stdbool.h>

#include "array.h"
#include "maths.h"

typedef array_t(struct collider_id) array_collider_id_t;
typedef array_t(struct collider) array_collider_t;
typedef array_t(struct collider *) array_collider_ptr_t;
typedef array_t(struct collision_info) array_collision_info_t;

enum collider_type {
    COLLIDER_TYPE_CIRCLE,
    COLLIDER_TYPE_BOX,
    COLLIDER_TYPE_LINE_SEGMENT,
};

enum collider_category {
    COLLIDER_CATEGORY_BUILDING = (1 << 0),
    COLLIDER_CATEGORY_GROUND_UNIT = (1 << 1),
    COLLIDER_CATEGORY_AIR_UNIT = (1 << 2),
};

struct collision_info {
    struct collider *other_collider;
    vec2 point;
    vec2 normal;
    float depth;
    bool first_contact;
};

struct collider_id {
    uint32_t gen, idx;
};

struct collider {
    struct collider_id id;

    enum collider_type type;
    bool does_collide;
    vec2 position, velocity;
    bool is_dynamic;
    float mass;
    array_collision_info_t collision_info_array;

    union {
        struct {
            float radius;
        } circle;

        struct {
            float width, height;
        } box;

        struct {
            float thickness;
            vec2 p0, p1;
        } line_segment;
    };

    void *cp_body;
    void *cp_shape;
};

void collider_manager_init(void);
struct collider_id allocate_collider(void);
void delete_collider(struct collider_id id);
struct collider *get_collider_from_id(struct collider_id id);
void get_all_active_colliders(array_collider_id_t *colliders);

void set_collider_position(struct collider *collider, vec2 position);
void apply_force_to_collider(struct collider *collider, vec2 force);

struct collider_id create_circle_collider(bool is_dynamic,
                                          bool does_collide,
                                          float radius,
                                          float mass,
                                          enum collider_category category);
struct collider_id create_box_collider(bool is_dynamic,
                                       bool does_collide,
                                       float width,
                                       float height,
                                       float mass,
                                       enum collider_category category);
struct collider_id create_line_segment_collider(bool is_dynamic,
                                                bool does_collide,
                                                float mass,
                                                float thickness,
                                                vec2 p0,
                                                vec2 p1,
                                                enum collider_category category);

#endif
