#ifndef _UTIL_QUEUE_H
#define _UTIL_QUEUE_H

#include "array.h"

#define queue_init array_init
#define queue_deinit array_deinit
#define queue_push array_push
#define queue_head array_first
#define queue_empty(q) ((q)->length == 0)
#define queue_pop(q) array_splice(q, 0, 1)

typedef array_vec2_t queue_vec2_t;
typedef array_int_t queue_int_t;

#endif
