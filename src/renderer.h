#ifndef _RENDERER_RENDERER_H
#define _RENDERER_RENDERER_H

//#define WINDOW_WIDTH (0.9 * 1920)
//#define WINDOW_HEIGHT (0.9 * 1080)

#include "array.h"
#include "maths.h"
#include "mesh.h"
#include "texture.h"

struct renderer_state {
	int window_width, window_height;

    mat4 proj_mat, isometric_mat;
    GLuint unit_fb, unit_fb_tex;

    bool clear_explored_fow;
    GLuint explored_fow_fb[3];
    GLuint explored_fow_tex[3];

    bool clear_revealed_fow;
    GLuint revealed_fow_fb[3];
    GLuint revealed_fow_tex[3];
} renderer_state;

void renderer_init(void);
void renderer_draw(void);

#endif
