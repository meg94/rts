#include <stdio.h>
#ifdef JS_BUILD
#include <emscripten/emscripten.h>
#endif

#include "IGL.h"
#include "collider.h"
#include "controls.h"
#include "editor.h"
#include "entity.h"
#include "game.h"
#include "mesh.h"
#include "physics.h"
#include "renderer.h"
#include "texture.h"
#include "ui.h"

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#define NK_IMPLEMENTATION
#define NK_GLFW_GL3_IMPLEMENTATION
#include "nuklear.h"
#include "nuklear_glfw_gl3.h"

static GLFWwindow *window;

static GLFWwindow *create_window(char *title, int width, int height) {
    GLFWwindow *window;

    if (!glfwInit()) {
        return NULL;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    //glfwWindowHint(GLFW_SAMPLES, 4);
    //window = glfwCreateWindow(width, height, title, glfwGetPrimaryMonitor(), NULL);
    window = glfwCreateWindow(width, height, title, NULL, NULL);
    if (!window) {
        return NULL;
    }

    glfwMakeContextCurrent(window);
    glViewport(0, 0, width, height);
    glfwSwapInterval(1);
    //glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK) {
        printf("Failed to setup GLEW\n");
        return NULL;
    }

    return window;
}

static void init(void) {
    int width = 1920;
    int height = 1080;
    window = create_window("test", width, height);
    if (!window) {
        printf("We've got problems\n");
        return;
    }

    controls_init(window);
    collider_manager_init();
    mesh_instance_manager_init();
    entity_manager_init();

    physics_init();
    renderer_init();
    ui_init();
    game_init();
    game_editor_init(window);
}

static void frame(float dt) {
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
    glClearColor(79.0f / 256.0f, 140.0f / 256.0f, 156.0f / 256.0f, 1.0f);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

    glfwGetWindowSize(window, &renderer_state.window_width, &renderer_state.window_height);

    controls_update(window);
    ui_update(dt);
    physics_update(dt);
    game_update(dt);
    game_editor_update(dt);

    renderer_draw();
    ui_draw();
    game_editor_draw();

    glfwPollEvents();
    glfwSwapBuffers(window);
}

int main(int argc, char **argv) {
    init();

#ifdef JS_BUILD
    emscripten_set_main_loop(frame, 0, 0);
#else

    while (!glfwWindowShouldClose(window)) {
        static double previous_time = 0.0f;
        static double accumulator = 0.0f;
        double current_time, delta;

        current_time = glfwGetTime();
        delta = current_time - previous_time;
        previous_time = current_time;
        accumulator += delta;
        while (accumulator > 1.0f / 62.0f) {
            frame(1.0f / 60.0f);
            accumulator -= 1.0f / 60.0f;
            if (accumulator < 0.0f) {
                accumulator = 0.0f;
            }
        }
    }
#endif
}
