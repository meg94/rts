#include "ui.h"

#include <assert.h>
#include <float.h>

#include "array.h"
#include "controls.h"
#include "editor.h"
#include "entity.h"
#include "game.h"
#include "parson.h"
#include "renderer.h"
#include "shader.h"

struct {
    array_entity_id_t entities;
    GLuint line_position_vbo;

    struct {
        GLuint position_vbo, color_vbo;
        array_vec2_t vertex_position_array;
        array_vec3_t vertex_color_array;
    } minimap;
} _ui_state;

void get_ui_entity_selection_points(enum entity_type entity_type,
                                    vec2 ui_position,
                                    vec2 *points) {
    memcpy(points, &ui_state.entity_selection_box[4 * entity_type],
           4 * sizeof(vec2));

    points[0] = vec2_scale(points[0], 4.0f / game_state.cam_size);
    points[1] = vec2_scale(points[1], 4.0f / game_state.cam_size);
    points[2] = vec2_scale(points[2], 4.0f / game_state.cam_size);
    points[3] = vec2_scale(points[3], 4.0f / game_state.cam_size);

    points[0] = vec2_add(ui_position, points[0]);
    points[1] = vec2_add(ui_position, points[1]);
    points[2] = vec2_add(ui_position, points[2]);
    points[3] = vec2_add(ui_position, points[3]);
}

vec2 game_to_ui_position(vec2 game_pos) {
    mat4 proj_mat;
    vec3 ui_position_3;
    vec2 ui_position;

    proj_mat = mat4_multiply_n(2, renderer_state.proj_mat,
                               renderer_state.isometric_mat);

    ui_position_3.x = game_pos.x;
    ui_position_3.y = game_pos.y;
    ui_position_3.z = 1.0f;
    ui_position_3 = vec3_apply_mat4(ui_position_3, 1.0f, proj_mat);

    ui_position.x = ui_position_3.x;
    ui_position.y = ui_position_3.y;

    ui_position.x *= 8.0f;
    ui_position.y *= 4.5f;

    return ui_position;
}

static vec2 ui_to_game_position(vec2 ui_pos) {
    mat4 proj_mat;
    vec3 game_pos_3;
    vec2 game_pos;

    proj_mat = mat4_multiply_n(2, renderer_state.proj_mat,
                               renderer_state.isometric_mat);
    proj_mat = mat4_inverse(proj_mat);

    ui_pos.x /= 8.0f;
    ui_pos.y /= 4.5f;

    game_pos_3.x = ui_pos.x;
    game_pos_3.y = ui_pos.y;
    game_pos_3.z = 1.0f;
    game_pos_3 = vec3_apply_mat4(game_pos_3, 1.0f, proj_mat);

    game_pos.x = game_pos_3.x;
    game_pos.y = game_pos_3.y;

    return game_pos;
}

static bool is_point_in_square(vec2 center, vec2 size, vec2 p) {
    return p.x > center.x - 0.5f * size.x &&
           p.x < center.x + 0.5f * size.x &&
           p.y > center.y - 0.5f * size.y &&
           p.y < center.y + 0.5f * size.y;
}

static void ui_draw_highlighted_string(const char *string,
                                       vec4 color,
                                       vec3 position,
                                       float size,
                                       enum font_justify_horizontal font_justify_horizontal) {
    vec3 scale;
    vec4 black;
    mat4 model_mat;
    enum font_justify_horizontal horiz;
    enum font_justify_vertical vert;

    scale = V3(size, size, 1.0f);
    black = V4(0.0f, 0.0f, 0.0f, 1.0f);
    horiz = font_justify_horizontal;
    vert = FONT_JUSTIFY_VERTICAL_CENTER;

    position.x += 0.01f;
    position.y -= 0.01f;
    position.z -= 0.01f;
    model_mat = mat4_multiply_n(3,
                                ui_state.proj_mat,
                                mat4_translation(position),
                                mat4_scale(scale));
    font_draw_string(string, model_mat, black, horiz, vert);

    position.x -= 0.01f;
    position.y += 0.01f;
    position.z += 0.01f;
    model_mat = mat4_multiply_n(3,
                                ui_state.proj_mat,
                                mat4_translation(position),
                                mat4_scale(scale));
    font_draw_string(string, model_mat, color, horiz, vert);
}

static void ui_draw_mesh(struct mesh *mesh, vec2 position, vec2 scale, float angle, struct texture texture, float opacity) {
    vec3 scale_vec;
    scale_vec.x = scale.x;
    scale_vec.y = scale.y;
    scale_vec.z = 1.0f;

    vec3 translation_vec;
    translation_vec.x = position.x;
    translation_vec.y = position.y;
    translation_vec.z = 1.0f;

    mat4 model_mat = mat4_multiply_n(3, mat4_translation(translation_vec), mat4_rotation_z(angle), mat4_scale(scale_vec));
    mat4 mvp_mat = mat4_multiply_n(2, ui_state.proj_mat, model_mat);

    switch (texture.type) {
        case TEXTURE_TYPE_IMAGE: {
            struct mesh_texture_shader *s = &shader_store.mesh_texture_shader;

            glUseProgram(s->program);
            glUniformMatrix4fv(s->mvp_mat_location, 1, GL_TRUE, mvp_mat.m);
            glUniform1i(s->texture_location, 0);
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, texture.image);
            glUniform1f(s->opacity_location, opacity);

            glBindBuffer(GL_ARRAY_BUFFER, mesh->position_vbo);
            glVertexAttribPointer(s->position_location, 3, GL_FLOAT, GL_FALSE, 0, NULL);
            glEnableVertexAttribArray(s->position_location);

            glBindBuffer(GL_ARRAY_BUFFER, mesh->texture_coord_vbo);
            glVertexAttribPointer(s->texture_coord_location, 2, GL_FLOAT, GL_FALSE, 0, NULL);
            glEnableVertexAttribArray(s->texture_coord_location);

            glDrawArrays(GL_TRIANGLES, 0, mesh->num_vertices);

            break;
        }

        case TEXTURE_TYPE_COLOR: {
            struct mesh_shader *s = &shader_store.mesh_shader;

            glUseProgram(s->program);
            glUniformMatrix4fv(s->mvp_mat_location, 1, GL_TRUE, mvp_mat.m);
            glUniform1f(s->opacity_location, opacity);
            glUniform1f(s->color_pct_location, 1.0f);
            glUniform1f(s->tex_pct_location, 0.0f);

            glUniform3f(s->color_location, texture.color.x, texture.color.y, texture.color.z);

            glBindBuffer(GL_ARRAY_BUFFER, mesh->position_vbo);
            glVertexAttribPointer(s->position_location, 3, GL_FLOAT, GL_FALSE, 0, NULL);
            glEnableVertexAttribArray(s->position_location);

            glBindBuffer(GL_ARRAY_BUFFER, mesh->texture_coord_vbo);
            glVertexAttribPointer(s->texture_coord_location, 2, GL_FLOAT, GL_FALSE, 0, NULL);
            glEnableVertexAttribArray(s->texture_coord_location);

            glDrawArrays(GL_TRIANGLES, 0, mesh->num_vertices);
            break;
        }

        case TEXTURE_TYPE_ATLAS_IMAGE: {
            struct mesh_texture_atlas_shader *s;
            struct texture_atlas_data atlas_data;

            s = &shader_store.mesh_texture_atlas_shader;
            atlas_data = texture.atlas_image;

            glUseProgram(s->program);
            glUniformMatrix4fv(s->mvp_mat_location, 1, GL_TRUE, mvp_mat.m);
            glUniform1i(s->texture_location, 0);
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, texture_store.texture_atlas.image);
            glUniform1f(s->opacity_location, opacity);
            glUniform3f(s->color_location, 0.0f, 0.0f, 0.0f);
            glUniform1f(s->color_pct_location, 0.0f);

            float px = ((float)atlas_data.x) / 2048.0f;
            float py = ((float)atlas_data.y) / 2048.0f;
            float sx = ((float)atlas_data.w) / 2048.0f;
            float sy = ((float)atlas_data.h) / 2048.0f;
            glUniform2f(s->vtc_p, px, py);
            glUniform2f(s->vtc_s, sx, sy);

            glBindBuffer(GL_ARRAY_BUFFER, mesh->position_vbo);
            glVertexAttribPointer(s->position_location, 3, GL_FLOAT, GL_FALSE, 0, NULL);
            glEnableVertexAttribArray(s->position_location);

            glBindBuffer(GL_ARRAY_BUFFER, mesh->texture_coord_vbo);
            glVertexAttribPointer(s->texture_coord_location, 2, GL_FLOAT, GL_FALSE, 0, NULL);
            glEnableVertexAttribArray(s->texture_coord_location);

            glDrawArrays(GL_TRIANGLES, 0, mesh->num_vertices);
            break;
        }
    }
}

static void draw_square(vec2 position, vec2 scale, vec3 color, float opacity) {
    struct mesh *mesh = &mesh_store.square_mesh;
    float angle = 0.0f;
    struct texture texture = create_color_texture(color);
    ui_draw_mesh(mesh, position, scale, angle, texture, opacity);
}

static vec2 get_ui_pos(vec2 screen_pos) {
    vec2 screen_size, ui_pos;

    screen_size = V2(16.0f, 9.0f);

    ui_pos.x = -0.5f * screen_size.x +
               screen_size.x * (screen_pos.x / renderer_state.window_width);
    ui_pos.y = 0.5f * screen_size.y -
               screen_size.y * (screen_pos.y / renderer_state.window_height);

    return ui_pos;
}

static void push_square_onto_vertex_array(vec2 pos, float w, float h,
                                          vec3 color,
                                          bool apply_isometric_proj_mat,
                                          array_vec2_t *vertex_position_array,
                                          array_vec3_t *vertex_color_array) {
    int i;
    vec2 p[6];

    p[0].x = pos.x - 0.5f * w;
    p[0].y = pos.y + 0.5f * h;

    p[1].x = pos.x - 0.5f * w;
    p[1].y = pos.y - 0.5f * h;

    p[2].x = pos.x + 0.5f * w;
    p[2].y = pos.y - 0.5f * h;

    p[3].x = pos.x - 0.5f * w;
    p[3].y = pos.y + 0.5f * h;

    p[4].x = pos.x + 0.5f * w;
    p[4].y = pos.y - 0.5f * h;

    p[5].x = pos.x + 0.5f * w;
    p[5].y = pos.y + 0.5f * h;

    if (apply_isometric_proj_mat) {
        for (i = 0; i < 6; i++) {
            vec3 p3 = V3(p[i].x, p[i].y, 1.0f);
            p3 = vec3_apply_mat4(p3, 1.0f, renderer_state.isometric_mat);
            p[i].x = p3.x;
            p[i].y = p3.y;
        }
    }

    array_push(vertex_position_array, p[0]);
    array_push(vertex_position_array, p[1]);
    array_push(vertex_position_array, p[2]);
    array_push(vertex_position_array, p[3]);
    array_push(vertex_position_array, p[4]);
    array_push(vertex_position_array, p[5]);

    array_push(vertex_color_array, color);
    array_push(vertex_color_array, color);
    array_push(vertex_color_array, color);
    array_push(vertex_color_array, color);
    array_push(vertex_color_array, color);
    array_push(vertex_color_array, color);
}

static void draw_minimap_panel() {
    array_vec2_t *vp_arr = &_ui_state.minimap.vertex_position_array;
    array_vec3_t *vc_arr = &_ui_state.minimap.vertex_color_array;

    array_clear(vp_arr);
    array_clear(vc_arr);

    //
    // Draw a black background
    //
    {
        vec2 pos;
        vec3 color;
        float w, h;

        pos.x = 0.5f * GAME_MAP_WIDTH;
        pos.y = 0.5f * GAME_MAP_WIDTH;

        w = GAME_MAP_WIDTH / 1.5f;
        h = GAME_MAP_WIDTH / 1.5f;

        color = V3(0.0f, 0.0f, 0.0f);

        push_square_onto_vertex_array(pos, w, h, color, false, vp_arr, vc_arr);
    }

    //
    // Draw the terrain
    //
    for (int row = 0; row < GAME_MAP_SIZE; row++) {
        for (int col = 0; col < GAME_MAP_SIZE; col++) {
            enum terrain_type tt;
            vec2 pos;
            float w;
            vec3 color;

            tt = game_state.terrain[col * GAME_MAP_SIZE + row];

            pos.x = (col + 0.5f) * GAME_TILE_WIDTH;
            pos.y = (row + 0.5f) * GAME_TILE_WIDTH;

            w = GAME_TILE_WIDTH;

            switch (tt) {
                case TERRAIN_TYPE_NOTHING:
                    color.x = 1.0f;
                    color.y = 1.0f;
                    color.z = 1.0f;
                    break;
                case TERRAIN_TYPE_WATER:
                    color.x = 79.0f / 256.0f;
                    color.y = 140.0f / 256.0f;
                    color.z = 156.0f / 256.0f;
                    break;
                case TERRAIN_TYPE_GRASS:
                    color.x = 96.0f / 256.0f;
                    color.y = 126.0f / 256.0f;
                    color.z = 65.0f / 256.0f;
                    break;
            }

            push_square_onto_vertex_array(pos, w, w, color, true, vp_arr, vc_arr);
        }
    }

    //
    // Draw the entities
    //
    {
        int i;
        struct entity_id entity_id;
        array_entity_id_t entities;

        array_init(&entities);
        get_all_active_entities_with_type(&entities, 0, NULL);
        array_foreach(&entities, entity_id, i) {
            struct entity *entity = get_entity_from_id(entity_id);
            if (!entity) {
                continue;
            }
            if (entity->minimap_width == 0.0f) {
                continue;
            }

            vec2 pos = V2(entity->position.x, entity->position.y);
            float w = entity->minimap_width;
            vec3 color = V3(0.0f, 0.0f, 0.0f);

            if (entity->team_num == 0) {
                color = V3(0.0f, 1.0f, 0.0f);
            } else if (entity->team_num == 1) {
                color = V3(1.0f, 0.0f, 0.0f);
            } else {
                color = V3(1.0f, 1.0f, 0.0f);
            }

            push_square_onto_vertex_array(pos, w, w, color, true, vp_arr, vc_arr);
        }
        array_deinit(&entities);
    }

    //
    // Calculate the mat to project from game coordinates
    // onto the minimap
    //
    mat4 mvp_mat = mat4_multiply_n(2,
                                   ui_state.proj_mat,
                                   ui_state.game_to_minimap_mat);

    {
        GLuint position_vbo = _ui_state.minimap.position_vbo;
        GLuint color_vbo = _ui_state.minimap.color_vbo;

        //
        // Load all the vertex data into OpenGL
        //
        glBindBuffer(GL_ARRAY_BUFFER, position_vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * vp_arr->length,
                     vp_arr->data, GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, color_vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * vc_arr->length,
                     vc_arr->data, GL_STATIC_DRAW);

        //
        // Draw the minimap
        //
        struct minimap_shader *s = &shader_store.minimap_shader;

        glUseProgram(s->program);
        glUniformMatrix4fv(s->mvp_mat_location, 1, GL_TRUE, mvp_mat.m);

        glBindBuffer(GL_ARRAY_BUFFER, position_vbo);
        glVertexAttribPointer(s->position_location, 2, GL_FLOAT,
                              GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(s->position_location);

        glBindBuffer(GL_ARRAY_BUFFER, color_vbo);
        glVertexAttribPointer(s->color_location, 3, GL_FLOAT,
                              GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(s->color_location);

        glDrawArrays(GL_TRIANGLES, 0, vp_arr->length);
    }

    //
    // Draw the fog of war
    //
    {
        vec3 position = V3(0.5f * GAME_MAP_WIDTH, 0.5f * GAME_MAP_WIDTH, 0.81f);
        vec3 scale = V3(GAME_MAP_WIDTH, GAME_MAP_WIDTH, 1.0f);

        mat4 model_mat = mat4_multiply_n(3,
                                         renderer_state.isometric_mat,
                                         mat4_translation(position),
                                         mat4_scale(scale));
        mat4 fow_mvp_mat = mat4_multiply_n(2,
                                           mvp_mat,
                                           model_mat);

        struct fog_of_war_shader *s = &shader_store.fog_of_war_shader;
        struct mesh *mesh = &mesh_store.square_mesh;

        glUseProgram(s->program);
        glUniformMatrix4fv(s->mvp_mat_location, 1, GL_TRUE, fow_mvp_mat.m);
        glUniform3f(s->color_location, 0.0f, 0.0f, 0.0f);
        glUniform1i(s->texture_location, 0);
        glActiveTexture(GL_TEXTURE0);

        glBindBuffer(GL_ARRAY_BUFFER, mesh->position_vbo);
        glVertexAttribPointer(s->position_location, 3,
                              GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(s->position_location);

        glBindBuffer(GL_ARRAY_BUFFER, mesh->texture_coord_vbo);
        glVertexAttribPointer(s->texture_coord_location, 2,
                              GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(s->texture_coord_location);

        glBindTexture(GL_TEXTURE_2D, renderer_state.revealed_fow_tex[1]);
        glDrawArrays(GL_TRIANGLES, 0, mesh->num_vertices);
    }

    {
        vec3 position = V3(0.5f * GAME_MAP_WIDTH, 0.5f * GAME_MAP_WIDTH, 0.82f);
        vec3 scale = V3(GAME_MAP_WIDTH, GAME_MAP_WIDTH, 1.0f);

        mat4 model_mat = mat4_multiply_n(3,
                                         renderer_state.isometric_mat,
                                         mat4_translation(position),
                                         mat4_scale(scale));
        mat4 fow_mvp_mat = mat4_multiply_n(2,
                                           mvp_mat,
                                           model_mat);

        struct fog_of_war_shader *s = &shader_store.fog_of_war_shader;
        struct mesh *mesh = &mesh_store.square_mesh;

        glUseProgram(s->program);
        glUniformMatrix4fv(s->mvp_mat_location, 1, GL_TRUE, fow_mvp_mat.m);
        glUniform3f(s->color_location, 0.0f, 0.0f, 0.0f);
        glUniform1i(s->texture_location, 0);
        glActiveTexture(GL_TEXTURE0);

        glBindBuffer(GL_ARRAY_BUFFER, mesh->position_vbo);
        glVertexAttribPointer(s->position_location, 3,
                              GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(s->position_location);

        glBindBuffer(GL_ARRAY_BUFFER, mesh->texture_coord_vbo);
        glVertexAttribPointer(s->texture_coord_location, 2,
                              GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(s->texture_coord_location);

        glBindTexture(GL_TEXTURE_2D, renderer_state.explored_fow_tex[1]);
        glDrawArrays(GL_TRIANGLES, 0, mesh->num_vertices);
    }

    array_clear(vp_arr);
    array_clear(vc_arr);

    //
    // Draw camera position
    //
    for (int i = 0; i < 2; i++) {
        int s;
        vec2 cam_pos;
        float cam_sx, cam_sy;

        s = i == 0 ? -1 : 1;

        cam_pos = game_state.cam_pos;
        cam_sx = game_state.cam_size;
        cam_sy = 9.0f / 16.0f * cam_sx;

        {
            vec2 pos;
            float w, h;
            vec3 color;

            color = V3(1.0f, 1.0f, 1.0f);

            // Up-Down lines
            pos.x = cam_pos.x + s * cam_sx;
            pos.y = cam_pos.y;

            w = 0.10f;
            h = 2.0f * cam_sy;

            push_square_onto_vertex_array(pos, w, h, color, false, vp_arr, vc_arr);

            // Left-Right lines
            pos.x = cam_pos.x;
            pos.y = cam_pos.y + s * cam_sy;

            w = 2.0f * cam_sx;
            h = 0.10f;

            push_square_onto_vertex_array(pos, w, h, color, false, vp_arr, vc_arr);
        }
    }

    {
        GLuint position_vbo = _ui_state.minimap.position_vbo;
        GLuint color_vbo = _ui_state.minimap.color_vbo;

        //
        // Load all the vertex data into OpenGL
        //
        glBindBuffer(GL_ARRAY_BUFFER, position_vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * vp_arr->length,
                     vp_arr->data, GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, color_vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * vc_arr->length,
                     vc_arr->data, GL_STATIC_DRAW);

        //
        // Draw the minimap
        //
        struct minimap_shader *s = &shader_store.minimap_shader;

        glUseProgram(s->program);
        glUniformMatrix4fv(s->mvp_mat_location, 1, GL_TRUE, mvp_mat.m);

        glBindBuffer(GL_ARRAY_BUFFER, position_vbo);
        glVertexAttribPointer(s->position_location, 2, GL_FLOAT,
                              GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(s->position_location);

        glBindBuffer(GL_ARRAY_BUFFER, color_vbo);
        glVertexAttribPointer(s->color_location, 3, GL_FLOAT,
                              GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(s->color_location);

        glDrawArrays(GL_TRIANGLES, 0, vp_arr->length);
    }
}

static void draw_resources_info(void) {
    {
        char supply_string[256];
        sprintf_s(supply_string, 256, "Supply: %d/%d", game_state.team_supply[0], game_state.team_supply_max[0]);
        ui_draw_highlighted_string(
            supply_string,
            V4(1.0f, 1.0f, 1.0f, 1.0f),
            V3(ui_state.resources_info_ui.supply_pos.x, ui_state.resources_info_ui.supply_pos.y, 1.0f),
            ui_state.resources_info_ui.supply_size,
            FONT_JUSTIFY_HORIZONTAL_RIGHT);
    }

    {
        char money_string[256];
        sprintf_s(money_string, 256, "Money: %d", game_state.team_money[0]);
        ui_draw_highlighted_string(
            money_string,
            V4(1.0f, 1.0f, 1.0f, 1.0f),
            V3(ui_state.resources_info_ui.money_pos.x, ui_state.resources_info_ui.money_pos.y, 1.0f),
            ui_state.resources_info_ui.money_size,
            FONT_JUSTIFY_HORIZONTAL_RIGHT);
    }
}

enum ui_button_state {
    UI_BUTTON_STATE_NOT_HOVERED,
    UI_BUTTON_STATE_HOVERED,
    UI_BUTTON_STATE_DOWN,
    UI_BUTTON_STATE_CLICKED,
};

static enum ui_button_state ui_draw_button(struct texture icon_tex, float w, vec2 pos) {
    vec2 md_pos, m_pos;
    bool md, mc, md_in_btn, m_in_btn;

    struct mesh *mesh;
    vec2 scale;
    float angle, opacity;
    struct texture tex;

    mc = controls_state.left_mouse_clicked;
    md = controls_state.left_mouse_down;
    m_pos = get_ui_pos(controls_state.left_mouse_pos);
    md_pos = get_ui_pos(controls_state.left_mouse_down_pos);
    m_in_btn = is_point_in_square(m_pos, V2(w, w), V2(pos.x, pos.y));
    md_in_btn = is_point_in_square(md_pos, V2(w, w), V2(pos.x, pos.y));

    mesh = &mesh_store.square_mesh;
    scale = V2(w, -w);
    angle = 0.0f;
    opacity = 1.0f;

    tex = texture_store.buttons_square[0];
    if (md && md_in_btn) {
        tex = texture_store.buttons_square[1];
    }
    ui_draw_mesh(mesh, pos, scale, angle, tex, opacity);

    tex = icon_tex;
    ui_draw_mesh(mesh, pos, scale, angle, tex, opacity);

    if (mc && m_in_btn && md_in_btn) {
        return UI_BUTTON_STATE_CLICKED;
    }
    if (m_in_btn && md_in_btn) {
        return UI_BUTTON_STATE_DOWN;
    }
    if (m_in_btn) {
        return UI_BUTTON_STATE_HOVERED;
    }
    return UI_BUTTON_STATE_NOT_HOVERED;
}

static void ui_draw_tooltip(vec2 pos, const char *text) {
    float w = 1.58f * 1.5f;
    float h = 1.0f * 1.5f;

    {
        struct mesh *mesh = &mesh_store.square_mesh;
        vec2 position = V2(pos.x, pos.y);
        vec2 scale = V2(w, h);
        float angle = 0.0f;
        struct texture texture = create_atlas_image_texture("menu-window.png");
        float opacity = 0.9f;

        ui_draw_mesh(mesh, position, scale, angle, texture, opacity);
    }

    {
        ui_draw_highlighted_string(
            text,
            V4(1.0f, 1.0f, 1.0f, 1.0f),
            V3(pos.x - 0.5f * w + 0.2f, pos.y + 0.5f * h - 0.1f, 0.95f),
            0.005f,
            FONT_JUSTIFY_HORIZONTAL_LEFT);
    }
}

static void draw_selected_entity_pane(struct ui_selected_entity_pane *pane, char **selected_action, int *selected_action_index) {
    *selected_action = NULL;

    for (int i = 0; i < pane->num_images; i++) {
        const char *texture_name;
        vec2 position, scale;
        float angle, opacity;
        struct texture texture;
        struct mesh *mesh;

        texture_name = pane->images[i].texture_name;
        position = pane->images[i].position;
        scale = pane->images[i].scale;
        mesh = &mesh_store.square_mesh;
        texture = create_atlas_image_texture(texture_name);
        angle = 0.0f;
        opacity = 1.0f;

        ui_draw_mesh(mesh, position, scale, angle, texture, opacity);
    }

    for (int i = 0; i < pane->num_strings; i++) {
        const char *text;
        vec3 color;
        vec2 position;
        float size;

        text = pane->strings[i].text;
        color = pane->strings[i].color;
        position = pane->strings[i].position;
        size = pane->strings[i].size;

        ui_draw_highlighted_string(text,
                                   V4(color.x, color.y, color.z, 1.0f),
                                   V3(position.x, position.y, 1.0f),
                                   size, FONT_JUSTIFY_HORIZONTAL_CENTER);
    }

    for (int i = 0; i < pane->num_buttons; i++) {
        char *action, *texture_name;
        vec2 position, scale;
        struct texture texture;
        enum button_state button_state;

        action = pane->buttons[i].action;
        texture_name = pane->buttons[i].texture_name;
        position = pane->buttons[i].position;
        scale = pane->buttons[i].scale;
        texture = create_atlas_image_texture(texture_name);

        button_state = ui_draw_button(texture, scale.x, position);
        if (button_state == UI_BUTTON_STATE_CLICKED) {
            *selected_action = action;
        }
    }

    if (pane->has_button_list) {
        vec2 pos, sz, pad;

        pos = pane->button_list.position;
        sz = pane->button_list.scale;
        pad = pane->button_list.padding;

        for (int i = 0; i < pane->button_list.num_buttons; i++) {
            struct texture texture;
            enum button_state button_state;
            char *action;

            action = pane->button_list.action;
            texture = pane->button_list.textures[i];
            button_state = ui_draw_button(texture, sz.x, pos);
            if (button_state == UI_BUTTON_STATE_CLICKED) {
                *selected_action = action;
                *selected_action_index = i;
            }

            pos.x += (sz.x + pad.x);
        }
    }

    const char *transition_to_do = NULL;
    for (int i = 0; i < pane->num_button_panels; i++) {
        vec2 panel_position, panel_padding;
        float panel_size;
        vec2 button_position_offsets[] = {
            V2(0.0f, 0.0f),
            V2(1.0f, 0.0f),
            V2(2.0f, 0.0f),
            V2(0.0f, -1.0f),
            V2(1.0f, -1.0f),
            V2(2.0f, -1.0f),
        };

        if (strcmp(pane->cur_button_panel_name, pane->button_panels[i].name) != 0) {
            continue;
        }

        for (int j = 0; j < pane->button_panels[i].num_strings; j++) {
            const char *str_text;
            vec2 str_position;
            vec3 str_color;
            float str_size;

            str_text = pane->button_panels[i].strings[j];
            str_position = pane->button_panels[i].string_positions[j];
            str_color = pane->button_panels[i].string_colors[j];
            str_size = pane->button_panels[i].string_sizes[j];

            ui_draw_highlighted_string(str_text,
                                       V4(str_color.x, str_color.y, str_color.z, 1.0f),
                                       V3(str_position.x, str_position.y, 1.0f),
                                       str_size, FONT_JUSTIFY_HORIZONTAL_LEFT);
        }

        panel_position = pane->button_panels[i].position;
        panel_padding = pane->button_panels[i].padding;
        panel_size = pane->button_panels[i].size;

        for (int j = 0; j < MAX_BUTTON_PANEL_NUM_BUTTONS; j++) {
            if (!pane->button_panels[i].active[j]) {
                continue;
            }

            vec2 off, pos;
            float sz;
            char *action, *texture_name, *transition;
            struct texture texture;
            enum button_state button_state;

            sz = panel_size;
            off = button_position_offsets[j];
            pos = panel_position;
            pos.x += off.x * (panel_padding.x + panel_size);
            pos.y += off.y * (panel_padding.y + panel_size);
            action = pane->button_panels[i].actions[j];
            texture_name = pane->button_panels[i].textures[j];
            transition = pane->button_panels[i].transitions[j];
            texture = create_atlas_image_texture(texture_name);

            button_state = ui_draw_button(texture, sz, pos);
            if (button_state == UI_BUTTON_STATE_CLICKED) {
                *selected_action = action;
                *selected_action_index = -1;

                if (transition[0]) {
                    transition_to_do = transition;
                }
            }
        }
    }
    if (transition_to_do) {
        strcpy_s(pane->cur_button_panel_name, MAX_BUTTON_PANEL_NAME_LENGTH, transition_to_do);
    }
}

static void draw_selected_building_construction(struct entity *building_construction) {
    char *selected_action;
    int selected_action_index;
    draw_selected_entity_pane(&ui_state.selected_building_construction_ui_pane, &selected_action, &selected_action_index);
    if (selected_action) {
        cancel_building_construction_entity(building_construction);
        if (strcmp(selected_action, "cancel") == 0) {
            cancel_building_construction_entity(building_construction);
        } else {
            assert(false);
        }
    }
}

static void draw_selected_command_center(struct entity *command_center) {
    struct ui_selected_entity_pane *pane;
    struct unit_construction_component *construction_component;

    construction_component = get_entity_component_of_type(command_center, COMPONENT_TYPE_UNIT_CONSTRUCTION, "unit_construction");
    assert(construction_component);

    if (construction_component->spawn_list.length > 0) {
        vec2 center0, center1, scale0, scale1;
        vec3 color0, color1;
        float a, opacity;

        a = construction_component->spawn_time_left;
        a /= construction_component->spawn_time;

        center0 = V2(3.08f, -3.2f);
        center1 = V2(3.08f + 2.4f * 0.5f * (a - 1.0f), -3.2f);

        scale0 = V2(2.4f, 0.2f);
        scale1 = V2(2.4f * a, 0.2f);

        color0 = V3(1.0f, 0.0f, 0.0f);
        color1 = V3(0.0f, 1.0f, 0.0f);

        opacity = 1.0f;

        draw_square(center0, scale0, color0, opacity);
        draw_square(center1, scale1, color1, opacity);
    }

    pane = &ui_state.selected_command_center_ui_pane;
    pane->button_list.num_buttons = construction_component->spawn_list.length;
    for (int i = 0; i < pane->button_list.num_buttons; i++) {
        const char *texture_name = NULL;
        switch (construction_component->spawn_list.data[i]) {
            case ENTITY_TYPE_WORKER:
                texture_name = "worker/down/1.png";
                break;
            default:
                texture_name = NULL;
        }
        assert(texture_name);
        pane->button_list.textures[i] = create_atlas_image_texture(texture_name);
    }

    char *selected_action;
    int selected_action_index = -1;
    draw_selected_entity_pane(pane, &selected_action, &selected_action_index);
    if (selected_action) {
        if (strcmp(selected_action, "create-worker") == 0) {
            bool can_afford, has_room;

            can_afford = entity_cost[ENTITY_TYPE_WORKER] <= game_state.team_money[command_center->team_num];
            has_room = construction_component->spawn_list.length < 5;

            if (can_afford && has_room) {
                game_state.team_money[command_center->team_num] -= entity_cost[ENTITY_TYPE_WORKER];
                push_to_unit_construction_component_spawn_list(construction_component, ENTITY_TYPE_WORKER);
            }
        } else if (strcmp(selected_action, "remove-unit") == 0) {
            assert(selected_action_index >= 0);
            game_state.team_money[command_center->team_num] += entity_cost[construction_component->spawn_list.data[selected_action_index]];
            remove_from_unit_construction_component_spawn_list(construction_component, selected_action_index);
        } else {
            assert(false);
        }
    }
}

static void draw_selected_barracks(struct entity *barracks) {
    struct ui_selected_entity_pane *pane;
    struct unit_construction_component *construction_component;

    construction_component = get_entity_component_of_type(barracks, COMPONENT_TYPE_UNIT_CONSTRUCTION, "unit_construction");
    assert(construction_component);

    if (construction_component->spawn_list.length > 0) {
        vec2 center0, center1, scale0, scale1;
        vec3 color0, color1;
        float a, opacity;

        a = construction_component->spawn_time_left;
        a /= construction_component->spawn_time;

        center0 = V2(3.08f, -3.2f);
        center1 = V2(3.08f + 2.4f * 0.5f * (a - 1.0f), -3.2f);

        scale0 = V2(2.4f, 0.2f);
        scale1 = V2(2.4f * a, 0.2f);

        color0 = V3(1.0f, 0.0f, 0.0f);
        color1 = V3(0.0f, 1.0f, 0.0f);

        opacity = 1.0f;

        draw_square(center0, scale0, color0, opacity);
        draw_square(center1, scale1, color1, opacity);
    }

    pane = &ui_state.selected_barracks_ui_pane;
    pane->button_list.num_buttons = construction_component->spawn_list.length;
    for (int i = 0; i < pane->button_list.num_buttons; i++) {
        const char *texture_name = NULL;
        switch (construction_component->spawn_list.data[i]) {
            case ENTITY_TYPE_SOLDIER:
                texture_name = "infantry/down/1.png";
                break;
            case ENTITY_TYPE_MEDIC:
                texture_name = "medic/down/2.png";
                break;
            default:
                texture_name = NULL;
        }
        assert(texture_name);
        pane->button_list.textures[i] = create_atlas_image_texture(texture_name);
    }

    char *selected_action;
    int selected_action_index = -1;
    draw_selected_entity_pane(pane, &selected_action, &selected_action_index);
    if (selected_action) {
        if (strcmp(selected_action, "create-soldier") == 0) {
            bool can_afford, has_room;

            can_afford = entity_cost[ENTITY_TYPE_SOLDIER] <= game_state.team_money[barracks->team_num];
            has_room = construction_component->spawn_list.length < 5;

            if (can_afford && has_room) {
                game_state.team_money[barracks->team_num] -= entity_cost[ENTITY_TYPE_SOLDIER];
                push_to_unit_construction_component_spawn_list(construction_component, ENTITY_TYPE_SOLDIER);
            }
        } else if (strcmp(selected_action, "create-medic") == 0) {
            bool can_afford, has_room;

            can_afford = entity_cost[ENTITY_TYPE_SOLDIER] <= game_state.team_money[barracks->team_num];
            has_room = construction_component->spawn_list.length < 5;

            if (can_afford && has_room) {
                game_state.team_money[barracks->team_num] -= entity_cost[ENTITY_TYPE_MEDIC];
                push_to_unit_construction_component_spawn_list(construction_component, ENTITY_TYPE_MEDIC);
            }
        } else if (strcmp(selected_action, "remove-unit") == 0) {
            assert(selected_action_index >= 0);
            game_state.team_money[barracks->team_num] += entity_cost[construction_component->spawn_list.data[selected_action_index]];
            remove_from_unit_construction_component_spawn_list(construction_component, selected_action_index);
        } else {
            assert(false);
        }
    }
}

static void draw_selected_factory(struct entity *factory) {
    struct ui_selected_entity_pane *pane;
    struct unit_construction_component *construction_component;

    construction_component = get_entity_component_of_type(factory, COMPONENT_TYPE_UNIT_CONSTRUCTION, "unit_construction");
    assert(construction_component);

    if (construction_component->spawn_list.length > 0) {
        vec2 center0, center1, scale0, scale1;
        vec3 color0, color1;
        float a, opacity;

        a = construction_component->spawn_time_left;
        a /= construction_component->spawn_time;

        center0 = V2(3.08f, -3.2f);
        center1 = V2(3.08f + 2.4f * 0.5f * (a - 1.0f), -3.2f);

        scale0 = V2(2.4f, 0.2f);
        scale1 = V2(2.4f * a, 0.2f);

        color0 = V3(1.0f, 0.0f, 0.0f);
        color1 = V3(0.0f, 1.0f, 0.0f);

        opacity = 1.0f;

        draw_square(center0, scale0, color0, opacity);
        draw_square(center1, scale1, color1, opacity);
    }

    pane = &ui_state.selected_factory_ui_pane;
    pane->button_list.num_buttons = construction_component->spawn_list.length;
    for (int i = 0; i < pane->button_list.num_buttons; i++) {
        const char *texture_name = NULL;
        switch (construction_component->spawn_list.data[i]) {
            case ENTITY_TYPE_TANK:
                texture_name = "tank/down/1.png";
                break;
            case ENTITY_TYPE_VEHICLE:
                texture_name = "vehicle/down/1.png";
                break;
            default:
                texture_name = NULL;
        }
        assert(texture_name);
        pane->button_list.textures[i] = create_atlas_image_texture(texture_name);
    }

    char *selected_action;
    int selected_action_index = -1;
    draw_selected_entity_pane(pane, &selected_action, &selected_action_index);
    if (selected_action) {
        if (strcmp(selected_action, "create-tank") == 0) {
            bool can_afford, has_room;

            can_afford = entity_cost[ENTITY_TYPE_TANK] <= game_state.team_money[factory->team_num];
            has_room = construction_component->spawn_list.length < 5;

            if (can_afford && has_room) {
                game_state.team_money[factory->team_num] -= entity_cost[ENTITY_TYPE_TANK];
                push_to_unit_construction_component_spawn_list(construction_component, ENTITY_TYPE_TANK);
            }
        } else if (strcmp(selected_action, "create-vehicle") == 0) {
            bool can_afford, has_room;

            can_afford = entity_cost[ENTITY_TYPE_VEHICLE] <= game_state.team_money[factory->team_num];
            has_room = construction_component->spawn_list.length < 5;

            if (can_afford && has_room) {
                game_state.team_money[factory->team_num] -= entity_cost[ENTITY_TYPE_VEHICLE];
                push_to_unit_construction_component_spawn_list(construction_component, ENTITY_TYPE_VEHICLE);
            }
        } else if (strcmp(selected_action, "remove-unit") == 0) {
            assert(selected_action_index >= 0);
            game_state.team_money[factory->team_num] += entity_cost[construction_component->spawn_list.data[selected_action_index]];
            remove_from_unit_construction_component_spawn_list(construction_component, selected_action_index);
        } else {
            assert(false);
        }
    }
}

static void draw_selected_heliport(struct entity *heliport) {
    struct ui_selected_entity_pane *pane;
    struct unit_construction_component *construction_component;

    construction_component = get_entity_component_of_type(heliport, COMPONENT_TYPE_UNIT_CONSTRUCTION, "unit_construction");
    assert(construction_component);

    if (construction_component->spawn_list.length > 0) {
        vec2 center0, center1, scale0, scale1;
        vec3 color0, color1;
        float a, opacity;

        a = construction_component->spawn_time_left;
        a /= construction_component->spawn_time;

        center0 = V2(3.08f, -3.2f);
        center1 = V2(3.08f + 2.4f * 0.5f * (a - 1.0f), -3.2f);

        scale0 = V2(2.4f, 0.2f);
        scale1 = V2(2.4f * a, 0.2f);

        color0 = V3(1.0f, 0.0f, 0.0f);
        color1 = V3(0.0f, 1.0f, 0.0f);

        opacity = 1.0f;

        draw_square(center0, scale0, color0, opacity);
        draw_square(center1, scale1, color1, opacity);
    }

    pane = &ui_state.selected_heliport_ui_pane;
    pane->button_list.num_buttons = construction_component->spawn_list.length;
    for (int i = 0; i < pane->button_list.num_buttons; i++) {
        const char *texture_name = NULL;
        switch (construction_component->spawn_list.data[i]) {
            case ENTITY_TYPE_HELICOPTER:
                texture_name = "helicopter/down/1.png";
                break;
            default:
                texture_name = NULL;
        }
        assert(texture_name);
        pane->button_list.textures[i] = create_atlas_image_texture(texture_name);
    }

    char *selected_action;
    int selected_action_index = -1;
    draw_selected_entity_pane(pane, &selected_action, &selected_action_index);
    if (selected_action) {
        if (strcmp(selected_action, "create-helicopter") == 0) {
            bool can_afford, has_room;

            can_afford = entity_cost[ENTITY_TYPE_HELICOPTER] <= game_state.team_money[heliport->team_num];
            has_room = construction_component->spawn_list.length < 5;

            if (can_afford && has_room) {
                game_state.team_money[heliport->team_num] -= entity_cost[ENTITY_TYPE_HELICOPTER];
                push_to_unit_construction_component_spawn_list(construction_component, ENTITY_TYPE_HELICOPTER);
            }
        } else if (strcmp(selected_action, "remove-unit") == 0) {
            assert(selected_action_index >= 0);
            game_state.team_money[heliport->team_num] += entity_cost[construction_component->spawn_list.data[selected_action_index]];
            remove_from_unit_construction_component_spawn_list(construction_component, selected_action_index);
        } else {
            assert(false);
        }
    }
}

static void draw_selected_refinery(struct entity *refinery) {
    ui_state.selected_refinery_ui_pane.button_list.num_buttons = refinery->refinery.worker_id_array.length;
    for (int i = 0; i < refinery->refinery.worker_id_array.length; i++) {
        ui_state.selected_refinery_ui_pane.button_list.textures[i] =
            create_atlas_image_texture("worker/down/1.png");
    }

    char *selected_action;
    int selected_action_index = -1;
    draw_selected_entity_pane(&ui_state.selected_refinery_ui_pane, &selected_action, &selected_action_index);
    if (selected_action) {
        if (strcmp(selected_action, "remove-worker") == 0) {
            assert(selected_action_index >= 0);
            remove_worker_from_refinery(refinery, selected_action_index);
        } else {
            assert(false);
        }
    }
}

static void draw_selected_worker(struct entity *worker) {
    char *selected_action;
    int selected_action_index;
    draw_selected_entity_pane(&ui_state.selected_worker_ui_pane, &selected_action, &selected_action_index);
    if (selected_action) {
        if (strcmp(selected_action, "build-supply-depot") == 0) {
            ui_state.is_placing_entity = true;
            ui_state.worker_id = worker->id;
            ui_state.placeholder_id = create_placeholder_entity(ENTITY_TYPE_SUPPLY_DEPOT);
        } else if (strcmp(selected_action, "build-turret") == 0) {
            ui_state.is_placing_entity = true;
            ui_state.worker_id = worker->id;
            ui_state.placeholder_id = create_placeholder_entity(ENTITY_TYPE_TURRET);
        } else if (strcmp(selected_action, "build-refinery") == 0) {
            ui_state.is_placing_entity = true;
            ui_state.worker_id = worker->id;
            ui_state.placeholder_id = create_placeholder_entity(ENTITY_TYPE_REFINERY);
        } else if (strcmp(selected_action, "build-barracks") == 0) {
            ui_state.is_placing_entity = true;
            ui_state.worker_id = worker->id;
            ui_state.placeholder_id = create_placeholder_entity(ENTITY_TYPE_BARRACKS);
        } else if (strcmp(selected_action, "build-factory") == 0) {
            ui_state.is_placing_entity = true;
            ui_state.worker_id = worker->id;
            ui_state.placeholder_id = create_placeholder_entity(ENTITY_TYPE_FACTORY);
        } else if (strcmp(selected_action, "build-heliport") == 0) {
            ui_state.is_placing_entity = true;
            ui_state.worker_id = worker->id;
            ui_state.placeholder_id = create_placeholder_entity(ENTITY_TYPE_HELIPORT);
        } else if (strcmp(selected_action, "goto-build-screen") == 0) {
        } else if (strcmp(selected_action, "goto-start-screen") == 0) {
        } else if (strcmp(selected_action, "cancel-barracks") == 0 ||
                   strcmp(selected_action, "cancel-factory") == 0) {
            ui_state.is_placing_entity = false;
            ui_state.worker_id.idx = 0;
            delete_entity(ui_state.placeholder_id);
            ui_state.placeholder_id.idx = 0;
        } else if (strcmp(selected_action, "normal-move") == 0 ||
                   strcmp(selected_action, "attack-move") == 0 ||
                   strcmp(selected_action, "patrol-move") == 0) {
            ui_state.is_doing_unit_action = true;
            ui_state.unit_action_type = UNIT_ACTION_TYPE_MOVE;
        } else if (strcmp(selected_action, "cancel-move") == 0) {
            ui_state.is_doing_unit_action = false;
        } else {
            assert(false);
        }
    }
}

static void draw_selected_soldier(struct entity *soldier) {
    char *selected_action;
    int selected_action_index;
    draw_selected_entity_pane(&ui_state.selected_soldier_ui_pane, &selected_action, &selected_action_index);
    if (selected_action) {
        ui_state.is_doing_unit_action = true;
        if (strcmp(selected_action, "normal-move") == 0) {
            ui_state.unit_action_type = UNIT_ACTION_TYPE_MOVE;
        } else if (strcmp(selected_action, "attack-move") == 0) {
            ui_state.unit_action_type = UNIT_ACTION_TYPE_ATTACK_MOVE;
        } else if (strcmp(selected_action, "patrol-move") == 0) {
            ui_state.unit_action_type = UNIT_ACTION_TYPE_PATROL_MOVE;
        } else if (strcmp(selected_action, "cancel-move") == 0) {
            ui_state.is_doing_unit_action = false;
        } else {
            assert(false);
        }
    }
}

static void draw_selected_medic(struct entity *medic) {
    char *selected_action;
    int selected_action_index;
    draw_selected_entity_pane(&ui_state.selected_medic_ui_pane, &selected_action, &selected_action_index);
    if (selected_action) {
        ui_state.is_doing_unit_action = true;
        if (strcmp(selected_action, "normal-move") == 0) {
            ui_state.unit_action_type = UNIT_ACTION_TYPE_MOVE;
        } else if (strcmp(selected_action, "cancel-move") == 0) {
            ui_state.is_doing_unit_action = false;
        } else {
            assert(false);
        }
    }
}

static void draw_selected_tank(struct entity *tank) {
    char *selected_action;
    int selected_action_index;
    draw_selected_entity_pane(&ui_state.selected_tank_ui_pane, &selected_action, &selected_action_index);
    if (selected_action) {
        ui_state.is_doing_unit_action = true;
        if (strcmp(selected_action, "normal-move") == 0) {
            ui_state.unit_action_type = UNIT_ACTION_TYPE_MOVE;
        } else if (strcmp(selected_action, "attack-move") == 0) {
            ui_state.unit_action_type = UNIT_ACTION_TYPE_ATTACK_MOVE;
        } else if (strcmp(selected_action, "patrol-move") == 0) {
            ui_state.unit_action_type = UNIT_ACTION_TYPE_PATROL_MOVE;
        } else if (strcmp(selected_action, "cancel-move") == 0) {
            ui_state.is_doing_unit_action = false;
        } else {
            assert(false);
        }
    }
}

static void draw_selected_vehicle(struct entity *vehicle) {
    char *selected_action;
    int selected_action_index;
    draw_selected_entity_pane(&ui_state.selected_vehicle_ui_pane, &selected_action, &selected_action_index);
    if (selected_action) {
        ui_state.is_doing_unit_action = true;
        if (strcmp(selected_action, "normal-move") == 0) {
            ui_state.unit_action_type = UNIT_ACTION_TYPE_MOVE;
        } else if (strcmp(selected_action, "attack-move") == 0) {
            ui_state.unit_action_type = UNIT_ACTION_TYPE_ATTACK_MOVE;
        } else if (strcmp(selected_action, "patrol-move") == 0) {
            ui_state.unit_action_type = UNIT_ACTION_TYPE_PATROL_MOVE;
        } else if (strcmp(selected_action, "cancel-move") == 0) {
            ui_state.is_doing_unit_action = false;
        } else {
            assert(false);
        }
    }
}

static void draw_selected_helicopter(struct entity *helicopter) {
    char *selected_action;
    int selected_action_index;
    draw_selected_entity_pane(&ui_state.selected_helicopter_ui_pane, &selected_action, &selected_action_index);
    if (selected_action) {
        ui_state.is_doing_unit_action = true;
        if (strcmp(selected_action, "normal-move") == 0) {
            ui_state.unit_action_type = UNIT_ACTION_TYPE_MOVE;
        } else if (strcmp(selected_action, "attack-move") == 0) {
            ui_state.unit_action_type = UNIT_ACTION_TYPE_ATTACK_MOVE;
        } else if (strcmp(selected_action, "patrol-move") == 0) {
            ui_state.unit_action_type = UNIT_ACTION_TYPE_PATROL_MOVE;
        } else if (strcmp(selected_action, "cancel-move") == 0) {
            ui_state.is_doing_unit_action = false;
        } else {
            assert(false);
        }
    }
}

static void draw_selected_turret(struct entity *turret) {
    char *selected_action;
    int selected_action_index;
    draw_selected_entity_pane(&ui_state.selected_turret_ui_pane, &selected_action, &selected_action_index);
    if (selected_action) {
        assert(false);
    }
}

static void draw_selected_supply_depot(struct entity *supply_depot) {
    char *selected_action;
    int selected_action_index;
    draw_selected_entity_pane(&ui_state.selected_supply_depot_ui_pane, &selected_action, &selected_action_index);
    if (selected_action) {
        assert(false);
    }
}

static void copy_ui_selected_pane_data(JSON_Object *json_parent_object,
                                       const char *name, struct ui_selected_entity_pane *pane) {
    JSON_Object *json_object, *json_button_list_object;
    JSON_Array *json_buttons_array, *json_images_array, *json_strings_array;

    json_object = json_object_get_object(json_parent_object, name);

    json_buttons_array = json_object_get_array(json_object, "buttons");
    pane->num_buttons = (int)json_array_get_count(json_buttons_array);
    for (int i = 0; i < pane->num_buttons; i++) {
        JSON_Object *json_button_object;
        const char *action, *texture_name;
        vec2 position, scale;

        json_button_object = json_array_get_object(json_buttons_array, i);

        action = json_object_get_string(json_button_object, "action");
        texture_name = json_object_get_string(json_button_object, "texture");

        position.x = (float)json_object_dotget_number(json_button_object, "position.x");
        position.y = (float)json_object_dotget_number(json_button_object, "position.y");

        scale.x = (float)json_object_dotget_number(json_button_object, "scale.x");
        scale.y = (float)json_object_dotget_number(json_button_object, "scale.y");

        strcpy_s(pane->buttons[i].action, MAX_UI_SELECTED_ENTITY_PANE_BUTTON_ACTION_LENGTH, action);
        strcpy_s(pane->buttons[i].texture_name, MAX_UI_SELECTED_ENTITY_PANE_BUTTON_TEXTURE_NAME_LENGTH, texture_name);
        pane->buttons[i].position = position;
        pane->buttons[i].scale = scale;
        pane->buttons[i].has_tooltip = false;
    }

    json_images_array = json_object_get_array(json_object, "images");
    pane->num_images = (int)json_array_get_count(json_images_array);
    for (int i = 0; i < pane->num_images; i++) {
        JSON_Object *json_image_object;
        const char *texture_name;
        vec2 position, scale;

        json_image_object = json_array_get_object(json_images_array, i);

        texture_name = json_object_get_string(json_image_object, "texture_name");

        position.x = (float)json_object_dotget_number(json_image_object, "position.x");
        position.y = (float)json_object_dotget_number(json_image_object, "position.y");

        scale.x = (float)json_object_dotget_number(json_image_object, "scale.x");
        scale.y = (float)json_object_dotget_number(json_image_object, "scale.y");

        strcpy_s(pane->images[i].texture_name, MAX_UI_SELECTED_ENTITY_PANE_IMAGE_TEXTURE_NAME_LENGTH, texture_name);
        pane->images[i].position = position;
        pane->images[i].scale = scale;
    }

    json_strings_array = json_object_get_array(json_object, "strings");
    pane->num_strings = (int)json_array_get_count(json_strings_array);
    for (int i = 0; i < pane->num_strings; i++) {
        JSON_Object *json_string_object;
        const char *text;
        vec3 color;
        vec2 position;
        float size;

        json_string_object = json_array_get_object(json_strings_array, i);

        text = json_object_get_string(json_string_object, "text");

        color.x = (float)json_object_dotget_number(json_string_object, "color.x");
        color.y = (float)json_object_dotget_number(json_string_object, "color.y");
        color.z = (float)json_object_dotget_number(json_string_object, "color.z");

        position.x = (float)json_object_dotget_number(json_string_object, "position.x");
        position.y = (float)json_object_dotget_number(json_string_object, "position.y");

        size = (float)json_object_get_number(json_string_object, "size");

        strcpy_s(pane->strings[i].text, MAX_UI_SELECTED_ENTITY_PANE_STRING_TEXT_LENGTH, text);
        pane->strings[i].color = color;
        pane->strings[i].position = position;
        pane->strings[i].size = size;
    }

    json_button_list_object = json_object_get_object(json_object, "button_list");
    if (json_button_list_object) {
        const char *action;
        vec2 position, scale, padding;

        action = json_object_get_string(json_button_list_object, "action");

        position.x = (float)json_object_dotget_number(json_button_list_object, "position.x");
        position.y = (float)json_object_dotget_number(json_button_list_object, "position.y");

        scale.x = (float)json_object_dotget_number(json_button_list_object, "scale.x");
        scale.y = (float)json_object_dotget_number(json_button_list_object, "scale.y");

        padding.x = (float)json_object_dotget_number(json_button_list_object, "padding.x");
        padding.y = (float)json_object_dotget_number(json_button_list_object, "padding.y");

        strcpy_s(pane->button_list.action, MAX_UI_SELECTED_ENTITY_PANE_BUTTON_LIST_ACTION_LENGTH, action);
        pane->button_list.position = position;
        pane->button_list.scale = scale;
        pane->button_list.padding = padding;

        pane->has_button_list = true;
    } else {
        pane->has_button_list = false;
    }

    JSON_Array *json_button_panels_array = json_object_get_array(json_object, "button_panels");
    pane->num_button_panels = (int)json_array_get_count(json_button_panels_array);
    for (int i = 0; i < pane->num_button_panels; i++) {
        JSON_Object *json_button_panel_object;
        JSON_Array *json_strings_array;
        const char *name;
        const char *textures[6], *action[6], *transition[6];
        vec2 position, padding;
        float size;

        json_button_panel_object = json_array_get_object(json_button_panels_array, i);
        json_strings_array = json_object_get_array(json_button_panel_object, "strings");

        name = json_object_get_string(json_button_panel_object, "name");

        pane->button_panels[i].num_strings = (int)json_array_get_count(json_strings_array);
        for (int j = 0; j < pane->button_panels[i].num_strings; j++) {
            JSON_Object *json_string_object;
            const char *str_text;
            vec2 str_position;
            vec3 str_color;
            float str_size;

            json_string_object = json_array_get_object(json_strings_array, j);

            str_text = json_object_get_string(json_string_object, "text");

            str_position.x = (float)json_object_dotget_number(json_string_object, "position.x");
            str_position.y = (float)json_object_dotget_number(json_string_object, "position.y");

            str_color.x = (float)json_object_dotget_number(json_string_object, "color.x");
            str_color.y = (float)json_object_dotget_number(json_string_object, "color.y");
            str_color.z = (float)json_object_dotget_number(json_string_object, "color.z");

            str_size = (float)json_object_dotget_number(json_string_object, "size");

            strcpy_s(pane->button_panels[i].strings[j], MAX_BUTTON_PANEL_STRING_LENGTH, str_text);
            pane->button_panels[i].string_positions[j] = str_position;
            pane->button_panels[i].string_colors[j] = str_color;
            pane->button_panels[i].string_sizes[j] = str_size;
        }

        position.x = (float)json_object_dotget_number(json_button_panel_object, "position.x");
        position.y = (float)json_object_dotget_number(json_button_panel_object, "position.y");

        padding.x = (float)json_object_dotget_number(json_button_panel_object, "padding.x");
        padding.y = (float)json_object_dotget_number(json_button_panel_object, "padding.y");

        size = (float)json_object_dotget_number(json_button_panel_object, "size");

        textures[0] = json_object_dotget_string(json_button_panel_object, "buttons.1.texture");
        action[0] = json_object_dotget_string(json_button_panel_object, "buttons.1.action");
        transition[0] = json_object_dotget_string(json_button_panel_object, "buttons.1.transition");

        textures[1] = json_object_dotget_string(json_button_panel_object, "buttons.2.texture");
        action[1] = json_object_dotget_string(json_button_panel_object, "buttons.2.action");
        transition[1] = json_object_dotget_string(json_button_panel_object, "buttons.2.transition");

        textures[2] = json_object_dotget_string(json_button_panel_object, "buttons.3.texture");
        action[2] = json_object_dotget_string(json_button_panel_object, "buttons.3.action");
        transition[2] = json_object_dotget_string(json_button_panel_object, "buttons.3.transition");

        textures[3] = json_object_dotget_string(json_button_panel_object, "buttons.4.texture");
        action[3] = json_object_dotget_string(json_button_panel_object, "buttons.4.action");
        transition[3] = json_object_dotget_string(json_button_panel_object, "buttons.4.transition");

        textures[4] = json_object_dotget_string(json_button_panel_object, "buttons.5.texture");
        action[4] = json_object_dotget_string(json_button_panel_object, "buttons.5.action");
        transition[4] = json_object_dotget_string(json_button_panel_object, "buttons.5.transition");

        textures[5] = json_object_dotget_string(json_button_panel_object, "buttons.6.texture");
        action[5] = json_object_dotget_string(json_button_panel_object, "buttons.6.action");
        transition[5] = json_object_dotget_string(json_button_panel_object, "buttons.6.transition");

        strcpy_s(pane->button_panels[i].name, MAX_BUTTON_PANEL_NAME_LENGTH, name);
        pane->button_panels[i].position = position;
        pane->button_panels[i].padding = padding;
        pane->button_panels[i].size = size;
        if (i == 0) {
            strcpy_s(pane->cur_button_panel_name, MAX_BUTTON_PANEL_NAME_LENGTH, name);
        }
        for (int j = 0; j < 6; j++) {
            if (textures[j]) {
                pane->button_panels[i].active[j] = true;
                strcpy_s(pane->button_panels[i].textures[j], MAX_BUTTON_PANEL_TEXTURE_LENGTH, textures[j]);
                strcpy_s(pane->button_panels[i].actions[j], MAX_BUTTON_PANEL_ACTION_LENGTH, action[j]);
                if (!transition[j]) {
                    transition[j] = "";
                }
                strcpy_s(pane->button_panels[i].transitions[j], MAX_BUTTON_PANEL_NAME_LENGTH, transition[j]);
            } else {
                pane->button_panels[i].active[j] = false;
            }
        }
    }
}

void ui_init(void) {
    {
        struct rect_2D rect;

        rect = rect_2D_create(V2(2.0f * -4.0f, 2.0f * -0.94f), V2(2.0f * 1.5f, 2.0f * 1.31f));
        ui_state.ui_pane_rects[UI_PANE_RECT_TYPE_MAIN_0] = rect;

        rect = rect_2D_create(V2(2.0f * -2.5f, 2.0f * -1.16f), V2(2.0f * 4.91f, 2.0f * 1.09f));
        ui_state.ui_pane_rects[UI_PANE_RECT_TYPE_MAIN_1] = rect;

        rect = rect_2D_create(V2(2.0f * 2.41f, 2.0f * -0.94f), V2(2.0f * 1.58f, 2.0f * 1.31f));
        ui_state.ui_pane_rects[UI_PANE_RECT_TYPE_MAIN_2] = rect;

        rect = rect_2D_create(V2(2.0f * -3.9f, 2.0f * -1.05f), V2(2.0f * 1.2f, 2.0f * 1.09f));
        ui_state.ui_pane_rects[UI_PANE_RECT_TYPE_MINIMAP] = rect;
    }

    font_mesh_init(&ui_state.gold_text);

    ui_state.is_doing_unit_action = false;
    ui_state.is_moving_minimap_camera = false;

    ui_state.selection_box_open = false;
    ui_state.is_placing_entity = false;
    ui_state.proj_mat = mat4_orthographic_projection(
        -8.0f, 8.0f,
        -4.5f, 4.5f,
        -1.0f, 1.0f);

    array_init(&_ui_state.entities);
    glGenBuffers(1, &_ui_state.line_position_vbo);

    memset(ui_state.entity_selection_box, 0, sizeof(ui_state.entity_selection_box));
    {
        vec2 *s;

        s = &ui_state.entity_selection_box[4 * ENTITY_TYPE_BARRACKS];
        s[0] = V2(0.0f, -0.3f);
        s[1] = V2(0.6f, 0.1f);
        s[2] = V2(-0.05f, 0.6f);
        s[3] = V2(-0.6f, 0.15f);

        s = &ui_state.entity_selection_box[4 * ENTITY_TYPE_BUILDING_CONSTRUCTION_LARGE];
        s[0] = V2(0.0f, -0.3f);
        s[1] = V2(0.6f, 0.1f);
        s[2] = V2(-0.05f, 0.6f);
        s[3] = V2(-0.6f, 0.15f);

        s = &ui_state.entity_selection_box[4 * ENTITY_TYPE_BUILDING_CONSTRUCTION_SMALL];
        s[0] = V2(-0.11f, -0.01f);
        s[1] = V2(0.25f, 0.17f);
        s[2] = V2(0.03f, 0.40f);
        s[3] = V2(-0.26f, 0.27f);

        s = &ui_state.entity_selection_box[4 * ENTITY_TYPE_REFINERY];
        s[0] = V2(0.0f, -0.3f);
        s[1] = V2(0.6f, 0.1f);
        s[2] = V2(-0.05f, 0.6f);
        s[3] = V2(-0.6f, 0.15f);

        s = &ui_state.entity_selection_box[4 * ENTITY_TYPE_TANK];
        s[0] = V2(-0.25f, -0.03f);
        s[1] = V2(0.27f, -0.03f);
        s[2] = V2(0.28f, 0.38f);
        s[3] = V2(-0.25f, 0.38f);

        s = &ui_state.entity_selection_box[4 * ENTITY_TYPE_VEHICLE];
        s[0] = V2(-0.25f, -0.03f);
        s[1] = V2(0.27f, -0.03f);
        s[2] = V2(0.28f, 0.38f);
        s[3] = V2(-0.25f, 0.38f);

        s = &ui_state.entity_selection_box[4 * ENTITY_TYPE_HELICOPTER];
        s[0] = V2(0.0f, -0.3f);
        s[1] = V2(0.6f, 0.1f);
        s[2] = V2(-0.05f, 0.6f);
        s[3] = V2(-0.6f, 0.15f);

        s = &ui_state.entity_selection_box[4 * ENTITY_TYPE_FACTORY];
        s[0] = V2(0.0f, -0.3f);
        s[1] = V2(0.6f, 0.1f);
        s[2] = V2(-0.05f, 0.6f);
        s[3] = V2(-0.6f, 0.15f);

        s = &ui_state.entity_selection_box[4 * ENTITY_TYPE_HELIPORT];
        s[0] = V2(0.0f, -0.3f);
        s[1] = V2(0.6f, 0.1f);
        s[2] = V2(-0.05f, 0.6f);
        s[3] = V2(-0.6f, 0.15f);

        s = &ui_state.entity_selection_box[4 * ENTITY_TYPE_SOLDIER];
        s[0] = V2(-0.15f, -0.1f);
        s[1] = V2(0.1f, -0.1f);
        s[2] = V2(0.1f, 0.3f);
        s[3] = V2(-0.15f, 0.3f);

        s = &ui_state.entity_selection_box[4 * ENTITY_TYPE_MEDIC];
        s[0] = V2(-0.15f, -0.1f);
        s[1] = V2(0.1f, -0.1f);
        s[2] = V2(0.1f, 0.3f);
        s[3] = V2(-0.15f, 0.3f);

        s = &ui_state.entity_selection_box[4 * ENTITY_TYPE_WORKER];
        s[0] = V2(-0.15f, -0.1f);
        s[1] = V2(0.1f, -0.1f);
        s[2] = V2(0.1f, 0.3f);
        s[3] = V2(-0.15f, 0.3f);

        s = &ui_state.entity_selection_box[4 * ENTITY_TYPE_COMMAND_CENTER];
        s[0] = V2(0.0f, -0.6f);
        s[1] = V2(1.2f, 0.2f);
        s[2] = V2(-0.1f, 1.2f);
        s[3] = V2(-1.2f, 0.3f);

        s = &ui_state.entity_selection_box[4 * ENTITY_TYPE_TURRET];
        s[0] = V2(-0.15f, -0.10f);
        s[1] = V2(0.15f, -0.11f);
        s[2] = V2(0.10f, 0.26f);
        s[3] = V2(-0.18f, 0.27f);

        s = &ui_state.entity_selection_box[4 * ENTITY_TYPE_SUPPLY_DEPOT];
        s[0] = V2(-0.187f, -0.10f);
        s[1] = V2(0.18f, -0.10f);
        s[2] = V2(0.13f, 0.24f);
        s[3] = V2(-0.22f, 0.24f);
    }

    {
        glGenBuffers(1, &_ui_state.minimap.position_vbo);
        glGenBuffers(1, &_ui_state.minimap.color_vbo);
        array_init(&_ui_state.minimap.vertex_position_array);
        array_init(&_ui_state.minimap.vertex_color_array);
    }

    {
        struct rect_2D mm_rect;
        vec2 mm_center, mm_size;

        mm_rect = ui_state.ui_pane_rects[UI_PANE_RECT_TYPE_MINIMAP];
        mm_center.x = mm_rect.top_left.x + 0.5f * mm_rect.size.x;
        mm_center.y = mm_rect.top_left.y - 0.5f * mm_rect.size.y;
        mm_size = mm_rect.size;

        vec3 translation_0, translation_1, scale_0;

        translation_0.x = -0.5f * GAME_MAP_WIDTH;
        translation_0.y = -0.5f * GAME_MAP_WIDTH;
        translation_0.z = 0.0f;

        scale_0.x = 1.5f * mm_size.x / GAME_MAP_WIDTH;
        scale_0.y = 1.5f * mm_size.y / GAME_MAP_WIDTH;
        scale_0.z = 1.0f;

        translation_1.x = mm_center.x;
        translation_1.y = mm_center.y;
        translation_1.z = 0.0f;

        ui_state.game_to_minimap_mat = mat4_multiply_n(3,
                                                       mat4_translation(translation_1),
                                                       mat4_scale(scale_0),
                                                       mat4_translation(translation_0));
    }

    {
        JSON_Object *json_parent_object = json_value_get_object(json_parse_file("assets/ui-data.json"));
        copy_ui_selected_pane_data(json_parent_object, "selected_soldier_ui", &ui_state.selected_soldier_ui_pane);
        copy_ui_selected_pane_data(json_parent_object, "selected_worker_ui", &ui_state.selected_worker_ui_pane);
        copy_ui_selected_pane_data(json_parent_object, "selected_refinery_ui", &ui_state.selected_refinery_ui_pane);
        copy_ui_selected_pane_data(json_parent_object, "selected_building_construction_ui", &ui_state.selected_building_construction_ui_pane);
        copy_ui_selected_pane_data(json_parent_object, "selected_barracks_ui", &ui_state.selected_barracks_ui_pane);
        copy_ui_selected_pane_data(json_parent_object, "selected_medic_ui", &ui_state.selected_medic_ui_pane);
        copy_ui_selected_pane_data(json_parent_object, "selected_factory_ui", &ui_state.selected_factory_ui_pane);
        copy_ui_selected_pane_data(json_parent_object, "selected_heliport_ui", &ui_state.selected_heliport_ui_pane);
        copy_ui_selected_pane_data(json_parent_object, "selected_command_center_ui", &ui_state.selected_command_center_ui_pane);
        copy_ui_selected_pane_data(json_parent_object, "selected_tank_ui", &ui_state.selected_tank_ui_pane);
        copy_ui_selected_pane_data(json_parent_object, "selected_vehicle_ui", &ui_state.selected_vehicle_ui_pane);
        copy_ui_selected_pane_data(json_parent_object, "selected_helicopter_ui", &ui_state.selected_helicopter_ui_pane);
        copy_ui_selected_pane_data(json_parent_object, "selected_turret_ui", &ui_state.selected_turret_ui_pane);
        copy_ui_selected_pane_data(json_parent_object, "selected_supply_depot_ui", &ui_state.selected_supply_depot_ui_pane);
    }

    {
        ui_state.resources_info_ui.money_pos = V2(7.75f, 4.5f);
        ui_state.resources_info_ui.money_size = 0.005f;
        ui_state.resources_info_ui.supply_pos = V2(7.75f, 4.17f);
        ui_state.resources_info_ui.supply_size = 0.005f;
    }
}

static void ui_set_selected_entities_target(vec2 mouse_game_pos, struct entity *hovered_entity, enum movement_type movement_type) {
    int i;
    struct entity_id entity_id;

    int w = (int)ceilf(sqrtf((float)game_state.selected_entity_array.length));
    array_foreach(&game_state.selected_entity_array, entity_id, i) {
        struct entity *entity = get_entity_from_id(entity_id);
        if (!entity) {
            continue;
        }

        if (hovered_entity) {
            do_entity_interaction(entity, hovered_entity);
        } else {
            int dx = i / w - w / 2;
            int dy = i % w - w / 2;

            vec2 offset = V2((float)dx, (float)dy);
            if (w % 2 == 0) {
                offset = vec2_add(offset, V2(0.5f, 0.5f));
            }

            vec2 p = vec2_add(mouse_game_pos, V2(0.25f * offset.x, 0.25f * offset.y));
            set_entity_target(entity, p, movement_type);
        }
    }
}

static void ui_reset_selected_entity_pane(struct ui_selected_entity_pane *pane) {
    strcpy_s(pane->cur_button_panel_name, MAX_BUTTON_PANEL_NAME_LENGTH, "start-screen");
}

static void ui_reset_selected_entity_panes(void) {
    ui_reset_selected_entity_pane(&ui_state.selected_soldier_ui_pane);
    ui_reset_selected_entity_pane(&ui_state.selected_worker_ui_pane);
    ui_reset_selected_entity_pane(&ui_state.selected_refinery_ui_pane);
    ui_reset_selected_entity_pane(&ui_state.selected_building_construction_ui_pane);
    ui_reset_selected_entity_pane(&ui_state.selected_medic_ui_pane);
    ui_reset_selected_entity_pane(&ui_state.selected_factory_ui_pane);
    ui_reset_selected_entity_pane(&ui_state.selected_heliport_ui_pane);
    ui_reset_selected_entity_pane(&ui_state.selected_command_center_ui_pane);
    ui_reset_selected_entity_pane(&ui_state.selected_barracks_ui_pane);
    ui_reset_selected_entity_pane(&ui_state.selected_tank_ui_pane);
    ui_reset_selected_entity_pane(&ui_state.selected_vehicle_ui_pane);
    ui_reset_selected_entity_pane(&ui_state.selected_helicopter_ui_pane);
    ui_reset_selected_entity_pane(&ui_state.selected_turret_ui_pane);
    ui_reset_selected_entity_pane(&ui_state.selected_supply_depot_ui_pane);
}

static void ui_select_all_units_in_selection_box(void) {
    int i;
    struct entity_id entity_id;

    array_foreach(&_ui_state.entities, entity_id, i) {
        struct entity *entity;
        vec2 ui_position;
        vec2 corner1, corner2;

        entity = get_entity_from_id(entity_id);
        if (!entity) {
            continue;
        }

        corner1 = ui_state.selection_box_corner_1;
        corner2 = ui_state.selection_box_corner_2;

        ui_position = game_to_ui_position(entity->position);

        if ((entity->type == ENTITY_TYPE_SOLDIER ||
             entity->type == ENTITY_TYPE_TANK ||
             entity->type == ENTITY_TYPE_VEHICLE ||
             entity->type == ENTITY_TYPE_HELICOPTER ||
             entity->type == ENTITY_TYPE_WORKER ||
             entity->type == ENTITY_TYPE_MEDIC)) {
            vec2 tl, br;

            tl = corner1;
            br = corner2;

            if (tl.x > br.x) {
                float temp = tl.x;
                tl.x = br.x;
                br.x = temp;
            }

            if (br.y > tl.y) {
                float temp = tl.y;
                tl.y = br.y;
                br.y = temp;
            }

            if (ui_position.x > tl.x && ui_position.x < br.x &&
                ui_position.y > br.y && ui_position.y < tl.y) {
                set_entity_is_selected(entity_id, true);
                ui_reset_selected_entity_panes();
            } else {
                set_entity_is_selected(entity_id, false);
                ui_reset_selected_entity_panes();
            }
        }
    }
}

static void ui_select_all_entities_on_screen_of_type(struct entity_id hovered_entity_id) {
    struct entity *hovered_entity = get_entity_from_id(hovered_entity_id);
    if (!hovered_entity) {
        return;
    }

    enum entity_type type = hovered_entity->type;
    int i;
    struct entity_id entity_id;
    array_foreach(&_ui_state.entities, entity_id, i) {
        struct entity *entity = get_entity_from_id(entity_id);
        if (!entity) {
            continue;
        }
        if (entity->type != type) {
            continue;
        }

        vec2 ui_position = game_to_ui_position(entity->position);
        if (ui_position.x < -8.0f || ui_position.x > 8.0f || ui_position.y < -8.0f || ui_position.y > 8.0f) {
            continue;
        }

        set_entity_is_selected(entity_id, true);
        ui_reset_selected_entity_panes();
    }
}

static void ui_deselect_all_entities(void) {
    int i;
    struct entity_id entity_id;

    array_foreach(&_ui_state.entities, entity_id, i) {
        struct entity *entity;
        entity = get_entity_from_id(entity_id);
        if (!entity) {
            continue;
        }
        if (!entity->is_selectable) {
            continue;
        }

        set_entity_is_selected(entity_id, false);
        ui_reset_selected_entity_panes();
    }
}

static struct entity_id ui_hover_closest_unit(vec2 mouse_ui_pos) {
    int i;
    struct entity_id entity_id;

    //
    // Finds the entity hovered by the mouse closest to the camera.
    //
    struct entity *prev_hovered_entity = NULL;
    struct entity *hovered_entity = NULL;
    float hovered_entity_dist_to_cam = FLT_MAX;
    array_foreach(&_ui_state.entities, entity_id, i) {
        struct entity *entity;
        entity = get_entity_from_id(entity_id);
        if (!entity) {
            continue;
        }
        if (!entity->is_selectable) {
            continue;
        }

        vec2 ui_position = game_to_ui_position(entity->position);
        vec2 points[4];
        get_ui_entity_selection_points(entity->type, ui_position, points);

        if (entity->is_hovered) {
            // Assert that only one entity is hovered at a time
            assert(prev_hovered_entity == NULL);
            prev_hovered_entity = entity;
        }

        if (vec2_point_in_polygon(mouse_ui_pos, 4, points)) {
            float dist_to_cam = entity->position.x + entity->position.y;
            if (dist_to_cam < hovered_entity_dist_to_cam) {
                hovered_entity = entity;
                hovered_entity_dist_to_cam = dist_to_cam;
            }
        }
    }

    if (prev_hovered_entity && prev_hovered_entity != hovered_entity) {
        set_entity_is_hovered(prev_hovered_entity->id, false);
    }

    if (hovered_entity && !hovered_entity->is_hovered) {
        set_entity_is_hovered(hovered_entity->id, true);
    }

    if (hovered_entity) {
        return hovered_entity->id;
    } else {
        struct entity_id id;
        id.idx = 0;
        return id;
    }
}

void ui_update(float dt) {
    {
        struct rect_2D mm_rect;
        vec2 mm_center, mm_size;

        mm_rect = ui_state.ui_pane_rects[UI_PANE_RECT_TYPE_MINIMAP];
        mm_center.x = mm_rect.top_left.x + 0.5f * mm_rect.size.x;
        mm_center.y = mm_rect.top_left.y - 0.5f * mm_rect.size.y;
        mm_size = mm_rect.size;

        vec3 translation_0, translation_1, scale_0;

        translation_0.x = -0.5f * GAME_MAP_WIDTH;
        translation_0.y = -0.5f * GAME_MAP_WIDTH;
        translation_0.z = 0.0f;

        scale_0.x = 1.5f * mm_size.x / GAME_MAP_WIDTH;
        scale_0.y = 1.5f * mm_size.y / GAME_MAP_WIDTH;
        scale_0.z = 1.0f;

        translation_1.x = mm_center.x;
        translation_1.y = mm_center.y;
        translation_1.z = 0.0f;

        ui_state.game_to_minimap_mat = mat4_multiply_n(3,
                                                       mat4_translation(translation_1),
                                                       mat4_scale(scale_0),
                                                       mat4_translation(translation_0));
    }

    bool mouse_in_game = false, mouse_in_minimap = false, mouse_in_pane = false, mouse_in_editor = false;
    vec2 mouse_ui_pos, mouse_game_pos;
    mouse_ui_pos = get_ui_pos(controls_state.left_mouse_pos);
    mouse_game_pos = ui_to_game_position(mouse_ui_pos);

    array_clear(&_ui_state.entities);
    get_all_active_entities_with_type(&_ui_state.entities, 0, NULL);

    struct rect_2D *rects = ui_state.ui_pane_rects;
    if (game_editor_state.is_editor_hovered) {
        mouse_in_editor = true;
    } else if (!rect_2D_contains_point(rects[UI_PANE_RECT_TYPE_MAIN_0], mouse_ui_pos) &&
               !rect_2D_contains_point(rects[UI_PANE_RECT_TYPE_MAIN_1], mouse_ui_pos) &&
               !rect_2D_contains_point(rects[UI_PANE_RECT_TYPE_MAIN_2], mouse_ui_pos) &&
               !rect_2D_contains_point(rects[UI_PANE_RECT_TYPE_MINIMAP], mouse_ui_pos)) {
        mouse_in_game = true;
    } else {
        if (rect_2D_contains_point(rects[UI_PANE_RECT_TYPE_MINIMAP], mouse_ui_pos)) {
            mouse_in_minimap = true;
        } else {
            mouse_in_pane = true;
        }
    }

    if (ui_state.is_placing_entity) {
        struct entity *placeholder;
        struct entity *worker;
        int row, col;
        bool can_afford, can_be_placed;

        placeholder = get_entity_from_id(ui_state.placeholder_id);
        worker = get_entity_from_id(ui_state.worker_id);
        assert(placeholder && worker);
        col = (int)(mouse_game_pos.x / GAME_TILE_WIDTH);
        row = (int)(mouse_game_pos.y / GAME_TILE_WIDTH);
        set_placeholder_row_col(placeholder, row, col);
        can_afford = entity_cost[placeholder->placeholder.type] < game_state.team_money[worker->team_num];
        can_be_placed = placeholder->placeholder.can_be_placed;

        if (can_be_placed && can_afford && mouse_in_game && controls_state.left_mouse_clicked) {
            switch (placeholder->placeholder.type) {
                case ENTITY_TYPE_BARRACKS:
                    set_worker_build_target(worker, ENTITY_TYPE_BARRACKS, row, col,
                                            ui_state.placeholder_id);
                    break;
                case ENTITY_TYPE_FACTORY:
                    set_worker_build_target(worker, ENTITY_TYPE_FACTORY, row, col,
                                            ui_state.placeholder_id);
                    break;
                case ENTITY_TYPE_HELIPORT:
                    set_worker_build_target(worker, ENTITY_TYPE_HELIPORT, row, col,
                                            ui_state.placeholder_id);
                    break;
                case ENTITY_TYPE_SUPPLY_DEPOT:
                    set_worker_build_target(worker, ENTITY_TYPE_SUPPLY_DEPOT, row, col,
                                            ui_state.placeholder_id);
                    break;
                case ENTITY_TYPE_TURRET:
                    set_worker_build_target(worker, ENTITY_TYPE_TURRET, row, col,
                                            ui_state.placeholder_id);
                    break;
                case ENTITY_TYPE_REFINERY:
                    create_refinery_entity(row, col);
                    break;
                default:
                    assert(false && "Invalid placeholder type");
                    break;
            }
            ui_state.placeholder_id.idx = 0;
            ui_state.is_placing_entity = false;
            ui_reset_selected_entity_pane(&ui_state.selected_worker_ui_pane);
        }
    } else if (ui_state.is_doing_unit_action) {
        if (mouse_in_game && controls_state.left_mouse_clicked) {
            enum movement_type movement_type;
            switch (ui_state.unit_action_type) {
                case UNIT_ACTION_TYPE_ATTACK_MOVE: {
                    movement_type = MOVEMENT_TYPE_ATTACK_MOVING;
                    break;
                }
                case UNIT_ACTION_TYPE_MOVE: {
                    movement_type = MOVEMENT_TYPE_MOVING;
                    break;
                }
                case UNIT_ACTION_TYPE_PATROL_MOVE: {
                    movement_type = MOVEMENT_TYPE_PATROLLING;
                    break;
                }
            }

            vec2 target_pos;
            if (mouse_in_minimap) {
                vec3 target_pos_3;

                target_pos_3 = V3(mouse_ui_pos.x, mouse_ui_pos.y, 1.0f);
                target_pos_3 = vec3_apply_mat4(target_pos_3, 1.0f, mat4_inverse(mat4_multiply(ui_state.game_to_minimap_mat, renderer_state.isometric_mat)));

                target_pos = V2(target_pos_3.x, target_pos_3.y);
            } else {
                target_pos = mouse_game_pos;
            }

            ui_set_selected_entities_target(target_pos, NULL, movement_type);
            ui_state.is_doing_unit_action = false;
            ui_reset_selected_entity_panes();
        }
    } else if (ui_state.selection_box_open) {
        ui_select_all_units_in_selection_box();

        if (controls_state.left_mouse_down) {
            ui_state.selection_box_corner_2 = mouse_ui_pos;
        } else {
            ui_state.selection_box_open = false;
        }
    } else if (ui_state.is_moving_minimap_camera) {
        vec2 new_cam_pos;
        vec3 new_cam_pos_3;

        new_cam_pos_3.x = mouse_ui_pos.x;
        new_cam_pos_3.y = mouse_ui_pos.y;
        new_cam_pos_3.z = 1.0f;
        new_cam_pos_3 = vec3_apply_mat4(new_cam_pos_3, 1.0f, mat4_inverse(ui_state.game_to_minimap_mat));
        new_cam_pos.x = new_cam_pos_3.x;
        new_cam_pos.y = new_cam_pos_3.y;

        game_set_cam_pos(new_cam_pos);

        if (!controls_state.left_mouse_down) {
            ui_state.is_moving_minimap_camera = false;
        }
    } else {
        if (mouse_in_game) {
            if (controls_state.left_mouse_down) {
                ui_deselect_all_entities();
                if (!vec2_equal(controls_state.left_mouse_pos, controls_state.left_mouse_down_pos, 0.1f)) {
                    ui_state.selection_box_open = true;
                    ui_state.selection_box_corner_1 = mouse_ui_pos;
                    ui_state.selection_box_corner_2 = mouse_ui_pos;
                }
            }

            struct entity_id hovered_entity = ui_hover_closest_unit(mouse_ui_pos);
            if (controls_state.left_mouse_clicked) {
                if (controls_state.key_down[GLFW_KEY_LEFT_CONTROL]) {
                    ui_select_all_entities_on_screen_of_type(hovered_entity);
                } else {
                    set_entity_is_selected(hovered_entity, true);
                    ui_reset_selected_entity_panes();
                }
            }

            if (controls_state.right_mouse_clicked) {
                ui_set_selected_entities_target(mouse_game_pos, get_entity_from_id(hovered_entity), MOVEMENT_TYPE_MOVING);
            }
        } else if (mouse_in_minimap) {
            if (controls_state.left_mouse_down) {
                ui_state.is_moving_minimap_camera = true;
            }
            if (controls_state.right_mouse_clicked) {
                vec2 game_pos;
                vec3 game_pos_3;

                game_pos_3 = V3(mouse_ui_pos.x, mouse_ui_pos.y, 1.0f);
                game_pos_3 = vec3_apply_mat4(game_pos_3, 1.0f, mat4_inverse(mat4_multiply(ui_state.game_to_minimap_mat, renderer_state.isometric_mat)));
                game_pos = V2(game_pos_3.x, game_pos_3.y);

                ui_set_selected_entities_target(game_pos, NULL, MOVEMENT_TYPE_MOVING);
            }
        }
    }

    {
        vec2 new_cam_pos = game_state.cam_pos;
        if (mouse_ui_pos.x <= -7.9f) {
            new_cam_pos.x -= 2.5f * dt;
        }
        if (mouse_ui_pos.x >= 7.9f) {
            new_cam_pos.x += 2.5f * dt;
        }
        if (mouse_ui_pos.y <= -4.4f) {
            new_cam_pos.y -= 2.5f * dt;
        }
        if (mouse_ui_pos.y >= 4.4f) {
            new_cam_pos.y += 2.5f * dt;
        }
        game_set_cam_pos(new_cam_pos);
    }
}

static void draw_filled_bar(vec2 game_pos, float ui_dy, vec2 size, float pct, vec3 bg_color, vec3 fg_color, vec3 border_color) {
    float a = pct;

    vec2 ui_pos = game_to_ui_position(game_pos);
    vec2 ui_pos_0 = game_to_ui_position(V2(game_pos.x - GAME_TILE_WIDTH, game_pos.y));
    vec2 ui_pos_1 = game_to_ui_position(V2(game_pos.x + GAME_TILE_WIDTH, game_pos.y));
    float s = fabsf(ui_pos_1.x - ui_pos_0.x);

    float w0 = size.x * s;
    float h0 = size.y;
    float w1 = size.x * s - 0.05f;
    float h1 = size.y - 0.05f;

    {
        vec2 position, scale;
        vec3 color;
        float opacity;

        position = ui_pos;
        position.y += ui_dy * s;
        scale = V2(w0, h0);
        color = bg_color;
        opacity = 1.0f;

        draw_square(position, scale, color, opacity);
    }

    {
        vec2 position, scale;
        vec3 color;
        float opacity;

        position = ui_pos;
        position.y += ui_dy * s;
        scale = V2(w1, h1);
        color = border_color;
        opacity = 1.0f;

        draw_square(position, scale, color, opacity);
    }

    {
        vec2 position, scale;
        vec3 color;
        float opacity;

        position = ui_pos;
        position.x -= 0.5f * w1;
        position.x += 0.5f * a * w1;
        position.y += ui_dy * s;
        scale = V2(a * w1, h1);
        color = fg_color;
        opacity = 1.0f;

        draw_square(position, scale, color, opacity);
    }
}

static void draw_rally_line(vec2 game_pos_0, vec2 game_pos_1) {
    {
        vec2 pos_0 = game_to_ui_position(game_pos_0);
        vec2 pos_1 = game_to_ui_position(game_pos_1);
        float len = vec2_distance(pos_0, pos_1);

        struct mesh *mesh = &mesh_store.square_mesh;
        vec2 pos = vec2_scale(vec2_add(pos_0, pos_1), 0.5f);
        vec2 scale = V2(len, 0.04f);
        float angle = acosf((pos_0.x - pos_1.x) / len);
        if (pos_1.y > pos_0.y) {
            angle *= -1.0f;
        }
        struct texture texture = create_color_texture(V3(1.0f, 0.0f, 0.0f));
        float opacity = 1.0f;
        ui_draw_mesh(mesh, pos, scale, angle, texture, opacity);
    }

    {
        struct mesh *mesh = &mesh_store.circle_mesh;
        vec2 pos = game_to_ui_position(game_pos_1);
        vec2 scale = V2(0.06f, 0.06f);
        float angle = 0.0f;
        struct texture texture = create_color_texture(V3(1.0f, 0.0f, 0.0f));
        float opacity = 1.0f;
        ui_draw_mesh(mesh, pos, scale, angle, texture, opacity);
    }
}

static void draw_command_center_ui(struct entity *command_center) {
    struct unit_construction_component *component;

    component = get_entity_component_of_type(command_center, COMPONENT_TYPE_UNIT_CONSTRUCTION, "unit_construction");
    assert(component);

    if (component->spawn_list.length > 0) {
        vec2 game_pos = command_center->position;
        float pct = 1.0f - component->spawn_time_left / component->spawn_time;
        float ui_dy = 0.8f;
        vec2 size = V2(1.5f, 0.1f);
        vec3 bg_color = V3(0.0f, 0.0f, 0.0f);
        vec3 fg_color = V3(0.0f, 0.0f, 0.7f);
        vec3 border_color = V3(0.4f, 0.4f, 0.4f);
        draw_filled_bar(game_pos, ui_dy, size, pct, bg_color, fg_color, border_color);
    }

    if (command_center->is_selected) {
        vec2 game_pos_0 = command_center->position;
        vec2 game_pos_1 = vec2_add(command_center->position, component->rally_point);
        draw_rally_line(game_pos_0, game_pos_1);
    }
}

static void draw_barracks_ui(struct entity *barracks) {
    struct unit_construction_component *component;

    component = get_entity_component_of_type(barracks, COMPONENT_TYPE_UNIT_CONSTRUCTION, "unit_construction");
    assert(component);

    if (component->spawn_list.length > 0) {
        vec2 game_pos = barracks->position;
        float pct = 1.0f - component->spawn_time_left / component->spawn_time;
        float ui_dy = 0.8f;
        vec2 size = V2(1.5f, 0.1f);
        vec3 bg_color = V3(0.0f, 0.0f, 0.0f);
        vec3 fg_color = V3(0.0f, 0.0f, 0.7f);
        vec3 border_color = V3(0.4f, 0.4f, 0.4f);
        draw_filled_bar(game_pos, ui_dy, size, pct, bg_color, fg_color, border_color);
    }

    if (barracks->is_selected) {
        vec2 game_pos_0 = barracks->position;
        vec2 game_pos_1 = vec2_add(barracks->position, component->rally_point);
        draw_rally_line(game_pos_0, game_pos_1);
    }
}

static void draw_factory_ui(struct entity *factory) {
    struct unit_construction_component *component;

    component = get_entity_component_of_type(factory, COMPONENT_TYPE_UNIT_CONSTRUCTION, "unit_construction");
    assert(component);

    if (component->spawn_list.length > 0) {
        vec2 game_pos = factory->position;
        float pct = 1.0f - component->spawn_time_left / component->spawn_time;
        float ui_dy = 0.8f;
        vec2 size = V2(1.5f, 0.1f);
        vec3 bg_color = V3(0.0f, 0.0f, 0.0f);
        vec3 fg_color = V3(0.0f, 0.0f, 0.7f);
        vec3 border_color = V3(0.4f, 0.4f, 0.4f);
        draw_filled_bar(game_pos, ui_dy, size, pct, bg_color, fg_color, border_color);
    }

    if (factory->is_selected) {
        vec2 game_pos_0 = factory->position;
        vec2 game_pos_1 = vec2_add(factory->position, component->rally_point);
        draw_rally_line(game_pos_0, game_pos_1);
    }
}

static void draw_tank_ui(struct entity *tank) {
    vec2 game_pos = tank->position;
    float pct = tank->health / tank->start_health;
    float ui_dy = 0.8f;
    vec2 size = V2(0.8f, 0.1f);
    vec3 bg_color = V3(0.0f, 0.0f, 0.0f);
    vec3 fg_color = V3(1.0f, 1.0f, 1.0f);
    vec3 border_color = V3(0.4f, 0.4f, 0.4f);

    if (pct > 0.7f) {
        fg_color = V3(0.1f, 0.7f, 0.2f);
    } else if (pct > 0.3f) {
        fg_color = V3(0.7f, 0.7f, 0.1f);
    } else {
        fg_color = V3(0.7f, 0.2f, 0.1f);
    }

    draw_filled_bar(game_pos, ui_dy, size, pct, bg_color, fg_color, border_color);
}

static void draw_vehicle_ui(struct entity *vehicle) {
    vec2 game_pos = vehicle->position;
    float pct = vehicle->health / vehicle->start_health;
    float ui_dy = 0.8f;
    vec2 size = V2(0.8f, 0.1f);
    vec3 bg_color = V3(0.0f, 0.0f, 0.0f);
    vec3 fg_color = V3(1.0f, 1.0f, 1.0f);
    vec3 border_color = V3(0.4f, 0.4f, 0.4f);

    if (pct > 0.7f) {
        fg_color = V3(0.1f, 0.7f, 0.2f);
    } else if (pct > 0.3f) {
        fg_color = V3(0.7f, 0.7f, 0.1f);
    } else {
        fg_color = V3(0.7f, 0.2f, 0.1f);
    }

    draw_filled_bar(game_pos, ui_dy, size, pct, bg_color, fg_color, border_color);
}

static void draw_helicopter_ui(struct entity *helicopter) {
    vec2 game_pos = helicopter->position;
    float pct = helicopter->health / helicopter->start_health;
    float ui_dy = 0.8f;
    vec2 size = V2(0.8f, 0.1f);
    vec3 bg_color = V3(0.0f, 0.0f, 0.0f);
    vec3 fg_color = V3(1.0f, 1.0f, 1.0f);
    vec3 border_color = V3(0.4f, 0.4f, 0.4f);

    if (pct > 0.7f) {
        fg_color = V3(0.1f, 0.7f, 0.2f);
    } else if (pct > 0.3f) {
        fg_color = V3(0.7f, 0.7f, 0.1f);
    } else {
        fg_color = V3(0.7f, 0.2f, 0.1f);
    }

    draw_filled_bar(game_pos, ui_dy, size, pct, bg_color, fg_color, border_color);
}

static void draw_soldier_ui(struct entity *soldier) {
    vec2 game_pos = soldier->position;
    float pct = soldier->health / soldier->start_health;
    float ui_dy = 0.8f;
    vec2 size = V2(0.5f, 0.1f);
    vec3 bg_color = V3(0.0f, 0.0f, 0.0f);
    vec3 fg_color = V3(1.0f, 1.0f, 1.0f);
    vec3 border_color = V3(0.4f, 0.4f, 0.4f);

    if (pct > 0.7f) {
        fg_color = V3(0.1f, 0.7f, 0.2f);
    } else if (pct > 0.3f) {
        fg_color = V3(0.7f, 0.7f, 0.1f);
    } else {
        fg_color = V3(0.7f, 0.2f, 0.1f);
    }

    draw_filled_bar(game_pos, ui_dy, size, pct, bg_color, fg_color, border_color);
}

static void draw_worker_ui(struct entity *worker) {
    vec2 game_pos = worker->position;
    float pct = worker->health / worker->start_health;
    float ui_dy = 0.8f;
    vec2 size = V2(0.5f, 0.1f);
    vec3 bg_color = V3(0.0f, 0.0f, 0.0f);
    vec3 fg_color = V3(1.0f, 1.0f, 1.0f);
    vec3 border_color = V3(0.4f, 0.4f, 0.4f);

    if (pct > 0.7f) {
        fg_color = V3(0.1f, 0.7f, 0.2f);
    } else if (pct > 0.3f) {
        fg_color = V3(0.7f, 0.7f, 0.1f);
    } else {
        fg_color = V3(0.7f, 0.2f, 0.1f);
    }

    draw_filled_bar(game_pos, ui_dy, size, pct, bg_color, fg_color, border_color);
}

static void draw_medic_ui(struct entity *medic) {
    vec2 game_pos = medic->position;
    float pct = medic->health / medic->start_health;
    float ui_dy = 0.8f;
    vec2 size = V2(0.5f, 0.1f);
    vec3 bg_color = V3(0.0f, 0.0f, 0.0f);
    vec3 fg_color = V3(1.0f, 1.0f, 1.0f);
    vec3 border_color = V3(0.4f, 0.4f, 0.4f);

    if (pct > 0.7f) {
        fg_color = V3(0.1f, 0.7f, 0.2f);
    } else if (pct > 0.3f) {
        fg_color = V3(0.7f, 0.7f, 0.1f);
    } else {
        fg_color = V3(0.7f, 0.2f, 0.1f);
    }

    draw_filled_bar(game_pos, ui_dy, size, pct, bg_color, fg_color, border_color);
}

void ui_draw(void) {
    glDisable(GL_DEPTH_TEST);

    {
        struct entity_id entity_id;
        int i;
        array_foreach(&_ui_state.entities, entity_id, i) {
            struct entity *entity;
            entity = get_entity_from_id(entity_id);
            if (!entity) {
                continue;
            }

            if (!entity->is_visible) {
                continue;
            }

            switch (entity->type) {
                case ENTITY_TYPE_COMMAND_CENTER:
                    draw_command_center_ui(entity);
                    break;

                case ENTITY_TYPE_BARRACKS:
                    draw_barracks_ui(entity);
                    break;

                case ENTITY_TYPE_FACTORY:
                    draw_factory_ui(entity);
                    break;

                case ENTITY_TYPE_SOLDIER:
                    draw_soldier_ui(entity);
                    break;

                case ENTITY_TYPE_WORKER:
                    draw_worker_ui(entity);
                    break;

                case ENTITY_TYPE_MEDIC:
                    draw_medic_ui(entity);
                    break;

                case ENTITY_TYPE_TANK:
                    draw_tank_ui(entity);
                    break;

                case ENTITY_TYPE_VEHICLE:
                    draw_vehicle_ui(entity);
                    break;

                case ENTITY_TYPE_HELICOPTER:
                    draw_helicopter_ui(entity);
                    break;

                default:
                    break;
            }
        }
    }

    //
    // Draw unit selection square
    //
    if (ui_state.selection_box_open) {
        vec2 tl, br, s, l, r, t, b;

        tl = ui_state.selection_box_corner_1;
        br = ui_state.selection_box_corner_2;

        s = V2(fabsf(br.x - tl.x), fabsf(tl.y - br.y));
        l = V2(tl.x, 0.5f * (tl.y + br.y));
        r = V2(br.x, 0.5f * (tl.y + br.y));
        t = V2(0.5f * (tl.x + br.x), tl.y);
        b = V2(0.5f * (tl.x + br.x), br.y);

        draw_square(l, V2(0.01f, s.y), V3(0.2f, 0.8f, 0.2f), 1.0f);
        draw_square(r, V2(0.01f, s.y), V3(0.2f, 0.8f, 0.2f), 1.0f);
        draw_square(t, V2(s.x, 0.01f), V3(0.2f, 0.8f, 0.2f), 1.0f);
        draw_square(b, V2(s.x, 0.01f), V3(0.2f, 0.8f, 0.2f), 1.0f);
    }

    draw_minimap_panel();
    draw_resources_info();

    {
        float a = 16.0f / 359.0f;
        float w = 359.0f * a;
        float h = 65.0f * a;

        struct mesh *mesh = &mesh_store.square_mesh;
        vec2 position = V2(0.0f, -4.5f + 0.5f * h);
        vec2 scale = V2(w, -h);
        float angle = 0.0f;
        struct texture texture = texture_store.ui_hud_panel;
        float opacity = 1.0f;
        ui_draw_mesh(mesh, position, scale, angle, texture, opacity);
    }

    {
        struct entity *selected_command_center = NULL;
        struct entity *selected_barracks = NULL;
        struct entity *selected_factory = NULL;
        struct entity *selected_heliport = NULL;
        struct entity *selected_refinery = NULL;
        struct entity *selected_building_construction = NULL;
        struct entity *selected_worker = NULL;
        struct entity *selected_soldier = NULL;
        struct entity *selected_medic = NULL;
        struct entity *selected_tank = NULL;
        struct entity *selected_vehicle = NULL;
        struct entity *selected_helicopter = NULL;
        struct entity *selected_turret = NULL;
        struct entity *selected_supply_depot = NULL;

        struct entity *entity;
        struct entity_id entity_id;
        int i;
        array_foreach(&game_state.selected_entity_array, entity_id, i) {
            entity = get_entity_from_id(entity_id);
            if (!entity) {
                continue;
            }

            switch (entity->type) {
                case ENTITY_TYPE_COMMAND_CENTER:
                    selected_command_center = entity;
                    break;
                case ENTITY_TYPE_BARRACKS:
                    selected_barracks = entity;
                    break;
                case ENTITY_TYPE_FACTORY:
                    selected_factory = entity;
                    break;
                case ENTITY_TYPE_HELIPORT:
                    selected_heliport = entity;
                    break;
                case ENTITY_TYPE_REFINERY:
                    selected_refinery = entity;
                    break;
                case ENTITY_TYPE_BUILDING_CONSTRUCTION_LARGE:
                case ENTITY_TYPE_BUILDING_CONSTRUCTION_SMALL:
                    selected_building_construction = entity;
                    break;
                case ENTITY_TYPE_WORKER:
                    selected_worker = entity;
                    break;
                case ENTITY_TYPE_SOLDIER:
                    selected_soldier = entity;
                    break;
                case ENTITY_TYPE_MEDIC:
                    selected_medic = entity;
                    break;
                case ENTITY_TYPE_TANK:
                    selected_tank = entity;
                    break;
                case ENTITY_TYPE_VEHICLE:
                    selected_vehicle = entity;
                    break;
                case ENTITY_TYPE_TURRET:
                    selected_turret = entity;
                    break;
                case ENTITY_TYPE_SUPPLY_DEPOT:
                    selected_supply_depot = entity;
                    break;
                case ENTITY_TYPE_HELICOPTER:
                    selected_helicopter = entity;
                    break;
                default:
                    break;
            }
        }

        if (selected_command_center) {
            draw_selected_command_center(selected_command_center);
        } else if (selected_barracks) {
            draw_selected_barracks(selected_barracks);
        } else if (selected_factory) {
            draw_selected_factory(selected_factory);
        } else if (selected_heliport) {
            draw_selected_heliport(selected_heliport);
        } else if (selected_refinery) {
            draw_selected_refinery(selected_refinery);
        } else if (selected_building_construction) {
            draw_selected_building_construction(selected_building_construction);
        } else if (selected_worker) {
            draw_selected_worker(selected_worker);
        } else if (selected_soldier) {
            draw_selected_soldier(selected_soldier);
        } else if (selected_medic) {
            draw_selected_medic(selected_medic);
        } else if (selected_tank) {
            draw_selected_tank(selected_tank);
        } else if (selected_vehicle) {
            draw_selected_vehicle(selected_vehicle);
        } else if (selected_helicopter) {
            draw_selected_helicopter(selected_helicopter);
        } else if (selected_turret) {
            draw_selected_turret(selected_turret);
        } else if (selected_supply_depot) {
            draw_selected_supply_depot(selected_supply_depot);
        }
    }

    /*
	{
		vec2 m_pos = get_ui_pos(controls_state.left_mouse_pos);
		vec2 size = V2(0.1f, 0.1f);
		vec3 color = V3(1.0f, 1.0f, 1.0f);
		float z =1.0f;
		float opacity = 1.0f;
		draw_square(m_pos, size, color, z, opacity);
	}
	*/
}
