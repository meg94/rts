#ifndef _RENDERER_TEXTURE_H
#define _RENDERER_TEXTURE_H

#include "IGL.h"
#include "map.h"
#include "maths.h"

struct texture_atlas_data {
    int x, y, w, h;
};

typedef map_t(struct texture_atlas_data) map_texture_atlas_data_t;

enum texture_type {
    TEXTURE_TYPE_IMAGE,
    TEXTURE_TYPE_COLOR,
    TEXTURE_TYPE_ATLAS_IMAGE,
    TEXTURE_TYPE_SOLID_COLOR_ATLAS_IMAGE,
};

struct texture {
    enum texture_type type;

    union {
        GLuint image;
        vec3 color;
        struct texture_atlas_data atlas_image;

        struct {
            vec3 color;
            float color_pct;
            struct texture_atlas_data atlas_data;
        } solid_color_atlas_image;
    };
};

struct texture_store {
    map_texture_atlas_data_t atlas_data_map;
    struct texture texture_atlas;

    struct texture circle_texture;
    struct texture bricks_texture;
    struct texture red, green, blue, pink, teal, yellow, white;

    struct texture map;
    struct texture tile;

    struct texture worker_down[4];
    struct texture worker_down_d[4];
    struct texture worker_side[4];
    struct texture worker_up[4];
    struct texture worker_up_d[4];

    struct texture infantry_down[4];
    struct texture infantry_down_d[4];
    struct texture infantry_side[4];
    struct texture infantry_up[4];
    struct texture infantry_up_d[4];

    struct texture elite_infantry_down[4];
    struct texture elite_infantry_down_d[4];
    struct texture elite_infantry_side[4];
    struct texture elite_infantry_up[4];
    struct texture elite_infantry_up_d[4];

    struct texture tank_down[2];
    struct texture tank_down_d[2];
    struct texture tank_side[2];
    struct texture tank_up[2];
    struct texture tank_up_d[2];

    struct texture helicopter_down[2];
    struct texture helicopter_down_d[2];
    struct texture helicopter_side[2];
    struct texture helicopter_up[2];
    struct texture helicopter_up_d[2];

    struct texture barracks[6];
    struct texture factory[6];
    struct texture heliport[6];
    struct texture command_center[6];
    struct texture turret[8];
    struct texture refinery[3];
    struct texture supply_depot[2];
    struct texture barrier;
    struct texture unit_selection[4];
    struct texture building_selection[4];
    struct texture vehicle_selection[5];
    struct texture explosion[9];
    struct texture bullet[5];
    struct texture under_construction[19];

    struct texture buttons_square[3];
    struct texture health_bar[5];

    struct texture ui_hud_panel;
} texture_store;

void texture_store_init(void);
GLuint texture_load(const char *file_name, int filter);
struct texture create_image_texture(GLuint image);
struct texture create_color_texture(vec3 color);
struct texture create_atlas_image_texture(const char *texture_name);
struct texture create_solid_color_atlas_image_texture(const char *texture_name, vec3 color, float color_pct);

#endif
