#ifndef _PHYSICS_PHYSICS_H
#define _PHYSICS_PHYSICS_H

#include <chipmunk/chipmunk.h>

#include "maths.h"

struct physics_state {
    cpSpace *cp_space;
} physics_state;

void physics_init(void);
void physics_update(float dt);
struct collider_id physics_segment_query(vec2 start, vec2 end, float radius);

#endif
