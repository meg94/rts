#include "entity.h"

#include <assert.h>
#include <float.h>

#include "collider.h"
#include "game.h"
#include "mesh.h"
#include "texture.h"

const char *entity_type_name[NUM_OF_ENTITY_TYPES] = {
    [ENTITY_TYPE_COMMAND_CENTER] = "command_center",
    [ENTITY_TYPE_BARRACKS] = "barracks",
    [ENTITY_TYPE_FACTORY] = "factory",
    [ENTITY_TYPE_HELIPORT] = "heliport",
    [ENTITY_TYPE_WORKER] = "worker",
    [ENTITY_TYPE_SOLDIER] = "soldier",
    [ENTITY_TYPE_MEDIC] = "medic",
    [ENTITY_TYPE_SOLDIER_PROJECTILE] = "soldier_projectile",
    [ENTITY_TYPE_TANK] = "tank",
    [ENTITY_TYPE_VEHICLE] = "vehicle",
    [ENTITY_TYPE_TANK_PROJECTILE] = "tank_projectile",
    [ENTITY_TYPE_HELICOPTER] = "helicopter",
    [ENTITY_TYPE_HELICOPTER_PROJECTILE] = "helicopter_projectile",
    [ENTITY_TYPE_EXPLOSION] = "explosion",
    [ENTITY_TYPE_PLACEHOLDER] = "placeholder",
    [ENTITY_TYPE_TERRAIN_BORDER] = "terrain_border",
    [ENTITY_TYPE_TURRET] = "turret",
    [ENTITY_TYPE_SUPPLY_DEPOT] = "supply_depot",
    [ENTITY_TYPE_REFINERY] = "refinery",
    [ENTITY_TYPE_BUILDING_CONSTRUCTION_LARGE] = "building_construction_large",
    [ENTITY_TYPE_BUILDING_CONSTRUCTION_SMALL] = "building_construction_small",
};
int entity_cost[NUM_OF_ENTITY_TYPES] = {
    [ENTITY_TYPE_COMMAND_CENTER] = 0,
    [ENTITY_TYPE_BARRACKS] = 150,
    [ENTITY_TYPE_FACTORY] = 200,
    [ENTITY_TYPE_HELIPORT] = 200,
    [ENTITY_TYPE_WORKER] = 50,
    [ENTITY_TYPE_SOLDIER] = 50,
    [ENTITY_TYPE_MEDIC] = 50,
    [ENTITY_TYPE_SOLDIER_PROJECTILE] = 0,
    [ENTITY_TYPE_TANK] = 100,
    [ENTITY_TYPE_VEHICLE] = 75,
    [ENTITY_TYPE_TANK_PROJECTILE] = 0,
    [ENTITY_TYPE_HELICOPTER] = 150,
    [ENTITY_TYPE_HELICOPTER_PROJECTILE] = 0,
    [ENTITY_TYPE_EXPLOSION] = 0,
    [ENTITY_TYPE_PLACEHOLDER] = 0,
    [ENTITY_TYPE_TERRAIN_BORDER] = 0,
    [ENTITY_TYPE_TURRET] = 100,
    [ENTITY_TYPE_SUPPLY_DEPOT] = 100,
    [ENTITY_TYPE_REFINERY] = 0,
    [ENTITY_TYPE_BUILDING_CONSTRUCTION_LARGE] = 0,
    [ENTITY_TYPE_BUILDING_CONSTRUCTION_SMALL] = 0,
};

void *get_entity_component_of_type(struct entity *entity, enum component_type type, const char *name) {
    struct component *compo;
    compo = map_get(&entity->components, name);
    if (!compo) {
        return NULL;
    }

    assert(compo->type == type);

    switch (type) {
        case COMPONENT_TYPE_ATTACK:
            return &compo->attack;
        case COMPONENT_TYPE_MOVEMENT:
            return &compo->movement;
        case COMPONENT_TYPE_ANIMATION:
            return &compo->animation;
        case COMPONENT_TYPE_ANIMATED_MESH:
            return &compo->animated_mesh;
        case COMPONENT_TYPE_ANIMATED_MESH_MOVEMENT:
            return &compo->animated_mesh_movement;
        case COMPONENT_TYPE_HEALING:
            return &compo->healing;
        case COMPONENT_TYPE_BUILDING_CONSTRUCTION:
            return &compo->building_construction;
        case COMPONENT_TYPE_MINING:
            return &compo->mining;
        case COMPONENT_TYPE_UNIT_CONSTRUCTION:
            return &compo->unit_construction;
    }
    return NULL;
}

static void init_animated_mesh_component(struct animated_mesh_component *component, struct texture *textures,
                                         int num_textures, struct mesh_instance_child *mesh, bool is_playing) {
    component->t = 0.0f;
    component->idx = 0;
    component->num_textures = num_textures;
    for (int i = 0; i < num_textures; i++) {
        component->textures[i] = textures[i];
    }
    component->mesh = mesh;
    component->play_once = false;
    component->is_playing = is_playing;
}

static void update_animated_mesh_component(struct animated_mesh_component *component, float dt) {
    if (!component->is_playing) {
        component->mesh->is_visible = false;
        return;
    }

    component->t += dt;
    if (component->t > 0.2f) {
        component->idx++;
        if (component->idx == component->num_textures) {
            if (component->play_once) {
                component->is_playing = false;
            }
            component->idx = 0;
        }
        component->t = 0.0f;
    }
    component->mesh->texture = component->textures[component->idx];
}

static void init_attack_component(struct attack_component *component, float attack_radius) {
    component->closest_enemy_id.idx = 0;
    component->time_since_attack = 0.0f;
    component->attack_radius = attack_radius;
}

static void update_attack_component(struct attack_component *component,
                                    vec2 position, vec2 *aim_dir,
                                    bool *did_shoot, float dt) {
    struct entity *closest_enemy;
    float dist_to_enemy;

    closest_enemy = get_entity_from_id(component->closest_enemy_id);
    if (!closest_enemy) {
        vec3 aim_dir3 = V3(0.0f, -1.0f, 0.0f);
        aim_dir3 = vec3_apply_mat4(aim_dir3, 0.0f, mat4_inverse(renderer_state.isometric_mat));
        *aim_dir = V2(aim_dir3.x, aim_dir3.y);
        return;
    }

    *aim_dir = vec2_subtract(closest_enemy->position, position);
    dist_to_enemy = vec2_length(*aim_dir),
    *aim_dir = vec2_scale(*aim_dir, 1.0f / dist_to_enemy);

    component->time_since_attack += dt;
    if (component->time_since_attack > 1.0f && dist_to_enemy < 2.0f) {
        *did_shoot = true;
        component->time_since_attack = 0.0f;
    } else {
        *did_shoot = false;
    }
}

static void init_movement_component(struct movement_component *component) {
    component->type = MOVEMENT_TYPE_MOVING;
    component->moving_to_target = false;
    component->path_index = 0;
    array_init(&component->path);
}

static void deinit_movement_component(struct movement_component *component) {
    array_deinit(&component->path);
}

static void update_movement_component(struct movement_component *component,
                                      vec2 position, vec2 velocity,
                                      vec2 *force, bool *arrived, float dt) {
    *arrived = false;
    *force = V2_ZERO;
    if (component->moving_to_target) {
        float dist, speed_toward;
        vec2 target, to_target, away_vel, toward_vel;
        int target_path_index;
        array_vec2_t *target_path;

        target_path_index = component->path_index;
        target_path = &component->path;

        if (target_path_index >= target_path->length) {
            *arrived = true;
            return;
        }

        target = target_path->data[target_path_index];
        to_target = vec2_subtract(target, position);
        dist = vec2_length(to_target);
        if (dist > 0.0f) {
            to_target = vec2_scale(to_target, 1.0f / dist);
        }

        away_vel = vec2_perpindicular_component(velocity, to_target);
        *force = vec2_add(*force, vec2_scale(away_vel, -100.0f));

        toward_vel = vec2_parallel_component(velocity, to_target);
        speed_toward = vec2_dot(vec2_normalize(toward_vel), to_target) *
                       vec2_length(toward_vel);

        *force = vec2_add(*force, vec2_scale(to_target, 50.0f * (0.5f - speed_toward)));

        if (dist < 0.01f) {
            component->path_index++;
            if (component->path_index >= target_path->length) {
                if (component->type == MOVEMENT_TYPE_PATROLLING) {
                    component->path_index = 0;
                    array_reverse(target_path);
                } else {
                    *arrived = true;
                }
            }
        }
    } else {
        *force = vec2_scale(velocity, -100.0f);
    }
}

static void init_healing_component(struct healing_component *component, float healing_radius) {
    component->healing_entity_id.idx = 0;
    component->time_since_healing = 0.0f;
    component->healing_radius = healing_radius;
}

static void update_healing_component(struct healing_component *component, struct entity *healer, float dt) {
    struct entity *healing_entity;
    struct animated_mesh_component *healing_animated_mesh_compo;
    struct animated_mesh_component *healing_source_animated_mesh_compo;

    healing_entity = get_entity_from_id(component->healing_entity_id);
    if (!healing_entity) {
        return;
    }

    healing_animated_mesh_compo = get_entity_component_of_type(healing_entity, COMPONENT_TYPE_ANIMATED_MESH, "healing_animated_mesh");
    if (healing_animated_mesh_compo) {
        if (!healing_animated_mesh_compo->is_playing) {
            healing_animated_mesh_compo->is_playing = true;
            healing_animated_mesh_compo->mesh->is_visible = true;
        }
    }

    healing_source_animated_mesh_compo = get_entity_component_of_type(healer, COMPONENT_TYPE_ANIMATED_MESH, "healing_source_animated_mesh");
    if (healing_source_animated_mesh_compo) {
        if (!healing_source_animated_mesh_compo->is_playing) {
            healing_source_animated_mesh_compo->is_playing = true;
            healing_source_animated_mesh_compo->mesh->is_visible = true;
        }
    }

    component->time_since_healing += dt;
    if (component->time_since_healing > 1.0f) {
        healing_entity->health += 10;
        if (healing_entity->health > healing_entity->start_health) {
            healing_entity->health = healing_entity->start_health;
        }
        component->time_since_healing = 0.0f;
    }
}

static void init_animation_component(struct animation_component *component) {
    component->time = 0.0f;
}

static void update_animation_component(struct animation_component *component,
                                       vec2 aim_dir, vec2 velocity,
                                       int *look_dir_num, int *frame_num,
                                       float dt) {
    vec2 unit_dir;
    float speed = vec2_length(velocity);

    component->time += dt;
    if (component->time > 1.0f || speed < 0.01f) {
        component->time = 0.0f;
    }

    if (speed < 0.01f) {
        unit_dir = aim_dir;
        vec3 dir_3 = V3(unit_dir.x, unit_dir.y, 0.0f);
        dir_3 = vec3_apply_mat4(dir_3, 0.0f, renderer_state.isometric_mat);
        unit_dir = V2(dir_3.x, dir_3.y);
    } else {
        unit_dir = vec2_scale(velocity, 1.0f / speed);
        vec3 dir_3 = V3(unit_dir.x, unit_dir.y, 0.0f);
        dir_3 = vec3_apply_mat4(dir_3, 0.0f, renderer_state.isometric_mat);
        unit_dir = V2(dir_3.x, dir_3.y);
    }

    vec2 dirs[8] = {
        vec2_rotate(V2(1.0f, 0.0f), 0.0f),
        vec2_rotate(V2(1.0f, 0.0f), 0.25f * (float)M_PI),
        vec2_rotate(V2(1.0f, 0.0f), 0.5f * (float)M_PI),
        vec2_rotate(V2(1.0f, 0.0f), 0.75f * (float)M_PI),
        vec2_rotate(V2(1.0f, 0.0f), 1.0f * (float)M_PI),
        vec2_rotate(V2(1.0f, 0.0f), 1.25f * (float)M_PI),
        vec2_rotate(V2(1.0f, 0.0f), 1.5f * (float)M_PI),
        vec2_rotate(V2(1.0f, 0.0f), 1.75f * (float)M_PI),
    };

    int closest_dir = 0;
    float closest_dir_dot = FLT_MAX;
    for (int i = 0; i < 8; i++) {
        float dot = vec2_dot(unit_dir, dirs[i]);
        if (dot < closest_dir_dot) {
            closest_dir = i;
            closest_dir_dot = dot;
        }
    }

    *look_dir_num = closest_dir;

    if (speed < 0.01f) {
        *frame_num = 1;
    } else {
        if (component->time < 0.25f) {
            *frame_num = 0;
        } else if (component->time < 0.5f) {
            *frame_num = 1;
        } else if (component->time < 0.75f) {
            *frame_num = 2;
        } else {
            *frame_num = 3;
        }
    }
}

static void init_animated_mesh_movement_component(struct animated_mesh_movement_component *component,
                                                  const char *base_texture_name, int num_textures,
                                                  struct mesh_instance_child *body) {
    component->time = 0.0f;
    strcpy_s(component->base_texture_name, 256, base_texture_name);
    component->body = body;
    component->num_textures = num_textures;
}

static void update_animated_mesh_movement_component(struct animated_mesh_movement_component *component,
                                                    vec2 aim_dir, vec2 velocity, float dt) {
    vec2 unit_dir;
    float speed;

    speed = vec2_length(velocity);

    if (speed < 0.01f) {
        vec3 dir_3;

        unit_dir = aim_dir;
        dir_3 = V3(unit_dir.x, unit_dir.y, 0.0f);
        dir_3 = vec3_apply_mat4(dir_3, 0.0f, renderer_state.isometric_mat);
        unit_dir = V2(dir_3.x, dir_3.y);
    } else {
        vec3 dir_3;

        unit_dir = vec2_scale(velocity, 1.0f / speed);
        dir_3 = V3(unit_dir.x, unit_dir.y, 0.0f);
        dir_3 = vec3_apply_mat4(dir_3, 0.0f, renderer_state.isometric_mat);
        unit_dir = V2(dir_3.x, dir_3.y);
    }

    vec2 dirs[8] = {
        vec2_rotate(V2(1.0f, 0.0f), 0.0f),
        vec2_rotate(V2(1.0f, 0.0f), 0.25f * (float)M_PI),
        vec2_rotate(V2(1.0f, 0.0f), 0.5f * (float)M_PI),
        vec2_rotate(V2(1.0f, 0.0f), 0.75f * (float)M_PI),
        vec2_rotate(V2(1.0f, 0.0f), 1.0f * (float)M_PI),
        vec2_rotate(V2(1.0f, 0.0f), 1.25f * (float)M_PI),
        vec2_rotate(V2(1.0f, 0.0f), 1.5f * (float)M_PI),
        vec2_rotate(V2(1.0f, 0.0f), 1.75f * (float)M_PI),
    };

    int look_dir_num;
    int frame_num;
    int closest_dir;
    float closest_dir_dot;

    closest_dir = 0;
    closest_dir_dot = FLT_MAX;
    for (int i = 0; i < 8; i++) {
        float dot;

        dot = vec2_dot(unit_dir, dirs[i]);
        if (dot < closest_dir_dot) {
            closest_dir = i;
            closest_dir_dot = dot;
        }
    }

    look_dir_num = closest_dir;

    component->time += dt;
    if (component->time > 0.25f * component->num_textures || speed < 0.01f) {
        component->time = 0.0f;
    }

    if (speed < 0.01f) {
        frame_num = 1;
    } else {
        frame_num = 0;

        for (int i = 0; i < component->num_textures; i++) {
            if (component->time < (i + 1) * 0.25f) {
                frame_num = i;
                break;
            }
        }
    }

    char texture_name[256];
    if (look_dir_num == 0) {
        component->body->flip_x = true;
        sprintf_s(texture_name, 256, "%s/side/%d.png", component->base_texture_name, frame_num + 1);
    }
    if (look_dir_num == 1) {
        component->body->flip_x = true;
        sprintf_s(texture_name, 256, "%s/down-d/%d.png", component->base_texture_name, frame_num + 1);
    }
    if (look_dir_num == 2) {
        component->body->flip_x = false;
        sprintf_s(texture_name, 256, "%s/down/%d.png", component->base_texture_name, frame_num + 1);
    }
    if (look_dir_num == 3) {
        component->body->flip_x = false;
        sprintf_s(texture_name, 256, "%s/down-d/%d.png", component->base_texture_name, frame_num + 1);
    }
    if (look_dir_num == 4) {
        component->body->flip_x = false;
        sprintf_s(texture_name, 256, "%s/side/%d.png", component->base_texture_name, frame_num + 1);
    }
    if (look_dir_num == 5) {
        component->body->flip_x = false;
        sprintf_s(texture_name, 256, "%s/up-d/%d.png", component->base_texture_name, frame_num + 1);
    }
    if (look_dir_num == 6) {
        component->body->flip_x = false;
        sprintf_s(texture_name, 256, "%s/up/%d.png", component->base_texture_name, frame_num + 1);
    }
    if (look_dir_num == 7) {
        component->body->flip_x = true;
        sprintf_s(texture_name, 256, "%s/up-d/%d.png", component->base_texture_name, frame_num + 1);
    }
    component->body->texture = create_atlas_image_texture(texture_name);
}

static void init_unit_construction_component(struct unit_construction_component *component, int team_num, vec2 rally_point) {
    component->team_num = team_num;
    component->rally_point = rally_point;
    component->spawn_time_left = 0.0f;
    component->spawn_time = 0.0f;
    array_init(&component->spawn_list);
}

static void update_unit_construction_component(struct unit_construction_component *component,
                                               float dt, struct entity_id *created_entity_id, bool is_entity_selected) {
    int team_num;

    team_num = component->team_num;
    created_entity_id->idx = 0;
    if (component->spawn_list.length > 0) {
        if (component->spawn_time_left > 0.0f) {
            component->spawn_time_left -= dt;
        } else {
            switch (component->spawn_list.data[0]) {
                    // Whoever called this is resposible for setting up data
                case ENTITY_TYPE_SOLDIER:
                    *created_entity_id = create_soldier_entity(0, 0, team_num);
                    break;
                case ENTITY_TYPE_MEDIC:
                    *created_entity_id = create_medic_entity(0, 0, team_num);
                    break;
                case ENTITY_TYPE_WORKER:
                    *created_entity_id = create_worker_entity(0, 0, team_num);
                    break;
                case ENTITY_TYPE_TANK:
                    *created_entity_id = create_tank_entity(0, 0, team_num);
                    break;
                case ENTITY_TYPE_VEHICLE:
                    *created_entity_id = create_vehicle_entity(0, 0, team_num);
                    break;
                case ENTITY_TYPE_HELICOPTER:
                    *created_entity_id = create_helicopter_entity(0, 0, team_num);
                    break;
                default:
                    assert(false && "Invalid type of entity to spawn.");
                    break;
            }
            array_splice(&component->spawn_list, 0, 1);
            component->spawn_time = 5.0f;
            component->spawn_time_left = component->spawn_time;
        }
    }
}

void push_to_unit_construction_component_spawn_list(struct unit_construction_component *component, enum entity_type type) {
    if (component->spawn_list.length >= 5) {
        return;
    }

    array_push(&component->spawn_list, type);
    if (component->spawn_list.length == 1) {
        component->spawn_time = 5.0f;
        component->spawn_time_left = component->spawn_time;
    }
}

void remove_from_unit_construction_component_spawn_list(struct unit_construction_component *component, int i) {
    array_splice(&component->spawn_list, i, 1);
    if (i == 0) {
        component->spawn_time = 5.0f;
        component->spawn_time_left = component->spawn_time;
    }
}

static void deinit_unit_construction_component(struct unit_construction_component *component) {
    array_deinit(&component->spawn_list);
}

static void init_building_construction_component(struct building_construction_component *component) {
    component->building_row = 0;
    component->building_col = 0;
    component->building_id.idx = 0;
}

static void init_mining_component(struct mining_component *component) {
    component->refinery_id.idx = 0;
}

static void init_entity(struct entity *entity) {
    entity->team_num = -1;
    entity->position = V2_ZERO;
    entity->velocity = V2_ZERO;
    entity->mesh_instance_id = allocate_mesh_instance();
    entity->collider_id.idx = 0;
    entity->sight_radius = 0.0f;
    entity->start_health = 100.0f;
    entity->health = 100.0f;
    entity->minimap_width = 0.0f;
    entity->is_selectable = false;
    entity->is_selected = false;
    entity->is_hovered = false;
    entity->is_visible = true;
    map_init(&entity->components);
}

static void deinit_entity(struct entity *entity) {
    set_entity_is_selected(entity->id, false);
    delete_collider(entity->collider_id);
    delete_mesh_instance(entity->mesh_instance_id);

    switch (entity->type) {
        case ENTITY_TYPE_SOLDIER:
        case ENTITY_TYPE_MEDIC:
        case ENTITY_TYPE_WORKER:
        case ENTITY_TYPE_VEHICLE:
            game_state.team_supply[entity->team_num] -= 1;
            break;
        case ENTITY_TYPE_TANK:
            game_state.team_supply[entity->team_num] -= 2;
            break;
        case ENTITY_TYPE_BARRACKS: {
            struct unit_construction_component *compo;
            compo = get_entity_component_of_type(entity, COMPONENT_TYPE_UNIT_CONSTRUCTION, "unit_construction");
            deinit_unit_construction_component(compo);
            break;
        }
        case ENTITY_TYPE_SUPPLY_DEPOT:
            game_state.team_supply_max[0] -= 8;
            break;
        case ENTITY_TYPE_COMMAND_CENTER:
            game_state.team_supply_max[entity->team_num] -= 16;
            break;
        default:
            break;
    }
}

static struct {
    array_entity_t entity_array;
    array_uint32_t gen_array;
    array_uint32_t free_array;
    array_bool_t active_array;
} _entity_manager;

void entity_manager_init(void) {
    array_init(&_entity_manager.entity_array);
    array_init(&_entity_manager.gen_array);
    array_init(&_entity_manager.free_array);
    array_init(&_entity_manager.active_array);

    // idx 0 represents NULL
    struct entity blank;
    memset(&blank, 0, sizeof(blank));
    array_push(&_entity_manager.entity_array, blank);
    array_push(&_entity_manager.gen_array, 0);
    array_push(&_entity_manager.active_array, true);
}

struct entity_id allocate_entity(void) {
    struct entity_id id;

    if (_entity_manager.free_array.length == 0) {
        struct entity entity;
        memset(&entity, 0, sizeof(entity));
        array_push(&_entity_manager.entity_array, entity);
        array_push(&_entity_manager.gen_array, 0);
        array_push(&_entity_manager.active_array, true);
        id.gen = 0;
        id.idx = _entity_manager.entity_array.length - 1;
    } else {
        uint32_t idx = array_pop(&_entity_manager.free_array);
        id.gen = _entity_manager.gen_array.data[idx];
        id.idx = idx;
        _entity_manager.active_array.data[id.idx] = true;
    }

    // printf("allocate entity: idx: %d, gen: %d\n", id.idx, id.gen);

    struct entity *entity = get_entity_from_id(id);
    init_entity(entity);
    entity->id = id;

    return id;
}

void delete_entity(struct entity_id id) {
    uint32_t cur_gen = _entity_manager.gen_array.data[id.idx];
    if (id.idx == 0 || id.gen != cur_gen) {
        return;
    }

    // printf("delete entity: idx: %d, gen: %d\n", id.idx, id.gen);

    struct entity *entity = get_entity_from_id(id);
    deinit_entity(entity);

    _entity_manager.gen_array.data[id.idx]++;
    _entity_manager.active_array.data[id.idx] = false;
    array_push(&_entity_manager.free_array, id.idx);
}

struct entity *get_entity_from_id(struct entity_id id) {
    uint32_t cur_gen = _entity_manager.gen_array.data[id.idx];
    if (id.idx == 0 || cur_gen != id.gen) {
        return NULL;
    }
    return &_entity_manager.entity_array.data[id.idx];
}

void get_all_active_entities_with_type(array_entity_id_t *entities,
                                       int num_types, enum entity_type *types) {
    int i, j;

    for (i = 0; i < _entity_manager.entity_array.length; i++) {
        bool push_entity;
        struct entity *entity;
        struct entity_id entity_id;

        if (!_entity_manager.active_array.data[i]) {
            continue;
        }

        push_entity = false;
        entity = &_entity_manager.entity_array.data[i];
        for (j = 0; j < num_types; j++) {
            if (types[j] == entity->type) {
                push_entity = true;
                break;
            }
        }
        if (num_types == 0) {
            push_entity = true;
        }

        if (push_entity) {
            entity_id.idx = i;
            entity_id.gen = _entity_manager.gen_array.data[i];
            array_push(entities, entity_id);
        }
    }
}

void do_entity_interaction(struct entity *entity, struct entity *interaction_entity) {
    if (interaction_entity->type == ENTITY_TYPE_REFINERY &&
        entity->type == ENTITY_TYPE_WORKER) {
        move_worker_to_refinery(entity, interaction_entity);
    } else {
        set_entity_target(entity, interaction_entity->position, MOVEMENT_TYPE_MOVING);
    }
}

void set_entity_target(struct entity *entity, vec2 target, enum movement_type movement_type) {
    switch (entity->type) {
        case ENTITY_TYPE_WORKER:
            set_worker_target(entity, target);
            break;
        case ENTITY_TYPE_SOLDIER:
            set_soldier_target(entity, target, movement_type);
            break;
        case ENTITY_TYPE_MEDIC:
            set_medic_target(entity, target, movement_type);
            break;
        case ENTITY_TYPE_TANK:
            set_tank_target(entity, target, movement_type);
            break;
        case ENTITY_TYPE_VEHICLE:
            set_vehicle_target(entity, target, movement_type);
            break;
        case ENTITY_TYPE_HELICOPTER:
            set_helicopter_target(entity, target, movement_type);
            break;
        case ENTITY_TYPE_BARRACKS:
        case ENTITY_TYPE_FACTORY:
        case ENTITY_TYPE_HELIPORT:
        case ENTITY_TYPE_COMMAND_CENTER: {
            struct unit_construction_component *compo;
            vec2 p = vec2_subtract(target, entity->position);
            compo = get_entity_component_of_type(entity, COMPONENT_TYPE_UNIT_CONSTRUCTION, "unit_construction");
            compo->rally_point = p;
            break;
        }
        default:
            break;
    }
}

void set_entity_is_selected(struct entity_id entity_id, bool is_selected) {
    struct entity *entity = get_entity_from_id(entity_id);
    if (!entity) {
        return;
    }
    if (!entity->is_selectable) {
        return;
    }

    if (entity->is_selected == is_selected) {
        return;
    }

    if (is_selected) {
        array_push(&game_state.selected_entity_array, entity_id);
    } else {
        int i;
        struct entity_id eid;
        array_foreach(&game_state.selected_entity_array, eid, i) {
            if (eid.idx == entity_id.idx && eid.gen == entity_id.gen) {
                break;
            }
        }
        assert(i < game_state.selected_entity_array.length);
        array_splice(&game_state.selected_entity_array, i, 1);
    }

    entity->is_selected = is_selected;

    struct mesh_instance *parent;
    struct mesh_instance_child *child;
    parent = get_mesh_instance_from_id(entity->mesh_instance_id);
    child = get_mesh_instance_child("selected2", parent);
    if (child) {
        child->is_visible = is_selected;
    }
}

void set_entity_is_hovered(struct entity_id entity_id, bool is_hovered) {
    struct entity *entity = get_entity_from_id(entity_id);
    if (!entity) {
        return;
    }
    if (!entity->is_selectable) {
        return;
    }

    if (entity->is_hovered == is_hovered) {
        return;
    }
    entity->is_hovered = is_hovered;

    struct mesh_instance *parent;
    struct mesh_instance_child *child;
    parent = get_mesh_instance_from_id(entity->mesh_instance_id);
    child = get_mesh_instance_child("hovered", parent);
    if (child) {
        child->is_visible = is_hovered;
    }
}

struct entity_id create_command_center_entity(int row, int col, int team_num) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;
    struct collider *collider;
    vec2 pos;

    assert(team_num == 0 || team_num == 1);

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);

    pos.x = col * GAME_TILE_WIDTH;
    pos.y = row * GAME_TILE_WIDTH;

    entity->type = ENTITY_TYPE_COMMAND_CENTER;
    entity->team_num = team_num;
    entity->position = pos;
    entity->is_selected = false;
    entity->is_selectable = true;
    entity->sight_radius = 1.5f;
    entity->minimap_width = 4.0f * GAME_TILE_WIDTH;
    game_state.team_supply_max[team_num] += 16;

    for (int d_row = 0; d_row < 4; d_row++) {
        for (int d_col = 0; d_col < 4; d_col++) {
            int idx = GAME_MAP_SIZE * (col - d_col) + row - d_row;
            game_state.map_tile_occupied[idx] = true;
        }
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.building_selection[0];
        mesh = &mesh_store.square_mesh;
        child = create_mesh_instance_child("hovered", mesh, texture);

        child.scale = V2(1.0f, 1.0f);
        child.position = V3(-0.1f, -0.1f, 0.0f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.building_selection[0];
        mesh = &mesh_store.square_mesh;
        child = create_mesh_instance_child("selected2", mesh, texture);

        child.scale = V2(1.0f, 1.0f);
        child.position = V3(-0.1f, -0.1f, 0.0f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.command_center[0];
        mesh = &mesh_store.square_mesh;
        child = create_mesh_instance_child("body2", mesh, texture);

        child.scale = V2(1.0f, -1.0f);
        child.position = V3(0.2f, 0.2f, 0.1f);
        child.already_isometric = true;
        array_push(&mesh_instance->children, child);
    }

    entity->collider_id = create_box_collider(false,
                                              true,
                                              4.0f * GAME_TILE_WIDTH,
                                              4.0f * GAME_TILE_WIDTH,
                                              1.0f,
                                              COLLIDER_CATEGORY_BUILDING);
    collider = get_collider_from_id(entity->collider_id);
    set_collider_position(collider, V2(pos.x - GAME_TILE_WIDTH,
                                       pos.y - GAME_TILE_WIDTH));

    {
        struct component component;
        vec2 rally_point;
        component.type = COMPONENT_TYPE_UNIT_CONSTRUCTION;
        rally_point = V2(0.0f, -1.5f * GAME_TILE_WIDTH);
        init_unit_construction_component(&component.unit_construction, team_num, rally_point);
        map_set(&entity->components, "unit_construction", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH;
        init_animated_mesh_component(&component.animated_mesh,
                                     texture_store.building_selection, 4,
                                     get_mesh_instance_child("hovered", mesh_instance), true);
        map_set(&entity->components, "hovered_animated_mesh", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH;
        init_animated_mesh_component(&component.animated_mesh,
                                     texture_store.command_center, 6,
                                     get_mesh_instance_child("body2", mesh_instance), true);
        map_set(&entity->components, "body_animated_mesh", component);
    }

    return id;
}

void update_command_center_entity(struct entity *command_center, float dt) {
    struct entity_id created_entity_id;
    struct unit_construction_component *unit_construction_compo;
    struct animated_mesh_component *hovered_animated_mesh_compo;
    struct animated_mesh_component *body_animated_mesh_compo;

    unit_construction_compo = get_entity_component_of_type(command_center, COMPONENT_TYPE_UNIT_CONSTRUCTION, "unit_construction");
    hovered_animated_mesh_compo = get_entity_component_of_type(command_center, COMPONENT_TYPE_ANIMATED_MESH, "hovered_animated_mesh");
    body_animated_mesh_compo = get_entity_component_of_type(command_center, COMPONENT_TYPE_ANIMATED_MESH, "body_animated_mesh");
    assert(unit_construction_compo && hovered_animated_mesh_compo && body_animated_mesh_compo);

    update_unit_construction_component(unit_construction_compo, dt,
                                       &created_entity_id, command_center->is_selected);

    if (created_entity_id.idx != 0) {
        struct entity *entity = get_entity_from_id(created_entity_id);
        struct collider *collider = get_collider_from_id(entity->collider_id);

        vec2 position;
        position.x = command_center->position.x;
        position.y = command_center->position.y - 2.5f * GAME_TILE_WIDTH;

        entity->position = position;
        set_collider_position(collider, position);

        vec2 target = vec2_add(command_center->position, unit_construction_compo->rally_point);
        set_entity_target(entity, target, MOVEMENT_TYPE_MOVING);
    }

    update_animated_mesh_component(hovered_animated_mesh_compo, dt);
    update_animated_mesh_component(body_animated_mesh_compo, dt);
}

struct entity_id create_barracks_entity(int row, int col, int team_num) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;
    struct collider *collider;
    vec2 pos;

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);

    pos.x = col * GAME_TILE_WIDTH;
    pos.y = row * GAME_TILE_WIDTH;

    entity->type = ENTITY_TYPE_BARRACKS;
    entity->team_num = team_num;
    entity->position = pos;
    entity->is_selected = false;
    entity->is_selectable = true;
    entity->sight_radius = 1.0f;

    for (int d_row = 0; d_row < 2; d_row++) {
        for (int d_col = 0; d_col < 2; d_col++) {
            int idx = GAME_MAP_SIZE * (col - d_col) + row - d_row;
            game_state.map_tile_occupied[idx] = true;
        }
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.building_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("hovered", mesh, texture);
        child.scale = V2(0.50f, 0.50f);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.building_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("selected2", mesh, texture);
        child.scale = V2(0.50f, 0.50f);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.barracks[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("body2", mesh, texture);
        child.scale = V2(0.60f, -0.60f);
        child.position = V3(0.15f, 0.15f, 0.1f);
        child.already_isometric = true;
        array_push(&mesh_instance->children, child);
    }

    entity->collider_id = create_box_collider(false,
                                              true,
                                              2.0f * GAME_TILE_WIDTH,
                                              2.0f * GAME_TILE_WIDTH,
                                              1.0f,
                                              COLLIDER_CATEGORY_BUILDING);
    collider = get_collider_from_id(entity->collider_id);
    set_collider_position(collider, pos);

    {
        struct component component;
        vec2 rally_point;
        component.type = COMPONENT_TYPE_UNIT_CONSTRUCTION;
        rally_point = V2(0.0f, -1.5f * GAME_TILE_WIDTH);
        init_unit_construction_component(&component.unit_construction, team_num, rally_point);
        map_set(&entity->components, "unit_construction", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH;
        init_animated_mesh_component(&component.animated_mesh,
                                     texture_store.building_selection, 4,
                                     get_mesh_instance_child("hovered", mesh_instance), true);
        map_set(&entity->components, "hovered_animated_mesh", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH;
        init_animated_mesh_component(&component.animated_mesh,
                                     texture_store.barracks, 6,
                                     get_mesh_instance_child("body2", mesh_instance), true);
        map_set(&entity->components, "body_animated_mesh", component);
    }

    return id;
}

void update_barracks_entity(struct entity *barracks, float dt) {
    struct entity_id created_entity_id;
    struct unit_construction_component *unit_construction_compo;
    struct animated_mesh_component *hovered_animated_mesh_compo;
    struct animated_mesh_component *body_animated_mesh_compo;

    unit_construction_compo = get_entity_component_of_type(barracks, COMPONENT_TYPE_UNIT_CONSTRUCTION, "unit_construction");
    hovered_animated_mesh_compo = get_entity_component_of_type(barracks, COMPONENT_TYPE_ANIMATED_MESH, "hovered_animated_mesh");
    body_animated_mesh_compo = get_entity_component_of_type(barracks, COMPONENT_TYPE_ANIMATED_MESH, "body_animated_mesh");
    assert(unit_construction_compo && hovered_animated_mesh_compo && body_animated_mesh_compo);

    update_unit_construction_component(unit_construction_compo, dt,
                                       &created_entity_id, barracks->is_selected);

    if (created_entity_id.idx != 0) {
        struct entity *entity = get_entity_from_id(created_entity_id);
        struct collider *collider = get_collider_from_id(entity->collider_id);

        entity->position = barracks->position;
        entity->position.y -= 1.5f * GAME_TILE_WIDTH;
        set_collider_position(collider, entity->position);

        vec2 target = vec2_add(barracks->position, unit_construction_compo->rally_point);
        set_entity_target(entity, target, MOVEMENT_TYPE_MOVING);
    }

    update_animated_mesh_component(hovered_animated_mesh_compo, dt);
    update_animated_mesh_component(body_animated_mesh_compo, dt);
}

void push_to_barracks_spawn_list(struct entity *barracks, enum entity_type type) {
    struct unit_construction_component *unit_construction_compo;

    unit_construction_compo = get_entity_component_of_type(barracks, COMPONENT_TYPE_UNIT_CONSTRUCTION, "unit_construction");
    assert(unit_construction_compo);

    push_to_unit_construction_component_spawn_list(unit_construction_compo, type);
}

void remove_from_barracks_spawn_list(struct entity *barracks, int i) {
    struct unit_construction_component *unit_construction_compo;

    unit_construction_compo = get_entity_component_of_type(barracks, COMPONENT_TYPE_UNIT_CONSTRUCTION, "unit_construction");
    assert(unit_construction_compo);

    remove_from_unit_construction_component_spawn_list(unit_construction_compo, i);
}

struct entity_id create_factory_entity(int row, int col, int team_num) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;
    struct collider *collider;
    vec2 pos;

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);

    pos.x = col * GAME_TILE_WIDTH;
    pos.y = row * GAME_TILE_WIDTH;

    entity->type = ENTITY_TYPE_FACTORY;
    entity->team_num = team_num;
    entity->position = pos;
    entity->is_selected = false;
    entity->sight_radius = 1.0f;
    entity->is_selectable = true;

    for (int d_row = 0; d_row < 2; d_row++) {
        for (int d_col = 0; d_col < 2; d_col++) {
            int idx = GAME_MAP_SIZE * (col - d_col) + row - d_row;
            game_state.map_tile_occupied[idx] = true;
        }
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.building_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("hovered", mesh, texture);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.scale = V2(0.50f, 0.50f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.building_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("selected2", mesh, texture);
        child.scale = V2(0.50f, 0.50f);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.factory[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("body2", mesh, texture);
        child.scale = V2(0.50f, -0.50f);
        child.position = V3(0.10f, 0.08f, 0.1f);
        child.already_isometric = true;
        array_push(&mesh_instance->children, child);
    }

    entity->collider_id = create_box_collider(false,
                                              true,
                                              2.0f * GAME_TILE_WIDTH,
                                              2.0f * GAME_TILE_WIDTH,
                                              1.0f,
                                              COLLIDER_CATEGORY_BUILDING);
    collider = get_collider_from_id(entity->collider_id);
    set_collider_position(collider, pos);

    {
        struct component component;
        vec2 rally_point;
        component.type = COMPONENT_TYPE_UNIT_CONSTRUCTION;
        rally_point = V2(0.0f, -1.5f * GAME_TILE_WIDTH);
        init_unit_construction_component(&component.unit_construction, team_num, rally_point);
        map_set(&entity->components, "unit_construction", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH;
        init_animated_mesh_component(&component.animated_mesh,
                                     texture_store.building_selection, 4,
                                     get_mesh_instance_child("hovered", mesh_instance), true);
        map_set(&entity->components, "hovered_animated_mesh", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH;
        init_animated_mesh_component(&component.animated_mesh,
                                     texture_store.factory, 6,
                                     get_mesh_instance_child("body2", mesh_instance), true);
        map_set(&entity->components, "body_animated_mesh", component);
    }

    return id;
}

void update_factory_entity(struct entity *factory, float dt) {
    struct entity_id created_entity_id;
    struct unit_construction_component *unit_construction_compo;
    struct animated_mesh_component *hovered_animated_mesh_compo;
    struct animated_mesh_component *body_animated_mesh_compo;

    unit_construction_compo = get_entity_component_of_type(factory, COMPONENT_TYPE_UNIT_CONSTRUCTION, "unit_construction");
    hovered_animated_mesh_compo = get_entity_component_of_type(factory, COMPONENT_TYPE_ANIMATED_MESH, "hovered_animated_mesh");
    body_animated_mesh_compo = get_entity_component_of_type(factory, COMPONENT_TYPE_ANIMATED_MESH, "body_animated_mesh");
    assert(unit_construction_compo && hovered_animated_mesh_compo && body_animated_mesh_compo);

    update_unit_construction_component(unit_construction_compo, dt,
                                       &created_entity_id, factory->is_selected);

    if (created_entity_id.idx != 0) {
        struct entity *entity = get_entity_from_id(created_entity_id);
        struct collider *collider = get_collider_from_id(entity->collider_id);

        vec2 position;
        position.x = factory->position.x;
        position.y = factory->position.y - 1.5f * GAME_TILE_WIDTH;

        entity->position = position;
        set_collider_position(collider, position);

        vec2 target = vec2_add(factory->position, unit_construction_compo->rally_point);
        set_entity_target(entity, target, MOVEMENT_TYPE_MOVING);
    }

    update_animated_mesh_component(hovered_animated_mesh_compo, dt);
    update_animated_mesh_component(body_animated_mesh_compo, dt);
}

void push_to_factory_spawn_list(struct entity *factory, enum entity_type type) {
}

void remove_from_factory_spawn_list(struct entity *factory, int i) {
}

struct entity_id create_heliport_entity(int row, int col) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;
    struct collider *collider;
    vec2 pos;

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);

    pos.x = col * GAME_TILE_WIDTH;
    pos.y = row * GAME_TILE_WIDTH;

    entity->type = ENTITY_TYPE_HELIPORT;
    entity->team_num = 0;
    entity->position = pos;
    entity->is_selected = false;
    entity->sight_radius = 1.0f;
    entity->is_selectable = true;

    for (int d_row = 0; d_row < 2; d_row++) {
        for (int d_col = 0; d_col < 2; d_col++) {
            int idx = GAME_MAP_SIZE * (col - d_col) + row - d_row;
            game_state.map_tile_occupied[idx] = true;
        }
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.building_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("hovered", mesh, texture);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.scale = V2(0.50f, 0.50f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.building_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("selected2", mesh, texture);
        child.scale = V2(0.50f, 0.50f);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.heliport[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("body2", mesh, texture);
        child.scale = V2(0.50f, -0.50f);
        child.position = V3(0.18f, 0.17f, 0.1f);
        child.already_isometric = true;
        array_push(&mesh_instance->children, child);
    }

    entity->collider_id = create_box_collider(false,
                                              true,
                                              2.0f * GAME_TILE_WIDTH,
                                              2.0f * GAME_TILE_WIDTH,
                                              1.0f,
                                              COLLIDER_CATEGORY_BUILDING);
    collider = get_collider_from_id(entity->collider_id);
    set_collider_position(collider, pos);

    {
        struct component component;
        vec2 rally_point;
        component.type = COMPONENT_TYPE_UNIT_CONSTRUCTION;
        rally_point = V2(0.0f, -1.5f * GAME_TILE_WIDTH);
        init_unit_construction_component(&component.unit_construction, 0, rally_point);
        map_set(&entity->components, "unit_construction", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH;
        init_animated_mesh_component(&component.animated_mesh,
                                     texture_store.building_selection, 4,
                                     get_mesh_instance_child("hovered", mesh_instance), true);
        map_set(&entity->components, "hovered_animated_mesh", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH;
        init_animated_mesh_component(&component.animated_mesh,
                                     texture_store.heliport, 6,
                                     get_mesh_instance_child("body2", mesh_instance), true);
        map_set(&entity->components, "body_animated_mesh", component);
    }

    return id;
}

void update_heliport_entity(struct entity *heliport, float dt) {
    struct entity_id created_entity_id;
    struct unit_construction_component *unit_construction_compo;
    struct animated_mesh_component *hovered_animated_mesh_compo;
    struct animated_mesh_component *body_animated_mesh_compo;

    unit_construction_compo = get_entity_component_of_type(heliport, COMPONENT_TYPE_UNIT_CONSTRUCTION, "unit_construction");
    hovered_animated_mesh_compo = get_entity_component_of_type(heliport, COMPONENT_TYPE_ANIMATED_MESH, "hovered_animated_mesh");
    body_animated_mesh_compo = get_entity_component_of_type(heliport, COMPONENT_TYPE_ANIMATED_MESH, "body_animated_mesh");
    assert(unit_construction_compo && hovered_animated_mesh_compo && body_animated_mesh_compo);

    update_unit_construction_component(unit_construction_compo, dt,
                                       &created_entity_id, heliport->is_selected);

    if (created_entity_id.idx != 0) {
        struct entity *entity = get_entity_from_id(created_entity_id);
        struct collider *collider = get_collider_from_id(entity->collider_id);

        vec2 position;
        position.x = heliport->position.x;
        position.y = heliport->position.y - 1.5f * GAME_TILE_WIDTH;
        set_collider_position(collider, position);
    }

    update_animated_mesh_component(hovered_animated_mesh_compo, dt);
    update_animated_mesh_component(body_animated_mesh_compo, dt);
}

struct entity_id create_worker_entity(float row, float col, int team_num) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;
    struct collider *collider;
    vec2 pos;

    assert(team_num == 0 || team_num == 1);

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);

    pos.x = GAME_TILE_WIDTH * col;
    pos.y = GAME_TILE_WIDTH * row;

    entity->type = ENTITY_TYPE_WORKER;
    entity->team_num = team_num;
    entity->position = pos;
    entity->is_selected = false;
    entity->is_selectable = true;
    entity->sight_radius = 1.0f;
    entity->minimap_width = 0.5f * GAME_TILE_WIDTH;
    game_state.team_supply[team_num] += 1;

    entity->worker.state = WORKER_STATE_IDLE;

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.unit_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("hovered", mesh, texture);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.scale = V2(0.18f, 0.18f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.unit_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("selected2", mesh, texture);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.scale = V2(0.18f, 0.18f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.worker_down[3];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("body2", mesh, texture);
        child.position = V3(0.1f, 0.1f, 0.1f);
        child.scale = V2(0.25f, -0.25f);
        child.already_isometric = true;
        array_push(&mesh_instance->children, child);
    }

    entity->collider_id = create_circle_collider(true, true, 0.09f, 1.0f,
                                                 COLLIDER_CATEGORY_GROUND_UNIT);
    collider = get_collider_from_id(entity->collider_id);
    set_collider_position(collider, pos);

    {
        struct component component;
        component.type = COMPONENT_TYPE_MOVEMENT;
        init_movement_component(&component.movement);
        map_set(&entity->components, "movement", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH_MOVEMENT;
        init_animated_mesh_movement_component(&component.animated_mesh_movement, "worker", 4,
                                              get_mesh_instance_child("body2", mesh_instance));
        map_set(&entity->components, "animated_mesh_movement", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH;
        init_animated_mesh_component(&component.animated_mesh, texture_store.unit_selection, 4,
                                     get_mesh_instance_child("hovered", mesh_instance), true);
        map_set(&entity->components, "hovered_animated_mesh", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_BUILDING_CONSTRUCTION;
        init_building_construction_component(&component.building_construction);
        map_set(&entity->components, "building_construction", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_MINING;
        init_mining_component(&component.mining);
        map_set(&entity->components, "mining", component);
    }

    return id;
}

void update_worker_entity(struct entity *worker, float dt) {
    struct movement_component *movement_compo;
    struct collider *collider;
    struct action current_action;
    vec2 aim_dir;
    bool arrived;

    movement_compo = get_entity_component_of_type(worker, COMPONENT_TYPE_MOVEMENT, "movement");
    assert(movement_compo);

    collider = get_collider_from_id(worker->collider_id);
    assert(collider);

    current_action = worker->current_action;
    aim_dir = V2(0.0f, 1.0f);

    {
        vec2 force, pos, vel;

        force = V2(0.0f, 0.0f);
        pos = worker->position;
        vel = worker->velocity;

        update_movement_component(movement_compo, pos, vel, &force, &arrived, dt);
        apply_force_to_collider(collider, force);
    }

    switch (worker->worker.state) {
        case WORKER_STATE_IDLE:
            movement_compo->moving_to_target = false;
            break;

        case WORKER_STATE_MOVING:
            movement_compo->moving_to_target = true;

            if (arrived) {
                worker->worker.state = WORKER_STATE_IDLE;
            }

            break;

        case WORKER_STATE_MOVING_TO_CONSTRUCTION:
            movement_compo->moving_to_target = true;

            if (arrived) {
                bool is_small;
                struct building_construction_component *compo;
                compo = get_entity_component_of_type(worker, COMPONENT_TYPE_BUILDING_CONSTRUCTION, "building_construction");

                worker->worker.state = WORKER_STATE_CONSTRUCTING;

                is_small = false;
                if (compo->building_type == ENTITY_TYPE_SUPPLY_DEPOT || compo->building_type == ENTITY_TYPE_TURRET) {
                    is_small = true;
                }
                compo->building_id = create_building_construction_entity(compo->building_type, compo->building_row, compo->building_col, is_small);

                delete_entity(compo->placeholder_id);
            }

            break;

        case WORKER_STATE_CONSTRUCTING: {
            struct entity *building;
            struct building_construction_component *compo;

            movement_compo->moving_to_target = false;
            compo = get_entity_component_of_type(worker, COMPONENT_TYPE_BUILDING_CONSTRUCTION, "building_construction");
            building = get_entity_from_id(compo->building_id);

            if (building == NULL) {
                worker->worker.state = WORKER_STATE_IDLE;
            }

            break;
        }

        case WORKER_STATE_MOVING_TO_REFINERY:
            movement_compo->moving_to_target = true;

            if (arrived) {
                struct mining_component *compo;
                struct entity *refinery;

                compo = get_entity_component_of_type(worker, COMPONENT_TYPE_MINING, "mining");
                refinery = get_entity_from_id(compo->refinery_id);

                if (refinery) {
                    add_worker_to_refinery(refinery, worker);
                    worker->worker.state = WORKER_STATE_IN_REFINERY;
                } else {
                    worker->worker.state = WORKER_STATE_IDLE;
                }
            }

            break;

        case WORKER_STATE_IN_REFINERY: {
            struct mining_component *compo;
            struct entity *refinery;

            compo = get_entity_component_of_type(worker, COMPONENT_TYPE_MINING, "mining");
            refinery = get_entity_from_id(compo->refinery_id);

            if (refinery) {
                set_collider_position(collider, refinery->position);
            } else {
                worker->worker.state = WORKER_STATE_IDLE;
            }

            break;
        }
    }

    {
        struct animated_mesh_movement_component *compo;
        vec2 vel;

        compo = get_entity_component_of_type(worker, COMPONENT_TYPE_ANIMATED_MESH_MOVEMENT, "animated_mesh_movement");
        assert(compo);

        vel = worker->velocity;

        update_animated_mesh_movement_component(compo, aim_dir, vel, dt);
    }

    {
        struct animated_mesh_component *compo;

        compo = get_entity_component_of_type(worker, COMPONENT_TYPE_ANIMATED_MESH, "hovered_animated_mesh");
        assert(compo);

        update_animated_mesh_component(compo, dt);
    }
}

void set_worker_target(struct entity *worker, vec2 target) {
    struct movement_component *movement_compo;

    movement_compo = get_entity_component_of_type(worker, COMPONENT_TYPE_MOVEMENT, "movement");
    assert(movement_compo);

    if (worker->worker.state == WORKER_STATE_IN_REFINERY ||
        worker->worker.state == WORKER_STATE_CONSTRUCTING) {
        return;
    }

    if (worker->worker.state == WORKER_STATE_MOVING_TO_CONSTRUCTION) {
        struct building_construction_component *compo;
        compo = get_entity_component_of_type(worker, COMPONENT_TYPE_BUILDING_CONSTRUCTION, "building_construction");

        delete_entity(compo->placeholder_id);
    }

    worker->worker.state = WORKER_STATE_MOVING;
    movement_compo->type = MOVEMENT_TYPE_MOVING;
    movement_compo->path_index = 0;
    game_do_pathfinding(worker->position, target, &movement_compo->path);
}

void set_worker_build_target(struct entity *worker, enum entity_type building_type,
                             int row, int col, struct entity_id placeholder_id) {
    struct building_construction_component *compo;
    compo = get_entity_component_of_type(worker, COMPONENT_TYPE_BUILDING_CONSTRUCTION, "building_construction");

    if (worker->worker.state == WORKER_STATE_CONSTRUCTING ||
        worker->worker.state == WORKER_STATE_IN_REFINERY) {
        delete_entity(placeholder_id);
        return;
    }

    set_worker_target(worker, V2((col - 0.5f) * GAME_TILE_WIDTH, (row - 0.5f) * GAME_TILE_WIDTH));
    worker->worker.state = WORKER_STATE_MOVING_TO_CONSTRUCTION;
    compo->building_row = row;
    compo->building_col = col;
    compo->building_type = building_type;
    compo->placeholder_id = placeholder_id;
}

void move_worker_to_refinery(struct entity *worker, struct entity *refinery) {
    vec2 target;
    struct mining_component *compo;

    compo = get_entity_component_of_type(worker, COMPONENT_TYPE_MINING, "mining");

    if (worker->worker.state == WORKER_STATE_IN_REFINERY ||
        worker->worker.state == WORKER_STATE_CONSTRUCTING) {
        return;
    }

    target = vec2_subtract(refinery->position, V2(GAME_TILE_WIDTH + 0.09f, GAME_TILE_WIDTH + 0.09f));
    set_worker_target(worker, target);
    worker->worker.state = WORKER_STATE_MOVING_TO_REFINERY;
    compo->refinery_id = refinery->id;
}

struct entity_id create_medic_entity(float row, float col, int team_num) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;
    struct collider *collider;
    vec2 pos;

    assert(team_num == 0 || team_num == 1);

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);

    pos.x = GAME_TILE_WIDTH * col;
    pos.y = GAME_TILE_WIDTH * row;

    entity->type = ENTITY_TYPE_MEDIC;
    entity->team_num = team_num;
    entity->position = pos;
    entity->is_selected = false;
    entity->is_selectable = true;
    entity->sight_radius = 1.7f;
    entity->minimap_width = 0.5f * GAME_TILE_WIDTH;
    entity->medic.state_type = MEDIC_STATE_IDLE;
    game_state.team_supply[team_num] += 1;

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.unit_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("hovered", mesh, texture);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.scale = V2(0.18f, 0.18f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.unit_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("selected2", mesh, texture);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.scale = V2(0.18f, 0.18f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = create_atlas_image_texture("medic/down/3.png");
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("body2", mesh, texture);
        child.position = V3(0.1f, 0.1f, 0.1f);
        child.scale = V2(0.25f, -0.25f);
        child.already_isometric = true;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = create_atlas_image_texture("healing-source/1.png");
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("healing_source", mesh, texture);
        child.position = V3(0.13f, 0.13f, 0.2f);
        child.scale = V2(0.25f, -0.25f);
        child.already_isometric = true;
        child.opacity = 0.5f;
        array_push(&mesh_instance->children, child);
    }

    entity->collider_id = create_circle_collider(true, true, 0.09f, 1.0f,
                                                 COLLIDER_CATEGORY_GROUND_UNIT);
    collider = get_collider_from_id(entity->collider_id);
    set_collider_position(collider, pos);

    {
        struct component component;
        component.type = COMPONENT_TYPE_MOVEMENT;
        init_movement_component(&component.movement);
        map_set(&entity->components, "movement", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_HEALING;
        init_healing_component(&component.healing, 1.6f);
        map_set(&entity->components, "healing", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH_MOVEMENT;
        init_animated_mesh_movement_component(&component.animated_mesh_movement, "medic", 4,
                                              get_mesh_instance_child("body2", mesh_instance));
        map_set(&entity->components, "animated_mesh_movement", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH;
        init_animated_mesh_component(&component.animated_mesh,
                                     texture_store.unit_selection, 4,
                                     get_mesh_instance_child("hovered", mesh_instance), true);
        map_set(&entity->components, "hovered_animated_mesh", component);
    }

    {
        struct texture textures[4];
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH;
        textures[0] = create_atlas_image_texture("healing-source/1.png");
        textures[1] = create_atlas_image_texture("healing-source/2.png");
        textures[2] = create_atlas_image_texture("healing-source/3.png");
        textures[3] = create_atlas_image_texture("healing-source/4.png");
        init_animated_mesh_component(&component.animated_mesh, textures, 4,
                                     get_mesh_instance_child("healing_source", mesh_instance), false);
        component.animated_mesh.play_once = true;
        map_set(&entity->components, "healing_source_animated_mesh", component);
    }

    entity->current_action.type = ACTION_TYPE_NONE;

    return id;
}

void update_medic_entity(struct entity *medic, float dt) {
    struct movement_component *movement_compo;
    struct healing_component *healing_compo;
    float closest_low_health_friend_dist;
    struct entity *closest_low_health_friend;
    bool arrived;
    vec2 aim_dir;

    movement_compo = get_entity_component_of_type(medic, COMPONENT_TYPE_MOVEMENT, "movement");
    healing_compo = get_entity_component_of_type(medic, COMPONENT_TYPE_HEALING, "healing");
    assert(movement_compo && healing_compo);

    closest_low_health_friend_dist = healing_compo->healing_radius;
    closest_low_health_friend = NULL;
    {
        array_entity_id_t *friends;
        struct entity_id id;
        int i;

        if (medic->team_num == 0) {
            friends = &game_state.team_0_entities;
        } else if (medic->team_num == 1) {
            friends = &game_state.team_1_entities;
        } else {
            assert(false);
        }

        array_foreach(friends, id, i) {
            struct entity *entity;

            entity = get_entity_from_id(id);
            if (!entity) {
                continue;
            }

            if (medic == entity) {
                continue;
            }

            if (entity->type != ENTITY_TYPE_SOLDIER) {
                continue;
            }

            if (entity->health < entity->start_health) {
                float dist;

                dist = vec2_distance(entity->position, medic->position);
                if (dist < closest_low_health_friend_dist) {
                    closest_low_health_friend_dist = dist;
                    closest_low_health_friend = entity;
                }
            }
        }
    }

    {
        struct collider *collider;
        vec2 force;

        collider = get_collider_from_id(medic->collider_id);
        assert(collider);

        update_movement_component(movement_compo, medic->position, medic->velocity, &force, &arrived, dt);
        apply_force_to_collider(collider, force);
    }

    aim_dir = V2(0.0f, 1.0f);
    switch (medic->medic.state_type) {
        case MEDIC_STATE_IDLE:
            movement_compo->moving_to_target = false;

            if (closest_low_health_friend) {
                healing_compo->healing_entity_id = closest_low_health_friend->id;
                medic->medic.state_type = MEDIC_STATE_HEALING;
            }
            break;
        case MEDIC_STATE_HEALING:
            movement_compo->moving_to_target = false;
            update_healing_component(healing_compo, medic, dt);
            if (!closest_low_health_friend) {
                medic->medic.state_type = MEDIC_STATE_IDLE;
            } else {
                aim_dir = vec2_subtract(closest_low_health_friend->position, medic->position);
                aim_dir = vec2_normalize(aim_dir);
            }
            break;
        case MEDIC_STATE_MOVING:
            movement_compo->moving_to_target = true;

            if (arrived) {
                medic->medic.state_type = MEDIC_STATE_IDLE;
            }
            break;
    }

    {
        struct animated_mesh_movement_component *animated_mesh_movement_compo;
        vec2 velocity;

        animated_mesh_movement_compo = get_entity_component_of_type(medic, COMPONENT_TYPE_ANIMATED_MESH_MOVEMENT, "animated_mesh_movement");
        assert(animated_mesh_movement_compo);

        velocity = medic->velocity;

        update_animated_mesh_movement_component(animated_mesh_movement_compo, aim_dir, velocity, dt);
    }

    {
        struct animated_mesh_component *hovered_animated_mesh_compo;

        hovered_animated_mesh_compo = get_entity_component_of_type(medic, COMPONENT_TYPE_ANIMATED_MESH, "hovered_animated_mesh");
        assert(hovered_animated_mesh_compo);

        update_animated_mesh_component(hovered_animated_mesh_compo, dt);
    }

    {
        struct animated_mesh_component *healing_source_animated_mesh_compo;

        healing_source_animated_mesh_compo = get_entity_component_of_type(medic, COMPONENT_TYPE_ANIMATED_MESH, "healing_source_animated_mesh");
        assert(healing_source_animated_mesh_compo);

        update_animated_mesh_component(healing_source_animated_mesh_compo, dt);
    }
}

void set_medic_target(struct entity *medic, vec2 target, enum movement_type movement_type) {
    struct movement_component *movement_compo;
    movement_compo = get_entity_component_of_type(medic, COMPONENT_TYPE_MOVEMENT, "movement");

    medic->current_action.type = ACTION_TYPE_NORMAL_MOVE;
    medic->medic.state_type = MEDIC_STATE_MOVING;

    movement_compo->type = movement_type;
    movement_compo->path_index = 0;
    game_do_pathfinding(medic->position, target, &movement_compo->path);
}

struct entity_id create_soldier_entity(float row, float col, int team_num) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;
    struct collider *collider;
    vec2 pos;

    assert(team_num == 0 || team_num == 1);

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);

    pos.x = GAME_TILE_WIDTH * col;
    pos.y = GAME_TILE_WIDTH * row;

    entity->type = ENTITY_TYPE_SOLDIER;
    entity->team_num = team_num;
    entity->position = pos;
    entity->is_selected = false;
    entity->is_selectable = true;
    entity->sight_radius = 1.7f;
    entity->minimap_width = 0.5f * GAME_TILE_WIDTH;
    entity->soldier.state_type = SOLDIER_STATE_IDLE;
    game_state.team_supply[team_num] += 1;

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.unit_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("hovered", mesh, texture);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.scale = V2(0.18f, 0.18f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.unit_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("selected2", mesh, texture);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.scale = V2(0.18f, 0.18f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = create_atlas_image_texture("healing/1.png");
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("healing", mesh, texture);
        child.position = V3(0.13f, 0.13f, 0.1f);
        child.scale = V2(0.25f, -0.25f);
        child.already_isometric = true;
        child.opacity = 0.5f;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = create_atlas_image_texture("infantry/down/3.png");
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("body2", mesh, texture);
        child.position = V3(0.1f, 0.1f, 0.1f);
        child.scale = V2(0.25f, -0.25f);
        child.already_isometric = true;
        array_push(&mesh_instance->children, child);
    }

    entity->collider_id = create_circle_collider(true, true, 0.09f, 1.0f,
                                                 COLLIDER_CATEGORY_GROUND_UNIT);
    collider = get_collider_from_id(entity->collider_id);
    set_collider_position(collider, pos);

    {
        struct component component;
        component.type = COMPONENT_TYPE_MOVEMENT;
        init_movement_component(&component.movement);
        map_set(&entity->components, "movement", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ATTACK;
        init_attack_component(&component.attack, 1.6f);
        map_set(&entity->components, "attack", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH_MOVEMENT;
        init_animated_mesh_movement_component(&component.animated_mesh_movement, "infantry", 4,
                                              get_mesh_instance_child("body2", mesh_instance));
        map_set(&entity->components, "animated_mesh_movement", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH;
        init_animated_mesh_component(&component.animated_mesh, texture_store.unit_selection, 4,
                                     get_mesh_instance_child("hovered", mesh_instance), true);
        map_set(&entity->components, "hovered_animated_mesh", component);
    }

    {
        struct texture textures[9];
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH;
        textures[0] = create_atlas_image_texture("healing/1.png");
        textures[1] = create_atlas_image_texture("healing/2.png");
        textures[2] = create_atlas_image_texture("healing/3.png");
        textures[3] = create_atlas_image_texture("healing/4.png");
        textures[4] = create_atlas_image_texture("healing/5.png");
        textures[5] = create_atlas_image_texture("healing/6.png");
        textures[6] = create_atlas_image_texture("healing/7.png");
        textures[7] = create_atlas_image_texture("healing/8.png");
        textures[8] = create_atlas_image_texture("healing/9.png");
        init_animated_mesh_component(&component.animated_mesh, textures, 9,
                                     get_mesh_instance_child("healing", mesh_instance), false);
        component.animated_mesh.play_once = true;
        map_set(&entity->components, "healing_animated_mesh", component);
    }

    entity->current_action.type = ACTION_TYPE_NONE;

    return id;
}

void update_soldier_entity(struct entity *soldier, float dt) {
    struct movement_component *movement_compo;
    struct attack_component *attack_compo;
    struct animated_mesh_component *hovered_animated_mesh_compo;
    struct animated_mesh_movement_component *animated_mesh_movement_compo;
    struct collider *collider;
    struct entity_id closest_enemy_id;
    struct entity *closest_enemy;
    struct action current_action;
    bool arrived;
    vec2 force, aim_dir;

    movement_compo = get_entity_component_of_type(soldier, COMPONENT_TYPE_MOVEMENT, "movement");
    attack_compo = get_entity_component_of_type(soldier, COMPONENT_TYPE_ATTACK, "attack");
    hovered_animated_mesh_compo = get_entity_component_of_type(soldier, COMPONENT_TYPE_ANIMATED_MESH, "hovered_animated_mesh");
    animated_mesh_movement_compo = get_entity_component_of_type(soldier, COMPONENT_TYPE_ANIMATED_MESH_MOVEMENT, "animated_mesh_movement");
    assert(movement_compo && attack_compo && hovered_animated_mesh_compo && animated_mesh_movement_compo);

    collider = get_collider_from_id(soldier->collider_id);
    assert(collider);

    closest_enemy_id = attack_compo->closest_enemy_id;
    closest_enemy = get_entity_from_id(closest_enemy_id);

    current_action = soldier->current_action;

    force = V2(0.0f, 0.0f);
    aim_dir = V2(0.0f, 1.0f);
    update_movement_component(movement_compo, soldier->position, soldier->velocity, &force, &arrived, dt);
    apply_force_to_collider(collider, force);

    switch (soldier->soldier.state_type) {
        case SOLDIER_STATE_IDLE:
            movement_compo->moving_to_target = false;

            if (closest_enemy) {
                soldier->soldier.state_type = SOLDIER_STATE_ATTACKING_ENEMY;
                attack_compo->closest_enemy_id = closest_enemy_id;
            }

            break;
        case SOLDIER_STATE_ATTACKING_ENEMY: {
            movement_compo->moving_to_target = false;

            if (!closest_enemy) {
                switch (current_action.type) {
                    case ACTION_TYPE_NONE:
                    case ACTION_TYPE_NORMAL_MOVE:
                        soldier->soldier.state_type = SOLDIER_STATE_IDLE;
                        break;
                    case ACTION_TYPE_ATTACK_MOVE:
                    case ACTION_TYPE_PATROL_MOVE:
                        soldier->soldier.state_type = SOLDIER_STATE_MOVING;
                        break;
                }
            } else {
                attack_compo->time_since_attack += dt;
                if (attack_compo->time_since_attack > 1.0f) {
                    attack_compo->time_since_attack = 0.0f;
                    create_soldier_projectile_entity(soldier->position, closest_enemy_id);
                }
                aim_dir = vec2_normalize(vec2_subtract(closest_enemy->position, soldier->position));
            }

            break;
        }
        case SOLDIER_STATE_MOVING: {
            movement_compo->moving_to_target = true;

            if (arrived) {
                soldier->soldier.state_type = SOLDIER_STATE_IDLE;
            }

            if (closest_enemy) {
                if (current_action.type == ACTION_TYPE_ATTACK_MOVE || current_action.type == ACTION_TYPE_PATROL_MOVE) {
                    soldier->soldier.state_type = SOLDIER_STATE_ATTACKING_ENEMY;
                    attack_compo->closest_enemy_id = closest_enemy->id;
                }
            }

            break;
        }
    }

    update_animated_mesh_movement_component(animated_mesh_movement_compo, aim_dir, soldier->velocity, dt);
    update_animated_mesh_component(hovered_animated_mesh_compo, dt);

    {
        struct animated_mesh_component *healing_animated_mesh_compo;

        healing_animated_mesh_compo = get_entity_component_of_type(soldier, COMPONENT_TYPE_ANIMATED_MESH, "healing_animated_mesh");
        assert(healing_animated_mesh_compo);

        update_animated_mesh_component(healing_animated_mesh_compo, dt);
    }
}

void set_soldier_target(struct entity *soldier, vec2 target, enum movement_type movement_type) {
    struct movement_component *movement_compo;
    movement_compo = get_entity_component_of_type(soldier, COMPONENT_TYPE_MOVEMENT, "movement");

    switch (movement_type) {
        case MOVEMENT_TYPE_MOVING:
            soldier->current_action.type = ACTION_TYPE_NORMAL_MOVE;
            break;
        case MOVEMENT_TYPE_ATTACK_MOVING:
            soldier->current_action.type = ACTION_TYPE_ATTACK_MOVE;
            break;
        case MOVEMENT_TYPE_PATROLLING:
            soldier->current_action.type = ACTION_TYPE_PATROL_MOVE;
            break;
    }

    soldier->soldier.state_type = SOLDIER_STATE_MOVING;
    movement_compo->type = movement_type;
    movement_compo->path_index = 0;
    game_do_pathfinding(soldier->position, target, &movement_compo->path);
}

struct entity_id create_soldier_projectile_entity(vec2 start_pos,
                                                  struct entity_id target_id) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);

    entity->type = ENTITY_TYPE_SOLDIER_PROJECTILE;
    entity->position = start_pos;
    entity->is_selected = false;
    entity->soldier_projectile.time_in_air = 0.0f;
    entity->soldier_projectile.target_id = target_id;

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = create_color_texture(V3(1.0f, 0.0f, 0.0f));
        mesh = &mesh_store.circle_mesh;
        child = create_mesh_instance_child("body2", mesh, texture);

        child.scale = V2(0.01f, 0.01f);
        child.position = V3(0.1f, 0.1f, 0.0f);
        child.already_isometric = true;
        array_push(&mesh_instance->children, child);
    }

    return id;
}

void update_soldier_projectile_entity(struct entity *projectile, float dt) {
    struct entity *target;
    target = get_entity_from_id(projectile->soldier_projectile.target_id);
    if (!target) {
        delete_entity(projectile->id);
        return;
    }

    float length = 0.5f;
    projectile->soldier_projectile.time_in_air += dt;
    if (projectile->soldier_projectile.time_in_air > length) {
        target->health -= 10.0f;
        if (target->health < 0.0f) {
            target->health = 0.0f;
        }
        delete_entity(projectile->id);
        return;
    }

    float time_left = length - projectile->soldier_projectile.time_in_air;
    vec2 dir = vec2_subtract(target->position, projectile->position);
    dir = vec2_scale(dir, 1.0f / time_left);
    projectile->position.x += dir.x * dt;
    projectile->position.y += dir.y * dt;
}

struct entity_id create_vehicle_entity(float row, float col, int team_num) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;
    struct collider *collider;
    vec2 pos;

    assert(team_num == 0 || team_num == 1);

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);

    pos.x = GAME_TILE_WIDTH * col;
    pos.y = GAME_TILE_WIDTH * row;

    entity->type = ENTITY_TYPE_VEHICLE;
    entity->team_num = team_num;
    entity->position = pos;
    entity->is_selected = false;
    entity->sight_radius = 1.0f;
    entity->is_selectable = true;
    entity->vehicle.state_type = VEHICLE_STATE_IDLE;
    game_state.team_supply[team_num] += 1;

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.vehicle_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("hovered", mesh, texture);
        child.position = V3(0.07f, 0.07f, 0.0f);
        child.scale = V2(0.36f, 0.36f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.vehicle_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("selected2", mesh, texture);
        child.position = V3(0.07f, 0.07f, 0.0f);
        child.scale = V2(0.36f, 0.36f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = create_atlas_image_texture("vehicle/down/1.png");
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("body2", mesh, texture);
        child.position = V3(0.1f, 0.1f, 0.1f);
        child.scale = V2(0.50f, -0.50f);
        child.already_isometric = true;
        array_push(&mesh_instance->children, child);
    }

    entity->collider_id = create_circle_collider(true, true, 0.10f, 1.0f,
                                                 COLLIDER_CATEGORY_GROUND_UNIT);
    collider = get_collider_from_id(entity->collider_id);
    set_collider_position(collider, pos);

    {
        struct component component;
        component.type = COMPONENT_TYPE_MOVEMENT;
        init_movement_component(&component.movement);
        map_set(&entity->components, "movement", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ATTACK;
        init_attack_component(&component.attack, 1.0f);
        map_set(&entity->components, "attack", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH_MOVEMENT;
        init_animated_mesh_movement_component(&component.animated_mesh_movement, "vehicle", 2,
                                              get_mesh_instance_child("body2", mesh_instance));
        map_set(&entity->components, "animated_mesh_movement", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH;
        init_animated_mesh_component(&component.animated_mesh, texture_store.vehicle_selection, 5,
                                     get_mesh_instance_child("hovered", mesh_instance), true);
        map_set(&entity->components, "hovered_animated_mesh", component);
    }

    return id;
}

void update_vehicle_entity(struct entity *vehicle, float dt) {
    struct movement_component *movement_compo;
    struct attack_component *attack_compo;
    struct collider *collider;
    struct entity_id closest_enemy_id;
    struct entity *closest_enemy;
    struct action current_action;
    vec2 aim_dir;
    bool arrived;

    movement_compo = get_entity_component_of_type(vehicle, COMPONENT_TYPE_MOVEMENT, "movement");
    assert(movement_compo);

    attack_compo = get_entity_component_of_type(vehicle, COMPONENT_TYPE_ATTACK, "attack");
    assert(attack_compo);

    collider = get_collider_from_id(vehicle->collider_id);
    assert(collider);

    closest_enemy_id = attack_compo->closest_enemy_id;
    closest_enemy = get_entity_from_id(closest_enemy_id);
    current_action = vehicle->current_action;
    aim_dir = V2(0.0f, 1.0f);

    {
        vec2 force, pos, vel;

        force = V2(0.0f, 0.0f);
        pos = vehicle->position;
        vel = vehicle->velocity;

        update_movement_component(movement_compo, pos, vel, &force, &arrived, dt);
        apply_force_to_collider(collider, force);
    }

    switch (vehicle->vehicle.state_type) {
        case VEHICLE_STATE_IDLE:
            movement_compo->moving_to_target = false;

            if (closest_enemy) {
                vehicle->vehicle.state_type = VEHICLE_STATE_ATTACKING;
                attack_compo->closest_enemy_id = closest_enemy_id;
            }

            break;
        case VEHICLE_STATE_ATTACKING:
            movement_compo->moving_to_target = false;

            if (!closest_enemy) {
                switch (current_action.type) {
                    case ACTION_TYPE_NONE:
                    case ACTION_TYPE_NORMAL_MOVE:
                        vehicle->vehicle.state_type = VEHICLE_STATE_IDLE;
                        break;
                    case ACTION_TYPE_ATTACK_MOVE:
                    case ACTION_TYPE_PATROL_MOVE:
                        vehicle->vehicle.state_type = VEHICLE_STATE_MOVING;
                        break;
                }
            } else {
                attack_compo->time_since_attack += dt;
                if (attack_compo->time_since_attack > 1.0f) {
                    attack_compo->time_since_attack = 0.0f;
                    create_soldier_projectile_entity(vehicle->position, closest_enemy->id);
                }
                aim_dir = vec2_normalize(vec2_subtract(closest_enemy->position, vehicle->position));
            }

            break;
        case VEHICLE_STATE_MOVING:
            movement_compo->moving_to_target = true;

            if (arrived) {
                vehicle->vehicle.state_type = VEHICLE_STATE_IDLE;
            }

            if (closest_enemy) {
                if (current_action.type == ACTION_TYPE_ATTACK_MOVE || current_action.type == ACTION_TYPE_PATROL_MOVE) {
                    vehicle->vehicle.state_type = VEHICLE_STATE_ATTACKING;
                    attack_compo->closest_enemy_id = closest_enemy->id;
                }
            }

            break;
    }

    {
        struct animated_mesh_movement_component *compo;
        vec2 vel;

        compo = get_entity_component_of_type(vehicle, COMPONENT_TYPE_ANIMATED_MESH_MOVEMENT, "animated_mesh_movement");
        assert(compo);

        vel = vehicle->velocity;

        update_animated_mesh_movement_component(compo, aim_dir, vel, dt);
    }

    {
        struct animated_mesh_component *compo;

        compo = get_entity_component_of_type(vehicle, COMPONENT_TYPE_ANIMATED_MESH, "hovered_animated_mesh");
        assert(compo);

        update_animated_mesh_component(compo, dt);
    }
}

void set_vehicle_target(struct entity *vehicle, vec2 target, enum movement_type movement_type) {
    struct movement_component *movement_compo;
    movement_compo = get_entity_component_of_type(vehicle, COMPONENT_TYPE_MOVEMENT, "movement");

    switch (movement_type) {
        case MOVEMENT_TYPE_MOVING:
            vehicle->current_action.type = ACTION_TYPE_NORMAL_MOVE;
            break;
        case MOVEMENT_TYPE_ATTACK_MOVING:
            vehicle->current_action.type = ACTION_TYPE_ATTACK_MOVE;
            break;
        case MOVEMENT_TYPE_PATROLLING:
            vehicle->current_action.type = ACTION_TYPE_PATROL_MOVE;
            break;
    }

    vehicle->vehicle.state_type = VEHICLE_STATE_MOVING;
    movement_compo->type = movement_type;
    movement_compo->path_index = 0;
    game_do_pathfinding(vehicle->position, target, &movement_compo->path);
}

struct entity_id create_tank_entity(float row, float col, int team_num) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;
    struct collider *collider;
    vec2 pos;

    assert(team_num == 0 || team_num == 1);

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);

    pos.x = GAME_TILE_WIDTH * col;
    pos.y = GAME_TILE_WIDTH * row;

    entity->type = ENTITY_TYPE_TANK;
    entity->team_num = team_num;
    entity->position = pos;
    entity->is_selected = false;
    entity->sight_radius = 1.0f;
    entity->is_selectable = true;
    entity->tank.state_type = TANK_STATE_IDLE;
    game_state.team_supply[team_num] += 2;

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.vehicle_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("hovered", mesh, texture);
        child.position = V3(0.07f, 0.07f, 0.0f);
        child.scale = V2(0.36f, 0.36f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.vehicle_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("selected2", mesh, texture);
        child.position = V3(0.07f, 0.07f, 0.0f);
        child.scale = V2(0.36f, 0.36f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.tank_down[1];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("body2", mesh, texture);
        child.position = V3(0.1f, 0.1f, 0.1f);
        child.scale = V2(0.50f, -0.50f);
        child.already_isometric = true;
        array_push(&mesh_instance->children, child);
    }

    entity->collider_id = create_circle_collider(true, true, 0.10f, 1.0f,
                                                 COLLIDER_CATEGORY_GROUND_UNIT);
    collider = get_collider_from_id(entity->collider_id);
    set_collider_position(collider, pos);

    {
        struct component component;
        component.type = COMPONENT_TYPE_MOVEMENT;
        init_movement_component(&component.movement);
        map_set(&entity->components, "movement", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ATTACK;
        init_attack_component(&component.attack, 1.0f);
        map_set(&entity->components, "attack", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH_MOVEMENT;
        init_animated_mesh_movement_component(&component.animated_mesh_movement, "tank", 2,
                                              get_mesh_instance_child("body2", mesh_instance));
        map_set(&entity->components, "animated_mesh_movement", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH;
        init_animated_mesh_component(&component.animated_mesh, texture_store.vehicle_selection, 5,
                                     get_mesh_instance_child("hovered", mesh_instance), true);
        map_set(&entity->components, "hovered_animated_mesh", component);
    }

    return id;
}

void update_tank_entity(struct entity *tank, float dt) {
    struct movement_component *movement_compo;
    struct attack_component *attack_compo;
    struct collider *collider;
    struct entity_id closest_enemy_id;
    struct entity *closest_enemy;
    struct action current_action;
    vec2 aim_dir;
    bool arrived;

    movement_compo = get_entity_component_of_type(tank, COMPONENT_TYPE_MOVEMENT, "movement");
    assert(movement_compo);

    attack_compo = get_entity_component_of_type(tank, COMPONENT_TYPE_ATTACK, "attack");
    assert(attack_compo);

    collider = get_collider_from_id(tank->collider_id);
    assert(collider);

    closest_enemy_id = attack_compo->closest_enemy_id;
    closest_enemy = get_entity_from_id(closest_enemy_id);
    current_action = tank->current_action;
    aim_dir = V2(0.0f, 1.0f);

    {
        vec2 force, pos, vel;

        force = V2(0.0f, 0.0f);
        pos = tank->position;
        vel = tank->velocity;

        update_movement_component(movement_compo, pos, vel, &force, &arrived, dt);
        apply_force_to_collider(collider, force);
    }

    switch (tank->tank.state_type) {
        case TANK_STATE_IDLE:
            movement_compo->moving_to_target = false;

            if (closest_enemy) {
                tank->tank.state_type = TANK_STATE_ATTACKING;
                attack_compo->closest_enemy_id = closest_enemy_id;
            }

            break;
        case TANK_STATE_ATTACKING:
            movement_compo->moving_to_target = false;

            if (!closest_enemy) {
                switch (current_action.type) {
                    case ACTION_TYPE_NONE:
                    case ACTION_TYPE_NORMAL_MOVE:
                        tank->tank.state_type = TANK_STATE_IDLE;
                        break;
                    case ACTION_TYPE_ATTACK_MOVE:
                    case ACTION_TYPE_PATROL_MOVE:
                        tank->tank.state_type = TANK_STATE_MOVING;
                        break;
                }
            } else {
                attack_compo->time_since_attack += dt;
                if (attack_compo->time_since_attack > 1.0f) {
                    attack_compo->time_since_attack = 0.0f;
                    create_tank_projectile_entity(tank->position, closest_enemy->position);
                }
                aim_dir = vec2_normalize(vec2_subtract(closest_enemy->position, tank->position));
            }

            break;
        case TANK_STATE_MOVING:
            movement_compo->moving_to_target = true;

            if (arrived) {
                tank->tank.state_type = TANK_STATE_IDLE;
            }

            if (closest_enemy) {
                if (current_action.type == ACTION_TYPE_ATTACK_MOVE || current_action.type == ACTION_TYPE_PATROL_MOVE) {
                    tank->tank.state_type = TANK_STATE_ATTACKING;
                    attack_compo->closest_enemy_id = closest_enemy->id;
                }
            }

            break;
    }

    {
        struct animated_mesh_movement_component *compo;
        vec2 vel;

        compo = get_entity_component_of_type(tank, COMPONENT_TYPE_ANIMATED_MESH_MOVEMENT, "animated_mesh_movement");
        assert(compo);

        vel = tank->velocity;

        update_animated_mesh_movement_component(compo, aim_dir, vel, dt);
    }

    {
        struct animated_mesh_component *compo;

        compo = get_entity_component_of_type(tank, COMPONENT_TYPE_ANIMATED_MESH, "hovered_animated_mesh");
        assert(compo);

        update_animated_mesh_component(compo, dt);
    }
}

void set_tank_target(struct entity *tank, vec2 target, enum movement_type movement_type) {
    struct movement_component *movement_compo;
    movement_compo = get_entity_component_of_type(tank, COMPONENT_TYPE_MOVEMENT, "movement");

    switch (movement_type) {
        case MOVEMENT_TYPE_MOVING:
            tank->current_action.type = ACTION_TYPE_NORMAL_MOVE;
            break;
        case MOVEMENT_TYPE_ATTACK_MOVING:
            tank->current_action.type = ACTION_TYPE_ATTACK_MOVE;
            break;
        case MOVEMENT_TYPE_PATROLLING:
            tank->current_action.type = ACTION_TYPE_PATROL_MOVE;
            break;
    }

    tank->tank.state_type = TANK_STATE_MOVING;
    movement_compo->type = movement_type;
    movement_compo->path_index = 0;
    game_do_pathfinding(tank->position, target, &movement_compo->path);
}

struct entity_id create_helicopter_entity(float row, float col, int team_num) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;
    struct collider *collider;
    vec2 pos;

    assert(team_num == 0 || team_num == 1);

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);
    mesh_instance->always_on_top = true;

    pos.x = GAME_TILE_WIDTH * col;
    pos.y = GAME_TILE_WIDTH * row;

    entity->type = ENTITY_TYPE_HELICOPTER;
    entity->team_num = team_num;
    entity->position = pos;
    entity->is_selected = false;
    entity->sight_radius = 1.0f;
    entity->is_selectable = true;

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.vehicle_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("hovered", mesh, texture);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.scale = V2(0.25f, 0.25f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.vehicle_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("selected2", mesh, texture);
        child.position = V3(0.0f, 0.f, 0.0f);
        child.scale = V2(0.25f, 0.25f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.helicopter_down[1];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("body2", mesh, texture);
        child.position = V3(0.0f, 0.0f, 0.1f);
        child.scale = V2(0.65f, -0.65f);
        child.already_isometric = true;
        array_push(&mesh_instance->children, child);
    }

    entity->collider_id = create_circle_collider(true, true, 0.10f, 1.0f,
                                                 COLLIDER_CATEGORY_AIR_UNIT);
    collider = get_collider_from_id(entity->collider_id);
    set_collider_position(collider, pos);

    {
        struct component component;
        component.type = COMPONENT_TYPE_MOVEMENT;
        init_movement_component(&component.movement);
        map_set(&entity->components, "movement", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ATTACK;
        init_attack_component(&component.attack, 1.0f);
        map_set(&entity->components, "attack", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH_MOVEMENT;
        init_animated_mesh_movement_component(&component.animated_mesh_movement, "helicopter", 2,
                                              get_mesh_instance_child("body2", mesh_instance));
        map_set(&entity->components, "animated_mesh_movement", component);
    }

    {
        struct component component;
        component.type = COMPONENT_TYPE_ANIMATED_MESH;
        init_animated_mesh_component(&component.animated_mesh, texture_store.vehicle_selection, 5,
                                     get_mesh_instance_child("hovered", mesh_instance), true);
        map_set(&entity->components, "hovered_animated_mesh", component);
    }

    return id;
}

void update_helicopter_entity(struct entity *helicopter, float dt) {
    struct movement_component *movement_compo;
    struct attack_component *attack_compo;
    struct collider *collider;
    struct entity_id closest_enemy_id;
    struct entity *closest_enemy;
    struct action current_action;
    vec2 aim_dir;
    bool arrived;

    movement_compo = get_entity_component_of_type(helicopter, COMPONENT_TYPE_MOVEMENT, "movement");
    assert(movement_compo);

    attack_compo = get_entity_component_of_type(helicopter, COMPONENT_TYPE_ATTACK, "attack");
    assert(attack_compo);

    collider = get_collider_from_id(helicopter->collider_id);
    assert(collider);

    closest_enemy_id = attack_compo->closest_enemy_id;
    closest_enemy = get_entity_from_id(closest_enemy_id);
    current_action = helicopter->current_action;
    aim_dir = V2(0.0f, 1.0f);

    {
        vec2 force, pos, vel;

        force = V2(0.0f, 0.0f);
        pos = helicopter->position;
        vel = helicopter->velocity;

        update_movement_component(movement_compo, pos, vel, &force, &arrived, dt);
        apply_force_to_collider(collider, force);
    }

    switch (helicopter->helicopter.state_type) {
        case HELICOPTER_STATE_IDLE:
            movement_compo->moving_to_target = false;

            if (closest_enemy) {
                helicopter->helicopter.state_type = HELICOPTER_STATE_ATTACKING;
                attack_compo->closest_enemy_id = closest_enemy_id;
            }

            break;
        case HELICOPTER_STATE_ATTACKING:
            movement_compo->moving_to_target = false;

            if (!closest_enemy) {
                switch (current_action.type) {
                    case ACTION_TYPE_NONE:
                    case ACTION_TYPE_NORMAL_MOVE:
                        helicopter->helicopter.state_type = HELICOPTER_STATE_IDLE;
                        break;
                    case ACTION_TYPE_ATTACK_MOVE:
                    case ACTION_TYPE_PATROL_MOVE:
                        helicopter->helicopter.state_type = HELICOPTER_STATE_MOVING;
                        break;
                }
            } else {
                attack_compo->time_since_attack += dt;
                if (attack_compo->time_since_attack > 1.0f) {
                    attack_compo->time_since_attack = 0.0f;
                    create_helicopter_projectile_entity(helicopter->position, closest_enemy->position);
                }
                aim_dir = vec2_normalize(vec2_subtract(closest_enemy->position, helicopter->position));
            }

            break;
        case HELICOPTER_STATE_MOVING:
            movement_compo->moving_to_target = true;

            if (arrived) {
                helicopter->helicopter.state_type = HELICOPTER_STATE_IDLE;
            }

            if (closest_enemy) {
                if (current_action.type == ACTION_TYPE_ATTACK_MOVE || current_action.type == ACTION_TYPE_PATROL_MOVE) {
                    helicopter->helicopter.state_type = HELICOPTER_STATE_ATTACKING;
                    attack_compo->closest_enemy_id = closest_enemy->id;
                }
            }

            break;
    }

    {
        struct animated_mesh_movement_component *compo;
        vec2 vel;

        compo = get_entity_component_of_type(helicopter, COMPONENT_TYPE_ANIMATED_MESH_MOVEMENT, "animated_mesh_movement");
        assert(compo);

        vel = helicopter->velocity;

        update_animated_mesh_movement_component(compo, aim_dir, vel, dt);
    }

    {
        struct animated_mesh_component *compo;

        compo = get_entity_component_of_type(helicopter, COMPONENT_TYPE_ANIMATED_MESH, "hovered_animated_mesh");
        assert(compo);

        update_animated_mesh_component(compo, dt);
    }
}

void set_helicopter_target(struct entity *helicopter, vec2 target, enum movement_type movement_type) {
    struct movement_component *movement_compo;
    movement_compo = get_entity_component_of_type(helicopter, COMPONENT_TYPE_MOVEMENT, "movement");

    switch (movement_type) {
        case MOVEMENT_TYPE_MOVING:
            helicopter->current_action.type = ACTION_TYPE_NORMAL_MOVE;
            break;
        case MOVEMENT_TYPE_ATTACK_MOVING:
            helicopter->current_action.type = ACTION_TYPE_ATTACK_MOVE;
            break;
        case MOVEMENT_TYPE_PATROLLING:
            helicopter->current_action.type = ACTION_TYPE_PATROL_MOVE;
            break;
    }

    helicopter->helicopter.state_type = HELICOPTER_STATE_MOVING;
    movement_compo->type = movement_type;
    movement_compo->path_index = 0;
    array_clear(&movement_compo->path);
    array_push(&movement_compo->path, helicopter->position);
    array_push(&movement_compo->path, target);
}

struct entity_id create_tank_projectile_entity(vec2 start_pos,
                                               vec2 target_pos) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);
    mesh_instance->always_on_top = true;

    entity->type = ENTITY_TYPE_TANK_PROJECTILE;
    entity->position = start_pos;
    entity->is_selected = false;
    entity->tank_projectile.time_in_air = 0.0f;
    entity->tank_projectile.target_pos = target_pos;

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = create_color_texture(V3(1.0f, 0.0f, 0.0f));
        mesh = &mesh_store.circle_mesh;
        child = create_mesh_instance_child("body2", mesh, texture);

        child.scale = V2(0.01f, 0.01f);
        child.position = V3(0.1f, 0.1f, 0.0f);
        child.already_isometric = true;
        array_push(&mesh_instance->children, child);
    }

    /*
	{
		struct texture texture;
		struct mesh *mesh;
		struct mesh_instance_child child;

		texture = texture_store.bullet[0];
		mesh = &mesh_store.circle_mesh;
		child = create_mesh_instance_child("body2", mesh, texture);

		child.scale = V2(3.3f * 0.02f, 0.02f);
		child.position = V3(0.2f, 0.2f, 0.0f);
		child.already_isometric = true;
		array_push(&mesh_instance->children, child);
	}
	*/

    return id;
}

void update_tank_projectile_entity(struct entity *projectile, float dt) {
    float length = 0.5f;
    projectile->tank_projectile.time_in_air += dt;
    if (projectile->tank_projectile.time_in_air > length) {
        create_explosion_entity(projectile->position);
        delete_entity(projectile->id);
        return;
    }

    float time_left = length - projectile->tank_projectile.time_in_air;
    vec2 dir = vec2_subtract(projectile->tank_projectile.target_pos,
                             projectile->position);
    dir = vec2_scale(dir, 1.0f / time_left);
    projectile->position.x += dir.x * dt;
    projectile->position.y += dir.y * dt;

    {
        vec3 dir_3 = V3(dir.x, dir.y, 0.0f);
        dir_3 = vec3_apply_mat4(dir_3, 0.0f, renderer_state.isometric_mat);
        dir = vec2_normalize(V2(dir_3.x, dir_3.y));
    }

    int frame_num = 0;
    float a = fmodf(projectile->tank_projectile.time_in_air, 0.2f) / 0.2f;
    if (a < 1.0f / 5.0f) {
        frame_num = 0;
    } else if (a < 2.0f / 5.0f) {
        frame_num = 1;
    } else if (a < 3.0f / 5.0f) {
        frame_num = 2;
    } else if (a < 4.0f / 5.0f) {
        frame_num = 3;
    } else if (a < 5.0f / 5.0f) {
        frame_num = 4;
    }

    float rotation = acosf(dir.x);
    if (dir.y < 0.0f) {
        rotation *= -1.0f;
    }

    struct mesh_instance *parent_mesh;
    struct mesh_instance_child *body_mesh;
    parent_mesh = get_mesh_instance_from_id(projectile->mesh_instance_id);
    body_mesh = get_mesh_instance_child("body2", parent_mesh);
    body_mesh->texture = texture_store.bullet[frame_num];
    body_mesh->rotation = rotation;
}

struct entity_id create_helicopter_projectile_entity(vec2 start_pos,
                                                     vec2 target_pos) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);

    entity->type = ENTITY_TYPE_HELICOPTER_PROJECTILE;
    entity->position = start_pos;
    entity->is_selected = false;
    entity->helicopter_projectile.time_in_air = 0.0f;
    entity->helicopter_projectile.target_pos = target_pos;

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = create_color_texture(V3(1.0f, 0.0f, 0.0f));
        mesh = &mesh_store.circle_mesh;
        child = create_mesh_instance_child("body2", mesh, texture);

        child.scale = V2(0.01f, 0.01f);
        child.position = V3(0.1f, 0.1f, 0.0f);
        child.already_isometric = true;
        array_push(&mesh_instance->children, child);
    }

    return id;
}

void update_helicopter_projectile_entity(struct entity *projectile, float dt) {
    projectile->helicopter_projectile.time_in_air += dt;
    if (projectile->helicopter_projectile.time_in_air > 0.5f) {
        create_explosion_entity(projectile->position);
        delete_entity(projectile->id);
        return;
    }

    float time_left = 0.5f - projectile->helicopter_projectile.time_in_air;
    vec2 dir = vec2_subtract(projectile->helicopter_projectile.target_pos,
                             projectile->position);
    dir = vec2_scale(dir, 1.0f / time_left);
    projectile->position.x += dir.x * dt;
    projectile->position.y += dir.y * dt;
}

struct entity_id create_explosion_entity(vec2 position) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);
    mesh_instance->always_on_top = true;

    entity->type = ENTITY_TYPE_EXPLOSION;
    entity->position = position;
    entity->is_selected = false;
    entity->explosion.t = 0.0f;

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;
        vec3 position;
        vec2 scale;

        mesh = &mesh_store.square_mesh;
        texture = texture_store.explosion[0];
        position = V3(0.2f, 0.2f, 0.0f);
        scale = V2(0.7f, -0.7f);

        child = create_mesh_instance_child("explosion", mesh, texture);
        child.position = position;
        child.scale = scale;
        child.already_isometric = true;
        array_push(&mesh_instance->children, child);
    }

    return id;
}

void update_explosion_entity(struct entity *explosion, float dt) {
    float length = 0.8f;
    int frame_num = 0, i;

    explosion->explosion.t += dt;
    if (explosion->explosion.t > length) {
        delete_entity(explosion->id);
        return;
    }

    for (i = 0; i < 9; i++) {
        if (explosion->explosion.t < (i + 1.0f) / 9.0f * length) {
            frame_num = i;
            break;
        }
    }

    struct mesh_instance *parent;
    struct mesh_instance_child *child;
    parent = get_mesh_instance_from_id(explosion->mesh_instance_id);
    child = get_mesh_instance_child("explosion", parent);
    child->texture = texture_store.explosion[frame_num];
}

struct entity_id create_placeholder_entity(enum entity_type type) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;
    vec2 pos;

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);

    pos.x = 0.0f;
    pos.y = 0.0f;

    entity->type = ENTITY_TYPE_PLACEHOLDER;
    entity->position = pos;
    entity->is_selected = false;
    entity->placeholder.type = type;
    entity->placeholder.row = 0;
    entity->placeholder.col = 0;
    entity->placeholder.can_be_placed = false;

    {
        const char *texture_name;
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;
        vec2 scale;
        vec3 position;

        mesh = &mesh_store.square_mesh;

        switch (type) {
            case ENTITY_TYPE_TURRET: {
                texture_name = "turret/1.png";
                scale = V2(0.25f, -0.25f);
                position = V3(0.07f, 0.06f, 0.1f);
                break;
            }
            case ENTITY_TYPE_SUPPLY_DEPOT: {
                texture_name = "supply-depot/1.png";
                scale = V2(0.25f, -0.25f);
                position = V3(0.07f, 0.06f, 0.1f);
                break;
            }
            case ENTITY_TYPE_BARRACKS: {
                texture_name = "barracks/1.png";
                scale = V2(0.60f, -0.60f);
                position = V3(0.15f, 0.15f, 0.1f);
                break;
            }
            case ENTITY_TYPE_FACTORY: {
                texture_name = "factory/1.png";
                scale = V2(0.50f, -0.50f);
                position = V3(0.10f, 0.08f, 0.1f);
                break;
            }
            case ENTITY_TYPE_HELIPORT: {
                texture_name = "heliport/1.png";
                scale = V2(0.50f, -0.50f);
                position = V3(0.18f, 0.17f, 0.1f);
                break;
            }
            case ENTITY_TYPE_REFINERY: {
                texture_name = "refinery/1.png";
                scale = V2(0.45f, -0.45f);
                position = V3(0.09f, 0.09f, 0.1f);
                break;
            }
            default:
                assert(false && "No placeholder code for this type");
        }

        texture = create_solid_color_atlas_image_texture(texture_name, V3(0.0f, 0.0f, 0.0f), 0.0f);
        child = create_mesh_instance_child("body2", mesh, texture);
        child.scale = scale;
        child.position = position;
        child.already_isometric = true;
        child.opacity = 0.5f;
        array_push(&mesh_instance->children, child);
    }

    return id;
}

void update_placeholder_entity(struct entity *placeholder, float dt) {
    struct mesh_instance *parent;
    struct mesh_instance_child *child;

    parent = get_mesh_instance_from_id(placeholder->mesh_instance_id);
    assert(parent);

    child = get_mesh_instance_child("body2", parent);
    assert(child);

    assert(child->texture.type == TEXTURE_TYPE_SOLID_COLOR_ATLAS_IMAGE);
    child->texture.solid_color_atlas_image.color = V3(1.0f, 0.0f, 0.0f);

    if (placeholder->placeholder.can_be_placed) {
        child->texture.solid_color_atlas_image.color_pct = 0.0f;
    } else {
        child->texture.solid_color_atlas_image.color_pct = 0.5f;
    }
}

void set_placeholder_row_col(struct entity *placeholder, int row, int col) {
    int size;

    placeholder->placeholder.row = row;
    placeholder->placeholder.col = col;

    switch (placeholder->placeholder.type) {
        case ENTITY_TYPE_TURRET:
        case ENTITY_TYPE_SUPPLY_DEPOT:
            placeholder->position.x = (col + 0.5f) * GAME_TILE_WIDTH;
            placeholder->position.y = (row + 0.5f) * GAME_TILE_WIDTH;
            size = 1;
            break;
        case ENTITY_TYPE_BARRACKS:
        case ENTITY_TYPE_FACTORY:
        case ENTITY_TYPE_HELIPORT:
        case ENTITY_TYPE_REFINERY:
            placeholder->position.x = col * GAME_TILE_WIDTH;
            placeholder->position.y = row * GAME_TILE_WIDTH;
            size = 2;
            break;
        default:
            assert(false && "Invalid placeholder type");
            break;
    }

    placeholder->placeholder.can_be_placed = true;
    for (int row_p = row; row_p < row + size; row_p++) {
        for (int col_p = col; col_p < col + size; col_p++) {
            int idx;

            idx = GAME_MAP_SIZE * col_p + row_p;
            if (size == 2) {
                idx = GAME_MAP_SIZE * (col_p - 1) + (row_p - 1);
            }

            if (idx >= GAME_MAP_SIZE * GAME_MAP_SIZE || idx < 0) {
                continue;
            }

            if (game_state.map_tile_occupied[idx]) {
                placeholder->placeholder.can_be_placed = false;
            }
        }
    }
}

struct entity_id create_terrain_border(int col0, int row0, int col1, int row1) {
    struct entity_id id;
    struct entity *entity;
    struct collider *collider;
    vec2 pos;
    vec2 p0, p1;

    id = allocate_entity();
    entity = get_entity_from_id(id);

    p0.x = col0 * GAME_TILE_WIDTH;
    p0.y = row0 * GAME_TILE_WIDTH;

    p1.x = col1 * GAME_TILE_WIDTH;
    p1.y = row1 * GAME_TILE_WIDTH;

    pos.x = 0.5f * (col0 + col1) * GAME_TILE_WIDTH;
    pos.y = 0.5f * (row0 + row1) * GAME_TILE_WIDTH;

    /*
	{
		int idx = GAME_MAP_SIZE * col + row;
		game_state.map_tile_occupied[idx] = true;
	}
	*/

    entity->type = ENTITY_TYPE_TERRAIN_BORDER;
    entity->position = pos;
    entity->is_selected = false;

    entity->collider_id = create_line_segment_collider(false, true,
                                                       1.0f, 1.0f,
                                                       p0, p1,
                                                       COLLIDER_CATEGORY_BUILDING);
    collider = get_collider_from_id(entity->collider_id);
    //set_collider_position(collider, V2(pos.x + 0.5f * GAME_TILE_WIDTH,
    //pos.y + 0.5f * GAME_TILE_WIDTH));
    set_collider_position(collider, V2(0.0f, 0.0f));

    return id;
}

struct entity_id create_turret_entity(int row, int col, int team_num) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;
    struct collider *collider;
    vec2 pos;

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);

    pos.x = GAME_TILE_WIDTH * (col + 0.5f);
    pos.y = GAME_TILE_WIDTH * (row + 0.5f);

    entity->type = ENTITY_TYPE_TURRET;
    entity->position = pos;
    entity->is_selected = false;
    entity->is_selectable = true;
    entity->sight_radius = 1.0f;
    entity->minimap_width = 0.5f * GAME_TILE_WIDTH;
    entity->team_num = team_num;

    {
        int idx = GAME_MAP_SIZE * col + row;
        game_state.map_tile_occupied[idx] = true;
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.building_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("selected2", mesh, texture);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.scale = V2(0.25f, 0.25f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.building_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("hovered", mesh, texture);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.scale = V2(0.25f, 0.25f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.turret[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("body2", mesh, texture);
        child.position = V3(0.07f, 0.06f, 0.1f);
        child.scale = V2(0.25f, -0.25f);
        child.already_isometric = true;
        array_push(&mesh_instance->children, child);
    }

    entity->collider_id = create_box_collider(false,
                                              true,
                                              GAME_TILE_WIDTH,
                                              GAME_TILE_WIDTH,
                                              1.0f,
                                              COLLIDER_CATEGORY_BUILDING);
    collider = get_collider_from_id(entity->collider_id);
    set_collider_position(collider, pos);

    return id;
}

struct entity_id create_supply_depot_entity(int row, int col) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;
    struct collider *collider;
    vec2 pos;

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);

    pos.x = GAME_TILE_WIDTH * (col + 0.5f);
    pos.y = GAME_TILE_WIDTH * (row + 0.5f);

    entity->type = ENTITY_TYPE_SUPPLY_DEPOT;
    entity->position = pos;
    entity->is_selected = false;
    entity->is_selectable = true;
    game_state.team_supply_max[0] += 8;

    {
        int idx = GAME_MAP_SIZE * col + row;
        game_state.map_tile_occupied[idx] = true;
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.building_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("selected2", mesh, texture);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.scale = V2(0.25f, -0.25f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.building_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("hovered", mesh, texture);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.scale = V2(0.25f, -0.25f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.supply_depot[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("body2", mesh, texture);
        child.position = V3(0.07f, 0.06f, 0.1f);
        child.scale = V2(0.25f, -0.25f);
        child.already_isometric = true;
        array_push(&mesh_instance->children, child);
    }

    entity->collider_id = create_box_collider(false,
                                              true,
                                              GAME_TILE_WIDTH,
                                              GAME_TILE_WIDTH,
                                              1.0f,
                                              COLLIDER_CATEGORY_BUILDING);
    collider = get_collider_from_id(entity->collider_id);
    set_collider_position(collider, pos);

    return id;
}

struct entity_id create_refinery_entity(int row, int col) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;
    struct collider *collider;
    vec2 pos;

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);

    pos.x = GAME_TILE_WIDTH * col;
    pos.y = GAME_TILE_WIDTH * row;

    entity->type = ENTITY_TYPE_REFINERY;
    entity->position = pos;
    entity->is_selected = false;
    entity->is_selectable = true;
    entity->sight_radius = 1.0f;
    entity->minimap_width = 2.0f * GAME_TILE_WIDTH;

    entity->refinery.t = 0.0f;
    array_init(&entity->refinery.worker_id_array);

    for (int d_row = 0; d_row < 2; d_row++) {
        for (int d_col = 0; d_col < 2; d_col++) {
            int idx = GAME_MAP_SIZE * (col - d_col) + row - d_row;
            game_state.map_tile_occupied[idx] = true;
        }
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.building_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("hovered", mesh, texture);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.scale = V2(0.50f, 0.50f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.building_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("selected2", mesh, texture);
        child.position = V3(0.0f, 0.0f, 0.0f);
        child.scale = V2(0.50f, 0.50f);
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.refinery[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("body2", mesh, texture);
        child.position = V3(0.09f, 0.09f, 0.1f);
        child.scale = V2(0.45f, -0.45f);
        child.already_isometric = true;
        array_push(&mesh_instance->children, child);
    }

    entity->collider_id = create_box_collider(false,
                                              true,
                                              2.0f * GAME_TILE_WIDTH,
                                              2.0f * GAME_TILE_WIDTH,
                                              1.0f,
                                              COLLIDER_CATEGORY_BUILDING);
    collider = get_collider_from_id(entity->collider_id);
    set_collider_position(collider, pos);

    init_animated_mesh_component(&entity->refinery.hovered_animated_mesh_component,
                                 texture_store.building_selection, 4,
                                 get_mesh_instance_child("hovered", mesh_instance), true);

    init_animated_mesh_component(&entity->refinery.body_animated_mesh_component,
                                 texture_store.refinery, 3,
                                 get_mesh_instance_child("body2", mesh_instance), true);

    return id;
}

void update_refinery_entity(struct entity *refinery, float dt) {
    update_animated_mesh_component(&refinery->refinery.hovered_animated_mesh_component, dt);
    update_animated_mesh_component(&refinery->refinery.body_animated_mesh_component, dt);
}

void add_worker_to_refinery(struct entity *refinery, struct entity *worker) {
    array_push(&refinery->refinery.worker_id_array, worker->id);
    worker->worker.state = WORKER_STATE_IN_REFINERY;
    set_entity_is_selected(worker->id, false);
    worker->is_selectable = false;

    struct mesh_instance *parent;
    struct mesh_instance_child *child;
    parent = get_mesh_instance_from_id(worker->mesh_instance_id);

    child = get_mesh_instance_child("selected2", parent);
    child->is_visible = false;

    child = get_mesh_instance_child("body2", parent);
    child->is_visible = false;

    worker->is_visible = false;
}

void remove_worker_from_refinery(struct entity *refinery, int worker_idx) {
    struct entity_id worker_id = refinery->refinery.worker_id_array.data[worker_idx];
    struct entity *worker = get_entity_from_id(worker_id);
    if (worker) {
        struct mesh_instance *parent;
        struct mesh_instance_child *child;
        parent = get_mesh_instance_from_id(worker->mesh_instance_id);

        child = get_mesh_instance_child("body2", parent);
        child->is_visible = true;

        worker->worker.state = WORKER_STATE_IDLE;
        worker->position = refinery->position;
        worker->is_selectable = true;

        worker->is_visible = true;

        struct collider *collider = get_collider_from_id(worker->collider_id);
        set_collider_position(collider, worker->position);
    }
    array_splice(&refinery->refinery.worker_id_array, worker_idx, 1);
}

struct entity_id create_building_construction_entity(enum entity_type type, int row, int col, bool is_small) {
    struct entity_id id;
    struct entity *entity;
    struct mesh_instance *mesh_instance;
    vec2 pos;

    id = allocate_entity();
    entity = get_entity_from_id(id);
    mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);

    pos.x = col * GAME_TILE_WIDTH - 0.0001f;
    pos.y = row * GAME_TILE_WIDTH - 0.0001f;

    if (is_small) {
        entity->type = ENTITY_TYPE_BUILDING_CONSTRUCTION_SMALL;
    } else {
        entity->type = ENTITY_TYPE_BUILDING_CONSTRUCTION_LARGE;
    }
    entity->position = pos;
    entity->is_selected = false;
    entity->is_selectable = true;
    entity->sight_radius = 1.0f;
    entity->building_construction.created_building = false;
    entity->building_construction.row = row;
    entity->building_construction.col = col;
    entity->building_construction.type = type;
    entity->building_construction.t = 0.0f;
    entity->building_construction.is_small = is_small;

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.building_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("hovered", mesh, texture);
        if (is_small) {
            child.scale = V2(0.25f, -0.25f);
            child.position = V3(0.10f, 0.10f, 0.0f);
        } else {
            child.scale = V2(0.50f, 0.50f);
            child.position = V3(0.0f, 0.0f, 0.0f);
        }
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        texture = texture_store.building_selection[0];
        mesh = &mesh_store.square_mesh;

        child = create_mesh_instance_child("selected2", mesh, texture);
        if (is_small) {
            child.scale = V2(0.25f, -0.25f);
            child.position = V3(0.10f, 0.10f, 0.0f);
        } else {
            child.scale = V2(0.50f, 0.50f);
            child.position = V3(0.0f, 0.0f, 0.0f);
        }
        child.already_isometric = false;
        child.is_visible = false;
        array_push(&mesh_instance->children, child);
    }

    {
        struct texture texture;
        struct mesh *mesh;
        struct mesh_instance_child child;

        mesh = &mesh_store.square_mesh;
        texture = texture_store.under_construction[0];

        child = create_mesh_instance_child("body2", mesh, texture);
        if (is_small) {
            child.scale = V2(0.30f, -0.30f);
            child.position = V3(0.15f, 0.15f, 0.1f);
        } else {
            child.scale = V2(0.60f, -0.60f);
            child.position = V3(0.12f, 0.12f, 0.1f);
        }
        child.already_isometric = true;
        array_push(&mesh_instance->children, child);
    }

    init_animated_mesh_component(&entity->building_construction.hovered_animated_mesh_component,
                                 texture_store.building_selection, 4,
                                 get_mesh_instance_child("hovered", mesh_instance), true);

    return id;
}

void update_building_construction_entity(struct entity *building_construction, float dt) {
    int i;
    building_construction->building_construction.t += dt;
    float t = building_construction->building_construction.t;

    float begining_length = 1.0f;
    float middle_length = 8.0f;
    float end_length = 1.0f;

    if (t > begining_length + middle_length + end_length) {
        delete_entity(building_construction->id);
        return;
    }

    // Beginning: 0 - 3
    // Middle: 4 - 11
    // End: 12 - 18

    if (t < begining_length) {
        for (i = 0; i < 4; i++) {
            if (t < begining_length * (i + 1) / 4.0f) {
                break;
            }
        }
    } else if (t < begining_length + middle_length) {
        t -= begining_length;
        t = fmodf(t, 2.0f);
        for (i = 0; i < 8; i++) {
            if (t < 2.0f * (i + 1) / 8.0f) {
                i += 4;
                break;
            }
        }
    } else {
        t -= begining_length;
        t -= middle_length;
        for (i = 0; i < 7; i++) {
            if (t < end_length * (i + 1) / 7.0f) {
                i += 4;
                i += 8;
                break;
            }
        }
    }

    if (i == 14 && !building_construction->building_construction.created_building) {
        building_construction->building_construction.created_building = true;
        int row = building_construction->building_construction.row;
        int col = building_construction->building_construction.col;
        enum entity_type type = building_construction->building_construction.type;
        struct entity_id created_entity_id;

        switch (type) {
            case ENTITY_TYPE_BARRACKS:
                created_entity_id = create_barracks_entity(row, col, 0);
                break;
            case ENTITY_TYPE_FACTORY:
                created_entity_id = create_factory_entity(row, col, 0);
                break;
            case ENTITY_TYPE_HELIPORT:
                created_entity_id = create_heliport_entity(row, col);
                break;
            case ENTITY_TYPE_SUPPLY_DEPOT:
                created_entity_id = create_supply_depot_entity(row, col);
                break;
            case ENTITY_TYPE_TURRET:
                created_entity_id = create_turret_entity(row, col, 0);
                break;
            default:
                assert(false && "Invalid type of entity type for a building construction");
                break;
        }

        if (building_construction->is_selected) {
            set_entity_is_selected(building_construction->id, false);
            set_entity_is_selected(created_entity_id, true);
        }
    }

    assert(i >= 0 && i < 19);
    struct mesh_instance *parent;
    struct mesh_instance_child *child;
    parent = get_mesh_instance_from_id(building_construction->mesh_instance_id);
    child = get_mesh_instance_child("body2", parent);
    child->texture = texture_store.under_construction[i];

    update_animated_mesh_component(&building_construction->building_construction.hovered_animated_mesh_component, dt);
}

void cancel_building_construction_entity(struct entity *building_construction) {
    if (building_construction->building_construction.created_building) {
        return;
    }
    delete_entity(building_construction->id);
}
