#include "controls.h"

#include "renderer.h"

static void check_if_clicked(int state, bool *clicked, bool *down) {
    *clicked = false;
    if (state == GLFW_PRESS && !(*down)) {
        *down = true;
    }
    if (state == GLFW_RELEASE && (*down)) {
        *clicked = true;
        *down = false;
    }
}

static double scroll_y_offset = 0.0f;
static double scroll_x_offset = 0.0f;

void scroll_callback(GLFWwindow *window, double xoffset, double yoffset) {
    scroll_x_offset = xoffset;
    scroll_y_offset = yoffset;
}

void controls_init(GLFWwindow *window) {
    for (int i = 0; i < 256; i++) {
        controls_state.key_down[i] = false;
        controls_state.key_clicked[i] = false;
    }

    controls_state.left_down = false;
    controls_state.left_clicked = false;

    controls_state.up_down = false;
    controls_state.up_clicked = false;

    controls_state.down_down = false;
    controls_state.down_clicked = false;

    controls_state.right_down = false;
    controls_state.right_clicked = false;

    controls_state.left_mouse_down = false;
    controls_state.left_mouse_clicked = false;

    controls_state.right_mouse_down = false;
    controls_state.right_mouse_clicked = false;

    controls_state.middle_mouse_down = false;
    controls_state.middle_mouse_clicked = false;
    controls_state.scroll_delta = 0.0f;

    glfwSetScrollCallback(window, scroll_callback);
}

void controls_update(GLFWwindow *window) {
    for (int c = 0; c < GLFW_KEY_LAST; c++) {
        check_if_clicked(glfwGetKey(window, c),
                         &controls_state.key_clicked[c],
                         &controls_state.key_down[c]);
    }
    check_if_clicked(glfwGetKey(window, GLFW_KEY_UP),
                     &controls_state.up_clicked,
                     &controls_state.up_down);
    check_if_clicked(glfwGetKey(window, GLFW_KEY_DOWN),
                     &controls_state.down_clicked,
                     &controls_state.down_down);
    check_if_clicked(glfwGetKey(window, GLFW_KEY_LEFT),
                     &controls_state.left_clicked,
                     &controls_state.left_down);
    check_if_clicked(glfwGetKey(window, GLFW_KEY_RIGHT),
                     &controls_state.right_clicked,
                     &controls_state.right_down);
    check_if_clicked(glfwGetKey(window, GLFW_KEY_RIGHT),
                     &controls_state.right_clicked,
                     &controls_state.right_down);

    {
        double xpos, ypos;
        glfwGetCursorPos(window, &xpos, &ypos);

		if (xpos < 0.0f) {
			xpos = 0.0f;
		}
		if (xpos > renderer_state.window_width) {
			xpos = renderer_state.window_width;
		}
		if (ypos < 0.0f) {
			ypos = 0.0f;
		}
		if (ypos > renderer_state.window_height) {
			ypos = renderer_state.window_height;
		}

        vec2 new_left_mouse_pos = V2((float)xpos, (float)ypos);
        controls_state.mouse_delta = vec2_subtract(new_left_mouse_pos, controls_state.left_mouse_pos);
        controls_state.left_mouse_pos = new_left_mouse_pos;

        bool mouse_down_before = controls_state.left_mouse_down;
        int mouse_state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
        check_if_clicked(mouse_state, &controls_state.left_mouse_clicked, &controls_state.left_mouse_down);
        if (!mouse_down_before && controls_state.left_mouse_down) {
            controls_state.left_mouse_down_pos = controls_state.left_mouse_pos;
        }
    }

    {
        double xpos, ypos;
        glfwGetCursorPos(window, &xpos, &ypos);

        bool mouse_down_before = controls_state.right_mouse_down;
        int mouse_state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT);
        check_if_clicked(mouse_state, &controls_state.right_mouse_clicked, &controls_state.right_mouse_down);
        if (!mouse_down_before && controls_state.right_mouse_down) {
            controls_state.right_mouse_down_pos = V2((float)xpos, (float)ypos);
        }
    }

    {
        int mouse_state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE);
        check_if_clicked(mouse_state, &controls_state.middle_mouse_clicked, &controls_state.middle_mouse_down);
    }

    controls_state.scroll_delta = (float) scroll_y_offset;
    scroll_x_offset = 0;
    scroll_y_offset = 0;
}
