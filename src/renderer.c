#include "renderer.h"

#include <assert.h>
#include <limits.h>

#include "controls.h"
#include "font.h"
#include "game.h"
#include "mesh.h"
#include "shader.h"
#include "terrain.h"

#define UNIT_FB_WIDTH (1.2 * 1920)
#define UNIT_FB_HEIGHT (1.2 * 1080)
#define FOG_OF_WAR_SIZE 256

struct {
    array_mesh_instance_ptr_t mesh_instances_array;
    array_entity_id_t entity_id_array;
} _renderer_state;

static void check_for_framebuffer_errors(void) {
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        printf("Could not create the frambuffer :(\n");

        switch (status) {
            case GL_FRAMEBUFFER_UNDEFINED: {
                printf("GL_FRAMEBUFFER_UNDEFINED\n");
                break;
            }

            case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT: {
                printf("GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT\n");
                break;
            }

            case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: {
                printf("GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT\n");
                break;
            }

            case GL_FRAMEBUFFER_UNSUPPORTED: {
                printf("GL_FRAMEBUFFER_UNSUPPORTED\n");
                break;
            }

            case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE: {
                printf("GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE\n");
                break;
            }
        }
    }
}

static void renderer_gaussian_blur(GLuint *fbs, GLuint *texs) {
    struct gaussian_blur_shader *shader = &shader_store.gaussian_blur_shader;
    struct mat4 mvp_mat = mat4_orthographic_projection(-0.5f, 0.5f, -0.5f, 0.5f, -1.0f, 1.0f);
    struct mesh *mesh = &mesh_store.square_mesh;

    glUseProgram(shader->program);
    glUniformMatrix4fv(shader->mvp_mat_location, 1, GL_TRUE, mvp_mat.m);
    glUniform1i(shader->texture_location, 0);
    glActiveTexture(GL_TEXTURE0);

    glBindBuffer(GL_ARRAY_BUFFER, mesh->position_vbo);
    glVertexAttribPointer(shader->position_location, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(shader->position_location);

    glBindBuffer(GL_ARRAY_BUFFER, mesh->texture_coord_vbo);
    glVertexAttribPointer(shader->texture_coord_location, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(shader->texture_coord_location);

    for (int i = 0; i < 1; i++) {
        {
            glBindFramebuffer(GL_FRAMEBUFFER, fbs[2]);
            glClear(GL_COLOR_BUFFER_BIT);
            if (i == 0) {
                glBindTexture(GL_TEXTURE_2D, texs[0]);
            } else {
                glBindTexture(GL_TEXTURE_2D, texs[1]);
            }
            glUniform1i(shader->horizontal_location, 0);
            glDrawArrays(GL_TRIANGLES, 0, mesh->num_vertices);
        }

        {
            glBindFramebuffer(GL_FRAMEBUFFER, fbs[1]);
            glClear(GL_COLOR_BUFFER_BIT);
            glBindTexture(GL_TEXTURE_2D, texs[2]);
            glUniform1i(shader->horizontal_location, 1);
            glDrawArrays(GL_TRIANGLES, 0, mesh->num_vertices);
        }
    }
}

void renderer_init(void) {
    renderer_state.window_width = 1920;
    renderer_state.window_height = 1080;

    mesh_store_init();
    texture_store_init();
    shader_store_init();
    renderer_terrain_init();

    array_init(&_renderer_state.mesh_instances_array);
    array_init(&_renderer_state.entity_id_array);

    {
        float x, y, sx, sy;

        x = 0.5f * GAME_MAP_WIDTH;
        y = 0.5f * GAME_MAP_WIDTH;
        sx = 4.0f;
        sy = 2.0f;

        renderer_state.proj_mat = mat4_orthographic_projection(
            x - sx, x + sx,
            y - sy, y + sy,
            -1.0f, 1.0f);
    }

    {
        float w, scale, rotation;
        mat4 translation_1_mat, scale_mat, rotation_mat, translation_0_mat;

        w = 0.5f * GAME_MAP_WIDTH;
        rotation = 0.8f;
        scale = 0.5f;

        translation_1_mat = mat4_translation(V3(w, w, 0.0f));
        scale_mat = mat4_scale(V3(1.0f, scale, 1.0f));
        rotation_mat = mat4_rotation_z(rotation);
        translation_0_mat = mat4_translation(V3(-w, -w, 0.0f));

        renderer_state.isometric_mat =
            mat4_multiply_n(4, translation_1_mat, scale_mat,
                            rotation_mat, translation_0_mat);
    }

    //
    // Set up the framebuffer we draw the units to, this is used
    // to tell us which unit the user selects on a mouse press.
    //
    {
        GLuint fb, fb_tex;
        GLenum draw_bufs[1];

        glGenTextures(1, &fb_tex);
        glBindTexture(GL_TEXTURE_2D, fb_tex);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32UI, (int)UNIT_FB_WIDTH, (int)UNIT_FB_HEIGHT,
                     0, GL_RG_INTEGER, GL_UNSIGNED_INT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glGenFramebuffers(1, &fb);
        glBindFramebuffer(GL_FRAMEBUFFER, fb);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                               GL_TEXTURE_2D, fb_tex, 0);

        draw_bufs[0] = GL_COLOR_ATTACHMENT0;
        glDrawBuffers(1, draw_bufs);

        renderer_state.unit_fb = fb;
        renderer_state.unit_fb_tex = fb_tex;

        check_for_framebuffer_errors();
    }

    for (int i = 0; i < 3; i++) {
        GLuint tex;
        glGenTextures(1, &tex);
        glBindTexture(GL_TEXTURE_2D, tex);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, FOG_OF_WAR_SIZE, FOG_OF_WAR_SIZE,
                     0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        GLuint fb = 0;
        glGenFramebuffers(1, &fb);
        glBindFramebuffer(GL_FRAMEBUFFER, fb);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);
        GLenum draw_bufs[1] = {GL_COLOR_ATTACHMENT0};
        glDrawBuffers(1, draw_bufs);

        check_for_framebuffer_errors();

        renderer_state.explored_fow_fb[i] = fb;
        renderer_state.explored_fow_tex[i] = tex;
        renderer_state.clear_explored_fow = true;
    }

    for (int i = 0; i < 3; i++) {
        GLuint tex;
        glGenTextures(1, &tex);
        glBindTexture(GL_TEXTURE_2D, tex);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, FOG_OF_WAR_SIZE, FOG_OF_WAR_SIZE,
                     0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        GLuint fb = 0;
        glGenFramebuffers(1, &fb);
        glBindFramebuffer(GL_FRAMEBUFFER, fb);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);
        GLenum draw_bufs[1] = {GL_COLOR_ATTACHMENT0};
        glDrawBuffers(1, draw_bufs);

        check_for_framebuffer_errors();

        renderer_state.revealed_fow_fb[i] = fb;
        renderer_state.revealed_fow_tex[i] = tex;
        renderer_state.clear_revealed_fow = true;
    }
}

static void draw_mesh_instance(struct mesh_instance *mesh_instance) {
    int i;
    struct mesh_instance_child child;

    if (!mesh_instance->is_visible) {
        return;
    }

    array_foreach(&mesh_instance->children, child, i) {
        if (!child.is_visible) {
            continue;
        }

        mat4 model_mat, mvp_mat;
        vec3 translation_vec, scale_vec;
        float rotation;

        translation_vec.x = mesh_instance->position.x + child.position.x;
        translation_vec.y = mesh_instance->position.y + child.position.y;
        translation_vec.z = mesh_instance->position.z +
                            child.position.z / 10000.0f;

        scale_vec.x = mesh_instance->scale.x * child.scale.x;
        scale_vec.y = mesh_instance->scale.y * child.scale.y;
        scale_vec.z = 1.0f;

        if (child.flip_x) {
            scale_vec.x *= -1.0f;
        }

        rotation = mesh_instance->rotation + child.rotation;

        if (child.already_isometric) {
            translation_vec = vec3_apply_mat4(translation_vec, 1.0f,
                                              renderer_state.isometric_mat);
            translation_vec.z = mesh_instance->position.z +
                                child.position.z / 10000.0f;

            model_mat = mat4_multiply_n(3,
                                        mat4_translation(translation_vec),
                                        mat4_rotation_z(rotation),
                                        mat4_scale(scale_vec));
        } else {
            model_mat = mat4_multiply_n(4,
                                        renderer_state.isometric_mat,
                                        mat4_translation(translation_vec),
                                        mat4_rotation_z(rotation),
                                        mat4_scale(scale_vec));
        }

        mvp_mat = mat4_multiply_n(2, renderer_state.proj_mat, model_mat);

        switch (child.texture.type) {
            case TEXTURE_TYPE_IMAGE: {
                struct mesh_texture_shader *s;
                s = &shader_store.mesh_texture_shader;

                glUseProgram(s->program);
                glUniformMatrix4fv(s->mvp_mat_location, 1, GL_TRUE, mvp_mat.m);
                glUniform1i(s->texture_location, 0);
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, child.texture.image);
                glUniform1f(s->opacity_location, child.opacity);

                glBindBuffer(GL_ARRAY_BUFFER, child.mesh->position_vbo);
                glVertexAttribPointer(s->position_location, 3, GL_FLOAT, GL_FALSE,
                                      0, NULL);
                glEnableVertexAttribArray(s->position_location);

                glBindBuffer(GL_ARRAY_BUFFER,
                             child.mesh->texture_coord_vbo);
                glVertexAttribPointer(s->texture_coord_location, 2, GL_FLOAT,
                                      GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(s->texture_coord_location);

                glDrawArrays(GL_TRIANGLES, 0, child.mesh->num_vertices);
                break;
            }
            case TEXTURE_TYPE_COLOR: {
                struct mesh_shader *s;
                s = &shader_store.mesh_shader;

                glUseProgram(s->program);
                glUniformMatrix4fv(s->mvp_mat_location, 1, GL_TRUE, mvp_mat.m);
                glUniform1f(s->opacity_location, child.opacity);
                glUniform1f(s->color_pct_location, 1.0f);
                glUniform1f(s->tex_pct_location, 0.0f);

                glUniform3f(s->color_location,
                            child.texture.color.x,
                            child.texture.color.y,
                            child.texture.color.z);

                glBindBuffer(GL_ARRAY_BUFFER, child.mesh->position_vbo);
                glVertexAttribPointer(s->position_location, 3, GL_FLOAT, GL_FALSE,
                                      0, NULL);
                glEnableVertexAttribArray(s->position_location);

                glBindBuffer(GL_ARRAY_BUFFER,
                             child.mesh->texture_coord_vbo);
                glVertexAttribPointer(s->texture_coord_location, 2, GL_FLOAT,
                                      GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(s->texture_coord_location);

                glDrawArrays(GL_TRIANGLES, 0, child.mesh->num_vertices);
                break;
            }
            case TEXTURE_TYPE_ATLAS_IMAGE: {
                struct mesh_texture_atlas_shader *s;
                struct texture_atlas_data atlas_data;

                s = &shader_store.mesh_texture_atlas_shader;
                atlas_data = child.texture.atlas_image;

                glUseProgram(s->program);
                glUniformMatrix4fv(s->mvp_mat_location, 1, GL_TRUE, mvp_mat.m);
                glUniform1i(s->texture_location, 0);
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, texture_store.texture_atlas.image);
                glUniform1f(s->opacity_location, child.opacity);
                glUniform3f(s->color_location, 0.0f, 0.0f, 0.0f);
                glUniform1f(s->color_pct_location, 0.0f);

                float px = ((float)atlas_data.x) / 2048.0f;
                float py = ((float)atlas_data.y) / 2048.0f;
                float sx = ((float)atlas_data.w) / 2048.0f;
                float sy = ((float)atlas_data.h) / 2048.0f;
                glUniform2f(s->vtc_p, px, py);
                glUniform2f(s->vtc_s, sx, sy);

                glBindBuffer(GL_ARRAY_BUFFER, child.mesh->position_vbo);
                glVertexAttribPointer(s->position_location, 3, GL_FLOAT, GL_FALSE,
                                      0, NULL);
                glEnableVertexAttribArray(s->position_location);

                glBindBuffer(GL_ARRAY_BUFFER,
                             child.mesh->texture_coord_vbo);
                glVertexAttribPointer(s->texture_coord_location, 2, GL_FLOAT,
                                      GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(s->texture_coord_location);

                glDrawArrays(GL_TRIANGLES, 0, child.mesh->num_vertices);
                break;
            }
            case TEXTURE_TYPE_SOLID_COLOR_ATLAS_IMAGE: {
                struct mesh_texture_atlas_shader *s;
                struct texture_atlas_data atlas_data;
				vec3 color;
				float color_pct;

                s = &shader_store.mesh_texture_atlas_shader;
                atlas_data = child.texture.solid_color_atlas_image.atlas_data;
				color = child.texture.solid_color_atlas_image.color;
				color_pct = child.texture.solid_color_atlas_image.color_pct;

                glUseProgram(s->program);
                glUniformMatrix4fv(s->mvp_mat_location, 1, GL_TRUE, mvp_mat.m);
                glUniform1i(s->texture_location, 0);
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, texture_store.texture_atlas.image);
                glUniform1f(s->opacity_location, child.opacity);
                glUniform3f(s->color_location, color.x, color.y, color.z);
                glUniform1f(s->color_pct_location, color_pct);

                float px = ((float)atlas_data.x) / 2048.0f;
                float py = ((float)atlas_data.y) / 2048.0f;
                float sx = ((float)atlas_data.w) / 2048.0f;
                float sy = ((float)atlas_data.h) / 2048.0f;
                glUniform2f(s->vtc_p, px, py);
                glUniform2f(s->vtc_s, sx, sy);

                glBindBuffer(GL_ARRAY_BUFFER, child.mesh->position_vbo);
                glVertexAttribPointer(s->position_location, 3, GL_FLOAT, GL_FALSE,
                                      0, NULL);
                glEnableVertexAttribArray(s->position_location);

                glBindBuffer(GL_ARRAY_BUFFER,
                             child.mesh->texture_coord_vbo);
                glVertexAttribPointer(s->texture_coord_location, 2, GL_FLOAT,
                                      GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(s->texture_coord_location);

                glDrawArrays(GL_TRIANGLES, 0, child.mesh->num_vertices);
                break;
            }
        }
    }
}

static int compare_mesh_instances(const void *p, const void *q) {
    const struct mesh_instance *a = *(const struct mesh_instance **)p;
    const struct mesh_instance *b = *(const struct mesh_instance **)q;

    if (a->always_on_top) {
        return 1;
    }
    if (a->always_on_bottom) {
        return -1;
    }
    if (b->always_on_top) {
        return -1;
    }
    if (b->always_on_bottom) {
        return 1;
    }

    if (a->position.x + a->position.y < b->position.x + b->position.y) {
        return 1;
    } else if (a->position.x + a->position.y > b->position.x + b->position.y) {
        return -1;
    } else {
        return 0;
    }
}

static void renderer_draw_fog_of_war(void) {
    {
        vec3 position = V3(0.5f * GAME_MAP_WIDTH, 0.5f * GAME_MAP_WIDTH, 0.1f);
        vec3 scale = V3(GAME_MAP_WIDTH, GAME_MAP_WIDTH, 1.0f);

        mat4 model_mat = mat4_multiply_n(3,
                                         renderer_state.isometric_mat,
                                         mat4_translation(position),
                                         mat4_scale(scale));
        mat4 mvp_mat = mat4_multiply_n(2,
                                       renderer_state.proj_mat,
                                       model_mat);

        struct fog_of_war_shader *s = &shader_store.fog_of_war_shader;
        struct mesh *mesh = &mesh_store.square_mesh;

        glUseProgram(s->program);
        glUniformMatrix4fv(s->mvp_mat_location, 1, GL_TRUE, mvp_mat.m);
        glUniform3f(s->color_location, 0.0f, 0.0f, 0.0f);
        glUniform1i(s->texture_location, 0);
        glActiveTexture(GL_TEXTURE0);

        glBindBuffer(GL_ARRAY_BUFFER, mesh->position_vbo);
        glVertexAttribPointer(s->position_location, 3,
                              GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(s->position_location);

        glBindBuffer(GL_ARRAY_BUFFER, mesh->texture_coord_vbo);
        glVertexAttribPointer(s->texture_coord_location, 2,
                              GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(s->texture_coord_location);

        glBindTexture(GL_TEXTURE_2D, renderer_state.revealed_fow_tex[1]);
        glDrawArrays(GL_TRIANGLES, 0, mesh->num_vertices);
    }

    {
        vec3 position = V3(0.5f * GAME_MAP_WIDTH, 0.5f * GAME_MAP_WIDTH, 0.2f);
        vec3 scale = V3(GAME_MAP_WIDTH, GAME_MAP_WIDTH, 1.0f);

        mat4 model_mat = mat4_multiply_n(3,
                                         renderer_state.isometric_mat,
                                         mat4_translation(position),
                                         mat4_scale(scale));
        mat4 mvp_mat = mat4_multiply_n(2,
                                       renderer_state.proj_mat,
                                       model_mat);

        struct fog_of_war_shader *s = &shader_store.fog_of_war_shader;
        struct mesh *mesh = &mesh_store.square_mesh;

        glUseProgram(s->program);
        glUniformMatrix4fv(s->mvp_mat_location, 1, GL_TRUE, mvp_mat.m);
        glUniform3f(s->color_location, 0.0f, 0.0f, 0.0f);
        glUniform1i(s->texture_location, 0);
        glActiveTexture(GL_TEXTURE0);

        glBindBuffer(GL_ARRAY_BUFFER, mesh->position_vbo);
        glVertexAttribPointer(s->position_location, 3,
                              GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(s->position_location);

        glBindBuffer(GL_ARRAY_BUFFER, mesh->texture_coord_vbo);
        glVertexAttribPointer(s->texture_coord_location, 2,
                              GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(s->texture_coord_location);

        glBindTexture(GL_TEXTURE_2D, renderer_state.explored_fow_tex[1]);
        glDrawArrays(GL_TRIANGLES, 0, mesh->num_vertices);
    }
}

void renderer_draw(void) {
    //
    // Set up some state
    //
    {
        float x, y, sx, sy;

        x = game_state.cam_pos.x;
        y = game_state.cam_pos.y;
        sx = game_state.cam_size;
        sy = 9.0f / 16.0f * game_state.cam_size;

        renderer_state.proj_mat = mat4_orthographic_projection(
            x - sx, x + sx,
            y - sy, y + sy,
            -1.0f, 1.0f);
    }

    //
    // Draw to the fog of war shader
    //
    {
        array_entity_id_t entity_ids;
        struct entity_id entity_id;
        int i;
        mat4 fog_proj_mat = mat4_orthographic_projection(
            0.0f, GAME_MAP_WIDTH,
            0.0f, GAME_MAP_WIDTH,
            -1.0f, 1.0f);

        array_init(&entity_ids);
        array_clear(&entity_ids);
        get_all_active_entities_with_type(&entity_ids, 0, NULL);

        glBindFramebuffer(GL_FRAMEBUFFER, renderer_state.revealed_fow_fb[0]);
        glViewport(0, 0, FOG_OF_WAR_SIZE, FOG_OF_WAR_SIZE);

        if (renderer_state.clear_revealed_fow) {
            glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            renderer_state.clear_revealed_fow = false;
        }

        array_foreach(&entity_ids, entity_id, i) {
            struct entity *entity = get_entity_from_id(entity_id);
            if (!entity) {
                continue;
            }

            if (entity->team_num == 1) {
                continue;
            }

            vec3 position = V3(entity->position.x,
                               entity->position.y, 0.0f);
            mat4 translation_mat = mat4_translation(position);
            vec3 scale = V3(entity->sight_radius,
                            entity->sight_radius, 1.0f);
            mat4 scale_mat = mat4_scale(scale);
            mat4 model_mat = mat4_multiply_n(2,
                                             translation_mat,
                                             scale_mat);
            mat4 mvp_mat = mat4_multiply_n(2,
                                           fog_proj_mat,
                                           model_mat);

            struct mesh_shader *s = &shader_store.mesh_shader;
            struct mesh *mesh = &mesh_store.circle_mesh;

            glUseProgram(s->program);
            glUniformMatrix4fv(s->mvp_mat_location, 1, GL_TRUE, mvp_mat.m);
            glUniform1f(s->opacity_location, 1.0f);
            glUniform1f(s->color_pct_location, 1.0f);
            glUniform1f(s->tex_pct_location, 0.0f);
            glUniform3f(s->color_location, 1.0f, 1.0f, 1.0f);

            glBindBuffer(GL_ARRAY_BUFFER, mesh->position_vbo);
            glVertexAttribPointer(s->position_location, 3,
                                  GL_FLOAT, GL_FALSE, 0, NULL);
            glEnableVertexAttribArray(s->position_location);

            glBindBuffer(GL_ARRAY_BUFFER, mesh->texture_coord_vbo);
            glVertexAttribPointer(s->texture_coord_location, 2,
                                  GL_FLOAT, GL_FALSE, 0, NULL);
            glEnableVertexAttribArray(s->texture_coord_location);

            glDrawArrays(GL_TRIANGLES, 0, mesh->num_vertices);
        }

        glBindFramebuffer(GL_FRAMEBUFFER, renderer_state.explored_fow_fb[0]);
        glViewport(0, 0, FOG_OF_WAR_SIZE, FOG_OF_WAR_SIZE);

        if (renderer_state.clear_explored_fow) {
            glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
            glClear(GL_COLOR_BUFFER_BIT);
            //renderer_state.clear_explored_fow = false;
        }

        array_foreach(&entity_ids, entity_id, i) {
            struct entity *entity = get_entity_from_id(entity_id);
            if (!entity) {
                continue;
            }

            if (entity->team_num == 1) {
                continue;
            }

            vec3 position = V3(entity->position.x,
                               entity->position.y, 0.0f);
            mat4 translation_mat = mat4_translation(position);
            vec3 scale = V3(entity->sight_radius,
                            entity->sight_radius, 1.0f);
            mat4 scale_mat = mat4_scale(scale);
            mat4 model_mat = mat4_multiply_n(2,
                                             translation_mat,
                                             scale_mat);
            mat4 mvp_mat = mat4_multiply_n(2,
                                           fog_proj_mat,
                                           model_mat);

            struct mesh_shader *s = &shader_store.mesh_shader;
            struct mesh *mesh = &mesh_store.circle_mesh;

            glUseProgram(s->program);
            glUniformMatrix4fv(s->mvp_mat_location, 1, GL_TRUE, mvp_mat.m);
            glUniform1f(s->opacity_location, 1.0f);
            glUniform1f(s->color_pct_location, 1.0f);
            glUniform1f(s->tex_pct_location, 0.0f);
            glUniform3f(s->color_location, 1.0f, 1.0f, 1.0f);

            glBindBuffer(GL_ARRAY_BUFFER, mesh->position_vbo);
            glVertexAttribPointer(s->position_location, 3,
                                  GL_FLOAT, GL_FALSE, 0, NULL);
            glEnableVertexAttribArray(s->position_location);

            glBindBuffer(GL_ARRAY_BUFFER, mesh->texture_coord_vbo);
            glVertexAttribPointer(s->texture_coord_location, 2,
                                  GL_FLOAT, GL_FALSE, 0, NULL);
            glEnableVertexAttribArray(s->texture_coord_location);

            glDrawArrays(GL_TRIANGLES, 0, mesh->num_vertices);
        }

        array_deinit(&entity_ids);
        renderer_gaussian_blur(renderer_state.explored_fow_fb, renderer_state.explored_fow_tex);
        renderer_gaussian_blur(renderer_state.revealed_fow_fb, renderer_state.revealed_fow_tex);
    }

    //
    // Draw the actual game to the screen
    //
    {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0, 0, renderer_state.window_width, renderer_state.window_height);
    }

    renderer_terrain_draw();

    {
        int i;
        struct mesh_instance *mesh_instance;

        array_clear(&_renderer_state.mesh_instances_array);
        get_all_active_mesh_instances(&_renderer_state.mesh_instances_array);
        array_sort(&_renderer_state.mesh_instances_array, compare_mesh_instances);
        array_foreach(&_renderer_state.mesh_instances_array, mesh_instance, i) {
            mesh_instance->position.z = 0.1f * (i / 10000.0f);
            draw_mesh_instance(mesh_instance);
        }
    }

    renderer_draw_fog_of_war();

//
// Debug draw map
//
#if 0
    {
        int row, col;

        for (row = 0; row < GAME_MAP_SIZE; row++) {
            for (col = 0; col < GAME_MAP_SIZE; col++) {
                if (!game_state.map_tile_occupied[GAME_MAP_SIZE * col + row]) {
                    continue;
                }

                float x, y;
                struct mesh_instance mesh_instance;
                struct mesh_instance_child child;

                x = GAME_TILE_WIDTH * (col + 0.5f);
                y = GAME_TILE_WIDTH * (row + 0.5f);

                mesh_instance.position = V3(x, y, 0.9f);
                mesh_instance.opacity = 0.2f;
                mesh_instance.rotation = 0.0f;
                mesh_instance.scale = V2(GAME_TILE_WIDTH, GAME_TILE_WIDTH);
                array_init(&mesh_instance.children);

                child.name = "collider";
                child.is_visible = true;
                child.texture = create_color_texture(V3(1.0f, 0.0f, 0.0f));
                child.position = V3(0.0f, 0.0f, 0.0f);
                child.scale = V2(1.0f, 1.0f);
                child.opacity = 0.2f;
                child.rotation = 0.0f;
                child.already_isometric = false;
                child.mesh = &mesh_store.square_mesh;

                array_push(&mesh_instance.children, child);
                draw_mesh_instance(&mesh_instance);

                array_deinit(&mesh_instance.children);
            }
        }
    }
#endif

//
// Debug draw bfs
//
#if 0
    {
        int row, col;

        for (row = 0; row < GAME_MAP_SIZE; row++) {
            for (col = 0; col < GAME_MAP_SIZE; col++) {
                float x, y;
                int dist;
                mat4 mvp_mat;
                char string[64];

                x = (col + 0.5f) / GAME_MAP_SIZE * GAME_MAP_WIDTH;
                y = (row + 0.5f) / GAME_MAP_SIZE * GAME_MAP_WIDTH;

                dist = game_state.game_bfs_dist[GAME_MAP_SIZE * col + row];
                if (dist == INT_MAX) {
                    dist = -1;
                }
                sprintf(string, "%d", dist);

                mvp_mat = mat4_multiply_n(4,
                                          renderer_state.proj_mat,
                                          renderer_state.isometric_mat,
                                          mat4_translation(V3(x, y, 0.91f)),
                                          mat4_scale(V3(0.002f, 0.002f, 0.002f)));

                font_draw_string(string, mvp_mat,
                                 V4(1.0f, 1.0f, 0.0f, 1.0f),
                                 FONT_JUSTIFY_HORIZONTAL_CENTER,
                                 FONT_JUSTIFY_VERTICAL_CENTER);
            }
        }
    }
#endif

#if 0
    //
    // Debug draw map lines
    //
    {
        int row, col;

        for (row = 0; row < GAME_MAP_SIZE; row++) {
            float x, y;
            struct mesh_instance mesh_instance;
            struct mesh_instance_child child;

            x = GAME_TILE_WIDTH * row;
            y = 0.5f * GAME_MAP_WIDTH;

            mesh_instance.position = V3(x, y, 0.91f);
            mesh_instance.opacity = 0.2f;
            mesh_instance.rotation = 0.0f;
            mesh_instance.scale = V2(0.01f, GAME_MAP_WIDTH);
            array_init(&mesh_instance.children);

            child.name = "collider";
            child.is_visible = true;
            child.texture = create_color_texture(V3(1.0f, 1.0f, 1.0f));
            child.position = V3(0.0f, 0.0f, 0.0f);
            child.scale = V2(1.0f, 1.0f);
            child.opacity = 0.2f;
            child.rotation = 0.0f;
            child.already_isometric = false;
            child.mesh = &mesh_store.square_mesh;

            array_push(&mesh_instance.children, child);
            draw_mesh_instance(&mesh_instance);

            array_deinit(&mesh_instance.children);
        }

        for (col = 0; col < GAME_MAP_SIZE; col++) {
            float x, y;
            struct mesh_instance mesh_instance;
            struct mesh_instance_child child;

            x = 0.5f * GAME_MAP_WIDTH;
            y = GAME_TILE_WIDTH * col;

            mesh_instance.position = V3(x, y, 0.91f);
            mesh_instance.opacity = 0.2f;
            mesh_instance.rotation = 0.0f;
            mesh_instance.scale = V2(GAME_MAP_WIDTH, 0.01f);
            array_init(&mesh_instance.children);

            child.name = "collider";
            child.is_visible = true;
            child.texture = create_color_texture(V3(1.0f, 1.0f, 1.0f));
            child.position = V3(0.0f, 0.0f, 0.0f);
            child.scale = V2(1.0f, 1.0f);
            child.opacity = 0.2f;
            child.rotation = 0.0f;
            child.already_isometric = false;
            child.mesh = &mesh_store.square_mesh;

            array_push(&mesh_instance.children, child);
            draw_mesh_instance(&mesh_instance);

            array_deinit(&mesh_instance.children);
        }
    }
#endif
}
