#ifndef _GAME_EDITOR_H
#define _GAME_EDITOR_H

#include <stdbool.h>

#include "IGL.h"

struct game_editor_state {
	bool is_editor_hovered;

	struct {
		int draw_selection_polygon;
		int draw_collider;
		int draw_attack_radius;
		int draw_sight_radius;
	} entity_editor;

	struct {
		int draw_tile_borders;
		int draw_tile_sight_visibility;
		int draw_tile_bfs_dist;
	} map_editor;
} game_editor_state;

void game_editor_init(struct GLFWwindow *window);
void game_editor_update(float dt);
void game_editor_draw(void);

#endif
