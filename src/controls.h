#ifndef _CONTROLS_CONTROLS_H
#define _CONTROLS_CONTROLS_H

#include <stdbool.h>

#include "IGL.h"
#include "maths.h"

struct controls_system_state {
    bool key_down[GLFW_KEY_LAST];
    bool key_clicked[GLFW_KEY_LAST];

    bool left_down, left_clicked;
    bool right_down, right_clicked;
    bool up_down, up_clicked;
    bool down_down, down_clicked;

    bool left_mouse_down, left_mouse_clicked;
    vec2 left_mouse_pos, left_mouse_down_pos;
    vec2 mouse_delta;

    bool right_mouse_down, right_mouse_clicked;
    vec2 right_mouse_down_pos;

    bool middle_mouse_down, middle_mouse_clicked;
    float scroll_delta;
} controls_state;

void controls_init(GLFWwindow *window);
void controls_update(GLFWwindow *window);

#endif
