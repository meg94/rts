#ifndef _RENDERER_MESH_H
#define _RENDERER_MESH_H

#include "IGL.h"
#include "maths.h"
#include "texture.h"
#include "array.h"

typedef array_t(struct mesh_instance) array_mesh_instance_t;
typedef array_t(struct mesh_instance *) array_mesh_instance_ptr_t;
typedef array_t(struct mesh_instance_child) array_mesh_instance_child_t;
typedef array_t(struct mesh_instance_child *) array_mesh_instance_child_ptr_t;

struct mesh_instance_id {
    uint32_t gen, idx;
};

struct mesh_instance {
    vec3 position;
    vec2 scale;
    float opacity;
    float rotation;
	bool is_visible;
    bool always_on_top;
    bool always_on_bottom;
    array_mesh_instance_child_t children;
};

struct mesh_instance_child {
    const char *name;
    bool is_visible;
    struct mesh *mesh;
    struct texture texture;
    vec3 position;
    vec2 scale;
    float opacity;
    float rotation;
    bool already_isometric;
    bool flip_x;
};

struct mesh {
    int num_vertices;
    GLuint position_vbo;
    GLuint texture_coord_vbo;
};

struct mesh_store {
    struct mesh square_mesh;
    struct mesh circle_mesh;
} mesh_store;

void mesh_instance_manager_init(void);
struct mesh_instance_id allocate_mesh_instance(void);
void delete_mesh_instance(struct mesh_instance_id id);
struct mesh_instance *get_mesh_instance_from_id(struct mesh_instance_id id);
void get_all_active_mesh_instances(array_mesh_instance_ptr_t *mesh_instances);

void mesh_store_init(void);
struct mesh_instance_child create_mesh_instance_child(
    const char *name,
    struct mesh *mesh,
    struct texture texture);
struct mesh_instance_child *get_mesh_instance_child(
    const char *name,
    struct mesh_instance *mesh_instance);

#endif
