#ifndef _GAME_ENTITY_H
#define _GAME_ENTITY_H

#include <stdbool.h>

#include "array.h"
#include "collider.h"
#include "maths.h"
#include "mesh.h"

typedef array_t(struct entity) array_entity_t;
typedef array_t(struct entity *) array_entity_ptr_t;
typedef array_t(enum entity_type) array_entity_type_t;
typedef array_t(enum unit_type) array_unit_type_t;
typedef array_t(struct entity_id) array_entity_id_t;

enum entity_type {
    ENTITY_TYPE_COMMAND_CENTER,
    ENTITY_TYPE_BARRACKS,
    ENTITY_TYPE_FACTORY,
    ENTITY_TYPE_HELIPORT,
    ENTITY_TYPE_WORKER,
    ENTITY_TYPE_SOLDIER,
    ENTITY_TYPE_MEDIC,
    ENTITY_TYPE_SOLDIER_PROJECTILE,
    ENTITY_TYPE_TANK,
    ENTITY_TYPE_TANK_PROJECTILE,
    ENTITY_TYPE_VEHICLE,
    ENTITY_TYPE_HELICOPTER,
    ENTITY_TYPE_HELICOPTER_PROJECTILE,
    ENTITY_TYPE_EXPLOSION,
    ENTITY_TYPE_PLACEHOLDER,
    ENTITY_TYPE_TERRAIN_BORDER,
    ENTITY_TYPE_TURRET,
    ENTITY_TYPE_SUPPLY_DEPOT,
    ENTITY_TYPE_REFINERY,
    ENTITY_TYPE_BUILDING_CONSTRUCTION_LARGE,
    ENTITY_TYPE_BUILDING_CONSTRUCTION_SMALL,
    NUM_OF_ENTITY_TYPES,
};
const char *entity_type_name[NUM_OF_ENTITY_TYPES];
int entity_cost[NUM_OF_ENTITY_TYPES];

enum action_type {
    ACTION_TYPE_NONE,
    ACTION_TYPE_NORMAL_MOVE,
    ACTION_TYPE_ATTACK_MOVE,
    ACTION_TYPE_PATROL_MOVE,
};

struct action {
    enum action_type type;

    union {
        vec2 target;
    };
};

enum worker_state {
    WORKER_STATE_IDLE,
    WORKER_STATE_MOVING,
    WORKER_STATE_MOVING_TO_REFINERY,
    WORKER_STATE_IN_REFINERY,
    WORKER_STATE_MOVING_TO_CONSTRUCTION,
    WORKER_STATE_CONSTRUCTING,
};

enum soldier_state {
    SOLDIER_STATE_IDLE,
    SOLDIER_STATE_ATTACKING_ENEMY,
    SOLDIER_STATE_MOVING,
};

enum medic_state {
    MEDIC_STATE_IDLE,
    MEDIC_STATE_HEALING,
    MEDIC_STATE_MOVING,
};

enum tank_state {
    TANK_STATE_IDLE,
    TANK_STATE_ATTACKING,
    TANK_STATE_MOVING,
};

enum vehicle_state {
    VEHICLE_STATE_IDLE,
    VEHICLE_STATE_ATTACKING,
    VEHICLE_STATE_MOVING,
};

enum helicopter_state {
    HELICOPTER_STATE_IDLE,
    HELICOPTER_STATE_ATTACKING,
    HELICOPTER_STATE_MOVING,
};

enum movement_type {
    MOVEMENT_TYPE_MOVING,
    MOVEMENT_TYPE_ATTACK_MOVING,
    MOVEMENT_TYPE_PATROLLING,
};

struct entity_id {
    uint32_t gen, idx;
};

struct attack_component {
    struct entity_id closest_enemy_id;
    float time_since_attack;
	float attack_radius;
};

struct healing_component {
    struct entity_id healing_entity_id;
    float time_since_healing;
	float healing_radius;
};

struct movement_component {
    enum movement_type type;
    bool moving_to_target;
    int path_index;
    array_vec2_t path;
};

struct animation_component {
    float time;
};

struct animated_mesh_movement_component {
    float time;
    int num_textures;
    char base_texture_name[256];
    struct mesh_instance_child *body;
};

struct unit_construction_component {
	int team_num;
    vec2 rally_point;
    float spawn_time_left, spawn_time;
    array_entity_type_t spawn_list;
};

struct animated_mesh_component {
    float t;
    int idx;
    struct texture textures[10];
    int num_textures;
    struct mesh_instance_child *mesh;

    bool play_once;
    bool is_playing;
};

struct building_construction_component {
    int building_row, building_col;
    enum entity_type building_type;
    struct entity_id building_id, placeholder_id;
};

struct mining_component {
    struct entity_id refinery_id;
};

enum component_type {
    COMPONENT_TYPE_ATTACK,
    COMPONENT_TYPE_MOVEMENT,
    COMPONENT_TYPE_ANIMATION,
    COMPONENT_TYPE_ANIMATED_MESH,
    COMPONENT_TYPE_ANIMATED_MESH_MOVEMENT,
    COMPONENT_TYPE_HEALING,
    COMPONENT_TYPE_BUILDING_CONSTRUCTION,
    COMPONENT_TYPE_MINING,
	COMPONENT_TYPE_UNIT_CONSTRUCTION,
};

struct component {
    enum component_type type;
    union {
        struct attack_component attack;
        struct movement_component movement;
        struct animation_component animation;
        struct animated_mesh_component animated_mesh;
        struct animated_mesh_movement_component animated_mesh_movement;
        struct healing_component healing;
        struct building_construction_component building_construction;
        struct mining_component mining;
        struct unit_construction_component unit_construction;
    };
};

typedef map_t(struct component) map_component_t;

struct entity {
    struct entity_id id;
    enum entity_type type;

    int team_num;
    bool is_selectable;
    bool is_selected;
    bool is_hovered;
	bool is_visible;
    vec2 position;
    vec2 velocity;
    struct mesh_instance_id mesh_instance_id;
    struct collider_id collider_id;
    float minimap_width;
    float sight_radius;
    float start_health, health;

    struct action current_action;
    map_component_t components;

    union {
        struct {
            enum worker_state state;
        } worker;

        struct {
            enum soldier_state state_type;
        } soldier;

        struct {
            enum medic_state state_type;
        } medic;

        struct {
            float time_in_air;
            struct entity_id target_id;
        } soldier_projectile;

        struct {
            enum tank_state state_type;
        } tank;

        struct {
            float time_in_air;
            vec2 target_pos;
        } tank_projectile;

        struct {
            enum vehicle_state state_type;
        } vehicle;

        struct {
            enum helicopter_state state_type;
        } helicopter;

        struct {
            float time_in_air;
            vec2 target_pos;
        } helicopter_projectile;

        struct {
            float t;
        } explosion;

        struct {
            float t;
            array_entity_id_t worker_id_array;
            struct animated_mesh_component hovered_animated_mesh_component;
            struct animated_mesh_component body_animated_mesh_component;
        } refinery;

        struct {
            enum entity_type type;
            int row, col;
            bool can_be_placed;
        } placeholder;

        struct {
            bool created_building;
			bool is_small;
            int row, col;
            enum entity_type type;
            float t;
            struct animated_mesh_component hovered_animated_mesh_component;
        } building_construction;
    };
};

void *get_entity_component_of_type(struct entity *entity, enum component_type type, const char *name);

void remove_from_unit_construction_component_spawn_list(struct unit_construction_component *component, int i);
void push_to_unit_construction_component_spawn_list(struct unit_construction_component *component, enum entity_type type);

void entity_manager_init(void);
struct entity_id allocate_entity(void);
void delete_entity(struct entity_id id);
struct entity *get_entity_from_id(struct entity_id id);
void get_all_active_entities_with_type(array_entity_id_t *entities,
                                       int num_types, enum entity_type *types);

void do_entity_interaction(struct entity *entity, struct entity *interaction_entity);
void set_entity_target(struct entity *entity, vec2 target, enum movement_type movement_type);
void set_entity_is_selected(struct entity_id entity_id, bool is_selected);
void set_entity_is_hovered(struct entity_id entity_id, bool is_hovered);

struct entity_id create_command_center_entity(int row, int col, int team_num);
void update_command_center_entity(struct entity *command_center, float dt);

struct entity_id create_barracks_entity(int row, int col, int team_num);
void update_barracks_entity(struct entity *barracks, float dt);
void push_to_barracks_spawn_list(struct entity *barracks, enum entity_type type);
void remove_from_barracks_spawn_list(struct entity *barracks, int i);

struct entity_id create_factory_entity(int row, int col, int team_num);
void update_factory_entity(struct entity *factory, float dt);
void push_to_factory_spawn_list(struct entity *factory, enum entity_type type);
void remove_from_factory_spawn_list(struct entity *factory, int i);

struct entity_id create_heliport_entity(int row, int col);
void update_heliport_entity(struct entity *heliport, float dt);

struct entity_id create_worker_entity(float row, float col, int team_num);
void update_worker_entity(struct entity *worker, float dt);
void set_worker_target(struct entity *worker, vec2 target);
void set_worker_build_target(struct entity *worker, enum entity_type building_type,
                             int row, int col, struct entity_id placeholder_id);
void move_worker_to_refinery(struct entity *worker, struct entity *refinery);

struct entity_id create_medic_entity(float row, float col, int team_num);
void update_medic_entity(struct entity *medic, float dt);
void set_medic_target(struct entity *medic, vec2 target, enum movement_type movement_type);

struct entity_id create_soldier_entity(float row, float col, int team_num);
void update_soldier_entity(struct entity *soldier, float dt);
void set_soldier_target(struct entity *soldier, vec2 target, enum movement_type movement_type);

struct entity_id create_soldier_projectile_entity(vec2 start_pos,
                                                  struct entity_id target_id);
void update_soldier_projectile_entity(struct entity *projectile, float dt);

struct entity_id create_tank_entity(float row, float col, int team_num);
void update_tank_entity(struct entity *tank, float dt);
void set_tank_target(struct entity *tank, vec2 target, enum movement_type movement_type);

struct entity_id create_vehicle_entity(float row, float col, int team_num);
void update_vehicle_entity(struct entity *vehicle, float dt);
void set_vehicle_target(struct entity *vehicle, vec2 target, enum movement_type movement_type);

struct entity_id create_tank_projectile_entity(vec2 start_pos, vec2 target_pos);
void update_tank_projectile_entity(struct entity *projectile, float dt);

struct entity_id create_helicopter_entity(float row, float col, int team_num);
void update_helicopter_entity(struct entity *helicopter, float dt);
void set_helicopter_target(struct entity *helicopter, vec2 target, enum movement_type movement_type);

struct entity_id create_helicopter_projectile_entity(vec2 start_pos,
                                                     vec2 target_pos);
void update_helicopter_projectile_entity(struct entity *projectile, float dt);

struct entity_id create_explosion_entity(vec2 position);
void update_explosion_entity(struct entity *explosion, float dt);

struct entity_id create_placeholder_entity(enum entity_type type);
void update_placeholder_entity(struct entity *placeholder, float dt);
void set_placeholder_row_col(struct entity *placeholder, int row, int col);

struct entity_id create_terrain_border(int col0, int row0, int col1, int row1);

struct entity_id create_turret_entity(int row, int col, int team_num);

struct entity_id create_supply_depot_entity(int row, int col);

struct entity_id create_refinery_entity(int row, int col);
void update_refinery_entity(struct entity *refinery, float dt);
void add_worker_to_refinery(struct entity *refinery, struct entity *worker);
void remove_worker_from_refinery(struct entity *refinery, int worker_idx);

struct entity_id create_building_construction_entity(enum entity_type type, int row, int col, bool is_small);
void update_building_construction_entity(struct entity *building_construction, float dt);
void cancel_building_construction_entity(struct entity *building_construction);

#endif
