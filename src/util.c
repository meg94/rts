#include "util.h"

#include <assert.h>

char *util_read_file(const char *filename, int *file_length, const char *mode) {
    FILE *file;
    char *file_string;

    file = util_fopen(filename, mode);
    if (!file) {
        printf("read_file: Unable to open file %s\n", filename);
        return NULL;
    }

    fseek(file, 0, SEEK_END);
    *file_length = ftell(file);
    fseek(file, 0, SEEK_SET);
    file_string = (char *)malloc(*file_length + 1);
    int ret = fread(file_string, sizeof(char), *file_length, file);
    if (ret == -1) {
        printf("Can't read from file: %s\n", filename);
        return NULL;
    }
    file_string[*file_length] = 0;
    fclose(file);

    return file_string;
}

#define MAX_FILENAME 256

FILE *util_fopen(const char *filename, const char *args) {
    assert(strlen(filename) + strlen("assets/") < MAX_FILENAME);
    char full_filename[MAX_FILENAME + 1];
    sprintf_s(full_filename, MAX_FILENAME, "assets/%s", filename);

    FILE *f;
    fopen_s(&f, full_filename, args);
    return f;
}

#if defined(ANDROID_BUILD)
char *util_read_file(const char *filename, int *file_length, const char *mode) {
    AAsset *asset = AAssetManager_open(asset_manager, filename, AASSET_MODE_BUFFER);
    char *buffer = AAsset_getBuffer(asset);
    *file_length = AAsset_getLength(asset);
    return buffer;
}

time_t util_get_file_modification_time(const char *filename) {
    return 0;
}

static int android_read(void *cookie, char *buf, int size) {
    return AAsset_read((AAsset *)cookie, buf, size);
}

static int android_write(void *cookie, const char *buf, int size) {
    return EACCES;  // can't provide write access to the apk
}

static fpos_t android_seek(void *cookie, fpos_t offset, int whence) {
    return AAsset_seek((AAsset *)cookie, offset, whence);
}

static int android_close(void *cookie) {
    AAsset_close((AAsset *)cookie);
    return 0;
}

FILE *util_fopen(const char *fname, const char *mode) {
    if (mode[0] == 'w') return NULL;

    AAsset *asset = AAssetManager_open(asset_manager, fname, 0);
    if (!asset) return NULL;

    FILE *fp = funopen(asset, android_read, android_write, android_seek, android_close);
    return fp;
}
#endif
