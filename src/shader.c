#include "shader.h"

#include <stdio.h>
#include <string.h>
#include <assert.h>

static void shader_check_for_shader_compile_errors(GLuint program, const char *name) {
    int params = -1;
    glGetShaderiv(program, GL_COMPILE_STATUS, &params);
    if (params != GL_TRUE) {
        char log[2048];
        int length;
        glGetShaderInfoLog(program, 2048, &length, log);

        printf("Could not compile shader: %s\n", name);
        printf("Shader log:\n%s\n", log);
    }
}

#define MAX_SHADER_LEN 2048

static GLuint load_shader(GLuint type, const char *shader_code) {
    GLuint shader = glCreateShader(type);

    static const char *shader_version_string = "#version 300 es\n";
    int shader_version_string_len = strlen(shader_version_string);

    char shader_code_with_version[MAX_SHADER_LEN];

    strcpy_s(shader_code_with_version, MAX_SHADER_LEN, shader_version_string);
    strcpy_s(shader_code_with_version + shader_version_string_len, MAX_SHADER_LEN - shader_version_string_len, shader_code);

    const char *stupid_const = (const char *)shader_code_with_version;
    glShaderSource(shader, 1, &stupid_const, NULL);
    glCompileShader(shader);

    return shader;
}

GLint shader_create_program(const char *vertex_shader_code, const char *fragment_shader_code) {
    GLint vertex_shader = load_shader(GL_VERTEX_SHADER, vertex_shader_code);
    shader_check_for_shader_compile_errors(vertex_shader, "Vertex Shader");

    GLint fragment_shader = load_shader(GL_FRAGMENT_SHADER, fragment_shader_code);
    shader_check_for_shader_compile_errors(fragment_shader, "Fragment Shader");

    GLint program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);

    GLint params = -1;
    glGetProgramiv(program, GL_LINK_STATUS, &params);
    if (params != GL_TRUE) {
        char log[2048];
        int length;
        glGetProgramInfoLog(program, 2048, &length, log);

        printf("Could not link shader program.\n");
        printf("Program log:\n%s\n", log);
    }

    return program;
}

void shader_store_init(void) {
    {
        static const char *vertex_shader =
            "in vec3 vertex_position;"
            "in vec2 vertex_texture_coord;"
            "out vec2 fragment_texture_coord;"
            "uniform mat4 mvp_mat;"

            "void main() {"
            "   fragment_texture_coord = vertex_texture_coord;"
            "   gl_Position = mvp_mat * vec4(vertex_position, 1.0);"
            "}";

        static const char *fragment_shader =
            "precision highp float;"
            "in vec2 fragment_texture_coord;"
            "out vec4 g_fragment_color;"

            "uniform float color_pct;"
            "uniform vec3 color;"
            "uniform float tex_pct;"
            "uniform sampler2D tex;"
            "uniform float opacity;"

            "void main() {"
            "   g_fragment_color.xyz = tex_pct * texture(tex, fragment_texture_coord).xyz;"
            "   g_fragment_color.xyz += color_pct * color;"
            "   g_fragment_color.w = opacity;"
            "}";

        GLint program = shader_create_program(vertex_shader, fragment_shader);
        GLint mvp_mat_loc = glGetUniformLocation(program, "mvp_mat");
        GLint tex_pct_loc = glGetUniformLocation(program, "tex_pct");
        GLint color_pct_loc = glGetUniformLocation(program, "color_pct");
        GLint color_loc = glGetUniformLocation(program, "color");
        GLint texture_loc = glGetUniformLocation(program, "tex");
        GLint opacity_loc = glGetUniformLocation(program, "opacity");
        GLint position_loc = glGetAttribLocation(program, "vertex_position");
        GLint texture_coord_loc = glGetAttribLocation(program, "vertex_texture_coord");

        shader_store.mesh_shader.program = (GLuint)program;
        shader_store.mesh_shader.mvp_mat_location = (GLuint)mvp_mat_loc;
        shader_store.mesh_shader.tex_pct_location = (GLuint)tex_pct_loc;
        shader_store.mesh_shader.color_pct_location = (GLuint)color_pct_loc;
        shader_store.mesh_shader.color_location = (GLuint)color_loc;
        shader_store.mesh_shader.texture_location = (GLuint)texture_loc;
        shader_store.mesh_shader.opacity_location = (GLuint)opacity_loc;
        shader_store.mesh_shader.position_location = (GLuint)position_loc;
        shader_store.mesh_shader.texture_coord_location = (GLuint)texture_coord_loc;
    }

    {
        static const char *vertex_shader =
            "in vec3 vertex_position;"
            "in vec2 vertex_texture_coord;"
            "out vec2 fragment_texture_coord;"
            "uniform mat4 mvp_mat;"

            "void main() {"
            "   fragment_texture_coord = vertex_texture_coord;"
            "   gl_Position = mvp_mat * vec4(vertex_position, 1.0);"
            "}";

        static const char *fragment_shader =
            "precision highp float;"
            "in vec2 fragment_texture_coord;"
            "out vec4 g_fragment_color;"

            "uniform sampler2D tex;"
            "uniform float opacity;"

            "void main() {"
            "   g_fragment_color = texture(tex, fragment_texture_coord);"
            "   g_fragment_color.w *= opacity;"
            "   if (g_fragment_color.w < 0.01) {"
            "     discard;"
            "   }"
            "}";

        GLint program = shader_create_program(vertex_shader, fragment_shader);
        GLint mvp_mat_loc = glGetUniformLocation(program, "mvp_mat");
        GLint texture_loc = glGetUniformLocation(program, "tex");
        GLint opacity_loc = glGetUniformLocation(program, "opacity");
        GLint position_loc = glGetAttribLocation(program, "vertex_position");
        GLint texture_coord_loc = glGetAttribLocation(program, "vertex_texture_coord");

        shader_store.mesh_texture_shader.program = (GLuint)program;
        shader_store.mesh_texture_shader.mvp_mat_location = (GLuint)mvp_mat_loc;
        shader_store.mesh_texture_shader.texture_location = (GLuint)texture_loc;
        shader_store.mesh_texture_shader.opacity_location = (GLuint)opacity_loc;
        shader_store.mesh_texture_shader.position_location = (GLuint)position_loc;
        shader_store.mesh_texture_shader.texture_coord_location = (GLuint)texture_coord_loc;
    }

    {
        static const char *vertex_shader =
            "in vec3 vertex_position;"
            "in vec2 vertex_texture_coord;"
            "out vec2 fragment_texture_coord;"
            "uniform mat4 mvp_mat;"
            "uniform vec2 vtc_p;"
            "uniform vec2 vtc_s;"

            "void main() {"
            "   fragment_texture_coord.x = vtc_p.x + vtc_s.x * vertex_texture_coord.x;"
            "   fragment_texture_coord.y = vtc_p.y + vtc_s.y * vertex_texture_coord.y;"
            "   gl_Position = mvp_mat * vec4(vertex_position, 1.0);"
            "}";

        static const char *fragment_shader =
            "precision highp float;"
            "in vec2 fragment_texture_coord;"
            "out vec4 g_fragment_color;"

            "uniform sampler2D tex;"
            "uniform float opacity;"
            "uniform vec3 color;"
            "uniform float color_pct;"

            "void main() {"
			"	vec4 tex_value = texture(tex, fragment_texture_coord);"	
			"	g_fragment_color.xyz = (1.0 - color_pct) * tex_value.xyz + color_pct * color;"
            "   g_fragment_color.w = tex_value.w * opacity;"
            "   if (g_fragment_color.w < 0.01) {"
            "     discard;"
            "   }"
            "}";

        GLint program = shader_create_program(vertex_shader, fragment_shader);
        GLint mvp_mat_loc = glGetUniformLocation(program, "mvp_mat");
        GLint texture_loc = glGetUniformLocation(program, "tex");
        GLint opacity_loc = glGetUniformLocation(program, "opacity");
        GLint color_loc = glGetUniformLocation(program, "color");
        GLint color_pct_loc = glGetUniformLocation(program, "color_pct");
        GLint position_loc = glGetAttribLocation(program, "vertex_position");
        GLint texture_coord_loc = glGetAttribLocation(program, "vertex_texture_coord");
        GLint vtc_p_loc = glGetUniformLocation(program, "vtc_p");
        GLint vtc_s_loc = glGetUniformLocation(program, "vtc_s");

        shader_store.mesh_texture_atlas_shader.program = (GLuint)program;
        shader_store.mesh_texture_atlas_shader.mvp_mat_location = (GLuint)mvp_mat_loc;
        shader_store.mesh_texture_atlas_shader.texture_location = (GLuint)texture_loc;
        shader_store.mesh_texture_atlas_shader.opacity_location = (GLuint)opacity_loc;
        shader_store.mesh_texture_atlas_shader.color_location = (GLuint)color_loc;
        shader_store.mesh_texture_atlas_shader.color_pct_location = (GLuint)color_pct_loc;
        shader_store.mesh_texture_atlas_shader.position_location = (GLuint)position_loc;
        shader_store.mesh_texture_atlas_shader.texture_coord_location = (GLuint)texture_coord_loc;
        shader_store.mesh_texture_atlas_shader.vtc_p = (GLuint)vtc_p_loc;
        shader_store.mesh_texture_atlas_shader.vtc_s = (GLuint)vtc_s_loc;
    }

    {
        static const char *vertex_shader =
            "in vec3 vertex_position;"
            "in vec2 vertex_texture_coord;"
            "out vec2 fragment_texture_coord;"
            "uniform mat4 mvp_mat;"

            "void main() {"
            "   fragment_texture_coord = vertex_texture_coord;"
            "   gl_Position = mvp_mat * vec4(vertex_position, 1.0);"
            "}";

        static const char *fragment_shader =
            "precision highp float;"
            "in vec2 fragment_texture_coord;"
            "out vec4 g_fragment_color;"

            "uniform vec3 color;"
            "uniform sampler2D tex;"

            "void main() {"
            "   float a = texture(tex, fragment_texture_coord).x;"
            "   g_fragment_color.xyz = vec3(0.0, 0.0, 0.0);"
            "   g_fragment_color.w = (1.0 - a) * 0.5;"
            "}";

        GLint program = shader_create_program(vertex_shader, fragment_shader);
        GLint mvp_mat_loc = glGetUniformLocation(program, "mvp_mat");
        GLint color_loc = glGetUniformLocation(program, "color");
        GLint texture_loc = glGetUniformLocation(program, "tex");
        GLint position_loc = glGetAttribLocation(program, "vertex_position");
        GLint texture_coord_loc = glGetAttribLocation(program, "vertex_texture_coord");

        shader_store.fog_of_war_shader.program = (GLuint)program;
        shader_store.fog_of_war_shader.mvp_mat_location = (GLuint)mvp_mat_loc;
        shader_store.fog_of_war_shader.color_location = (GLuint)color_loc;
        shader_store.fog_of_war_shader.texture_location = (GLuint)texture_loc;
        shader_store.fog_of_war_shader.position_location = (GLuint)position_loc;
        shader_store.fog_of_war_shader.texture_coord_location = (GLuint)texture_coord_loc;
    }

    {
        static const char *vertex_shader =
            "in vec3 vertex_position;"
            "in vec2 vertex_texture_coord;"
            "out vec2 fragment_texture_coord;"
            "uniform mat4 mvp_mat;"

            "void main() {"
            "   fragment_texture_coord = vertex_texture_coord;"
            "   gl_Position = mvp_mat * vec4(vertex_position, 1.0);"
            "}";

        static const char *fragment_shader =
            "precision highp float;"
            "in vec2 fragment_texture_coord;"
            "out vec4 g_fragment_color;"

            "uniform sampler2D tex;"
            "uniform vec2 tex_coord_top_left;"
            "uniform vec2 tex_coord_width;"

            "void main() {"
            "   vec2 tex_coord = tex_coord_top_left + tex_coord_width * fragment_texture_coord;"
            "   g_fragment_color = texture(tex, tex_coord);"
            "}";

        GLint program = shader_create_program(vertex_shader, fragment_shader);
        GLint mvp_mat_loc = glGetUniformLocation(program, "mvp_mat");
        GLint texture_loc = glGetUniformLocation(program, "tex");
        GLint tex_coord_top_left_loc = glGetUniformLocation(program, "tex_coord_top_left");
        GLint tex_coord_width_loc = glGetUniformLocation(program, "tex_coord_width");
        GLint position_loc = glGetAttribLocation(program, "vertex_position");
        GLint texture_coord_loc = glGetAttribLocation(program, "vertex_texture_coord");

        shader_store.map_tile_shader.program = (GLuint)program;
        shader_store.map_tile_shader.mvp_mat_location = (GLuint)mvp_mat_loc;
        shader_store.map_tile_shader.texture_location = (GLuint)texture_loc;
        shader_store.map_tile_shader.tex_coord_top_left_location = (GLuint)tex_coord_top_left_loc;
        shader_store.map_tile_shader.tex_coord_width_location = (GLuint)tex_coord_width_loc;
        shader_store.map_tile_shader.position_location = (GLuint)position_loc;
        shader_store.map_tile_shader.texture_coord_location = (GLuint)texture_coord_loc;
    }

    {
        static const char *vertex_shader =
            "in vec2 vertex_position;"
            "in vec3 vertex_color;"
            "out vec3 fragment_color;"
            "uniform mat4 mvp_mat;"

            "void main() {"
            "   gl_Position = mvp_mat * vec4(vertex_position, 1.0, 1.0);"
            "   fragment_color = vertex_color;"
            "}";

        static const char *fragment_shader =
            "precision highp float;"
            "in vec3 fragment_color;"
            "out vec4 g_fragment_color;"

            "void main() {"
            "   g_fragment_color = vec4(fragment_color, 1.0);"
            "}";

        GLint program = shader_create_program(vertex_shader, fragment_shader);
        GLint mvp_mat_loc = glGetUniformLocation(program, "mvp_mat");
        GLint position_loc = glGetAttribLocation(program, "vertex_position");
        GLint color_loc = glGetAttribLocation(program, "vertex_color");

        shader_store.minimap_shader.program = (GLuint)program;
        shader_store.minimap_shader.mvp_mat_location = (GLuint)mvp_mat_loc;
        shader_store.minimap_shader.position_location = (GLuint)position_loc;
        shader_store.minimap_shader.color_location = (GLuint)color_loc;
    }

    {
        static const char *vertex_shader =
            "in vec2 vertex_position;"
            "in vec2 vertex_texture_coord;"
            "out vec2 fragment_texture_coord;"
            "uniform mat4 mvp_mat;"

            "void main() {"
            "   gl_Position = mvp_mat * vec4(vertex_position, 1.0, 1.0);"
            "   fragment_texture_coord = vertex_texture_coord;"
            "}";

        static const char *fragment_shader =
            "precision highp float;"
            "in vec2 fragment_texture_coord;"
            "out uvec2 g_out;"
            "uniform uint idx;"
            "uniform uint gen;"
            "uniform sampler2D tex;"

            "void main() {"
            "   vec4 color = texture(tex, fragment_texture_coord);"
            "   if (color.w < 0.01) {"
            "     discard;"
            "   }"
            "   g_out = uvec2(idx, gen);"
            "}";

        GLint program = shader_create_program(vertex_shader, fragment_shader);
        GLint mvp_mat_loc = glGetUniformLocation(program, "mvp_mat");
        GLint idx_loc = glGetUniformLocation(program, "idx");
        GLint gen_loc = glGetUniformLocation(program, "gen");
        GLint texture_loc = glGetUniformLocation(program, "tex");
        GLint position_loc = glGetAttribLocation(program, "vertex_position");
        GLint texture_coord_loc = glGetAttribLocation(program, "vertex_texture_coord");

        shader_store.unit_body_shader.program = (GLuint)program;
        shader_store.unit_body_shader.mvp_mat_location = (GLuint)mvp_mat_loc;
        shader_store.unit_body_shader.idx_location = (GLuint)idx_loc;
        shader_store.unit_body_shader.gen_location = (GLuint)gen_loc;
        shader_store.unit_body_shader.texture_location = (GLuint)texture_loc;
        shader_store.unit_body_shader.position_location = (GLuint)position_loc;
        shader_store.unit_body_shader.texture_coord_location = (GLuint)texture_coord_loc;
    }

    {
        static const char *vertex_shader =
            "in vec2 vertex_position;"
            "uniform mat4 mvp_mat;"

            "void main() {"
            "   gl_Position = mvp_mat * vec4(vertex_position, 1.0, 1.0);"
            "}";

        static const char *fragment_shader =
            "precision highp float;"
            "uniform vec3 color;"
            "out vec4 g_color;"

            "void main() {"
            "   g_color = vec4(color, 1.0);"
            "}";

        GLint program = shader_create_program(vertex_shader, fragment_shader);
        GLint mvp_mat_loc = glGetUniformLocation(program, "mvp_mat");
        GLint color_loc = glGetUniformLocation(program, "color");
        GLint position_loc = glGetAttribLocation(program, "vertex_position");

        shader_store.debug_shader.program = (GLuint)program;
        shader_store.debug_shader.mvp_mat_location = (GLuint)mvp_mat_loc;
        shader_store.debug_shader.color_location = (GLuint)color_loc;
        shader_store.debug_shader.position_location = (GLuint)position_loc;
    }

    {
        static const char *vertex_shader = 
            "in vec2 vertex_position;"
            "in vec2 vertex_texture_coord;"
            "out vec2 fragment_texture_coord;"
            "uniform mat4 mvp_mat;"

            "void main() {"
            "   gl_Position = mvp_mat * vec4(vertex_position, 1.0, 1.0);"
            "   fragment_texture_coord = vertex_texture_coord;"
            "}";

        static const char *fragment_shader = 
            "precision highp float;"
            "in vec2 fragment_texture_coord;"
            "out vec4 g_color;"
            "uniform sampler2D tex;"
            "uniform bool horizontal;"

            "void main() {"
            "  const float weight[5] = float[5](0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216);"
            "  ivec2 tex_size = textureSize(tex, 0);"
            "  vec2 tex_offset = vec2(1.0 / float(tex_size.x), 1.0 / float(tex_size.y));"
            "  vec4 result = texture(tex, fragment_texture_coord)* weight[0];"
            "  if (horizontal) {"
            "    for (int i = 1; i < 5; i++) {"         
            "      vec2 tex_coord;"
            "      tex_coord = fragment_texture_coord + vec2(tex_offset.x * float(i), 0.0);"
            "      result += texture(tex, tex_coord)* weight[i];"
            "      tex_coord = fragment_texture_coord - vec2(tex_offset.x * float(i), 0.0);"
            "      result += texture(tex, tex_coord)* weight[i];"
            "    }"
            "  } else {"
            "    for (int i = 1; i < 5; i++) {"         
            "      vec2 tex_coord;"
            "      tex_coord = fragment_texture_coord + vec2(0.0, tex_offset.y * float(i));"
            "      result += texture(tex, tex_coord)* weight[i];"
            "      tex_coord = fragment_texture_coord - vec2(0.0, tex_offset.y * float(i));"
            "      result += texture(tex, tex_coord)* weight[i];"
            "    }"
            "  }"
            "  g_color = result;"
            "}";

        GLint program = shader_create_program(vertex_shader, fragment_shader);
        GLint mvp_mat_loc = glGetUniformLocation(program, "mvp_mat");
        GLint texture_loc = glGetUniformLocation(program, "tex");
        GLint horizontal_loc = glGetUniformLocation(program, "horizontal");
        GLint position_loc = glGetAttribLocation(program, "vertex_position");
        GLint texture_coord_loc = glGetAttribLocation(program, "vertex_texture_coord");

        shader_store.gaussian_blur_shader.program = (GLuint)program;
        shader_store.gaussian_blur_shader.mvp_mat_location = (GLuint)mvp_mat_loc;
        shader_store.gaussian_blur_shader.texture_location = (GLuint)texture_loc;
        shader_store.gaussian_blur_shader.horizontal_location = (GLuint)horizontal_loc;
        shader_store.gaussian_blur_shader.position_location = (GLuint)position_loc;
        shader_store.gaussian_blur_shader.texture_coord_location = (GLuint)texture_coord_loc;
    }
}
