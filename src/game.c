#include "game.h"

#include <assert.h>
#include <limits.h>

#include "collider.h"
#include "controls.h"
#include "entity.h"
#include "mesh.h"
#include "physics.h"
#include "queue.h"
#include "renderer.h"
#include "terrain.h"
#include "texture.h"
#include "ui.h"

static enum terrain_type get_terrain_type(int row, int col) {
    int idx = col * GAME_MAP_SIZE + row;
    if (idx >= 0 && idx < GAME_MAP_SIZE * GAME_MAP_SIZE) {
        return game_state.terrain[idx];
    }
    return TERRAIN_TYPE_NOTHING;
}

static void set_terrain_block(int col0, int row0, int col1, int row1, enum terrain_type type) {
    for (int row = row0; row < row1; row++) {
        for (int col = col0; col < col1; col++) {
            game_state.terrain[GAME_MAP_SIZE * col + row] = type;
        }
    }
}

static void create_terrain_boundaries(void) {
    int row, col;
    bool created_border[GAME_MAP_SIZE * GAME_MAP_SIZE];

    for (row = 0; row < GAME_MAP_SIZE; row++) {
        for (col = 0; col < GAME_MAP_SIZE; col++) {
            created_border[GAME_MAP_SIZE * col + row] = false;
        }
    }

    for (row = 0; row < GAME_MAP_SIZE; row++) {
        for (col = 0; col < GAME_MAP_SIZE; col++) {
            enum terrain_type t0, t1, t2, t3, t4;

            t0 = get_terrain_type(row, col);
            t1 = get_terrain_type(row, col + 1);
            t2 = get_terrain_type(row, col - 1);
            t3 = get_terrain_type(row + 1, col);
            t4 = get_terrain_type(row - 1, col);

            if (t0 == TERRAIN_TYPE_GRASS) {
                if (t1 == TERRAIN_TYPE_WATER && !created_border[GAME_MAP_SIZE * (col + 1) + row]) {
                    // RIGHT
                    int row_0, row_1;
                    created_border[GAME_MAP_SIZE * (col + 1) + row] = true;

                    row_0 = row;
                    while (get_terrain_type(row_0, col + 1) == TERRAIN_TYPE_WATER &&
                           get_terrain_type(row_0, col) == TERRAIN_TYPE_GRASS) {
                        created_border[GAME_MAP_SIZE * (col + 1) + row_0] = true;
                        row_0--;
                    }
                    row_0++;

                    row_1 = row;
                    while (get_terrain_type(row_1, col + 1) == TERRAIN_TYPE_WATER &&
                           get_terrain_type(row_1, col) == TERRAIN_TYPE_GRASS) {
                        created_border[GAME_MAP_SIZE * (col + 1) + row_1] = true;
                        row_1++;
                    }

                    create_terrain_border(col + 1, row_0, col + 1, row_1);
                }
                if (t2 == TERRAIN_TYPE_WATER && !created_border[GAME_MAP_SIZE * (col - 1) + row]) {
                    // LEFT
                    int row_0, row_1;
                    created_border[GAME_MAP_SIZE * (col - 1) + row] = true;

                    row_0 = row;
                    while (get_terrain_type(row_0, col - 1) == TERRAIN_TYPE_WATER &&
                           get_terrain_type(row_0, col) == TERRAIN_TYPE_GRASS) {
                        created_border[GAME_MAP_SIZE * (col - 1) + row_0] = true;
                        row_0--;
                    }
                    row_0++;

                    row_1 = row;
                    while (get_terrain_type(row_1, col - 1) == TERRAIN_TYPE_WATER &&
                           get_terrain_type(row_1, col) == TERRAIN_TYPE_GRASS) {
                        created_border[GAME_MAP_SIZE * (col - 1) + row_1] = true;
                        row_1++;
                    }

                    create_terrain_border(col, row_0, col, row_1);
                }
                if (t3 == TERRAIN_TYPE_WATER && !created_border[GAME_MAP_SIZE * col + (row + 1)]) {
                    // TOP
                    int col_0, col_1;
                    created_border[GAME_MAP_SIZE * col + (row + 1)] = true;

                    col_0 = col;
                    while (get_terrain_type(row, col_0) == TERRAIN_TYPE_WATER &&
                           get_terrain_type(row + 1, col_0) == TERRAIN_TYPE_GRASS) {
                        created_border[GAME_MAP_SIZE * col_0 + (row + 1)] = true;
                        col_0--;
                    }
                    col_0++;

                    col_1 = col;
                    while (get_terrain_type(row, col_1) == TERRAIN_TYPE_WATER &&
                           get_terrain_type(row + 1, col_1) == TERRAIN_TYPE_GRASS) {
                        created_border[GAME_MAP_SIZE * col_1 + (row + 1)] = true;
                        col_1++;
                    }

                    create_terrain_border(col_0, row + 1, col_1, row + 1);
                }
                if (t4 == TERRAIN_TYPE_WATER && !created_border[GAME_MAP_SIZE * col + (row - 1)]) {
                    // BOTTOM
                    int col_0, col_1;
                    created_border[GAME_MAP_SIZE * col + (row - 1)] = true;

                    col_0 = col;
                    while (get_terrain_type(row, col_0) == TERRAIN_TYPE_WATER &&
                           get_terrain_type(row - 1, col_0) == TERRAIN_TYPE_GRASS) {
                        created_border[GAME_MAP_SIZE * col_0 + (row - 1)] = true;
                        col_0--;
                    }
                    col_0++;

                    col_1 = col;
                    while (get_terrain_type(row, col_1) == TERRAIN_TYPE_WATER &&
                           get_terrain_type(row - 1, col_1) == TERRAIN_TYPE_GRASS) {
                        created_border[GAME_MAP_SIZE * col_1 + (row - 1)] = true;
                        col_1++;
                    }

                    create_terrain_border(col_0, row, col_1, row);
                }
            }
        }
    }
}

static void do_bfs(vec2 target_pos_idx, int *bfs_dist) {
    static bool bfs_visited[GAME_MAP_SIZE * GAME_MAP_SIZE];
    for (int i = 0; i < GAME_MAP_SIZE * GAME_MAP_SIZE; i++) {
        bfs_dist[i] = INT_MAX;
        bfs_visited[i] = false;
    }

    queue_vec2_t pos_queue;
    queue_init(&pos_queue);
    queue_push(&pos_queue, target_pos_idx);

    queue_int_t dist_queue;
    queue_init(&dist_queue);
    queue_push(&dist_queue, 0);

    while (!queue_empty(&pos_queue)) {
        vec2 cur_pos_idx = queue_head(&pos_queue);
        queue_pop(&pos_queue);

        int cur_dist = queue_head(&dist_queue);
        queue_pop(&dist_queue);

        int cur_x = (int)cur_pos_idx.x;
        int cur_y = (int)cur_pos_idx.y;

        bfs_dist[GAME_MAP_SIZE * cur_x + cur_y] = cur_dist;
        bfs_visited[GAME_MAP_SIZE * cur_x + cur_y] = true;

        int dx[4] = {0, 0, -1, 1};
        int dy[4] = {-1, 1, 0, 0};

        for (int i = 0; i < 4; i++) {
            int next_x = cur_x + dx[i];
            int next_y = cur_y + dy[i];

            if (next_x < 0 || next_y < 0 ||
                next_x >= GAME_MAP_SIZE || next_y >= GAME_MAP_SIZE) {
                continue;
            }

            if (bfs_visited[GAME_MAP_SIZE * next_x + next_y]) {
                continue;
            }

            if (game_state.map_tile_occupied[GAME_MAP_SIZE * next_x + next_y]) {
                continue;
            }

            if (get_terrain_type(next_y, next_x) == TERRAIN_TYPE_WATER) {
                continue;
            }

            bfs_visited[GAME_MAP_SIZE * next_x + next_y] = true;

            queue_push(&pos_queue, V2((float)next_x, (float)next_y));
            queue_push(&dist_queue, cur_dist + 1);
        }
    }

    queue_deinit(&pos_queue);
    queue_deinit(&dist_queue);
}

void game_do_pathfinding(vec2 start, vec2 target, array_vec2_t *path) {
    int start_pos_idx_x, start_pos_idx_y;
    int target_pos_idx_x, target_pos_idx_y;
    int cur_pos_idx_x, cur_pos_idx_y;

    array_clear(path);
    array_push(path, start);

    start_pos_idx_x = (int)(start.x / GAME_MAP_WIDTH * GAME_MAP_SIZE);
    start_pos_idx_y = (int)(start.y / GAME_MAP_WIDTH * GAME_MAP_SIZE);

    target_pos_idx_x = (int)(target.x / GAME_MAP_WIDTH * GAME_MAP_SIZE);
    target_pos_idx_y = (int)(target.y / GAME_MAP_WIDTH * GAME_MAP_SIZE);

    if (game_state.map_tile_occupied[GAME_MAP_SIZE * target_pos_idx_x + target_pos_idx_y] ||
        game_state.terrain[GAME_MAP_SIZE * target_pos_idx_x + target_pos_idx_y] == TERRAIN_TYPE_WATER) {
        return;
    }
    do_bfs(V2((float)target_pos_idx_x, (float)target_pos_idx_y), game_state.game_bfs_dist);

    cur_pos_idx_x = start_pos_idx_x;
    cur_pos_idx_y = start_pos_idx_y;

    while (cur_pos_idx_x != target_pos_idx_x ||
           cur_pos_idx_y != target_pos_idx_y) {
        int i;
        int cur_dist, next_dist;
        int next_pos_idx_x, next_pos_idx_y;
        bool found_next_pos = false;
        int dx[4] = {0, 0, -1, 1};
        int dy[4] = {-1, 1, 0, 0};

        cur_dist = game_state.game_bfs_dist[GAME_MAP_SIZE * cur_pos_idx_x + cur_pos_idx_y];

        for (i = 0; i < 4; i++) {
            next_pos_idx_x = cur_pos_idx_x + dx[i];
            next_pos_idx_y = cur_pos_idx_y + dy[i];

            if (next_pos_idx_x < 0 ||
                next_pos_idx_y < 0 ||
                next_pos_idx_x >= GAME_MAP_SIZE ||
                next_pos_idx_y >= GAME_MAP_SIZE) {
                continue;
            }

            next_dist = game_state.game_bfs_dist[GAME_MAP_SIZE * next_pos_idx_x + next_pos_idx_y];
            if (next_dist < cur_dist) {
                found_next_pos = true;
                break;
            }
        }
        if (!found_next_pos) {
            printf("could not do pathfinding!!!\n");
        }

        cur_pos_idx_x = next_pos_idx_x;
        cur_pos_idx_y = next_pos_idx_y;

        vec2 cur_pos;
        cur_pos.x = (cur_pos_idx_x + 0.5f) / GAME_MAP_SIZE * GAME_MAP_WIDTH;
        cur_pos.y = (cur_pos_idx_y + 0.5f) / GAME_MAP_SIZE * GAME_MAP_WIDTH;
        array_push(path, cur_pos);
    }
    array_push(path, target);

    int target_path_length = path->length;
    for (int i = 0; i < target_path_length; i++) {
        vec2 start = array_get(path, i);
        for (int j = target_path_length - 1; j > i; j--) {
            vec2 end;
            struct collider_id id;

            end = array_get(path, j);
            id = physics_segment_query(start, end, 0.05f);
            if (!get_collider_from_id(id)) {
                array_splice(path, i + 1, j - i - 1);
                target_path_length = path->length;
                break;
            }
        }
    }
}

static struct {
    array_entity_id_t entities;
} _game_state;

void game_init(void) {
    game_state.team_supply[0] = 0;
    game_state.team_supply[1] = 0;
    game_state.team_supply_max[0] = 0;
    game_state.team_supply_max[1] = 0;
    game_state.team_money[0] = 500;
    game_state.team_money[1] = 50;

    game_state.cam_pos = V2(0.5f * GAME_MAP_WIDTH, 0.5f * GAME_MAP_WIDTH);
    game_state.cam_size = 4.0f;

    for (int i = 0; i < GAME_MAP_SIZE * GAME_MAP_SIZE; i++) {
        game_state.map_tile_occupied[i] = false;
        game_state.map_tile_visible[i] = false;
    }

    {
        for (int row = 0; row < GAME_MAP_SIZE; row++) {
            for (int col = 0; col < GAME_MAP_SIZE; col++) {
                game_state.terrain[GAME_MAP_SIZE * col + row] = TERRAIN_TYPE_WATER;
            }
        }
        set_terrain_block(20, 20, 80, 80, TERRAIN_TYPE_GRASS);
        set_terrain_block(30, 45, 70, 55, TERRAIN_TYPE_WATER);
        renderer_terrain_update();
        create_terrain_boundaries();
    }

    {
        create_command_center_entity(25, 25, 0);
        create_worker_entity(25.0f, 28.0f, 0);
    }

    array_init(&_game_state.entities);
    array_init(&game_state.selected_entity_array);
    array_init(&game_state.team_0_entities);
    array_init(&game_state.team_1_entities);
}

void game_deinit(void) {
    struct entity_id entity_id;
    int i;
    array_foreach(&_game_state.entities, entity_id, i) {
        delete_entity(entity_id);
    }

    array_deinit(&_game_state.entities);
    array_deinit(&game_state.selected_entity_array);
    array_deinit(&game_state.team_0_entities);
    array_deinit(&game_state.team_1_entities);
}

vec2 game_get_pos(vec2 screen_pos) {
    return V2_ZERO;
}

static int entity_idx_compare(const void *p1, const void *p2) {
    const struct entity_id *id1 = (const struct entity_id *)p1;
    const struct entity_id *id2 = (const struct entity_id *)p2;
    return id1->idx - id2->idx;
}

void game_update(float dt) {
    array_sort(&game_state.selected_entity_array, entity_idx_compare);

    //
    // Handle the controls
    //
    {
        vec2 new_cam_pos = game_state.cam_pos;
        if (controls_state.key_down[GLFW_KEY_A]) {
            new_cam_pos.x += -2.5f * dt;
        }
        if (controls_state.key_down[GLFW_KEY_D]) {
            new_cam_pos.x += 2.5f * dt;
        }
        if (controls_state.key_down[GLFW_KEY_W]) {
            new_cam_pos.y += 2.5f * dt;
        }
        if (controls_state.key_down[GLFW_KEY_S]) {
            new_cam_pos.y += -2.5f * dt;
        }
        game_set_cam_pos(new_cam_pos);

        float new_cam_size = game_state.cam_size;
        if (controls_state.key_down[GLFW_KEY_Q]) {
            new_cam_size += 2.5f * dt;
        }
        if (controls_state.key_down[GLFW_KEY_E]) {
            new_cam_size -= 2.5f * dt;
        }
        game_set_cam_size(new_cam_size);
    }

    for (int i = 0; i < GAME_MAP_SIZE * GAME_MAP_SIZE; i++) {
        game_state.map_tile_visible[i] = false;
    }

    //
    // Update the entities
    //
    {
        int i;
        struct entity_id entity_id;

        array_clear(&_game_state.entities);
        get_all_active_entities_with_type(&_game_state.entities, 0, NULL);

        array_clear(&game_state.team_0_entities);
        array_clear(&game_state.team_1_entities);
        array_foreach(&_game_state.entities, entity_id, i) {
            struct entity *entity = get_entity_from_id(entity_id);
            if (!entity) {
                continue;
            }
            if (entity->team_num == 0) {
                array_push(&game_state.team_0_entities, entity_id);
            } else if (entity->team_num == 1) {
                array_push(&game_state.team_1_entities, entity_id);
            }

            if (entity->team_num == 0) {
                int entity_col = (int)(entity->position.x / GAME_TILE_WIDTH);
                int entity_row = (int)(entity->position.y / GAME_TILE_WIDTH);
                int sight_map_radius = (int)(entity->sight_radius / GAME_TILE_WIDTH);
                for (int row = entity_row - sight_map_radius; row <= entity_row + sight_map_radius; row++) {
                    for (int col = entity_col - sight_map_radius; col <= entity_col + sight_map_radius; col++) {
                        vec2 p;
                        p.x = (col + 0.5f) * GAME_TILE_WIDTH;
                        p.y = (row + 0.5f) * GAME_TILE_WIDTH;
                        if (vec2_distance(p, entity->position) > entity->sight_radius) {
                            continue;
                        }

                        if (row < 0 || col < 0 || row >= GAME_MAP_SIZE || col >= GAME_MAP_SIZE) {
                            continue;
                        }
                        game_state.map_tile_visible[GAME_MAP_SIZE * col + row] = true;
                    }
                }
            }
        }

        array_foreach(&_game_state.entities, entity_id, i) {
            struct entity *entity;
            struct collider *collider;
            struct mesh_instance *mesh_instance;

            entity = get_entity_from_id(entity_id);
            if (!entity) {
                continue;
            }

            collider = get_collider_from_id(entity->collider_id);
            if (collider) {
                entity->position = collider->position;
                entity->velocity = collider->velocity;
            }

            mesh_instance = get_mesh_instance_from_id(entity->mesh_instance_id);
            if (mesh_instance) {
                mesh_instance->position.x = entity->position.x;
                mesh_instance->position.y = entity->position.y;
            }

            if (entity->team_num == 1) {
                int col, row;
                col = (int)(entity->position.x / GAME_TILE_WIDTH);
                row = (int)(entity->position.y / GAME_TILE_WIDTH);
                if (row >= 0 && col >= 0 && row < GAME_MAP_SIZE && col < GAME_MAP_SIZE) {
                    entity->is_visible = game_state.map_tile_visible[GAME_MAP_SIZE * col + row];
                }
            }

            if (mesh_instance) {
                mesh_instance->is_visible = entity->is_visible;
            }

            struct attack_component *attack_compo = get_entity_component_of_type(entity, COMPONENT_TYPE_ATTACK, "attack");
            if (attack_compo) {
                int j;
                struct entity_id enemy_id, closest_enemy_id;
                float closest_enemy_dist = FLT_MAX;
                array_entity_id_t *enemy_entities;

                closest_enemy_id.idx = 0;

                if (entity->team_num == 0) {
                    enemy_entities = &game_state.team_1_entities;
                } else if (entity->team_num == 1) {
                    enemy_entities = &game_state.team_0_entities;
                } else {
                    assert(false && "Invalid team num for unit");
                }

                array_foreach(enemy_entities, enemy_id, j) {
                    struct entity *enemy;
                    float dist;

                    enemy = get_entity_from_id(enemy_id);
                    if (!enemy || !enemy->is_visible) {
                        continue;
                    }

                    dist = vec2_distance(enemy->position, entity->position);
                    if (dist < closest_enemy_dist && dist < attack_compo->attack_radius) {
                        closest_enemy_dist = dist;
                        closest_enemy_id = enemy_id;
                    }
                }

                attack_compo->closest_enemy_id = closest_enemy_id;
            }

            switch (entity->type) {
                case ENTITY_TYPE_COMMAND_CENTER: {
                    update_command_center_entity(entity, dt);
                    break;
                }

                case ENTITY_TYPE_WORKER: {
                    update_worker_entity(entity, dt);
                    break;
                }

                case ENTITY_TYPE_SOLDIER: {
                    update_soldier_entity(entity, dt);
                    break;
                }

                case ENTITY_TYPE_MEDIC: {
                    update_medic_entity(entity, dt);
                    break;
                }

                case ENTITY_TYPE_BARRACKS: {
                    update_barracks_entity(entity, dt);
                    break;
                }

                case ENTITY_TYPE_SOLDIER_PROJECTILE: {
                    update_soldier_projectile_entity(entity, dt);
                    break;
                }

                case ENTITY_TYPE_TANK: {
                    update_tank_entity(entity, dt);
                    break;
                }

                case ENTITY_TYPE_VEHICLE: {
                    update_vehicle_entity(entity, dt);
                    break;
                }

                case ENTITY_TYPE_TANK_PROJECTILE: {
                    update_tank_projectile_entity(entity, dt);
                    break;
                }

                case ENTITY_TYPE_EXPLOSION: {
                    update_explosion_entity(entity, dt);
                    break;
                }

                case ENTITY_TYPE_HELICOPTER: {
                    update_helicopter_entity(entity, dt);
                    break;
                }

                case ENTITY_TYPE_HELICOPTER_PROJECTILE: {
                    update_helicopter_projectile_entity(entity, dt);
                    break;
                }

                case ENTITY_TYPE_HELIPORT: {
                    update_heliport_entity(entity, dt);
                    break;
                }

                case ENTITY_TYPE_FACTORY: {
                    update_factory_entity(entity, dt);
                    break;
                }

                case ENTITY_TYPE_REFINERY: {
                    update_refinery_entity(entity, dt);
                    break;
                }

                case ENTITY_TYPE_BUILDING_CONSTRUCTION_LARGE:
                case ENTITY_TYPE_BUILDING_CONSTRUCTION_SMALL: {
                    update_building_construction_entity(entity, dt);
                    break;
                }

                case ENTITY_TYPE_PLACEHOLDER: {
                    update_placeholder_entity(entity, dt);
                    break;
                }

                case ENTITY_TYPE_TERRAIN_BORDER:
                case ENTITY_TYPE_TURRET:
                case ENTITY_TYPE_SUPPLY_DEPOT:
                case NUM_OF_ENTITY_TYPES:
                    break;
            }
        }
    }
}

void game_set_cam_pos(vec2 cam_pos) {
    struct rect_2D rect = ui_state.ui_pane_rects[UI_PANE_RECT_TYPE_MINIMAP];
    vec2 p0, p1;
    vec3 p0_3, p1_3;
    vec2 cam_sz;

    cam_sz = V2(game_state.cam_size, 9.0f / 16.0f * game_state.cam_size);
    p0 = rect.top_left;
    p1.x = p0.x + rect.size.x;
    p1.y = p0.y - rect.size.y;

    p0_3 = vec3_apply_mat4(V3(p0.x, p0.y, 1.0f), 1.0f, mat4_inverse(ui_state.game_to_minimap_mat));
    p1_3 = vec3_apply_mat4(V3(p1.x, p1.y, 1.0f), 1.0f, mat4_inverse(ui_state.game_to_minimap_mat));

    if (cam_pos.x - cam_sz.x < p0_3.x) {
        cam_pos.x = p0_3.x + cam_sz.x;
    }
    if (cam_pos.x + cam_sz.x > p1_3.x) {
        cam_pos.x = p1_3.x - cam_sz.x;
    }
    if (cam_pos.y - cam_sz.y < p1_3.y) {
        cam_pos.y = p1_3.y + cam_sz.y;
    }
    if (cam_pos.y + cam_sz.y > p0_3.y) {
        cam_pos.y = p0_3.y - cam_sz.y;
    }

    game_state.cam_pos = cam_pos;
}

void game_set_cam_size(float cam_size) {
    struct rect_2D rect = ui_state.ui_pane_rects[UI_PANE_RECT_TYPE_MINIMAP];
    vec2 p0, p1;
    vec3 p0_3, p1_3;

    p0 = rect.top_left;
    p1.x = p0.x + rect.size.x;
    p1.y = p0.y - rect.size.y;

    p0_3 = vec3_apply_mat4(V3(p0.x, p0.y, 1.0f), 1.0f, mat4_inverse(ui_state.game_to_minimap_mat));
    p1_3 = vec3_apply_mat4(V3(p1.x, p1.y, 1.0f), 1.0f, mat4_inverse(ui_state.game_to_minimap_mat));

    float max_size = p1_3.x - p0_3.x;
    if (cam_size > 0.5f * max_size) {
        cam_size = 0.5f * max_size;
    }

    float min_size = 1.0f;
    if (cam_size < min_size) {
        cam_size = min_size;
    }

    game_state.cam_size = cam_size;
}
