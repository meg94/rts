#ifndef _RENDERER_FONT_H
#define _RENDERER_FONT_H

#include "IGL.h"
#include "maths.h"

enum font_justify_horizontal {
    FONT_JUSTIFY_HORIZONTAL_LEFT,
    FONT_JUSTIFY_HORIZONTAL_RIGHT,
    FONT_JUSTIFY_HORIZONTAL_CENTER,
};

enum font_justify_vertical {
    FONT_JUSTIFY_VERTICAL_BOTTOM,
    FONT_JUSTIFY_VERTICAL_CENTER,
    FONT_JUSTIFY_VERTICAL_TOP,
};

struct font_mesh {
    GLuint position_vbo;
    GLuint texture_coord_vbo;
    int num_characters;
    float width, height;
};

void font_mesh_init(struct font_mesh *mesh);
void font_mesh_change_string(struct font_mesh *font_mesh, const char *string);
void font_draw_string(const char *string, mat4 model_mat, vec4 color,
                      enum font_justify_horizontal horizontal_justify,
                      enum font_justify_vertical vertical_justify);

#endif
