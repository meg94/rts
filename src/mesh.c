#include "mesh.h"

#include <assert.h>

static void init_mesh_instance(struct mesh_instance *mesh_instance) {
    mesh_instance->position = V3(0.0f, 0.0f, 0.0f);
    mesh_instance->scale = V2(1.0f, 1.0f);
    mesh_instance->opacity = 1.0f;
    mesh_instance->rotation = 0.0f;
    mesh_instance->always_on_top = false;
    mesh_instance->always_on_bottom = false;
	mesh_instance->is_visible = true;
    array_init(&mesh_instance->children);
}

static void deinit_mesh_instance(struct mesh_instance *mesh_instance) {
    array_deinit(&mesh_instance->children);
}

static struct {
    array_mesh_instance_t mesh_instance_array;
    array_uint32_t gen_array;
    array_uint32_t free_array;
    array_bool_t active_array;
} _mesh_instance_manager;

void mesh_instance_manager_init(void) {
    array_init(&_mesh_instance_manager.mesh_instance_array);
    array_init(&_mesh_instance_manager.gen_array);
    array_init(&_mesh_instance_manager.free_array);
    array_init(&_mesh_instance_manager.active_array);
}

struct mesh_instance_id allocate_mesh_instance(void) {
    struct mesh_instance_id id;

    if (_mesh_instance_manager.free_array.length == 0) {
        struct mesh_instance mesh_instance;
        memset(&mesh_instance, 0, sizeof(mesh_instance));
        array_push(&_mesh_instance_manager.mesh_instance_array, mesh_instance);
        array_push(&_mesh_instance_manager.gen_array, 0);
        array_push(&_mesh_instance_manager.active_array, true);
        id.gen = 0;
        id.idx = _mesh_instance_manager.mesh_instance_array.length - 1;
    } else {
        uint32_t idx = array_pop(&_mesh_instance_manager.free_array);
        id.gen = _mesh_instance_manager.gen_array.data[idx];
        id.idx = idx;
        _mesh_instance_manager.active_array.data[id.idx] = true;
    }

    // printf("allocate mesh_instance: idx: %d, gen: %d\n", id.idx, id.gen);

    struct mesh_instance *mesh_instance = get_mesh_instance_from_id(id);
    init_mesh_instance(mesh_instance);

    return id;
}

void delete_mesh_instance(struct mesh_instance_id id) {
    uint32_t cur_gen = _mesh_instance_manager.gen_array.data[id.idx];
    if (cur_gen != id.gen) {
        return;
    }

    // printf("delete mesh_instance: idx: %d, gen: %d\n", id.idx, id.gen);

    struct mesh_instance *mesh_instance = get_mesh_instance_from_id(id);
    deinit_mesh_instance(mesh_instance);

    _mesh_instance_manager.gen_array.data[id.idx]++;
    _mesh_instance_manager.active_array.data[id.idx] = false;
    array_push(&_mesh_instance_manager.free_array, id.idx);
}

struct mesh_instance *get_mesh_instance_from_id(struct mesh_instance_id id) {
    uint32_t cur_gen = _mesh_instance_manager.gen_array.data[id.idx];
    if (cur_gen != id.gen) {
        return NULL;
    }
    return &_mesh_instance_manager.mesh_instance_array.data[id.idx];
}

void get_all_active_mesh_instances(array_mesh_instance_ptr_t *mesh_instances) {
    int i;

    for (i = 0; i < _mesh_instance_manager.mesh_instance_array.length; i++) {
        struct mesh_instance *mesh_instance;

        if (!_mesh_instance_manager.active_array.data[i]) {
            continue;
        }

        mesh_instance = &_mesh_instance_manager.mesh_instance_array.data[i];
        array_push(mesh_instances, mesh_instance);
    }
}

static struct mesh mesh_2D_create(vec3 *vertex_coords, vec2 *texture_coords, int num_vertices) {
    struct mesh mesh;
    mesh.num_vertices = num_vertices;

    glGenBuffers(1, &mesh.position_vbo);
    glGenBuffers(1, &mesh.texture_coord_vbo);

    glBindBuffer(GL_ARRAY_BUFFER, mesh.position_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * num_vertices, vertex_coords, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, mesh.texture_coord_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * num_vertices, texture_coords, GL_STATIC_DRAW);

    return mesh;
}

void mesh_store_init(void) {
    {
        vec3 vertex_coords[] = {
            V3(-0.5f, 0.5f, 0.0f), V3(-0.5f, -0.5f, 0.0f), V3(0.5f, -0.5f, 0.0f),
            V3(-0.5f, 0.5f, 0.0f), V3(0.5f, -0.5f, 0.0f), V3(0.5f, 0.5f, 0.0f)};

        vec2 texture_coords[] = {
            V2(0.0f, 1.0f),
            V2(0.0f, 0.0f),
            V2(1.0f, 0.0f),
            V2(0.0f, 1.0f),
            V2(1.0f, 0.0f),
            V2(1.0f, 1.0f),
        };

        mesh_store.square_mesh = mesh_2D_create(vertex_coords, texture_coords, 6);
    }

    {
#define NUM_TRIANGLES 50

        vec3 vertex_coords[3 * NUM_TRIANGLES];
        vec2 texture_coords[3 * NUM_TRIANGLES];

        for (int i = 0; i < NUM_TRIANGLES; i++) {
            float r0 = (i / (float)NUM_TRIANGLES) * 2.0f * (float) M_PI;
            float x0 = cosf(r0);
            float y0 = sinf(r0);

            float r1 = ((i + 1) / (float)NUM_TRIANGLES) * 2.0f * (float) M_PI;
            float x1 = cosf(r1);
            float y1 = sinf(r1);

            vertex_coords[3 * i + 0] = V3(0.0f, 0.0f, 0.0f);
            vertex_coords[3 * i + 1] = V3(x0, y0, 0.0f);
            vertex_coords[3 * i + 2] = V3(x1, y1, 0.0f);

            float tx0 = 0.5f * x0 + 0.5f;
            float ty0 = 0.5f * y0 + 0.5f;

            float tx1 = 0.5f * x1 + 0.5f;
            float ty1 = 0.5f * y1 + 0.5f;

            texture_coords[3 * i + 0] = V2(0.5f, 0.5f);
            texture_coords[3 * i + 1] = V2(tx0, ty0);
            texture_coords[3 * i + 2] = V2(tx1, ty1);
        }

        mesh_store.circle_mesh = mesh_2D_create(vertex_coords, texture_coords, 3 * NUM_TRIANGLES);
    }
#undef NUM_TRIANGLES
}

struct mesh_instance_child create_mesh_instance_child(const char *name,
                                                      struct mesh *mesh,
                                                      struct texture texture) {
    struct mesh_instance_child child;
    child.name = name;
    child.is_visible = true;
    child.mesh = mesh;
    child.texture = texture;
    child.position = V3(0.0f, 0.0f, 0.0f);
    child.scale = V2(1.0f, 1.0f);
    child.opacity = 1.0f;
    child.rotation = 0.0f;
    child.already_isometric = false;
    child.flip_x = false;
    return child;
}

struct mesh_instance_child *get_mesh_instance_child(const char *name,
                                                    struct mesh_instance *mesh_instance) {
    int i;
    struct mesh_instance_child *child;
    array_foreach_ptr(&mesh_instance->children, child, i) {
        if (strcmp(child->name, name) == 0) {
            return child;
        }
    }
    return NULL;
}
