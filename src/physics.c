#include "physics.h"

#include <assert.h>

#include "entity.h"
#include "collider.h"

static struct {
    array_collider_id_t collider_array;
} _physics_state;

static cpBool physics_begin_func(cpArbiter *arb, struct cpSpace *space, cpDataPointer data) {
    struct collider_id id_a, id_b;
    struct collider *collider_a, *collider_b;
    cpBody *cp_body_a, *cp_body_b;

    cpArbiterGetBodies(arb, &cp_body_a, &cp_body_b);
    id_a = *((struct collider_id *)cpBodyGetUserData(cp_body_a));
    id_b = *((struct collider_id *)cpBodyGetUserData(cp_body_b));

    collider_a = get_collider_from_id(id_a);
    collider_b = get_collider_from_id(id_b);

    assert(collider_a && collider_b);

    cpVect cp_normal = cpArbiterGetNormal(arb);
    cpVect cp_point = cpArbiterGetPointA(arb, 0);
    float cp_depth = (float) cpArbiterGetDepth(arb, 0);
    bool cp_first_contact = cpArbiterIsFirstContact(arb);

    struct collision_info info;
    info.other_collider = collider_a;
    info.normal = V2((float) cp_normal.x, (float) cp_normal.y);
    info.point = V2((float) cp_point.x, (float) cp_point.y);
    info.depth = (float) cp_depth;
    info.first_contact = cp_first_contact;
    array_push(&collider_b->collision_info_array, info);

    info.other_collider = collider_b;
    array_push(&collider_a->collision_info_array, info);

    return collider_a->does_collide && collider_b->does_collide;
}

static void physics_seperate_func(cpArbiter *arb, struct cpSpace *space, cpDataPointer data) {
    struct collider_id id_a, id_b;
    struct collider *collider_a, *collider_b;
    cpBody *cp_body_a, *cp_body_b;

    cpArbiterGetBodies(arb, &cp_body_a, &cp_body_b);
    id_a = *((struct collider_id *)cpBodyGetUserData(cp_body_a));
    id_b = *((struct collider_id *)cpBodyGetUserData(cp_body_b));

    collider_a = get_collider_from_id(id_a);
    collider_b = get_collider_from_id(id_b);

    assert(collider_a && collider_b);

    int i;
    struct collision_info info;

    assert(collider_a->collision_info_array.length > 0);
    assert(collider_b->collision_info_array.length > 0);

    bool removed_one = false;
    array_foreach(&collider_a->collision_info_array, info, i) {
        if (info.other_collider == collider_b) {
            removed_one = true;
            array_splice(&collider_a->collision_info_array, i, 1);
            break;
        }
    }
    assert(removed_one);

    removed_one = false;
    array_foreach(&collider_b->collision_info_array, info, i) {
        if (info.other_collider == collider_a) {
            removed_one = true;
            array_splice(&collider_b->collision_info_array, i, 1);
            break;
        }
    }
    assert(removed_one);
}

void physics_init(void) {
    physics_state.cp_space = cpSpaceNew();
    cpSpaceSetGravity(physics_state.cp_space, cpv(0.0f, 0.0f));

    //cpCollisionHandler *collision_handler = cpSpaceAddDefaultCollisionHandler(physics_state.cp_space);
    //collision_handler->beginFunc = physics_begin_func;
    //collision_handler->separateFunc = physics_seperate_func;

    array_init(&_physics_state.collider_array);
}

void physics_update(float dt) {
    struct collider_id collider_id;
    int i;

    for (i = 0; i < 8; i++) {
        cpSpaceStep(physics_state.cp_space, (1.0f / 8.0f) * dt);
    }

    get_all_active_colliders(&_physics_state.collider_array);
    array_foreach(&_physics_state.collider_array, collider_id, i) {
        struct collider *collider;
        cpVect cp_pos, cp_vel;

        collider = get_collider_from_id(collider_id);
        if (!collider) {
            continue;
        }

        cp_pos = cpBodyGetPosition(collider->cp_body);
        cp_vel = cpBodyGetVelocity(collider->cp_body);

        collider->position.x = (float) cp_pos.x;
        collider->position.y = (float) cp_pos.y;

        collider->velocity.x = (float) cp_vel.x;
        collider->velocity.y = (float) cp_vel.y;
    }
}

struct collider_id physics_segment_query(vec2 start, vec2 end, float radius) {
    cpSegmentQueryInfo query_info;
    cpShapeFilter filter;
    cpShape *shape;
    struct collider_id id;

    filter = cpShapeFilterNew(CP_NO_GROUP,
                              COLLIDER_CATEGORY_BUILDING,
                              COLLIDER_CATEGORY_BUILDING);
    shape = cpSpaceSegmentQueryFirst(physics_state.cp_space,
                                     cpv(start.x, start.y),
                                     cpv(end.x, end.y),
                                     radius,
                                     filter,
                                     &query_info);

    if (!shape) {
        id.idx = 0;
        return id;
    }
    id = *((struct collider_id *)cpBodyGetUserData(cpShapeGetBody(shape)));
    return id;
}
