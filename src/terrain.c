#include "terrain.h"
#include "game.h"
#include "shader.h"
#include "array.h"

#define TILE_WIDTH_LR (0.139341f)
#define TILE_WIDTH_TB (0.143471f)

enum tile_type {
    TILE_TYPE_GRASS,
    TILE_TYPE_LEFT_CLIFF_1,
    TILE_TYPE_LEFT_CLIFF_2,
    TILE_TYPE_RIGHT_CLIFF,
    TILE_TYPE_TOP_CLIFF,
    TILE_TYPE_BOTTOM_CLIFF_1,
    TILE_TYPE_BOTTOM_CLIFF_2,
    TILE_TYPE_TOP_LEFT_CLIFF_CORNER_1,
    TILE_TYPE_TOP_LEFT_CLIFF_CORNER_2,
    TILE_TYPE_BOTTOM_RIGHT_CLIFF_CORNER_1,
    TILE_TYPE_BOTTOM_RIGHT_CLIFF_CORNER_2,
    TILE_NUM_TYPES,
};

struct tile {
    enum tile_type type;
    vec2 p;
    vec2 top_left;
    float w;
};

typedef array_t(struct tile) array_tile_t;

static struct terrain_state {
    array_tile_t tile_array;
    array_vec2_t grass_tile_pos_array;
    vec2 texture_pos[TILE_NUM_TYPES];

    array_vec3_t vertex_coords_array;
    array_vec2_t texture_coords_array;
} terrain_state;

static int compare_tiles(const void *p, const void *q) {
    struct tile a = *(const struct tile *)p;
    struct tile b = *(const struct tile *)q;

    if (a.p.x + a.p.y < b.p.x + b.p.y) {
        return 1;
    } else if (a.p.x + a.p.y > b.p.x + b.p.y) {
        return -1;
    } else {
        return 0;
    }
}

void renderer_terrain_init(void) {
    array_init(&terrain_state.tile_array);
    array_init(&terrain_state.grass_tile_pos_array);
    array_init(&terrain_state.vertex_coords_array);
    array_init(&terrain_state.texture_coords_array);

    terrain_state.texture_pos[TILE_TYPE_GRASS] = V2(4, 1);
    terrain_state.texture_pos[TILE_TYPE_TOP_CLIFF] = V2(3, 8);
    terrain_state.texture_pos[TILE_TYPE_LEFT_CLIFF_1] = V2(3, 16);
    terrain_state.texture_pos[TILE_TYPE_LEFT_CLIFF_2] = V2(3, 18);
    terrain_state.texture_pos[TILE_TYPE_RIGHT_CLIFF] = V2(8, 8);
    terrain_state.texture_pos[TILE_TYPE_BOTTOM_CLIFF_1] = V2(8, 16);
    terrain_state.texture_pos[TILE_TYPE_BOTTOM_CLIFF_2] = V2(8, 18);
    terrain_state.texture_pos[TILE_TYPE_TOP_LEFT_CLIFF_CORNER_1] = V2(1, 10);
    terrain_state.texture_pos[TILE_TYPE_TOP_LEFT_CLIFF_CORNER_2] = V2(1, 13);
    terrain_state.texture_pos[TILE_TYPE_BOTTOM_RIGHT_CLIFF_CORNER_1] = V2(10, 9);
    terrain_state.texture_pos[TILE_TYPE_BOTTOM_RIGHT_CLIFF_CORNER_2] = V2(10, 13);
}

static enum terrain_type get_terrain_type(int row, int col) {
    int idx = GAME_MAP_SIZE * col + row;
    if (idx >= 0 && idx < GAME_MAP_SIZE * GAME_MAP_SIZE) {
        return game_state.terrain[idx];
    }
    return TERRAIN_TYPE_NOTHING;
}

void renderer_terrain_update(void) {
    array_clear(&terrain_state.tile_array);
    array_clear(&terrain_state.grass_tile_pos_array);

    for (int row = 0; row < GAME_MAP_SIZE; row++) {
        for (int col = 0; col < GAME_MAP_SIZE; col++) {
            enum terrain_type t0 = get_terrain_type(row, col);
            enum terrain_type t1 = get_terrain_type(row, col + 1);
            enum terrain_type t2 = get_terrain_type(row, col - 1);
            enum terrain_type t3 = get_terrain_type(row + 1, col);
            enum terrain_type t4 = get_terrain_type(row - 1, col);

            vec2 p;
            {
                vec3 p_3 = V3(col * GAME_TILE_WIDTH, row * GAME_TILE_WIDTH, 1.0f);
                p_3 = vec3_apply_mat4(p_3, 1.0f, renderer_state.isometric_mat);
                p = V2(p_3.x, p_3.y);
            }

            if (t0 == TERRAIN_TYPE_GRASS) {
                {
                    vec2 grass_pos = V2(col * GAME_TILE_WIDTH, (row + 1) * GAME_TILE_WIDTH);
                    array_push(&terrain_state.grass_tile_pos_array, grass_pos);
                }

                if (t1 == TERRAIN_TYPE_WATER) {
                    // RIGHT
                    struct tile tile;
                    tile.top_left = V2(p.x, p.y + 2.3f * TILE_WIDTH_TB);
                    tile.w = TILE_WIDTH_TB;
                    tile.p = V2(col * GAME_TILE_WIDTH, row * GAME_TILE_WIDTH);
                    tile.type = TILE_TYPE_RIGHT_CLIFF;
                    array_push(&terrain_state.tile_array, tile);
                }
                if (t2 == TERRAIN_TYPE_WATER) {
                    // LEFT
                    struct tile tile;
                    tile.top_left = V2(p.x - 1.0f * TILE_WIDTH_TB, p.y + 0.8f * TILE_WIDTH_TB);
                    tile.w = TILE_WIDTH_TB;
                    tile.p = V2(col * GAME_TILE_WIDTH, row * GAME_TILE_WIDTH);
                    tile.type = TILE_TYPE_LEFT_CLIFF_1;
                    array_push(&terrain_state.tile_array, tile);

                    tile.top_left = V2(tile.top_left.x, tile.top_left.y - 2.0f * TILE_WIDTH_TB);
                    tile.type = TILE_TYPE_LEFT_CLIFF_2;
                    array_push(&terrain_state.tile_array, tile);
                }
                if (t3 == TERRAIN_TYPE_WATER) {
                    // TOP
                    struct tile tile;
                    tile.top_left = V2(p.x - 1.0f * TILE_WIDTH_LR, p.y + 2.3f * TILE_WIDTH_LR);
                    tile.w = TILE_WIDTH_LR;
                    tile.p = V2(col * GAME_TILE_WIDTH, row * GAME_TILE_WIDTH);
                    tile.type = TILE_TYPE_TOP_CLIFF;
                    array_push(&terrain_state.tile_array, tile);
                }
                if (t4 == TERRAIN_TYPE_WATER) {
                    // BOTTOM
                    struct tile tile;
                    tile.top_left = V2(p.x, p.y + 0.8f * TILE_WIDTH_TB);
                    tile.w = TILE_WIDTH_TB;
                    tile.p = V2(col * GAME_TILE_WIDTH, row * GAME_TILE_WIDTH);
                    tile.type = TILE_TYPE_BOTTOM_CLIFF_1;
                    array_push(&terrain_state.tile_array, tile);

                    tile.top_left = V2(tile.top_left.x, tile.top_left.y - 2.0f * TILE_WIDTH_TB);
                    tile.type = TILE_TYPE_BOTTOM_CLIFF_2;
                    array_push(&terrain_state.tile_array, tile);
                }
                if (t2 == TERRAIN_TYPE_WATER && t3 == TERRAIN_TYPE_WATER) {
                    // TOP LEFT CORNER
                    struct tile tile;
                    tile.top_left = V2(p.x - 2.0f * TILE_WIDTH_LR, p.y + 0.9f * TILE_WIDTH_LR);
                    tile.w = TILE_WIDTH_LR;
                    tile.p = V2(col * GAME_TILE_WIDTH, row * GAME_TILE_WIDTH);
                    tile.type = TILE_TYPE_TOP_LEFT_CLIFF_CORNER_1;
                    array_push(&terrain_state.tile_array, tile);

                    tile.top_left = V2(p.x - 2.0f * TILE_WIDTH_LR, p.y - 0.6f * TILE_WIDTH_LR);
                    tile.type = TILE_TYPE_TOP_LEFT_CLIFF_CORNER_2;
                    array_push(&terrain_state.tile_array, tile);
                }
                if (t1 == TERRAIN_TYPE_WATER && t4 == TERRAIN_TYPE_WATER) {
                    // TOP LEFT CORNER
                    struct tile tile;
                    tile.top_left = V2(p.x + 1.0f * TILE_WIDTH_LR, p.y + 1.4f * TILE_WIDTH_LR);
                    tile.w = TILE_WIDTH_LR;
                    tile.p = V2(col * GAME_TILE_WIDTH, row * GAME_TILE_WIDTH);
                    tile.type = TILE_TYPE_BOTTOM_RIGHT_CLIFF_CORNER_1;
                    array_push(&terrain_state.tile_array, tile);

                    tile.top_left = V2(p.x + 1.0f * TILE_WIDTH_LR, p.y - 0.6f * TILE_WIDTH_LR);
                    tile.type = TILE_TYPE_BOTTOM_RIGHT_CLIFF_CORNER_2;
                    array_push(&terrain_state.tile_array, tile);
                }
            }
        }
    }

    array_sort(&terrain_state.tile_array, compare_tiles);
}

void renderer_terrain_draw(void) {
    array_clear(&terrain_state.vertex_coords_array);
    array_clear(&terrain_state.texture_coords_array);

    int i;
    struct tile tile;
    array_foreach(&terrain_state.tile_array, tile, i) {
        vec2 texture_pos = terrain_state.texture_pos[tile.type];

        float x = tile.top_left.x;
        float y = tile.top_left.y;
        float z = 0.1f * i / terrain_state.tile_array.length;
        float w = tile.w;

        float tx = texture_pos.x / 24.0f;
        float ty = texture_pos.y / 24.0f;
        float tw = 1.0f / 24.0f;
        float th = 2.0f / 24.0f;

        vec3 vertex_coords[6];
        vec2 texture_coords[6];

        // Triangle 1
        vertex_coords[0].x = x;
        vertex_coords[0].y = y;
        vertex_coords[0].z = z;
        texture_coords[0].x = tx;
        texture_coords[0].y = ty;

        vertex_coords[1].x = x;
        vertex_coords[1].y = y - 2.0f * w;
        vertex_coords[1].z = z;
        texture_coords[1].x = tx;
        texture_coords[1].y = ty + th;

        vertex_coords[2].x = x + 1.0f * w;
        vertex_coords[2].y = y;
        vertex_coords[2].z = z;
        texture_coords[2].x = tx + tw;
        texture_coords[2].y = ty;

        // Triangle 2
        vertex_coords[3].x = x;
        vertex_coords[3].y = y - 2.0f * w;
        vertex_coords[3].z = z;
        texture_coords[3].x = tx;
        texture_coords[3].y = ty + th;

        vertex_coords[4].x = x + 1.0f * w;
        vertex_coords[4].y = y - 2.0f * w;
        vertex_coords[4].z = z;
        texture_coords[4].x = tx + tw;
        texture_coords[4].y = ty + th;

        vertex_coords[5].x = x + 1.0f * w;
        vertex_coords[5].y = y;
        vertex_coords[5].z = z;
        texture_coords[5].x = tx + tw;
        texture_coords[5].y = ty;

        for (int j = 0; j < 6; j++) {
            array_push(&terrain_state.vertex_coords_array, vertex_coords[j]);
            array_push(&terrain_state.texture_coords_array, texture_coords[j]);
        }
    }

    vec2 grass_pos;
    array_foreach(&terrain_state.grass_tile_pos_array, grass_pos, i) {
        vec2 texture_pos = terrain_state.texture_pos[TILE_TYPE_GRASS];

        vec3 p0 = V3(grass_pos.x, grass_pos.y, 1.0f);
        vec3 p1 = V3(grass_pos.x, grass_pos.y - GAME_TILE_WIDTH, 1.0f);
        vec3 p2 = V3(grass_pos.x + GAME_TILE_WIDTH, grass_pos.y, 1.0f);
        vec3 p3 = V3(grass_pos.x + GAME_TILE_WIDTH, grass_pos.y - GAME_TILE_WIDTH, 1.0f);

        p0 = vec3_apply_mat4(p0, 1.0f, renderer_state.isometric_mat);
        p1 = vec3_apply_mat4(p1, 1.0f, renderer_state.isometric_mat);
        p2 = vec3_apply_mat4(p2, 1.0f, renderer_state.isometric_mat);
        p3 = vec3_apply_mat4(p3, 1.0f, renderer_state.isometric_mat);

        float z = 0.1f;

        float tx = texture_pos.x / 24.0f;
        float ty = texture_pos.y / 24.0f;
        float tw = 1.0f / 24.0f;
        float th = 1.0f / 24.0f;

        vec3 vertex_coords[6];
        vec2 texture_coords[6];

        // Triangle 1
        vertex_coords[0].x = p0.x;
        vertex_coords[0].y = p0.y;
        vertex_coords[0].z = z;
        texture_coords[0].x = tx;
        texture_coords[0].y = ty;

        vertex_coords[1].x = p1.x;
        vertex_coords[1].y = p1.y;
        vertex_coords[1].z = z;
        texture_coords[1].x = tx;
        texture_coords[1].y = ty + th;

        vertex_coords[2].x = p2.x;
        vertex_coords[2].y = p2.y;
        vertex_coords[2].z = z;
        texture_coords[2].x = tx + tw;
        texture_coords[2].y = ty;

        // Triangle 2
        vertex_coords[3].x = p1.x;
        vertex_coords[3].y = p1.y;
        vertex_coords[3].z = z;
        texture_coords[3].x = tx;
        texture_coords[3].y = ty + th;

        vertex_coords[4].x = p3.x;
        vertex_coords[4].y = p3.y;
        vertex_coords[4].z = z;
        texture_coords[4].x = tx + tw;
        texture_coords[4].y = ty + th;

        vertex_coords[5].x = p2.x;
        vertex_coords[5].y = p2.y;
        vertex_coords[5].z = z;
        texture_coords[5].x = tx + tw;
        texture_coords[5].y = ty;

        for (int j = 0; j < 6; j++) {
            array_push(&terrain_state.vertex_coords_array, vertex_coords[j]);
            array_push(&terrain_state.texture_coords_array, texture_coords[j]);
        }
    }

    static bool generated_buffers = false;
    static GLuint position_vbo;
    static GLuint texture_coord_vbo;

    if (!generated_buffers) {
        glGenBuffers(1, &position_vbo);
        glGenBuffers(1, &texture_coord_vbo);
        generated_buffers = true;
    }

    int num_vertices = terrain_state.vertex_coords_array.length;

    glBindBuffer(GL_ARRAY_BUFFER, position_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * num_vertices, terrain_state.vertex_coords_array.data, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, texture_coord_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * num_vertices, terrain_state.texture_coords_array.data, GL_STATIC_DRAW);

    struct mesh_texture_shader *shader = &shader_store.mesh_texture_shader;
    glUseProgram(shader->program);

    mat4 model_mat = mat4_translation(V3(0.0f, 0.0f, -0.20f));
    mat4 mvp_mat = mat4_multiply_n(2, renderer_state.proj_mat, model_mat);
    glUniformMatrix4fv(shader->mvp_mat_location, 1, GL_TRUE, mvp_mat.m);

    glUniform1i(shader->texture_location, 0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture_store.map.image);
    glUniform1f(shader->opacity_location, 1.0f);

    glBindBuffer(GL_ARRAY_BUFFER, position_vbo);
    glVertexAttribPointer(shader->position_location, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(shader->position_location);

    glBindBuffer(GL_ARRAY_BUFFER, texture_coord_vbo);
    glVertexAttribPointer(shader->texture_coord_location, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(shader->texture_coord_location);

    glDrawArrays(GL_TRIANGLES, 0, num_vertices);
}
